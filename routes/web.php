<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (Auth::guest()) {
        return redirect()->route('login');
    } else {
        if (Auth::user()->role == "admin1" || Auth::user()->role == "admin2") {
            return redirect()->route('administrator.dashboard');
        } else {
            return redirect()->route('agency.dashboard');
        }
    }
});

Auth::routes();

Route::group(['prefix' => '/administrator', 'as' => 'administrator.', 'middleware' => []], function () {
    // dashboard
    Route::get('/', 'AdministratorController@dashboard')->name('dashboard');

    // profile
    Route::match(['get', 'post'], '/profile', 'AdministratorController@profile')->name('profile');

    // change password
    Route::post('/change-password', 'AdministratorController@changePassword')->name('changePassword');

    // requirement type
    Route::resource('/requirement-types', 'RequirementTypeController');
    // add biodata to the requirement type
    Route::post('/requirement-types/{requirement_type}/add-biodata', 'RequirementTypeController@addBiodata')->name('requirement-types.addBiodata');
    Route::delete('/requirement-types/{requirement_type}/remove-biodata', 'RequirementTypeController@removeBiodata')->name('requirement-types.removeBiodata');
    // update order biodata
    Route::put('/requirement-types/{requirement_type}/update-order-biodata', 'RequirementTypeController@updateOrderBiodata')->name('requirement-types.updateOrderBiodata');

    // biodatas
    Route::resource('/biodatas', 'BiodataController');
    // add requirement type to the biodata
    Route::post('/biodatas/{biodata}/add-requirement-type', 'BiodataController@addRequirementType')->name('biodatas.addRequirementType');
    Route::delete('/biodatas/{biodata}/remove-requirement-type', 'BiodataController@removeRequirementType')->name('biodatas.removeRequirementType');

    // requirement form
    Route::resource('/requirement-types/{requirement_type}/requirement-forms', 'RequirementFormController');

    // agencies
    Route::resource('/agencies', 'AgencyController');
    // agency reset password
    Route::post('/agencies/{agency}/reset-password', 'AgencyController@resetPassword')->name('agencies.resetPassword');

    // requirement
    Route::resource('/requirement-types/{requirement_type}/requirements', 'RequirementController');
    Route::get('/requirement-types/{requirement_type}/requirements/agencies/{agency}', 'RequirementController@filterByAgency')->name('requirements.filterByAgency');
	Route::get('/requirement-types/{requirement_type}/requirements/by/all', 'RequirementController@byAll')->name('requirements.byAll');

    // requirement verify
    Route::post('/requirements/{requirement}/verify', 'RequirementController@verify')->name('requirements.verify')->middleware('admin1');
    // requirement finish
    Route::post('/requirements/{requirement}/finish', 'RequirementController@finish')->name('requirements.finish')->middleware('admin2');
    // requirement print
    Route::get('/requirements/{requirement}/print', 'RequirementController@print')->name('requirements.print');

});

Route::group(['prefix' => '/agency', 'as' => 'agency.', 'middleware' => ['auth', 'agency']], function () {
    // dashboard
    Route::get('/', 'AgencyController@dashboard')->name('dashboard');

    // guide
    Route::get('/guide', 'AgencyController@guide')->name('guide');

    // profile
    Route::match(['get', 'post'], '/profile', 'AgencyController@profile')->name('profile');

    // change password
    Route::post('/change-password', 'AgencyController@changePassword')->name('changePassword');

    // biodata
    Route::resource('/requirement-types/{requirement_type}/requirements', 'BiodataPostController');

    // requirement post
    Route::resource('/requirement-type/{requirement_type}/requirements/{requirement}/requirement-posts', 'RequirementPostController');

    // requirement sent
    Route::post('/requirements/{requirement}/sent', 'BiodataPostController@sent')->name('requirements.sent');
});

