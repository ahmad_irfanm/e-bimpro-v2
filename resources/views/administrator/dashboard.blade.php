@extends('layouts.master')

@section('dashboard')
    active
@endsection

@section('title')
    Dashboard
@endsection

@section('menu')
    <i class="fa fa-chart-line"></i> Dashboard
@endsection

@section('breadcumb')
    <li class="breadcrumb-item active">Dashboard</li>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
                <div class="jumbotron d-flex align-items-center justify-content-center" style="background: #ffe7c3;">
                    <img src="{{asset('assets/img/illustration-6.svg')}}" class="illustration mr-5" alt="">
                    <div class="col-md-5">
                        <h4 class="mb-3"><span style="font-weight: normal">Halo, </span> {{convertRole(Auth::user()->role)}}</h4>
                        <p style="margin-bottom: 0px;">Selamat datang di aplikasi e-Bimpro. Kamu dapat memulai dengan melihat detail persyaratan yang telah dikirim oleh dinas, dan memverifikasinya untuk melanjutkan proses.</p>
                    </div>
                </div>
            </div>

            <div class="col-md-5">
                <div class="card card-default">
                    <div class="card-header">
                        <h4 class="card-title">
                            Aktifitas
                        </h4>
                    </div>
                    <div class="card-body">

                        <canvas id="chart-pie"  width="400" height="400"></canvas>

                    </div>
                </div>

                <div class="card card-default mt-4">
                    <div class="card-header">
                        <h4 class="card-title">
                            Grafik
                        </h4>
                    </div>
                    <div class="card-body">

                        <canvas id="chart-line"  style="width: 100%;"></canvas>

                    </div>
                </div>

                <!-- About Me Box -->
                <div class="card card-default mt-4">
                    <div class="card-header d-flex align-items-center justify-content-between">
                        <h4 class="card-title">Tentang Saya</h4>
                        <a href="{{route('administrator.profile')}}">Lihat profile <i class="fa fa-arrow-right ml-2 text-bold"></i></a>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <strong><i class="fas fa-user-circle mr-1"></i> Username</strong>

                        <p class="text-muted">
                            {{Auth::user()->username}}
                        </p>

                        <hr>

                        <strong><i class="fas fa-user-tie mr-1"></i> Wewenang</strong>

                        <p class="text-muted">{{convertRole(Auth::user()->role)}}</p>

                        <hr>

                        <strong><i class="fas fa-calendar mr-1"></i> Dibuat pada</strong>

                        <p class="text-muted">
                            {{Auth::user()->created_at->diffForHumans()}}
                        </p>

                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>


            <div class="col-md-7">

                <div class="row">

                    <div class="col-md-12">
                        <h5 class="mb-3" style="display: block;">Perhitungan data</h5>
                    </div>

                    <div class="col-md-4 col-sm-4 col-12">
                        <div class="info-box bg-info">
                            <span class="info-box-icon"><i class="fa fa-users"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text mb-2">Persyaratan</span>
                                <h3 class="info-box-number">{{\App\Requirement::whereIn('status', ['2', '3', '4'])->count()}}</h3>

                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>

                    <div class="col-md-4 col-sm-4 col-12">
                        <div class="info-box bg-default">
                            <span class="info-box-icon"><i class="fa fa-boxes"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text mb-2">Tipe Persyaratan</span>
                                <h3 class="info-box-number">{{\App\RequirementType::count()}}</h3>

                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>

                    <div class="col-md-4 col-sm-4 col-12">
                        <div class="info-box bg-light">
                            <span class="info-box-icon"><i class="fa fa-landmark"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text mb-2">Dinas</span>
                                <h3 class="info-box-number">{{\App\Agency::count()}}</h3>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>

                    <div class="col-md-12 mt-5">
                        <h5 class="mb-3">Terbaru</h5>

                        @php
                            $requirements = \App\Requirement::whereIn('status', [2, 3, 4])->orderBy('updated_at', 'desc')->skip(0)->take(10)->get();
                        @endphp
                        @foreach($requirements as $requirement)
                            <div class="card card-default">
                                <div class="card-body" style="padding-bottom: 0px;">
                                    <div class="row d-flex align-items-center justify-content-between">
                                        <div class="col-md-8">
                                            <h6 class="mb-2" style="white-space: nowrap; overflow: hidden;">{{$requirement->agency->name}}</h6>
                                        </div>
                                        <div class="col-md-4 text-right">
                                            <p class="text-muted text-sm">
                                                <span class="mr-2">
                                                    <i class="fa fa-calendar mr-1"></i> {{getDatetime($requirement->status, $requirement)}}
                                                </span>
                                                <span>
                                                    <i class="fa fa-clock mr-1"></i> {{getOnlyTime($requirement->status, $requirement)}}
                                                </span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body row align-items-center">
                                    <div class="col-md-3">
                                        <div class="d-flex align-items-center">
                                            <div class="mr-2">
                                                {!! $requirement->getAvatar() !!}
                                            </div>
                                            <div style="white-space: nowrap; overflow: hidden" data-toggle="tooltip" title="{{$requirement->getContent(getMainFirst()->id)}}">
                                                {{$requirement->getContent(getMainFirst()->id)}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <span style="padding: 8px; font-weight: normal;" class="badge d-block badge-{{convertColor($requirement->status)}}">
                                            <i class="fa fa-{{convertIcon($requirement->status)}} mr-1"></i> {{convertStatus($requirement->status)}}
                                        </span>
                                    </div>
                                    <div class="col-md-4" style="white-space: nowrap; overflow: hidden;" data-toggle="tooltip" title="{{$requirement->requirement_type->name}}">
                                        <span class="text-muted">
                                            {{$requirement->requirement_type->name}}
                                        </span>
                                    </div>
                                    <div class="col-md-3 text-right">
                                        <a href="{{route('administrator.requirements.show', ['requirement_type' => $requirement->requirement_type_id, 'requirement' => $requirement->id])}}">
                                            Lihat persyaratan <i class="fa fa-arrow-right ml-2"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        @if($requirements->count() == 0)
                            <p class="text-muted">Tidak ada draft untuk ditampilkan.</p>
                        @endif
                    </div>

                </div>


            </div>


        </div>
    </div>
@endsection

@section('css')
@endsection

@section('js')
    <script src="{{asset('assets/plugins/chart.js/Chart.min.js')}}"></script>
    <script>
        var ctx = document.getElementById('chart-pie').getContext('2d');

        var data = {
            datasets: [{
                data: {{$pie}},
                backgroundColor: ['#17a2b8', '#007bff', '#28a745']
            }],

            // These labels appear in the legend and in the tooltips when hovering different arcs
            labels: [
                'Diterima',
                'Terverifikasi ke-1',
                'Selesai'
            ]
        };

        var myPieChart = new Chart(ctx, {
            type: 'pie',
            data: data,
            options: {
                legend: {
                    position: 'top'
                }
            }
        });

        ctx = document.getElementById('chart-line').getContext('2d');

        data = {
            datasets: [
                {
                    label: 'Biodata dibuat oleh dinas',
                    data: {!! $line !!} ,
                    backgroundColor: 'transparent',
                    borderColor: '#ffc107',
                    borderWidth: 5
                }
            ],

            labels: {!! $labels !!}
        };

        var myLineChart = new Chart(ctx, {
            type: 'line',
            data: data
        });
    </script>
@endsection

