@php
    $state = isset($requirement);
    $theme = $state ? 'warning' : 'primary';
@endphp

@extends('layouts.agency')

@section('requirement-type')
    active
@endsection

@section('title')
    {{$state ? 'Edit' : 'Tambah'}} Biodata
@endsection

@section('requirement-tree')
    menu-open
@endsection

@section('requirement')
    active
@endsection

@section('requirement-' . $requirementType->id)
    active
@endsection

@section('breadcumb')
    <li class="breadcrumb-item"><a href="{{route('agency.dashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="{{route('agency.requirements.index', ['requirement_type' => $requirementType])}}">{{$requirementType->name}}</a></li>
    <li class="breadcrumb-item active">{{$state ? 'Edit' : 'Tambah'}} Biodata</li>
@endsection

@section('content')
    <div class="container-fluid">

        <div class="jumbotron d-flex align-items-center justify-content-center">
            <img src="{{asset('assets/img/illustration-3.svg')}}" class="illustration mr-5" alt="">
            <div class="col-md-5">
                <h4 class="mb-3">
                    @if($state)
                        <span class="text-warning">Edit</span>
                    @else
                        <span class="text-info">Tambah</span>
                    @endif
                    Biodata {{$requirementType->name}}
                </h4>
                <p style="margin-bottom: 0px;" class="text-muted">
                    Isi formulir dibawah sesuai dengan form biodata. Kemudian tekan tombol <span class="text-info">Simpan</span> untuk menyimpan data Form biodata.
                </p>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card card-{{$theme}}">
                    <div class="card-header">
                        <h3 class="card-title">Form {{ $state ? 'edit biodata' : 'biodata baru' }}</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form class="form-horizontal" action="{{route('agency.requirements.' . ($state ? 'update' : 'store'), $state ? ['requirement_type' => $requirementType->id, 'requirement' => $requirement->id] : ['requirement_type' => $requirementType->id])}}" method="post">
                        @csrf
                        @if($state)
                            @method('put')
                        @endif
                        <div class="card-body">

                            @if(session('msg'))
                                <div class="alert alert-success">
                                    {{session('msg')}}
                                </div>
                            @elseif(session('msgError'))
                                <div class="alert alert-danger">
                                    {{session('msgError')}}
                                </div>
							@elseif($errors->any())
								<div class="alert alert-danger">
									Opps.. Ada yang error! Harap check kembali semua formulir dibawah.
                                    {{--@foreach($errors->all() as $error)--}}
                                        {{--{{$error}}--}}
                                    {{--@endforeach--}}
								</div>
                            @endif

                            @foreach($requirementType->biodatas()->orderBy('requirement_type_biodata.order', 'asc')->get() as $biodata)
                            <div class="form-group row align-items-center">
                                <label for="inputEmail3" class="col-md-2 col-form-label">{{$biodata->name}}</label>
                                <div class="col-md-10">

                                    @if($biodata->type == "date")
                                    <input type="date" name="input_{{$biodata->id}}" class="form-control @error('input_' . $biodata->id) is-invalid @enderror" id="inputEmail3" placeholder="{{$biodata->placeholder}}" value="{{old('input_' . $biodata->id) ? old('input_' . $biodata->id) : $state ? $requirement->getContent($biodata->id, true) : ''}}">
                                    @elseif($biodata->type == "text")
                                    <input type="text" name="input_{{$biodata->id}}" class="form-control @error('input_' . $biodata->id) is-invalid @enderror" id="inputEmail3" placeholder="{{$biodata->placeholder}}" value="{{old('input_' . $biodata->id) ? old('input_' . $biodata->id) : $state ? $requirement->getContent($biodata->id) : ''}}">
                                    @elseif($biodata->type == "numeric")
                                    <input type="number" name="input_{{$biodata->id}}" class="form-control @error('input_' . $biodata->id) is-invalid @enderror" id="inputEmail3" placeholder="{{$biodata->placeholder}}" value="{{old('input_' . $biodata->id) ? old('input_' . $biodata->id) : $state ? $requirement->getContent($biodata->id) : ''}}">
                                    @elseif($biodata->type == "textarea")
                                        <textarea  cols="30" rows="10" name="input_{{$biodata->id}}" class="form-control @error('input_' . $biodata->id) is-invalid @enderror" id="inputEmail3" placeholder="{{$biodata->placeholder}}">{{old($biodata->id) ? old($biodata->id) : $state ? $requirement->getContent($biodata->id) : ''}}</textarea>
                                    @elseif($biodata->type == "radio")
                                        @foreach(explode(PHP_EOL, $biodata->choices) as $i => $choice)
                                            <div class="custom-control custom-radio">
                                                <input class="custom-control-input" type="radio" id="department{{$i}}" name="input_{{$biodata->id}}" value="{{$choice}}" {{old('input_' . $biodata->id) ? old('input_' . $biodata->id) == $choice ? 'checked' : '' : ($state ? $requirement->getContent($biodata->id) == $choice ? 'checked' : '' : ($i == 0 ? 'checked' : ''))}}>
                                                <label for="department{{$i}}" class="custom-control-label">{{$choice}}</label>
                                            </div>
                                        @endforeach
                                    @endif

                                    @if($errors->first('input_' . $biodata->id))
                                        <div class="invalid-feedback">
                                            {{$errors->first('input_' . $biodata->id)}}
                                        </div>
                                    @endif
                                </div>
                            </div>
                            @endforeach



                            <div class="form-group row form-submit">
                                <div class="col-md-2"></div>
                                <div class="col-md-10">
                                    <button type="submit" class="btn btn-{{$theme}} mr-2"><i class="fa fa-save mr-2"></i> Simpan</button>
                                    <a href="{{route('agency.requirements.index', ['requirement_type' => $requirementType])}}" class="btn">Kembali</a>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('css')
@endsection

@section('js')
    <!-- bs-custom-file-input -->
    <script src="{{asset('assets/plugins/bs-custom-file-input/bs-custom-file-input.min.js')}}"></script>

@endsection

