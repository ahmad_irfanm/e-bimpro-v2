@extends('layouts.agency')

@section('title')
    List Biodata {{$requirementType->name}}
@endsection

@section('requirement-tree')
    menu-open
@endsection

@section('requirement')
    active
@endsection

@section('requirement-' . $requirementType->id)
    active
@endsection

@section('breadcumb')
    <li class="breadcrumb-item"><a href="{{route('agency.dashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item active">{{$requirementType->name}}</li>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="jumbotron d-flex align-items-center justify-content-center">
            <img src="{{asset('assets/img/illustration-5.svg')}}" class="illustration mr-5" alt="">
            <div class="col-md-5">
                <h4 class="mb-3">{{$requirementType->name}}</h4>
                <p style="margin-bottom: 0px;" class="text-muted">Dibawah ini merupakan list biodata dengan menu persyaratan {{$requirementType->name}}. Kamu dapat melihat detail persyaratan menekan tombol <span class="text-info">Lihat persyaratan</span>.</p>
            </div>
        </div>

            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header align-items-center justify-content-between" style="display: grid; grid-template-columns: repeat(2, auto)">
                        <h3 class="card-title">List Biodata</h3>
                        <a href="{{ route('agency.requirements.create', ['requirement_type' => $requirementType->id]) }}" class="text-bold btn text-primary" style="background: #f5f6fb;"><i class="fa fa-plus-circle mr-1"></i> Tambah Biodata</a>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example1" class="table table-striped">
                            <thead>
                            <tr>
                                @foreach(getMain() as $main)
                                    <th>{{$main->name}}</th>
                                @endforeach
                                <th class="text-center">Tanggal Dibuat</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($requirementType->requirements()->whereAgencyId(Auth::user()->agency_id)->get() as $requirement)
                                    <tr>
                                        @foreach(getMain() as $i => $biodata)
                                            <td>{!! $i == 0 ? $requirement->getAvatar() : '' !!}<span class="ml-2">{{$requirement->getContent($biodata->id)}}</span></td>
                                        @endforeach
                                        <td class="text-center">
                                            {{$requirement->created_at->diffForHumans()}}
                                        </td>
                                        <td class="text-center">
                                            <span class="badge badge-{{convertColor($requirement->status)}}">
                                                {{convertStatus($requirement->status)}}
                                            </span>
                                        </td>
                                        <td class="text-center">
                                            <a href="{{route('agency.requirements.show', ['requirement_type' => $requirementType->id, 'requirement' => $requirement->id])}}" class="btn btn-xs text-info mr-2" title="Lihat Persyaratan">
                                                 Lihat Persyaratan <i class="fa fa-arrow-right ml-1"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                @foreach(getMain() as $main)
                                    <th>{{$main->name}}</th>
                                @endforeach
                                <th class="text-center">Tanggal Dibuat</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Aksi</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>
@endsection

@section('css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}">
@endsection

@section('js')
    <!-- DataTables -->
    <script src="{{asset('assets/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <!-- page script -->
    <script>
        $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
            });
        });
    </script>
@endsection

