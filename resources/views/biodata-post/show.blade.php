@extends('layouts.agency')

@section('title')
    {{$requirementType->name}} {{$requirement->getContent(getMainFirst()->id)}}
@endsection

@section('requirement-tree')
    menu-open
@endsection

@section('requirement')
    active
@endsection

@section('requirement-' . $requirementType->id)
    active
@endsection

@section('css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}">
    <style>
        .preloader {
            display: flex;
            align-items: center;
            justify-content: center;
        }
        .lds-dual-ring {
            display: inline-block;
            box-sizing: border-box;
            margin-right: 16px;
        }
        .lds-dual-ring:after {
            box-sizing: border-box;
            content: " ";
            display: block;
            width: 30px;
            height: 30px;
            margin: 8px;
            border-radius: 50%;
            border: 6px solid #333;
            border-color: #333 transparent #333 transparent;
            animation: lds-dual-ring 1.2s linear infinite;
        }
        .lds-dual-ring-dark {
            display: inline-block;
            box-sizing: border-box;
            margin-right: 16px;
        }
        .lds-dual-ring-dark:after {
            box-sizing: border-box;
            content: " ";
            display: block;
            width: 30px;
            height: 30px;
            margin: 8px;
            border-radius: 50%;
            border: 6px solid #e83e8c;
            border-color: #e83e8c transparent #e83e8c transparent;
            animation: lds-dual-ring 1.2s linear infinite;
        }
        .btn-step {
            width: 100%;
        }
        .border-step {
            height: 4px;
            background: #f5f6fb;
        }
        .btn-file-zone {
            padding: 48px 24px;
            border-radius: 12px;
            text-align: center;
            border: 1.5px dashed #808080;
            cursor: pointer;
            font-weight: normal !important;
        }
        @keyframes lds-dual-ring {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }
    </style>
@endsection

@section('breadcumb')
    <li class="breadcrumb-item"><a href="{{route('agency.dashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="{{route('agency.requirements.index', ['requirement_type' => $requirementType->id])}}">{{$requirementType->name}}</a></li>
    <li class="breadcrumb-item active">{{$requirement->getContent(getMainFirst()->id)}}</li>

@endsection

@section('content')
    <div class="container-fluid">

        @if(session('msg'))
        <div class="alert alert-success">
            {{session('msg')}}
        </div>
        @endif

        <div class="jumbotron d-flex align-items-center justify-content-center">
            <img src="{{asset('assets/img/illustration-13.svg')}}" class="illustration mr-5" alt="">
            <div class="col-md-5">
                <h4 class="mb-3">{{$requirement->getContent(getMainFirst()->id)}}</h4>
                <p style="margin-bottom: 0px;" class="text-muted">
                    Kamu dapat melihat detail biodata disebelah dikiri, dan kamu dapat mengedit biodata dengan mengklik tombol edit. Kemudian detail persyaratan disebelah kanan. Lengkapi persyaratan dengan melampirkan file persyaratan agar dapat dikirim untuk diverifikasi.
                </p>
            </div>
        </div>

        <div class="row">

            <div class="col-md-3">

                <div class="card card-primary">
                    <div class="card-header align-items-center justify-content-between" style="display: grid; grid-template-columns: repeat(2, auto)">
                        <h4 class="card-title">Biodata</h4>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">

                        @include('layouts.biodata')

                    </div>
                    <!-- /.card-body -->
                </div>

            </div>
            <div class="col-md-9">
                <div class="card card-default">
                    <div class="card-header d-flex align-items-center justify-content-between">
                        <h4 class="card-title">Persyaratan {{$requirementType->name}}</h4>
                        <div>
                            <i class="fa fa-paper-plane mr-1"></i> <span data-toggle="tooltip" title="Status persyaratan" class="badge badge-{{convertColor($requirement->status)}}">{{convertStatus($requirement->status)}}</span>
                            <i class="fa fa-calendar ml-4 mr-1"></i> <span data-toggle="tooltip" title="Tanggal entri persyaratan" class="badge badge-default">{{$requirement->created_at->diffForHumans()}}</span>
                        </div>
                    </div>
                    <div class="card-body align-items-center justify-content-between" style="display: grid; grid-template-columns: 1fr 0.3fr 1fr 0.3fr 1fr 0.3fr 1fr;">
                        <a href="" class="btn btn-step btn-{{$requirement->status == 1 || $requirement->status == 2 || $requirement->status == 3 || $requirement->status == 4 ? convertColor($requirement->status) : 'default disabled'}} mr-2" style="pointer-events: none; user-select: none;  font-size: 14px; white-space: nowrap; overflow: hidden;">
                            <i class="fa fa-box-open mr-2"></i> Draft
                        </a>
                        <div class="border-step bg-{{$requirement->status == 2 || $requirement->status == 3 || $requirement->status == 4 ? convertColor($requirement->status) : 'default'}}"></div>
                        <a href="" class="btn btn-step btn-{{$requirement->status == 2 || $requirement->status == 3 || $requirement->status == 4 ? convertColor($requirement->status) : 'default disabled'}} mr-2" style="pointer-events: none; user-select: none;   font-size: 14px; white-space: nowrap; overflow: hidden;">
                            <i class="fa fa-box mr-2"></i> Terkirim
                        </a>
                        <div class="border-step bg-{{$requirement->status == 3 || $requirement->status == 4 ? convertColor($requirement->status) : 'default'}}"></div>
                        <a href="" class="btn btn-step btn-{{$requirement->status == 3 || $requirement->status == 4 ? convertColor($requirement->status) : 'default disabled'}} mr-2" style="pointer-events: none; user-select: none;   font-size: 14px; white-space: nowrap; overflow: hidden;">
                            <i class="fa fa-check mr-2"></i> Terverifikasi
                        </a>
                        <div class="border-step bg-{{$requirement->status == 4 ? convertColor($requirement->status) : 'default'}}"></div>
                        <a href="" class="btn btn-step btn-{{$requirement->status == 4 ? convertColor($requirement->status) : 'default disabled'}}" style="pointer-events: none; user-select: none; font-size: 14px; white-space: nowrap; overflow: hidden;">
                            <i class="fa fa-check-double mr-2"></i> Selesai
                        </a>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            @foreach($requirementType->requirement_forms as $requirementForm)
                                @php
                                    $requirementPost = $requirement->requirement_posts()->whereRequirementFormId($requirementForm->id)->first()
                                @endphp
                                <div class="col-md-12">
                                    <div class="card card-outline card-{{ $requirementPost ? 'success' : 'danger' }}">
                                        <div class="card-header">
                                            <h3 class="card-title">{{$requirementForm->name}}</h3 class="card-title">
                                        </div>
                                        <!-- /.card-header -->
                                        <div class="card-body" style="padding: 0px;">
                                            @if($requirementPost)
                                                @if($requirementPost->file_type == 'application/pdf')
                                                <a href="{{asset('storage/' . $requirementPost->file)}}" target="_blank" class="btn btn-lg text-danger d-block text-center"><i class="fa fa-file-pdf mr-2"></i> File PDF</a>
                                                @else
                                                <img src="{{asset('storage/' . $requirementPost->file)}}" alt="{{$requirementForm->name}}" style="width: 100%; height: auto; object-fit: cover">
                                                @endif
                                            @else
                                                <form action="{{route('agency.requirement-posts.store', ['requirement_type' => $requirementType->id, 'requirement' => $requirement->id])}}" enctype="multipart/form-data" class="form-input" style="padding: 0px 24px;">
                                                    <input type="file" class="file-input" id="file-{{$requirementForm->id}}" style="display: none;" name="image"/>
                                                    <label for="file-{{$requirementForm->id}}" class="btn-file-zone text-bg btn-block">
                                                        <i class="fa fa-plus mr-2"></i> Klik untuk melampirkan file
                                                    </label>
                                                    <input type="hidden" name="requirement_form_id" value="{{$requirementForm->id}}">
                                                </form>
                                            @endif
                                        </div>
                                        <div class="card-body justify-content-between align-items-center" style="display: grid; grid-template-columns: repeat(2, auto)">
                                            @if($requirementPost)
                                                @if($requirement->status != 3 && $requirement->status != 4)
                                                <div>
                                                    <a href="{{route('agency.requirement-posts.destroy', ['requirement_type' => $requirementType->id, 'requirement' => $requirement->id, 'requirement_post' => $requirementPost->id])}}" class="btn text-danger btn-xs mr-1 btn-remove"><i class="fa fa-trash-alt mr-1"></i> Hapus</a>
                                                    <a href="" class="btn text-warning mr-2 btn-change btn-xs"><i class="fa fa-edit mr-1"></i> Edit</a>
                                                    <form class="form-edit" action="{{route('agency.requirement-posts.update', ['requirement_type' => $requirementType->id, 'requirement' => $requirement->id, 'requirement_post' => $requirementPost->id])}}" method="post" enctype="multipart/form-data">
                                                        <input type="file" class="file-input-edit" name="image" style="display: none;">
                                                    </form>
                                                </div>
                                                @endif
                                                <div>
                                                    <a download="{{$requirementForm->name . $requirementPost->file_ext}}" href="{{asset('storage/' . $requirementPost->file)}}" class="text-primary btn btn-xs text-orange" style="background: #f5f6fb;"><i class="fa fa-download mr-1"></i> Download</a>
                                                </div>
                                            @else
                                                <small class="text-info"><i class="fa fa-exclamation-circle mr-1"></i> Belum diinputkan</small>
                                            @endif
                                        </div>
                                        <!-- /.card-body -->
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="card-body d-flex align-items-center justify-content-end">
                        @if($requirement->status == 1 || $requirement->status == 2)
                        <a href="{{route('agency.requirements.destroy', ['requirement_type' => $requirementType->id, 'requirement' => $requirement->id])}}" class="btn text-danger text-bg mr-3" id="btn-delete"><i class="fa fa-trash mr-1"></i> Hapus persyaratan</a>
                        @endif

                        @if($requirement->status == 1)
                            @if($requirement->requirement_posts->count() >= $requirementType->requirement_forms->count())
                            <a href="{{route('agency.requirements.sent', ['requirement' => $requirement->id])}}" class="btn btn-info btn-animated animated tada" style="animation-iteration-count: infinite;" id="btn-send"><i class="fa fa-paper-plane mr-1"></i> Kirim persyaratan</a>
                            @else
                                <span tabindex="1"  data-toggle="tooltip" title="Persyaratan belum terpenuhi">
                                    <a href="" onclick="event.preventDefault()" class="btn btn-default disabled"><i class="fa fa-paper-plane mr-1"></i> Kirim persyaratan</a>
                                </span>
                            @endif
                        @endif
                    </div>
                </div>
            </div>

        </div>
    </div>
    </div>
@endsection


@section('js')
    <!-- DataTables -->
    <script src="{{asset('assets/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>

    <!-- page script -->
    <script>
        setTimeout(() => {
            $('.alert').fadeOut();
        }, 3000);

        $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
            });
        });

        $('.file-input').change(function () {
            let preloaderAnimation = $('<div></div>').addClass('lds-dual-ring');
            let preloaderText = $('<div></div>').text('Sedang mengupload...');
            let preloader = $('<div class="preloader"></div>').append(preloaderAnimation, preloaderText);
            $(this).siblings('label').html(preloader).addClass('disabled');
            $(this).parents('.form-input').submit();
        });

        $('.btn-change').click(function (e) {
            e.preventDefault();
            $(this).siblings('form').children('.file-input-edit').click();
        });

        $('.file-input-edit').change(function () {
            let preloaderAnimation = $('<div></div>').addClass('lds-dual-ring-dark');
            let preloaderText = $('<div></div>').text('Sedang mengupload...');
            let preloader = $('<div class="preloader"></div>').append(preloaderAnimation, preloaderText);

            $(this).parents('.card-footer').siblings('.card-body').html(preloader);
            $(this).parents('.form-edit').submit();
        });

        $('.form-edit').submit(function (e) {
            e.preventDefault();

            var formData = new FormData(this);
            let token = $('meta[name="csrf-token"]').attr('content');
            formData.append('_token', token);
            formData.append('_method', 'put');

            $.ajax({
                url: $(this).attr('action'),
                type: 'POST',
                data: formData,
                success: function (data) {
                    Swal.fire({
                        type: 'success',
                        title: 'Berhasil',
                        text: data.message
                    }).then((result) => {
                        if (result.value) {
                            location.reload();
                        }
                    });
                },
                error: function (errors) {
                    console.log(errors);
                    Swal.fire({
                        type: 'error',
                        title: 'Ada yang error',
                        text: errors.responseJSON.errors.image.toString()
                    }).then((result) => {
                        if (result.value) {
                            location.reload();
                        }
                    })
                },
                cache: false,
                contentType: false,
                processData: false
            });

        });

        $('.form-input').submit(function (e) {
            e.preventDefault();

            var formData = new FormData(this);
            let token = $('meta[name="csrf-token"]').attr('content');
            formData.append('_token', token);


            $.ajax({
                url: $(this).attr('action'),
                type: 'POST',
                data: formData,
                success: function (data) {
                    Swal.fire({
                        type: 'success',
                        title: 'Berhasil',
                        text: data.message
                    }).then((result) => {
                        if (result.value) {
                            location.reload();
                        }
                    });
                },
                error: function (errors) {
                    Swal.fire({
                        type: 'error',
                        title: 'Ada yang error',
                        text: errors.responseJSON.errors.image.toString()
                    }).then((result) => {
                        if (result.value) {
                            location.reload();
                        }
                    })
                },
                cache: false,
                contentType: false,
                processData: false
            });
        });

        $('#btn-send').click(function (e) {
            e.preventDefault();

            let url = $(this).attr('href');
            let token = $('meta[name="csrf-token"]').attr('content');

            Swal.fire({
                title: 'Konfirmasi',
                text: "Apakah semua persyaratan sudah fix dan ingin mengirimnya ?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, kirim sekarang',
                cancelButtonText: 'Jangan dulu'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        method: 'post',
                        url,
                        data: {_token: token},
                        success: function (response) {
                            Swal.fire({
                                type: 'success',
                                title: 'Berhasil',
                                text: response.msg
                            }).then((result) => {
                                if (result.value) {
                                    location.reload();
                                }
                            });
                        },
                        error: function (error) {
                            Swal.fire({
                                type: 'error',
                                title: 'Ada yang error',
                                text: error.msg
                            }).then((result) => {
                                if (result.value) {
                                    location.reload();
                                }
                            });
                        }
                    })
                }
            });

        });

        $('#btn-delete').click(function (e) {
            e.preventDefault();

            let url = $(this).attr('href');
            let token = $('meta[name="csrf-token"]').attr('content');

            Swal.fire({
                title: 'Konfirmasi',
                text: "Apakah kamu ingin menghapus persyaratan ini ?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, hapus sekarang',
                cancelButtonText: 'Jangan'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        method: 'post',
                        url,
                        data: {_token: token, _method: 'delete'},
                        success: function (response) {
                            Swal.fire({
                                title: 'Berhasil',
                                text: response.msg,
                                type: 'success'
                            }).then(result => {
                                if (result.value) {
                                    window.location = response.redirect;
                                }
                            })
                        },
                        error: function (error) {
                            Swal.fire({
                                title: 'Ada yang error',
                                text: error.msg,
                                type: 'error'
                            }).then(result => {
                                if (result.value) {
                                    location.reload();
                                }
                            })
                        }
                    })
                }
            });


        })

        $('.btn-remove').click(function (e) {
            e.preventDefault();

            let url = $(this).attr('href');
            let token = $('meta[name="csrf-token"]').attr('content');

            Swal.fire({
                title: 'Konfirmasi',
                text: "Apakah kamu ingin menghapus file persyaratan ini ?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, hapus sekarang',
                cancelButtonText: 'Jangan'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        method: 'post',
                        url,
                        data: {_token: token, _method: 'delete'},
                        success: function (response) {
                            Swal.fire({
                                title: 'Berhasil',
                                text: response.msg,
                                type: 'success'
                            }).then(result => {
                                if (result.value) {
                                    location.reload();
                                }
                            })
                        },
                        error: function (error) {
                            Swal.fire({
                                title: 'Ada yang error',
                                text: error.msg,
                                type: 'error'
                            }).then(result => {
                                if (result.value) {
                                    // location.reload();
                                }
                            })
                        }
                    })
                }
            });


        })
    </script>

    <script>

    </script>
@endsection

