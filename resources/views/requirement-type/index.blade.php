@extends('layouts.master')

@section('title')
    List Tipe Persyaratan
@endsection

@section('requirement-type')
    active
@endsection

@section('breadcumb')
    <li class="breadcrumb-item"><a href="{{route('administrator.dashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item active">Tipe Persayaratan</li>
@endsection

@section('menu')
    <i class="fa fa-boxes"></i> Tipe Persyaratan
@endsection

@section('content')
    <div class="container-fluid">
        <div class="jumbotron d-flex align-items-center justify-content-center">
            <img src="{{asset('assets/img/illustration-5.svg')}}" class="illustration mr-5" alt="">
            <div class="col-md-5">
                <h4 class="mb-3"><span style="font-weight: normal">Tipe persyaratan</h4>
                <p style="margin-bottom: 0px;" class="text-muted">Dibawah ini merupakan list tipe persyaratan yang dapat dinas isi. Kamu dapat membuatnya dengan menekan tombol <span class="text-info">Tambah Persyaratan</span>. Kemudian Kamu dapat melihat detail form persyaratan dari tipe persyaratan dengan menekan tombol <span class="text-info">Detail</span>, dan kamu juga dapat mengatur form biodata serta urutannya dengan menekan tombol <span class="text-info">Atur Biodata</span>.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header align-items-center justify-content-between" style="display: grid; grid-template-columns: repeat(2, auto)">
                        <h3 class="card-title">List Tipe Persyaratan</h3>
                        <a href="{{ route('administrator.requirement-types.create') }}" class="btn btn-primary"><i class="fa fa-plus-circle mr-1"></i> Tambah Persyaratan</a>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example1" class="table table-striped">
                            <thead>
                            <tr>
                                <th>Nama Persyaratan</th>
                                <th class="text-center">Jml. Form</th>
                                <th class="text-center">Jml. Biodata</th>
                                <th class="text-center">Surat</th>
                                <th class="text-center">Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($requirement_types as $rt)
                                    <tr>
                                        <td>{{$rt->name}}</td>
                                        <td class="text-center">
                                            <a href="{{route('administrator.requirement-forms.index', ['requirement_type' => $rt->id])}}" class="text-info font-weight-bold">
                                                {{$rt->requirement_forms->count()}}
                                            </a>
                                        </td>
                                        <td class="text-center">
                                            <a href="" class="text-info font-weight-bold">
                                                {{$rt->biodatas->count()}}
                                            </a>
                                        </td>
                                        <td class="text-center">
                                            @if($rt->print_file)
                                                <span class="badge badge-success">
                                                    Ada
                                                </span>
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            <a href="{{route('administrator.requirement-types.show', ['requirement_type' => $rt->id])}}" class="btn-xs btn btn-default" title="Atur Biodata" data-toggle="tooltip">
                                                <i class="fa fa-id-card mr-1"></i> Atur Biodata
                                            </a>
                                            <a href="{{route('administrator.requirement-forms.index', ['requirement_type' => $rt->id])}}" class="btn-xs text-info ml-2" title="List formulir" data-toggle="tooltip">
                                                 Detail <i class="fa fa-arrow-right ml-1"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>
@endsection

@section('css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.bootstrap4.min.css">
@endsection

@section('js')
    <!-- DataTables -->
    <script src="{{asset('assets/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>
    <!-- page script -->
    <script>
        $(function () {
            $("#example1").DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
            });
        });
    </script>
@endsection

