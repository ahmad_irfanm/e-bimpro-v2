@php
    $state = isset($requirement_type);
    $theme = $state ? 'warning' : 'primary';
@endphp

@extends('layouts.master')

@section('requirement-type')
    active
@endsection

@section('title')
    List Tipe Persyaratan
@endsection

@section('menu')
    <i class="fa fa-boxes"></i> Tipe Persyaratan
@endsection

@section('breadcumb')
    <li class="breadcrumb-item"><a href="{{route('administrator.dashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="{{route('administrator.requirement-types.index')}}">List Tipe Persayaratan</a></li>
    @if($state)
    <li class="breadcrumb-item"><a href="{{route('administrator.requirement-forms.index', ['requirement_type' => $requirement_type->id])}}">{{$requirement_type->name}}</a></li>
    <li class="breadcrumb-item active">Edit Persayaratan</li>
    @else
    <li class="breadcrumb-item active">Tambah Persayaratan</li>
    @endif
@endsection

@section('content')
    <div class="container-fluid">
        <div class="jumbotron d-flex align-items-center justify-content-center">
            <img src="{{asset('assets/img/illustration-3.svg')}}" class="illustration mr-5" alt="">
            <div class="col-md-5">
                <h4 class="mb-3"><span style="font-weight: normal">{{($state ? 'Edit' : 'Tambah') . " Tipe Persyaratan"}}</h4>
                <p style="margin-bottom: 0px;" class="text-muted">
                    Isi formulir dibawah sesuai dengan form tipe persyaratan. Kemudian tekan tombol <span class="text-info">Simpan</span> untuk menyimpan data Tipe persyaratan.
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-{{$theme}}">
                    <div class="card-header">
                        <h3 class="card-title">Form {{ $state ? 'edit tipe persyaratan' : 'tipe persyaratan baru' }}</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form class="form-horizontal" action="{{route('administrator.requirement-types.' . ($state ? 'update' : 'store'), $state ? ['requirement_type' => $requirement_type->id] : [])}}" method="post" enctype="multipart/form-data">
                        @csrf
                        @if($state)
                            @method('put')
                        @endif
                        <div class="card-body">

                            @if(session('msg'))
                                <div class="alert alert-success">
                                    {{session('msg')}}
                                </div>
                            @elseif(session('msgError'))
                                <div class="alert alert-danger">
                                    {{session('msgError')}}
                                </div>
                            @endif

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-2 col-form-label">Nama Persyaratan</label>
                                <div class="col-sm-10">
                                    <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="inputEmail3" placeholder="Nama persyaratan" value="{{old('name') ? old('name') : ($state ? $requirement_type->name : '')}}">
                                    @if($errors->first('name'))
                                    <div class="invalid-feedback">
                                        {{$errors->first('name')}}
                                    </div>
                                    @endif
                                </div>
                            </div> 
                            <div class="form-group row">
                                <label for="inputPassword3" class="col-sm-2 col-form-label">Template Surat</label>
                                <div class="col-sm-10">
                                    @if($state)
                                        @if($requirement_type->print_file)
                                            <a href="{{ URL::asset('/storage/app/' . $requirement_type->print_file) }}" class="text-info mb-3 d-block" download="{{$requirement_type->print_file}}"><i class="fa fa-file-alt mr-1"></i> File template surat</a>
                                        @endif
                                    @endif
                                    <input type="file" class="form-control @error('print_file') is-invalid @enderror" id="inputPassword3" name="print_file">
                                    @if($errors->first('print_file'))
                                        <div class="invalid-feedback">
                                            {{$errors->first('print_file')}}
                                        </div>
                                    @else
                                        <small id="passwordHelpBlock" class="form-text text-muted">
                                            Lampirkan template surat jika hasil akhir dari persyaratan ini ingin dapat dicetak.
                                        </small>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row form-submit">
                                <div class="col-md-2"></div>
                                <div class="col-md-10">
                                    <button type="submit" class="btn btn-{{$theme}} mr-2"><i class="fa fa-save mr-2"></i> Simpan</button>
                                    @if($state)
                                    <a href="{{route('administrator.requirement-forms.index', ['requirement_type' => $requirement_type->id])}}" class="btn">Kembali</a>
                                    @else
                                    <a href="{{route('administrator.requirement-types.index')}}" class="btn">Kembali</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('css')
@endsection

@section('js')
@endsection

