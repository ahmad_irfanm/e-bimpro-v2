@extends('layouts.master')

@section('title')
    Pengaturan Form Biodata {{$requirementType->name}}
@endsection

@section('requirement-type')
    active
@endsection

@section('menu')
    <i class="fa fa-user-circle"></i> Pengaturan Form Biodata
@endsection

@section('breadcumb')
    <li class="breadcrumb-item"><a href="{{route('administrator.dashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item active"><a href="{{route('administrator.requirement-types.index')}}">List Tipe Persayaratan</a></li>
    <li class="breadcrumb-item active"><a href="{{route('administrator.requirement-forms.index', ['requirement_type' => $requirementType->id])}}">{{$requirementType->name}}</a></li>
    <li class="breadcrumb-item active">Pengaturan Form Biodata</li>
@endsection

@section('content')
    <div class="container-fluid">
        @if(session('msg'))
            <div class="alert alert-success">
                {{session('msg')}}
            </div>
        @endif

        <div class="jumbotron d-flex align-items-center justify-content-center">
            <img src="{{asset('assets/img/illustration-13.svg')}}" class="illustration mr-5" alt="">
            <div class="col-md-5">
                <h4 class="mb-3">Atur biodata <span class="text-info">{{$requirementType->name}}</span></h4>
                <p style="margin-bottom: 0px;" class="text-muted">
                    Kamu dapat mengatur urutan form biodata disebelah kiri dengan menekan lalu menahan list biodata, dan arahkan ke posisi yang diinginkan. Kemudian kamu dapat menyeleksi Form biodata yang ingin ditambahkan dengan menceklis data biodata yang ada disebelah kanan.
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3">
                <div class="card card-default">
                    <div class="card-header">
                        <h3 class="card-title">
                            Urutan Form Biodata
                        </h3>
                    </div>
                    <div class="card-body">
                        <ul class="list-group" id="sortable">
                            @foreach($requirementType->biodatas()->orderBy('requirement_type_biodata.order', 'asc')->get() as $i => $biodata)
                            <li class="list-group-item" data-id="{{$biodata->id}}" style="cursor:grab;">
                                <a href="" class="mr-2 text-muted"><i class="fa fa-arrows-alt-v"></i></a>
                                {{$biodata->name}}
                                <span style="float: right;"></span>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="card-footer d-flex text-info">
                        <i class="fa fa-info-circle mr-2"></i> Klik dan seret list form biodata dan pindahkan sesuai dengan diinginkan
                    </div>
                </div>
            </div>
            <div class="col-lg-9">
                <div class="card card-default">
                    <div class="card-header d-flex align-items-center justify-content-between">
                        <h3 class="card-title">
                            Seleksi Form Biodata
                        </h3>
                        <a href="{{route('administrator.biodatas.create')}}" class="text-right">Tambah form biodata baru <i class="fa fa-arrow-right ml-2"></i></a>
                    </div>
                    <div class="card-body">
                        @foreach(\App\Biodata::orderBy('name', 'asc')->get()->chunk(3) as $biodatas)
                            <div class="row">
                                @foreach($biodatas as $biodata)
                                    <div class="col-md-4 form-group clearfix">
                                        <div class="icheck-primary d-inline">
                                            <input type="checkbox" id="checkbox-{{$biodata->id}}" class="biodata" value="{{$biodata->id}}" {{$biodata->requirement_types()->whereRequirementTypeId($requirementType->id)->first() ? 'checked' : ''}}>
                                            <label for="checkbox-{{$biodata->id}}">
                                            </label>
                                        </div>
                                        {{$biodata->name}}
                                    </div>
                                @endforeach
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('css')
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="{{asset('assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
@endsection

@section('js')

    <!-- page script -->
    <script>
        $( function() {
            let token = $('meta[name="csrf-token"]').attr('content');

            $( "#sortable" ).sortable({
                cursor: 'grabbing',
                beforeStop: function (event, ui) {
                    let list = Array.from($('.list-group-item')).map(function (item) {
                        return item.getAttribute('data-id');
                    }).filter(item => item != null);

                    $.ajax({
                        url: '{{route('administrator.requirement-types.updateOrderBiodata', ['requirement_type' => $requirementType->id])}}',
                        method: 'post',
                        data: {_method: 'put', _token: token, list},
                        success: function (response) {
                        },
                        error: function (errors) {
                            alert('error');
                        }
                    })
                }
            });
            $( "#sortable" ).disableSelection();

            $('.biodata').click(function () {
                let url, data = {
                    _token: token,
                    biodata_id: $(this).val()
                };

                if ($(this).is(':checked')) {
                    url = '{{route('administrator.requirement-types.addBiodata', ['requirement_type' => $requirementType->id])}}';
                } else {
                    url = '{{route('administrator.requirement-types.removeBiodata', ['requirement_type' => $requirementType->id])}}';
                    data._method = "DELETE";
                }

                $.ajax({
                    url,
                    method: 'post',
                    data,
                    success: function (response) {
                        if (response.method == "add") {
                            let template = `
                                <li class="list-group-item" data-id="${response.data.id}" style="cursor:grab;">
                                            <a href="" class="mr-2 text-muted"><i class="fa fa-arrows-alt-v"></i></a>
                                            ${response.data.name}
                                    <span style="float: right;"></span>
                                </li>
                            `;

                            $('#sortable').append(template);
                        } else {
                            $(`.list-group-item[data-id="${data.biodata_id}"]`).remove();
                        }
                    },
                    error: function (errors) {
                        alert('error');
                    }
                })
            })
        } );
    </script>
@endsection

