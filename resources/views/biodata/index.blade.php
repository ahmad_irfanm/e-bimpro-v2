@extends('layouts.master')

@section('title')
    List Form Biodata
@endsection

@section('biodatas')
    active
@endsection

@section('breadcumb')
    <li class="breadcrumb-item"><a href="{{route('administrator.dashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item active">Form Biodata</li>
@endsection

@section('menu')
    <i class="fa fa-id-card"></i> Form Biodata
@endsection

@section('content')
    <div class="container-fluid">
        <div class="jumbotron d-flex align-items-center justify-content-center">
            <img src="{{asset('assets/img/ill-list.svg')}}" class="illustration mr-5" alt="">
            <div class="col-md-5">
                <h4 class="mb-3">List Form Biodata</h4>
                <p style="margin-bottom: 0px;" class="text-muted">
                    Dibawah kamu dapat melihat list form biodata. Kamu dapat menambah, mengedit, menghapus, serta melihat detail dari form biodata.
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header align-items-center justify-content-between" style="display: grid; grid-template-columns: repeat(2, auto)">
                        <h3 class="card-title">List Form Biodata</h3>
                        <a href="{{ route('administrator.biodatas.create') }}" class="text-bold btn text-primary" style="background: #f5f6fb;"><i class="fa fa-plus-circle mr-1"></i> Tambah Form Biodata</a>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example1" class="table table-striped">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Form</th>
                                <th>Placeholder</th>
                                <th class="text-center">Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($biodatas as $i => $biodata)
                                    <tr>
                                        <td>{{$i+1}}</td>
                                        <td>{{$biodata->name}}</td>
                                        <td>{{$biodata->placeholder}}</td>
                                        <td class="text-center">
                                            <a href="{{route('administrator.biodatas.destroy', ['biodata' => $biodata->id])}}" class="text-danger mr-3 btn-delete" title="Delete" data-toggle="tooltip">
                                                <i class="fa fa-trash mr-1"></i> Hapus
                                            </a>
                                            <a href="{{route('administrator.biodatas.edit', ['biodata' => $biodata->id])}}" class="text-warning mr-3" title="Edit" data-toggle="tooltip">
                                                <i class="fa fa-edit mr-1"></i> Edit
                                            </a>
                                            <a href="{{route('administrator.biodatas.show', ['biodata' => $biodata->id])}}" class="btn btn-sm btn-default" title="List formulir" data-toggle="tooltip">
                                                <i class="fa fa-check-square mr-1"></i> Seleksi persyaratan
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>
@endsection

@section('css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.bootstrap4.min.css">
@endsection

@section('js')
    <!-- DataTables -->
    <script src="{{asset('assets/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>
    <!-- page script -->
    <script>
        $(function () {
            $("#example1").DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
            });
        });
    </script>
@endsection

