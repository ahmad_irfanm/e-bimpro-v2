@php
    $state = isset($biodata);
    $theme = $state ? 'warning' : 'primary';
@endphp

@extends('layouts.master')

@section('biodatas')
    active
@endsection

@section('title')
    {{($state ? 'Edit' : 'Tambah') . " Form Biodata"}}
@endsection

@section('menu')
    <i class="fa fa-id-card"></i> Form Biodata
@endsection

@section('breadcumb')
    <li class="breadcrumb-item"><a href="{{route('administrator.dashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="{{route('administrator.biodatas.index')}}">List Form Biodata</a></li>
    <li class="breadcrumb-item active">Tambah Persayaratan</li>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="jumbotron">
            <div class="jumbotron d-flex align-items-center justify-content-center">
                <img src="{{asset('assets/img/illustration-3.svg')}}" class="illustration mr-5" alt="">
                <div class="col-md-5">
                    <h4 class="mb-3">{{($state ? 'Edit' : 'Tambah') . " Form Biodata"}}</h4>
                    <p style="margin-bottom: 0px;" class="text-muted">
                        Isi formulir dibawah sesuai dengan form biodata. Kemudian seleksi tipe persyaratan yang akan menggunakan form biodata ini. Kemudian tekan tombol <span class="text-info">Simpan</span> untuk menyimpan data Form biodata.
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-{{$theme}}">
                    <div class="card-header">
                        <h3 class="card-title">Form {{ $state ? 'edit form biodata' : 'form biodata baru' }}</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form class="form-horizontal" action="{{route('administrator.biodatas.' . ($state ? 'update' : 'store'), $state ? ['biodata' => $biodata->id] : [])}}" method="post" enctype="multipart/form-data">
                        @csrf
                        @if($state)
                            @method('put')
                        @endif
                        <div class="card-body">

                            @if(session('msg'))
                                <div class="alert alert-success">
                                    {{session('msg')}}
                                </div>
                            @elseif(session('msgError'))
                                <div class="alert alert-danger">
                                    {{session('msgError')}}
                                </div>
                            @endif

                            <div class="form-group row  align-items-center">
                                <label for="inputEmail3" class="col-sm-2 col-form-label">Nama Field</label>
                                <div class="col-sm-10">
                                    <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="inputEmail3" placeholder="Nama Form" value="{{old('name') ? old('name') : ($state ? $biodata->name : '')}}" autofocus>
                                    @if($errors->first('name'))
                                    <div class="invalid-feedback">
                                        {{$errors->first('name')}}
                                    </div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row d-flex align-items-center">
                                <label for="inputEmail3" class="col-sm-2 col-form-label">Keterangan atau Placeholder</label>
                                <div class="col-sm-10">
                                    <input type="text" name="placeholder" class="form-control @error('placeholder') is-invalid @enderror" id="inputEmail3" placeholder="Keterangan/Placeholder" value="{{old('placeholder') ? old('placeholder') : ($state ? $biodata->placeholder : '')}}">
                                    @if($errors->first('placeholder'))
                                        <div class="invalid-feedback">
                                            {{$errors->first('placeholder')}}
                                        </div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row d-flex align-items-center">
                                <label for="inputEmail3" class="col-sm-2 col-form-label">Tipe form</label>
                                <div class="col-sm-10">
                                    <select name="type" id="type" class="form-control @error('type') is-invalid @enderror">
                                        <option value="text" {{old('type') ? old('type') == "text" ? 'selected' : '' : ($state ? $biodata->type == 'text' ? 'selected' : '' : 'selected')}}>Teks</option>
                                        <option value="textarea {{old('type') ? old('type') == "textarea" ? 'selected' : '' : ($state ? $biodata->type == 'textarea' ? 'selected' : '' : '')}}">Teks Area</option>
                                        <option value="numeric" {{old('type') ? old('type') == "numeric" ? 'selected' : '' : ($state ? $biodata->type == 'numeric' ? 'selected' : '' : '')}}>Numerik</option>
                                        <option value="radio" {{old('type') ? old('type') == "radio" ? 'selected' : '' : ($state ? $biodata->type == 'radio' ? 'selected' : '' : '')}}>Pilihan</option>
                                        <option value="date" {{old('type') ? old('type') == "date" ? 'selected' : '' : ($state ? $biodata->type == 'date' ? 'selected' : '' : '')}}>Tanggal</option>
                                    </select>
                                    @if($errors->first('type'))
                                        <div class="invalid-feedback">
                                            {{$errors->first('type')}}
                                        </div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row" id="choices-wrap">
                                <label for="inputEmail3" class="col-sm-2 col-form-label">Pilihan</label>
                                <div class="col-sm-10">
                                    <textarea name="choices" id="choices" cols="30" rows="10"  class="form-control mb-2 @error('placeholder') is-invalid @enderror" id="inputEmail3" placeholder="Ketik pilihan dan enter untuk menambah pilihan baru" value="{{old('choices') ? old('choices') : ($state ? $biodata->choices : '')}}"></textarea>
                                    <h6>List Pilihan :</h6>
                                    <div id="choices-pre" class="text-sm">

                                    </div>
                                    @if($errors->first('choices'))
                                        <div class="invalid-feedback">
                                            {{$errors->first('choices')}}
                                        </div>
                                    @endif
                                </div>
                            </div>

                            @if(!$state)
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-2 col-form-label">Terapkan untuk tipe persyaratan</label>
                                <div class="col-sm-10">
                                    <div class="form-group clearfix">
                                        <div class="icheck-primary d-inline">
                                            <input type="checkbox" id="checkboxall" {{ $state ? $biodata->requirement_types()->count() == \App\RequirementType::count() ? 'checked' : '' : ''}}>
                                            <label for="checkboxall">
                                            </label>
                                        </div>
                                        Semua
                                    </div>
                                    @foreach(\App\RequirementType::orderBy('name', 'asc')->get()->chunk(3) as $requirementTypes)
                                        <div class="row">
                                            @foreach($requirementTypes as $requirementType)
                                                    <div class="form-group col-md-4 clearfix">
                                                        <div class="icheck-primary d-inline">
                                                            <input type="checkbox" id="checkbox-{{$requirementType->id}}" class="requirement-type-biodata" value="{{$requirementType->id}}" name="requirement_type_biodata[]"
                                                           {{ old('requirement_type_biodata') ? in_array($requirementType->id, old('requirement_type_biodata')) ? 'checked' : '' : $state ? $requirementType->biodatas()->whereRequirementTypeId($requirementType->id)->first() ? 'checked' : '' : ''}}
                                                            >
                                                            <label for="checkbox-{{$requirementType->id}}">
                                                            </label>
                                                        </div>
                                                        {{$requirementType->name}}
                                                    </div>
                                            @endforeach
                                        </div>
                                    @endforeach

                                    @if($errors->first('requirement_type_biodatas'))
                                        <div class="invalid-feedback">
                                            {{$errors->first('requirement_type_biodatas')}}
                                        </div>
                                    @endif
                                </div>
                            </div>
                            @endif

                            <div class="form-group row form-submit">
                                <div class="col-md-2"></div>
                                <div class="col-md-10">
                                    <button type="submit" class="btn btn-{{$theme}} mr-2"><i class="fa fa-save mr-2"></i> Simpan</button>
                                    <a href="{{route('administrator.biodatas.index')}}" class="btn">Kembali</a>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('css')
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="{{asset('assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
@endsection

@section('js')
    <!-- Select2 -->
    <script src="{{asset('assets/plugins/select2/js/select2.full.min.js')}}"></script>
    <script>
        //Initialize Select2 Elements
        $('.select2bs4').select2({
            theme: 'bootstrap4'
        })

        $(document).ready(function () {
            $('#choices-wrap').hide();
            $('#choices').on('input', function () {
                let choices = $(this).val().toString().split('\n');
                let preview = "";
                choices.forEach((choice, i) => {
                    preview += (i+1) + '. ' + choice + "<br>";
                });
                $('#choices-pre').html(preview);
            })

            $('#type').on('change', function () {
                if ($(this).val() == "radio") {
                    $('#choices-wrap').show();
                } else {
                    $('#choices-wrap').hide();
                }
            })

            $("#checkboxall").click( function(){
                if( $(this).is(':checked') ) {
                    $(".requirement-type-biodata").attr('checked', 'checked');
                } else {
                    $(".requirement-type-biodata").click();
                }
            });

            $('.requirement-type-biodata').click(function () {
                if( !$(this).is(':checked') ) {
                    $("#checkboxall").removeAttr('checked');
                }
            })
        })
    </script>
@endsection

