@extends('layouts.master')

@section('title')
    Seleksi Form Biodata {{$biodata->name}}
@endsection

@section('biodatas')
    active
@endsection

@section('menu')
    <i class="fa fa-id-card mr-2"></i> Form Biodata <span class="text-info">{{$biodata->name}}</span>
@endsection

@section('breadcumb')
    <li class="breadcrumb-item"><a href="{{route('administrator.dashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item active"><a href="{{route('administrator.biodatas.index')}}">Form Biodata</a></li>
    <li class="breadcrumb-item active">{{$biodata->name}}</li>
@endsection

@section('content')
    <div class="container-fluid">
        @if(session('msg'))
            <div class="alert alert-success">
                {{session('msg')}}
            </div>
        @endif

        <div class="jumbotron d-flex align-items-center justify-content-center">
            <img src="{{asset('assets/img/illustration-13.svg')}}" class="illustration mr-5" alt="">
            <div class="col-md-5">
                <h4 class="mb-3">Form biodata <span class="text-info">{{$biodata->name}}</span></h4>
                <p style="margin-bottom: 0px;" class="text-muted">
                    Seleksi Tipe persyaratan yang akan menggunakan biodata <span class="text-info">{{$biodata->name}}</span> dibawah sesuai dengan apa yang kamu inginkan.
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-default">
                    <div class="card-header">
                        <h3 class="card-title">
                            Seleksi Tipe Persyaratan
                        </h3>
                    </div>
                    <div class="card-body">
                        @foreach(\App\RequirementType::get()->chunk(3) as $requirementTypes)
                            <div class="row">
                                @foreach($requirementTypes as $requirementType)
                                    <div class="col-md-4 form-group clearfix">
                                        <div class="icheck-primary d-inline">
                                            <input type="checkbox" id="checkbox-{{$requirementType->id}}" class="biodata" value="{{$requirementType->id}}" {{$requirementType->biodatas()->whereBiodataId($biodata->id)->first() ? 'checked' : ''}}>
                                            <label for="checkbox-{{$requirementType->id}}">
                                            </label>
                                        </div>
                                        {{$requirementType->name}}
                                    </div>
                                @endforeach
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('css')
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="{{asset('assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
@endsection

@section('js')

    <script>
        $( function() {
            let token = $('meta[name="csrf-token"]').attr('content');

            $('.biodata').click(function () {
                let url, data = {
                    _token: token,
                    requirement_type_id: $(this).val()
                };

                if ($(this).is(':checked')) {
                    url = '{{route('administrator.biodatas.addRequirementType', ['biodata' => $biodata->id])}}';
                } else {
                    url = '{{route('administrator.biodatas.removeRequirementType', ['biodata' => $biodata->id])}}';
                    data._method = "DELETE";
                }

                $.ajax({
                    url,
                    method: 'post',
                    data,
                    success: function (response) {
                        console.log(response);
                    },
                    error: function (errors) {
                        alert('error');
                    }
                })
            })
        } );
    </script>
    <!-- page script -->
@endsection

