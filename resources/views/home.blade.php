@if(shownField('study_old', $requirementType->abbr))
    <div class="form-group row d-flex align-items-center">
        <label for="inputEmail3" class="col-sm-2 col-form-label">{{label('study_old', $requirementType->abbr)}}</label>
        <div class="col-sm-10">
            <input type="text" name="study_old" class="form-control @error('study_old') is-invalid @enderror" id="inputEmail3" placeholder="{{placeholder('study_old', $requirementType->abbr)}}" value="{{old('study_old') ? old('study_old') : ($state ? $biodata->study_old : '')}}">
            @if($errors->first('study_old'))
                <div class="invalid-feedback">
                    {{$errors->first('study_old')}}
                </div>
            @endif
        </div>
    </div>
@endif
