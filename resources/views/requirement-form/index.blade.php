@extends('layouts.master')

@section('title')
    List Form Persyaratan {{$requirementType->name}}
@endsection

@section('requirement-type')
    active
@endsection

@section('menu')
    <i class="fa fa-clipboard-list"></i> Form Persyaratan
@endsection

@section('breadcumb')
    <li class="breadcrumb-item"><a href="{{route('administrator.dashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item active"><a href="{{route('administrator.requirement-types.index')}}">List Tipe Persayaratan</a></li>
    <li class="breadcrumb-item active">{{$requirementType->name}}</li>
@endsection

@section('content')
    <div class="container-fluid">
        @if(session('msg'))
            <div class="alert alert-success">
                {{session('msg')}}
            </div>
        @endif

        <div class="jumbotron d-flex align-items-center justify-content-center">
            <img src="{{asset('assets/img/illustration-8.svg')}}" class="illustration mr-5" alt="">
            <div class="col-md-5">
                <h4 class="mb-3">
                    Form persyaratan <span class="text-info">{{$requirementType->name}}</span>
                </h4>
                <p style="margin-bottom: 0px;" class="text-muted">
                    Kamu dapat melihat detail tipe persyaratan disebelah dikiri, dan list form persyaratan disebelah kanan. Kamu dapat mengatur form biodata, mengedit tipe persyaratan, menghapus tipe persyaratan, membuat, mengedit, dan menghapus form persyaratan.
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            Detail Tipe Persyaratan
                        </h3>
                    </div>
                    <div class="card-body">

                            <h6 class="mb-3">Urutan biodata</h6>

                            <ul class="list-group">
                                @foreach($requirementType->biodatas as $bidoata)
                                <li class="list-group-item">
                                    {{$bidoata->name}}
                                </li>
                                @endforeach
                            </ul>

                            <hr>

                            <a href="{{route('administrator.requirement-types.show', ['requirement_type' => $requirementType->id])}}" class="btn text-info mb-3 btn-block"><i class="fa fa-id-card"></i> Atur form biodata</a>

                        <hr class="mr-2">

                            <div class="col-md-12">
                                <a href="{{route('administrator.requirement-types.edit', ['requirement_type'=>$requirementType->id])}}" class="btn text-warning btn-block"><i class="fa fa-edit"></i> Edit</a>
                            </div>

                             <hr>

                            <div class="col-md-12">
                                <a href="{{route('administrator.requirement-types.destroy', ['requirement_type'=>$requirementType->id])}}" class="btn text-danger btn-block btn-delete"><i class="fa fa-trash"></i> Hapus</a>
                            </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-9">
                <div class="card">
                    <div class="card-header align-items-center justify-content-between" style="display: grid; grid-template-columns: repeat(2, auto)">
                        <h3 class="card-title">List Form Persyaratan <b>{{$requirementType->name}}</b> </h3>
                        <div>
                            <a href="{{ route('administrator.requirement-forms.create', ['requirement_type' => $requirementType->id]) }}" class="btn text-bg text-primary text-bold"><i class="fa fa-plus-circle mr-1"></i> Tambah Form Persyaratan</a>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example1" class="table table-striped">
                            <thead>
                            <tr>
                                <th>Nama Form Persyaratan</th>
                                <th>Deskripsi</th>
                                <th class="text-center">Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($requirementType->requirement_forms as $rf)
                                    <tr>
                                        <td>{{$rf->name}}</td>
                                        <td>{{$rf->description ? $rf->description : '-'}}</td>
                                        <td class="text-center">
                                            <a href="{{route('administrator.requirement-forms.destroy', ['requirement_type' => $requirementType->id, 'requirement_form' => $rf->id])}}" class="btn btn-xs text-bg text-danger mr-2 btn-delete" title="Delete">
                                                <i class="fa fa-trash mr-1"></i> Hapus
                                            </a>
                                            <a href="{{route('administrator.requirement-forms.edit', ['requirement_type' => $requirementType->id, 'requirement_form' => $rf->id])}}" class="btn text-warning btn-xs" title="Edit">
                                                <i class="fa fa-edit mr-1"></i> Edit
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Nama Form Persyaratan</th>
                                <th>Deskripsi</th>
                                <th class="text-center">Aksi</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>
@endsection

@section('css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.bootstrap4.min.css">
@endsection

@section('js')
    <!-- DataTables -->
    <script src="{{asset('assets/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>
    <!-- page script -->
    <script>
        $(function () {
            $("#example1").DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
            });
        });
    </script>
@endsection

