@php
    $state = isset($requirementForm);
    $theme = $state ? 'warning' : 'primary';
@endphp

@extends('layouts.master')

@section('requirement-type')
    active
@endsection

@section('title')
    Tambah Form Persyaratan
@endsection

@section('menu')
    <i class="fa fa-clipboard-list"></i> Form Persyaratan
@endsection

@section('breadcumb')
    <li class="breadcrumb-item"><a href="{{route('administrator.dashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="{{route('administrator.requirement-types.index')}}">List Tipe Persayaratan</a></li>
    <li class="breadcrumb-item"><a href="{{route('administrator.requirement-forms.index', ['requirement_type' => $requirementType->id])}}">List Form Persyaratan {{$requirementType->name}}</a></li>
    <li class="breadcrumb-item active">Tambah Form Persyaratan</li>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="jumbotron d-flex align-items-center justify-content-center">
            <img src="{{asset('assets/img/illustration-3.svg')}}" class="illustration mr-5" alt="">
            <div class="col-md-5">
                <h4 class="mb-3">{{($state ? 'Edit' : 'Tambah') . ' form persyaratan'}} <span class="text-info">{{$requirementType->name}}</span></h4>
                <p style="margin-bottom: 0px;" class="text-muted">
                    Isi formulir dibawah sesuai dengan form form persyaratan. Kemudian tekan tombol <span class="text-info">Simpan</span> untuk menyimpan data Form persyaratan.
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-{{$theme}}">
                    <div class="card-header">
                        <h3 class="card-title">Form {{ $state ? 'edit form persyaratan' : 'form persyaratan baru' }}</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form class="form-horizontal" action="{{route('administrator.requirement-forms.' . ($state ? 'update' : 'store'), $state ? ['requirement_type' => $requirementType->id, 'requirement_form' => $requirementForm->id] : ['requirement_type' => $requirementType->id])}}" method="post">
                        @csrf
                        @if($state)
                            @method('put')
                        @endif
                        <div class="card-body">

                            @if(session('msg'))
                                <div class="alert alert-success">
                                    {{session('msg')}}
                                </div>
                            @elseif(session('msgError'))
                                <div class="alert alert-danger">
                                    {{session('msgError')}}
                                </div>
                            @endif

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-2 col-form-label">Nama Form</label>
                                <div class="col-sm-10">
                                    <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="inputEmail3" placeholder="Nama form" value="{{old('name') ? old('name') : ($state ? $requirementForm->name : '')}}" autofocus>
                                    @if($errors->first('name'))
                                    <div class="invalid-feedback">
                                        {{$errors->first('name')}}
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputPassword3" class="col-sm-2 col-form-label">Deskripsi Form</label>
                                <div class="col-sm-10">
                                    <textarea name="description" class="form-control" id="inputPassword3" cols="30" rows="10" placeholder="Deskripsi form">{{old('description') ? old('description') : ($state ? $requirementForm->description : '') }}</textarea>
                                    <small id="passwordHelpBlock" class="form-text text-muted">
                                        Deskripsi bersifat optional. Anda dapat mengisi deskripsi untuk menjelaskan lebih detail tentang form.
                                    </small>
                                </div>
                            </div>
                            <div class="form-group row form-submit">
                                <div class="col-md-2"></div>
                                <div class="col-md-10">
                                    <button type="submit" class="btn btn-{{$theme}} mr-2"><i class="fa fa-save mr-2"></i> Simpan</button>
                                    <a href="{{route('administrator.requirement-forms.index', ['requirement_type' => $requirementType])}}" class="btn">Kembali</a>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('css')
@endsection

@section('js')
@endsection

