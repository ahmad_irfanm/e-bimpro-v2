@extends('layouts.agency')

@section('dashboard')
    active
@endsection

@section('title')
    Dashboard
@endsection

@section('header')
    Dashboard
@endsection

@section('breadcumb')
    <li class="breadcrumb-item active">Dashboard</li>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
                <div class="jumbotron d-flex align-items-center justify-content-center" style="background: #ffe7c3;">
                    <img src="{{asset('assets/img/illustration-10.svg')}}" class="illustration mr-5" alt="">
                    <div class="col-md-6">
                        <h5 class="mb-3"><span style="font-weight: normal">Halo, </span> {{Auth::user()->agency->name}}</h5>
                        <p class="mb-4" style="margin-bottom: 0px;">
                            Selamat datang di aplikasi e-Bimpro. Kamu dapat memulai dengan membuat biodata berdasarkan menu persyaratan yang tersedia. Setelah itu kamu dapat mengupload file perysaratan dan mengirimnya untuk dapat diverifikasi.
                        </p>
                        <div class="row">
                            <div class="col-sm-6">

                                <a href="{{route('agency.guide')}}" class="btn text-bg">
                                    Panduan Aplikasi <i class="fa fa-arrow-right ml-2"></i>
                                </a>

                            </div>
                        </div>
                    </div>
                </div>
            </div>



            <div class="col-md-5">
                <div class="card card-default">
                    <div class="card-header">
                        <h4 class="card-title">
                            Aktifitas
                        </h4>
                    </div>
                    <div class="card-body">

                        <canvas id="chart-pie"  width="400" height="400"></canvas>

                    </div>
                </div>

                <!-- About Me Box -->
                <div class="card card-default mt-4">
                    <div class="card-header d-flex align-items-center justify-content-between">
                        <h4 class="card-title">Tentang Saya</h4>
                        <a href="{{route('agency.profile')}}">Lihat profile <i class="fa fa-arrow-right ml-2 text-bold"></i></a>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <strong><i class="fas fa-user-circle mr-1"></i> Username</strong>

                        <p class="text-muted">
                            {{Auth::user()->username}}
                        </p>

                        <hr>

                        <strong><i class="fas fa-user-tie mr-1"></i> Wewenang</strong>

                        <p class="text-muted">{{convertRole(Auth::user()->role)}}</p>

                        <hr>

                        <strong><i class="fas fa-calendar mr-1"></i> Dibuat pada</strong>

                        <p class="text-muted">
                            {{Auth::user()->created_at->diffForHumans()}}
                        </p>

                    </div>
                    <!-- /.card-body -->
                </div>

                <div class="card card-default mt-4">
                    <div class="card-header">
                        <h4 class="card-title">
                            Grafik
                        </h4>
                    </div>
                    <div class="card-body">

                        <canvas id="chart-line"  style="width: 100%;"></canvas>

                    </div>
                </div>


            </div>

            <div class="col-md-7">

                <div class="row">

                    <div class="col-md-12">
                        <h5 class="mb-3" style="display: block;">Perhitungan data</h5>
                    </div>

                    <div class="col-md-12 col-sm-12 col-12">
                        <div class="info-box bg-info">
                            <span class="info-box-icon"><i class="fa fa-users"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text mb-2">Semua Persyaratan</span>
                                <h3 class="info-box-number">{{\App\Requirement::where('agency_id', Auth::user()->agency->id)->whereIn('status', ['2', '3', '4'])->count()}}</h3>

                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>



                    <div class="col-md-12 mb-4">
                        <h5 class="mb-3 mt-3">Draft</h5>

                        @php
                            $requirements = \App\Requirement::where('agency_id', Auth::user()->agency_id)->whereIn('status', [1])->orderBy('updated_at', 'desc')->skip(0)->take(3)->get();
                        @endphp
                        @foreach($requirements as $requirement)
                            <div class="card card-default">
                                <div class="card-body" style="padding-bottom: 0px;">
                                    <div class="row align-items-center justify-content-between">
                                        <div class="col-md-6">
                                            <h6 class="mb-2" style="white-space: nowrap; overflow: hidden;">{{$requirement->requirement_type->name}}</h6>
                                        </div>
                                        <div class="col-md-6 text-right">
                                            <p class="text-muted text-sm">
                                                <span class="mr-2">
                                                    <i class="fa fa-calendar mr-1"></i> {{getDatetime($requirement->status, $requirement)}}
                                                </span>
                                                <span>
                                                    <i class="fa fa-clock mr-1"></i> {{getOnlyTime($requirement->status, $requirement)}}
                                                </span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body row align-items-center">
                                    <div class="col-md-4">
                                        <div class="d-flex align-items-center">
                                            <div class="mr-2">
                                                {!! $requirement->getAvatar() !!}
                                            </div>
                                            <div style="white-space: nowrap; overflow: hidden">
                                                {{$requirement->getContent(getMainFirst()->id)}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="progress" style="height: 25px;">
                                            <div class="progress-bar progress-bar-striped progress-bar-animated bg-{{ $requirement->requirement_posts->count() / $requirement->requirement_type->requirement_forms->count() * 100 == 100 ? 'primary' : 'warning'}}" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" style="width: {{ $requirement->requirement_posts->count() / $requirement->requirement_type->requirement_forms->count() * 100}}%">

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 text-right">
                                        <a class="text-orange" href="{{route('agency.requirements.show', ['requirement_type' => $requirement->requirement_type_id, 'requirement' => $requirement->id])}}">
                                            Lihat persyaratan <i class="fa fa-arrow-right ml-2"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        @if($requirements->count() == 0)
                        <p class="text-muted">Tidak ada draft untuk ditampilkan.</p>
                        @endif
                    </div>

                    <div class="col-md-12">
                        <h5 class="mb-3">Terbaru</h5>

                        @php
                            $requirements = \App\Requirement::where('agency_id', Auth::user()->agency_id)->whereIn('status', [2, 3, 4])->orderBy('updated_at', 'desc')->skip(0)->take(3)->get();
                        @endphp
                        @foreach($requirements as $requirement)
                            <div class="card card-default">
                                <div class="card-body" style="padding-bottom: 0px;">
                                    <div class="row align-items-center justify-content-between">
                                        <div class="col-md-6" style="white-space: nowrap; overflow: hidden;">
                                            <h6 class="mb-2">{{$requirement->requirement_type->name}}</h6>
                                        </div>
                                        <div class="col-md-6 text-right">
                                            <p class="text-muted text-sm">
                                                <span class="mr-2">
                                                    <i class="fa fa-calendar mr-1"></i> {{getDatetime($requirement->status, $requirement)}}
                                                </span>
                                                <span>
                                                    <i class="fa fa-clock mr-1"></i> {{getOnlyTime($requirement->status, $requirement)}}
                                                </span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body row align-items-center">
                                    <div class="col-md-4">
                                        <div class="d-flex align-items-center">
                                            <div class="mr-2">
                                                {!! $requirement->getAvatar() !!}
                                            </div>
                                            <div style="white-space: nowrap; overflow: hidden">
                                                {{$requirement->getContent(getMainFirst()->id)}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <span style="padding: 8px; font-size: 14px; font-weight: normal;" class="badge d-block badge-{{convertColor($requirement->status)}}">
                                            <i class="fa fa-{{convertIcon($requirement->status)}} mr-1"></i> {{convertStatus($requirement->status)}}
                                        </span>
                                    </div>
                                    <div class="col-md-2 text-center">
                                        <span class="text-muted ml-2">
                                        </span>
                                    </div>
                                    <div class="col-md-3 text-right">
                                        <a class="text-orange" href="{{route('agency.requirements.show', ['requirement_type' => $requirement->requirement_type_id, 'requirement' => $requirement->id])}}">
                                            Lihat persyaratan <i class="fa fa-arrow-right ml-2"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        @if($requirements->count() == 0)
                            <p class="text-muted">Tidak ada draft untuk ditampilkan.</p>
                        @endif
                    </div>

                </div>


            </div>

        </div>
    </div>
@endsection

@section('css')
    <style>
    </style>
@endsection

@section('js')
    <!-- ChartJS -->
    <script src="{{asset('assets/plugins/chart.js/Chart.min.js')}}"></script>
    <script>
        var ctx = document.getElementById('chart-pie').getContext('2d');

        var data = {
            datasets: [{
                data: {{$pie}},
                backgroundColor: ['#ffc107', '#17a2b8', '#007bff', '#28a745']
            }],

            // These labels appear in the legend and in the tooltips when hovering different arcs
            labels: [
                'Draft',
                'Terkirim',
                'Terverifikasi ke-1',
                'Selesai'
            ]
        };

        var myPieChart = new Chart(ctx, {
            type: 'pie',
            data: data,
            options: {
                legend: {
                    position: 'top'
                }
            }
        });

        ctx = document.getElementById('chart-line').getContext('2d');

        data = {
            datasets: [
                {
                    label: 'Membuat Biodata',
                    data: {!! $line !!} ,
                    backgroundColor: 'transparent',
                    borderColor: '#ffc107',
                    borderWidth: 5
                }
            ],

            labels: {!! $labels !!}
        };

        var myLineChart = new Chart(ctx, {
            type: 'line',
            data: data
        });
    </script>
@endsection

