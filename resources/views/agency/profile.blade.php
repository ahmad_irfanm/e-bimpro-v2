@extends('layouts.agency')

@section('profile')
    active
@endsection

@section('title')
    Profile
@endsection

@section('header')
    Profile
@endsection

@section('menu')
    <i class="fa fa-user-circle"></i> Profile
@endsection

@section('breadcumb')
    <li class="breadcrumb-item"><a href="{{route('agency.dashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item active">Profile</li>
@endsection

@section('content')
    <div class="container-fluid">
        @if(session('msg'))
            <div class="alert alert-success">
                {{session('msg')}}
            </div>
        @elseif(session('msgError'))
            <div class="alert alert-danger">
                {{session('msgError')}}
            </div>
        @elseif($errors->any())
            <div class="alert alert-danger">
                Opps.. Ada yang error! <br>
                @foreach($errors->all() as $error)
                    {{$error}} <br>
                @endforeach
            </div>
        @endif

        <div class="jumbotron d-flex align-items-center justify-content-center">
            <img src="{{asset('assets/img/illustration-13.svg')}}" class="illustration mr-5" alt="">
            <div class="col-md-5">
                <h4 class="mb-3">Profile Saya</h4>
                <p style="margin-bottom: 0px;" class="text-muted">
                    Kamu dapat mengubah data diri, mengganti password, serta melihat timeline proses dari biodata yang kamu buat.
                </p>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3">


                <!-- Profile Image -->
                <div class="card card-primary card-outline">
                    <div class="card-body box-profile">
                        <div class="text-center">
                            <img class="profile-user-img img-fluid img-circle"
                                 src="{{asset('assets/dist/img/man-1.png')}}"
                                 alt="User profile picture" style="height: 100px; object-fit: cover;">
                        </div>

                        <h3 class="profile-username text-center">{{Auth::user()->agency->name}}</h3>

                        <p class="text-muted text-center">{{convertRole(Auth::user()->role)}}</p>

                        <ul class="list-group list-group-unbordered mb-3">
                            <li class="list-group-item">
                                <b>Semua Biodata</b>
                                <a class="float-right">
                                    {{Auth::user()->agency->requirements()->count()}}
                                </a>
                            </li>
                            <li class="list-group-item">
                                <b>Belum Dikirim</b>
                                <a class="float-right">
                                    {{Auth::user()->agency->requirements()->where('status', 1)->count()}}
                                </a>
                            </li>
                            <li class="list-group-item">
                                <b>Dikirim</b>
                                <a class="float-right">
                                    {{Auth::user()->agency->requirements()->where('status', 2)->count()}}
                                </a>
                            </li>
                            <li class="list-group-item">
                                <b>Diverifikasi ke-1</b>
                                <a class="float-right">
                                    {{Auth::user()->agency->requirements()->where('status', 3)->count()}}
                                </a>
                            </li>
                            <li class="list-group-item">
                                <b>Diverifikasi ke-2</b>
                                <a class="float-right">
                                    {{Auth::user()->agency->requirements()->where('status', 4)->count()}}
                                </a>
                            </li>
                        </ul>

                        <button class="btn btn-primary btn-block" data-toggle="modal" data-target="#modal-default"><b><i class="fa fa-lock mr-2"></i> Ganti Password</b></button>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->

                <!-- About Me Box -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Tentang Saya</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <strong><i class="fas fa-landmark mr-1"></i> Nama Dinas</strong>

                        <p class="text-muted">
                            {{Auth::user()->agency->name}}
                        </p>

                        <hr>

                        <strong><i class="fas fa-user-circle mr-1"></i> Username</strong>

                        <p class="text-muted">
                            {{Auth::user()->username}}
                        </p>

                        <hr>

                        <strong><i class="fas fa-envelope mr-1"></i> Email</strong>

                        <p class="text-muted">
                            {{Auth::user()->agency->email}}
                        </p>

                        <hr>

                        <strong><i class="fas fa-user-tie mr-1"></i> Wewenang</strong>

                        <p class="text-muted">{{convertRole(Auth::user()->role)}}</p>

                        <hr>

                        <strong><i class="fas fa-calendar mr-1"></i> Dibuat pada</strong>

                        <p class="text-muted">
                            {{Auth::user()->created_at->diffForHumans()}}
                        </p>

                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">
                        <ul class="nav nav-pills">
                            <li class="nav-item"><a class="nav-link active" href="#timeline" data-toggle="tab">Timeline</a></li>
                            <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">Pengaturan</a></li>
                        </ul>
                    </div><!-- /.card-header -->
                    <div class="card-body">
                        <div class="tab-content">
                            <!-- /.tab-pane -->
                            <div class="tab-pane active" id="timeline">
                                <!-- The timeline -->
                                <div class="timeline timeline-inverse">

                                    @foreach($activities as $activity)
                                        <!-- timeline time label -->
                                        <div class="time-label">
                                            <span class="bg-{{convertColor($activity->status)}}">
                                                {{convertDate($activity->datetime)}}
                                            </span>
                                            </div>
                                            <!-- /.timeline-label -->
                                            <!-- timeline item -->
                                            <div>
                                                <i class="fas fa-{{convertIcon($activity->status)}} bg-{{convertColor($activity->status)}}"></i>

                                                <div class="timeline-item">
                                                    <span class="time"><i class="far fa-clock"></i> {{getTime($activity->datetime)}}</span>

                                                    <h3 class="timeline-header">
                                                        @if($activity->status == 1)
                                                            Membuat persyaratan <span class="text-info">{{$activity->requirement->requirement_type->name}}</span>
                                                        @elseif($activity->status == 2)
                                                            Mengirimkan persyaratan <span class="text-info">{{$activity->requirement->requirement_type->name}}</span>
                                                        @elseif($activity->status == 3)
                                                            <a href="#">Admin1</a> telah memverifikasi persyaratan
                                                        @elseif($activity->status == 4)
                                                            <a href="#">Admin2</a> Telah memverifikasi persyaratan
                                                        @endif
                                                    </h3>

                                                    <div class="timeline-body row">

                                                        @foreach(collect($activity->requirement->biodata_posts)->chunk($activity->requirement->biodata_posts->count() / 2) as $chunk)
                                                            <div class="col-md-6">
                                                                <div class="row">
                                                                    @foreach($chunk as $biodata_post)
                                                                        <div class="col-md-6 text-right"><strong>{{$biodata_post->biodata->name}}</strong></div>
                                                                        <div class="col-md-6"><p class="text-muted">{{$activity->requirement->getContent($biodata_post->biodata_id)}}</p></div>
                                                                    @endforeach
                                                                </div>
                                                            </div>
                                                        @endforeach

                                                    </div>
                                                    <div class="timeline-footer d-flex align-items-center justify-content-end">
                                                        @if($activity->requirement->status == 1 || $activity->requirement->status == 2)
                                                            <a href="{{route('agency.requirements.destroy', ['requirement_type' => $activity->requirement->requirement_type->id, 'requirement' => $activity->requirement->id])}}" class="text-danger text-sm btn-delete"><i class="fa fa-trash mr-1"></i> Hapus</a>
                                                        @endif
                                                        <a href="{{route('agency.requirements.show', ['requirement_type' => $activity->requirement->requirement_type->id, 'requirement' => $activity->requirement->id])}}" class="text-info btn-sm ml-4"> Lihat biodata persyaratan <i class="fa fa-arrow-right ml-1"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                    @endforeach

                                    <!-- timeline time label -->
                                    <div class="time-label">
                                        <span class="bg-pink">
                                            {{convertDate(Auth::user()->created_at)}}
                                        </span>
                                    </div>
                                    <!-- /.timeline-label -->
                                    <!-- timeline item -->
                                    <div>
                                        <i class="fas fa-hand-point-right bg-pink"></i>

                                        <div class="timeline-item">
                                            <span class="time"><i class="far fa-clock"></i> {{getTime(Auth::user()->created_at)}}</span>

                                            <h3 class="timeline-header">
                                                Dinas <b class="text-info">{{Auth::user()->agency->name}}</b> Bergabung
                                            </h3>

                                            <div class="timeline-body">
                                                <table class="table table-striped col-md-12">
                                                    Selamat bergabung Dinas <b class="text-info">{{Auth::user()->agency->name}}</b> di aplikasi <b>{{config('app.name', 'Laravel')}}</b>
                                                </table>
                                            </div>
                                            <div class="timeline-footer">
                                                <a href="{{route('agency.dashboard')}}" class="btn btn-info bg-pink btn-sm mr-2" style="border: 0px;">
                                                    <i class="fa fa-tachometer-alt mr-2"></i> Pergi ke dashboard
                                                </a>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- END timeline item -->
                                    <div>
                                        <i class="far fa-clock bg-gray"></i>
                                    </div>
                                </div>
                            </div>
                            <!-- /.tab-pane -->

                            <div class="tab-pane" id="settings">
                                <form class="form-horizontal" {{route('agency.profile')}} method="post">
                                    @csrf
                                    <div class="form-group row">
                                        <label for="inputName" class="col-sm-2 col-form-label">Nama Dinas</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="agency_name" class="form-control" id="inputName" placeholder="Nama Dinas" value="{{Auth::user()->agency->name}}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputName" class="col-sm-2 col-form-label">Username</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="username" class="form-control" id="inputName" placeholder="Username" value="{{Auth::user()->username}}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputName" class="col-sm-2 col-form-label">Email</label>
                                        <div class="col-sm-10">
                                            <input type="email" name="agency_email" class="form-control" id="inputName" placeholder="Email" value="{{Auth::user()->agency->email}}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="offset-sm-2 col-sm-10">
                                            <button type="submit" class="btn btn-danger"><i class="fa fa-save mr-2"></i> Perbarui</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div><!-- /.card-body -->
                </div>
                <!-- /.nav-tabs-custom -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->


    <form class="modal fade" id="modal-default" action="{{route('agency.changePassword')}}" method="post">
        @csrf
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Ganti Password</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                   <div class="form-group">
                        <label for="">Password Lama</label>
                        <input type="password" name="old_password" class="form-control" placeholder="Password Lama">
                    </div>
                    <div class="form-group">
                        <label for="" >Password Baru</label>
                        <input type="password" name="new_password" class="form-control" placeholder="Password Baru">
                    </div>
                    <div class="form-group">
                        <label for="" >Konfirmasi Password Baru</label>
                        <input type="password" name="new_password_confirmation" class="form-control" placeholder="Password Baru">
                    </div>

                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn text-danger" data-dismiss="modal">Batalkan</button>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-lock mr-2"></i> Ganti sekarang</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </form>
    <!-- /.modal -->
@endsection

@section('css')

@endsection

@section('js')

@endsection

