@php
    $state = isset($agency);
    $theme = $state ? 'warning' : 'primary';
@endphp

@extends('layouts.master')

@section('title')
    List Dinas
@endsection

@section('agency')
    active
@endsection

@section('menu')
    <i class="fa fa-landmark"></i> Dinas
@endsection

@section('breadcumb')
    <li class="breadcrumb-item"><a href="{{route('administrator.dashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="{{route('administrator.requirement-types.index')}}">List Tipe Dinas</a></li>
    <li class="breadcrumb-item active">Tambah Dinas</li>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="jumbotron d-flex align-items-center justify-content-center">
            <img src="{{asset('assets/img/illustration-3.svg')}}" class="illustration mr-5" alt="">
            <div class="col-md-5">
                <h4 class="mb-3">{{($state ? 'Edit' : 'Tambah') . " Form Dinas"}}</h4>
                <p style="margin-bottom: 0px;" class="text-muted">
                    Isi formulir dibawah sesuai dengan form dinas. Kemudian tekan tombol <span class="text-info">Simpan</span> untuk menyimpan data Dinas.
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-{{$theme}}">
                    <div class="card-header">
                        <h3 class="card-title">Form {{ $state ? 'edit dinas' : 'dinas baru' }}</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form class="form-horizontal" action="{{route('administrator.agencies.' . ($state ? 'update' : 'store'), $state ? ['agency' => $agency->id] : [])}}" method="post">
                        @csrf
                        @if($state)
                            @method('put')
                        @endif
                        <div class="card-body">

                            @if(session('msg'))
                                <div class="alert alert-success">
                                    {{session('msg')}}
                                </div>
                            @elseif(session('msgError'))
                                <div class="alert alert-danger">
                                    {{session('msgError')}}
                                </div>
                            @endif

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-2 col-form-label">Nama Dinas</label>
                                <div class="col-sm-10">
                                    <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="inputEmail3" placeholder="Nama Dinas" value="{{old('name') ? old('name') : ($state ? $agency->name : '')}}">
                                    @if($errors->first('name'))
                                    <div class="invalid-feedback">
                                        {{$errors->first('name')}}
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputPassword3" class="col-sm-2 col-form-label">Email</label>
                                <div class="col-sm-10">
                                    <input type="email" class="form-control" id="inputPassword3" placeholder="Email" name="email" value="{{old('email') ? old('email') : ($state ? $agency->email: '') }}">
                                    <small id="passwordHelpBlock" class="form-text text-muted">
                                        Email bersifat optional. Dengan mengisi email, system dapat mengirimkan notifikasi.
                                    </small>
                                </div>
                            </div>
                            <div class="form-group row form-submit">
                                <div class="col-md-2"></div>
                                <div class="col-md-10">
                                    <button type="submit" class="btn btn-{{$theme}} mr-2"><i class="fa fa-save mr-2"></i> Simpan</button>
                                    <a href="{{route('administrator.agencies.index')}}" class="btn">Kembali</a>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('css')
@endsection

@section('js')
@endsection

