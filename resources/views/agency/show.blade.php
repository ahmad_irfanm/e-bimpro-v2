@extends('layouts.master')

@section('title')
    Biodata dari {{$agency->name}}
@endsection

@section('requirement-tree')
    menu-open
@endsection

@section('agency')
    active
@endsection

@section('breadcumb')
    <li class="breadcrumb-item"><a href="{{route('administrator.dashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="{{route('administrator.agencies.index')}}">Dinas</a></li>
    <li class="breadcrumb-item active"> {{$agency->name}}</li>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="jumbotron d-flex align-items-center justify-content-center">
            <img src="{{asset('assets/img/illustration-13.svg')}}" class="illustration mr-5" alt="">
            <div class="col-md-5">
                <h4 class="mb-3">Dinas {{$agency->name}}</h4>
                <p style="margin-bottom: 0px;" class="text-muted">
                    Dibawah ini merupakan list biodata dari semua persyaratan yang telah dibuat oleh Dinas <span class="text-info">{{$agency->name}}</span>. Anda dapat melihat detail persyaratan dari setiap bidoata.
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header d-flex align-items-center justify-content-between">
                        <h3 class="card-title">List Biodata dari {{$agency->name}} </h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                            <thead>
                            <tr>
                                @foreach(getMain() as $biodata)
                                <th>{{$biodata->name}}</th>
                                @endforeach
                                <th>Tipe Persyaratan</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($agency->requirements()->where('status', '!=', 1)->get() as $requirement)
                                <tr>

                                    @foreach(getMain() as $biodata)
                                        <td>{{$requirement->getContent($biodata->id)}}</td>
                                    @endforeach

                                    <td>{{$requirement->requirement_type->name}}</td>

                                    <td class="text-center">
                                        <span class="badge badge-{{convertColor($requirement->status)}}">
                                            {{convertStatus($requirement->status)}}
                                        </span>
                                    </td>
                                    <td class="text-center">

                                        @if(Auth::user()->role == "admin2")
                                            <a href="{{route('administrator.requirements.destroy', ['requirement_type' => $requirement->requirement_type->id, 'requirement' => $requirement->id])}}" class="btn btn-xs text-danger btn-delete" title="Hapus Persyaratan">
                                                <i class="fa fa-trash mr-1"></i> Hapus
                                            </a>
                                        @endif

                                        <a href="{{route('administrator.requirements.update', ['requirement_type' => $requirement->requirement_type->id, 'requirement' => $requirement->id]) }}" class="btn btn-xs text-info" title="Lihat Persyaratan">
                                             Lihat persyaratan <i class="fa fa-arrow-right ml-1"></i>
                                        </a>
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>

                                @foreach(getMain() as $biodata)
                                <th>{{$biodata->name}}</th>
                                @endforeach
                                <th>Tipe Persyaratan</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Aksi</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>
@endsection

@section('css')
    <!-- DataTables -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
    <link rel="stylesheet" href="{{asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}">
    <link rel="stylesheet" href="{{asset('assets/plugins/datatables/buttons.dataTables.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/plugins/datatables/buttons.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/plugins/datatables/responsive.bootstrap4.min.css')}}">
@endsection

@section('js')
    <!-- DataTables -->
    <script src="{{asset('assets/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables/responsive.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables/buttons.flash.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables/jszip.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables/vfs_fonts.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables/buttons.print.min.js')}}"></script>
    <!-- page script -->
    <script>
        $(function () {
            $('#example').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
            $("#example1").DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
            });
        });
    </script>
@endsection

