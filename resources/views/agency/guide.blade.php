@extends('layouts.agency')

@section('guide')
    active
@endsection

@section('title')
    Panduan
@endsection

@section('header')
    Panduan
@endsection

@section('breadcumb')
    <li class="breadcrumb-item">
        <a href="{{route('agency.dashboard')}}">Dashboard</a>
    </li>
    <li class="breadcrumb-item active">Panduan</li>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
                <div class="jumbotron d-flex align-items-center justify-content-center" style="background: #ffe7c3;">
                    <img src="{{asset('assets/img/ill-guide.svg')}}" class="illustration mr-5" alt="">
                    <div class="col-md-6">
                        <h5 class="mb-3">Panduan Aplikasi {{config('app.name')}}</h5>
                        <p style="margin-bottom: 0px;">
                            Selamat datang di aplikasi e-Bimpro. Kamu dapat memulai dengan membuat biodata berdasarkan menu persyaratan yang tersedia. Setelah itu kamu dapat mengupload file perysaratan dan mengirimnya untuk dapat diverifikasi.
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    1. Membuat Biodata
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                <div class="timeline">
                                    <!-- timeline item -->
                                    <div>
                                        <i class="fas fa-arrow-circle-right bg-blue"></i>
                                        <div class="timeline-item">
                                            <h3 class="timeline-header">1. Klik menu persyaratan</h3>

                                            {{--<div class="timeline-body">--}}
                                            {{--Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,--}}
                                            {{--weebly ning heekya handango imeem plugg dopplr jibjab, movity--}}
                                            {{--jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle--}}
                                            {{--quora plaxo ideeli hulu weebly balihoo...--}}
                                            {{--</div>--}}
                                        </div>
                                    </div>

                                    <div>
                                        <i class="fas fa-arrow-circle-right bg-blue"></i>
                                        <div class="timeline-item">
                                            <h3 class="timeline-header">2. Klik tambah biodata</h3>

                                            {{--<div class="timeline-body">--}}
                                            {{--Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,--}}
                                            {{--weebly ning heekya handango imeem plugg dopplr jibjab, movity--}}
                                            {{--jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle--}}
                                            {{--quora plaxo ideeli hulu weebly balihoo...--}}
                                            {{--</div>--}}
                                        </div>
                                    </div>

                                    <div>
                                        <i class="fas fa-arrow-circle-right bg-blue"></i>
                                        <div class="timeline-item">
                                            <h3 class="timeline-header">3. Isi form biodata</h3>

                                            {{--<div class="timeline-body">--}}
                                            {{--Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,--}}
                                            {{--weebly ning heekya handango imeem plugg dopplr jibjab, movity--}}
                                            {{--jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle--}}
                                            {{--quora plaxo ideeli hulu weebly balihoo...--}}
                                            {{--</div>--}}
                                        </div>
                                    </div>

                                    <div>
                                        <i class="fas fa-arrow-circle-right bg-blue"></i>
                                        <div class="timeline-item">
                                            <h3 class="timeline-header">4. Data biodata yang telah dibuat akan tambil di list biodata</h3>

                                            {{--<div class="timeline-body">--}}
                                            {{--Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,--}}
                                            {{--weebly ning heekya handango imeem plugg dopplr jibjab, movity--}}
                                            {{--jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle--}}
                                            {{--quora plaxo ideeli hulu weebly balihoo...--}}
                                            {{--</div>--}}
                                        </div>
                                    </div>

                                    <!-- END timeline item -->
                                    <div>
                                        <i class="fas fa-check bg-success"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    2. Memenuhi Form Persyaratan
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                <div class="timeline">
                                    <!-- timeline item -->
                                    <div>
                                        <i class="fas fa-arrow-circle-right bg-blue"></i>
                                        <div class="timeline-item">
                                            <h3 class="timeline-header">1. Klik menu persyaratan</h3>

                                            {{--<div class="timeline-body">--}}
                                            {{--Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,--}}
                                            {{--weebly ning heekya handango imeem plugg dopplr jibjab, movity--}}
                                            {{--jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle--}}
                                            {{--quora plaxo ideeli hulu weebly balihoo...--}}
                                            {{--</div>--}}
                                        </div>
                                    </div>

                                    <div>
                                        <i class="fas fa-arrow-circle-right bg-blue"></i>
                                        <div class="timeline-item">
                                            <h3 class="timeline-header">2. Klik lihat persyaratan</h3>

                                            {{--<div class="timeline-body">--}}
                                            {{--Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,--}}
                                            {{--weebly ning heekya handango imeem plugg dopplr jibjab, movity--}}
                                            {{--jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle--}}
                                            {{--quora plaxo ideeli hulu weebly balihoo...--}}
                                            {{--</div>--}}
                                        </div>
                                    </div>

                                    <div>
                                        <i class="fas fa-arrow-circle-right bg-blue"></i>
                                        <div class="timeline-item">
                                            <h3 class="timeline-header">3. Di sisi kanan terdapat list file persyaratan yang harus dilampirkan</h3>

                                            {{--<div class="timeline-body">--}}
                                            {{--Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,--}}
                                            {{--weebly ning heekya handango imeem plugg dopplr jibjab, movity--}}
                                            {{--jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle--}}
                                            {{--quora plaxo ideeli hulu weebly balihoo...--}}
                                            {{--</div>--}}
                                        </div>
                                    </div>

                                    <div>
                                        <i class="fas fa-arrow-circle-right bg-blue"></i>
                                        <div class="timeline-item">
                                            <h3 class="timeline-header">4. Klik upload zone untuk melampirkan file</h3>

                                            {{--<div class="timeline-body">--}}
                                            {{--Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,--}}
                                            {{--weebly ning heekya handango imeem plugg dopplr jibjab, movity--}}
                                            {{--jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle--}}
                                            {{--quora plaxo ideeli hulu weebly balihoo...--}}
                                            {{--</div>--}}
                                        </div>
                                    </div>

                                    <div>
                                        <i class="fas fa-arrow-circle-right bg-blue"></i>
                                        <div class="timeline-item">
                                            <h3 class="timeline-header">5. Pilih file yang akan dilampirkan. File harus berbentuk JPG, PNG, atau JPEG.</h3>

                                            {{--<div class="timeline-body">--}}
                                            {{--Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,--}}
                                            {{--weebly ning heekya handango imeem plugg dopplr jibjab, movity--}}
                                            {{--jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle--}}
                                            {{--quora plaxo ideeli hulu weebly balihoo...--}}
                                            {{--</div>--}}
                                        </div>
                                    </div>

                                    <div>
                                        <i class="fas fa-arrow-circle-right bg-blue"></i>
                                        <div class="timeline-item">
                                            <h3 class="timeline-header">6. Kemudian file persyaratan berhasil diupload </h3>

                                            {{--<div class="timeline-body">--}}
                                            {{--Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,--}}
                                            {{--weebly ning heekya handango imeem plugg dopplr jibjab, movity--}}
                                            {{--jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle--}}
                                            {{--quora plaxo ideeli hulu weebly balihoo...--}}
                                            {{--</div>--}}
                                        </div>
                                    </div>

                                    <!-- END timeline item -->
                                    <div>
                                        <i class="fas fa-check bg-success"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapsFour" aria-expanded="false" aria-controls="collapseTwo">
                                    3. Mengirimkan Biodata
                                </button>
                            </h5>
                        </div>
                        <div id="collapsFour" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                <div class="timeline">
                                    <!-- timeline item -->
                                    <div>
                                        <i class="fas fa-arrow-circle-right bg-blue"></i>
                                        <div class="timeline-item">
                                            <h3 class="timeline-header">1. Klik menu persyaratan</h3>

                                            {{--<div class="timeline-body">--}}
                                            {{--Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,--}}
                                            {{--weebly ning heekya handango imeem plugg dopplr jibjab, movity--}}
                                            {{--jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle--}}
                                            {{--quora plaxo ideeli hulu weebly balihoo...--}}
                                            {{--</div>--}}
                                        </div>
                                    </div>

                                    <div>
                                        <i class="fas fa-arrow-circle-right bg-blue"></i>
                                        <div class="timeline-item">
                                            <h3 class="timeline-header">2. Klik lihat persyaratan</h3>

                                            {{--<div class="timeline-body">--}}
                                            {{--Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,--}}
                                            {{--weebly ning heekya handango imeem plugg dopplr jibjab, movity--}}
                                            {{--jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle--}}
                                            {{--quora plaxo ideeli hulu weebly balihoo...--}}
                                            {{--</div>--}}
                                        </div>
                                    </div>

                                    <div>
                                        <i class="fas fa-arrow-circle-right bg-blue"></i>
                                        <div class="timeline-item">
                                            <h3 class="timeline-header">3. Di sisi kanan terdapat list file persyaratan yang harus dilampirkan</h3>

                                            {{--<div class="timeline-body">--}}
                                            {{--Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,--}}
                                            {{--weebly ning heekya handango imeem plugg dopplr jibjab, movity--}}
                                            {{--jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle--}}
                                            {{--quora plaxo ideeli hulu weebly balihoo...--}}
                                            {{--</div>--}}
                                        </div>
                                    </div>

                                    <div>
                                        <i class="fas fa-arrow-circle-right bg-blue"></i>
                                        <div class="timeline-item">
                                            <h3 class="timeline-header">4. Penuhi semua form persyaratan. pastikan tidak ada persyaratan yang belum diisi/dilampirkan.</h3>

                                            {{--<div class="timeline-body">--}}
                                            {{--Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,--}}
                                            {{--weebly ning heekya handango imeem plugg dopplr jibjab, movity--}}
                                            {{--jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle--}}
                                            {{--quora plaxo ideeli hulu weebly balihoo...--}}
                                            {{--</div>--}}
                                        </div>
                                    </div>

                                    <div>
                                        <i class="fas fa-arrow-circle-right bg-blue"></i>
                                        <div class="timeline-item">
                                            <h3 class="timeline-header">5. Paling bawah terdapat tombol kirim persyaratan</h3>

                                            {{--<div class="timeline-body">--}}
                                            {{--Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,--}}
                                            {{--weebly ning heekya handango imeem plugg dopplr jibjab, movity--}}
                                            {{--jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle--}}
                                            {{--quora plaxo ideeli hulu weebly balihoo...--}}
                                            {{--</div>--}}
                                        </div>
                                    </div>

                                    <div>
                                        <i class="fas fa-arrow-circle-right bg-blue"></i>
                                        <div class="timeline-item">
                                            <h3 class="timeline-header">6. Jika semua form persyaratan terpenuhi, maka tombol kirim persyaratan akan aktif </h3>

                                            {{--<div class="timeline-body">--}}
                                            {{--Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,--}}
                                            {{--weebly ning heekya handango imeem plugg dopplr jibjab, movity--}}
                                            {{--jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle--}}
                                            {{--quora plaxo ideeli hulu weebly balihoo...--}}
                                            {{--</div>--}}
                                        </div>
                                    </div>

                                    <div>
                                        <i class="fas fa-arrow-circle-right bg-blue"></i>
                                        <div class="timeline-item">
                                            <h3 class="timeline-header">7. Klik tombol kirim persyaratan untuk mengirim semua file persyaratan untuk biodata tersebut. </h3>

                                            {{--<div class="timeline-body">--}}
                                            {{--Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,--}}
                                            {{--weebly ning heekya handango imeem plugg dopplr jibjab, movity--}}
                                            {{--jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle--}}
                                            {{--quora plaxo ideeli hulu weebly balihoo...--}}
                                            {{--</div>--}}
                                        </div>
                                    </div>

                                    <!-- END timeline item -->
                                    <div>
                                        <i class="fas fa-check bg-success"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    4. Mengedit Biodata
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                            <div class="card-body">
                                <div class="timeline">
                                    <!-- timeline item -->
                                    <div>
                                        <i class="fas fa-arrow-circle-right bg-blue"></i>
                                        <div class="timeline-item">
                                            <h3 class="timeline-header">1. Klik menu persyaratan</h3>

                                            {{--<div class="timeline-body">--}}
                                            {{--Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,--}}
                                            {{--weebly ning heekya handango imeem plugg dopplr jibjab, movity--}}
                                            {{--jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle--}}
                                            {{--quora plaxo ideeli hulu weebly balihoo...--}}
                                            {{--</div>--}}
                                        </div>
                                    </div>

                                    <div>
                                        <i class="fas fa-arrow-circle-right bg-blue"></i>
                                        <div class="timeline-item">
                                            <h3 class="timeline-header">2. Klik lihat persyaratan</h3>

                                            {{--<div class="timeline-body">--}}
                                            {{--Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,--}}
                                            {{--weebly ning heekya handango imeem plugg dopplr jibjab, movity--}}
                                            {{--jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle--}}
                                            {{--quora plaxo ideeli hulu weebly balihoo...--}}
                                            {{--</div>--}}
                                        </div>
                                    </div>

                                    <div>
                                        <i class="fas fa-arrow-circle-right bg-blue"></i>
                                        <div class="timeline-item">
                                            <h3 class="timeline-header">3. Di sisi kiri terdapat detail biodata</h3>

                                            {{--<div class="timeline-body">--}}
                                            {{--Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,--}}
                                            {{--weebly ning heekya handango imeem plugg dopplr jibjab, movity--}}
                                            {{--jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle--}}
                                            {{--quora plaxo ideeli hulu weebly balihoo...--}}
                                            {{--</div>--}}
                                        </div>
                                    </div>

                                    <div>
                                        <i class="fas fa-arrow-circle-right bg-blue"></i>
                                        <div class="timeline-item">
                                            <h3 class="timeline-header">4. Klik button edit</h3>

                                            {{--<div class="timeline-body">--}}
                                            {{--Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,--}}
                                            {{--weebly ning heekya handango imeem plugg dopplr jibjab, movity--}}
                                            {{--jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle--}}
                                            {{--quora plaxo ideeli hulu weebly balihoo...--}}
                                            {{--</div>--}}
                                        </div>
                                    </div>

                                    <div>
                                        <i class="fas fa-arrow-circle-right bg-blue"></i>
                                        <div class="timeline-item">
                                            <h3 class="timeline-header">5. Edit form biodata.</h3>

                                            {{--<div class="timeline-body">--}}
                                            {{--Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,--}}
                                            {{--weebly ning heekya handango imeem plugg dopplr jibjab, movity--}}
                                            {{--jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle--}}
                                            {{--quora plaxo ideeli hulu weebly balihoo...--}}
                                            {{--</div>--}}
                                        </div>
                                    </div>

                                    <div>
                                        <i class="fas fa-arrow-circle-right bg-blue"></i>
                                        <div class="timeline-item">
                                            <h3 class="timeline-header">6. Klik save </h3>

                                            {{--<div class="timeline-body">--}}
                                            {{--Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,--}}
                                            {{--weebly ning heekya handango imeem plugg dopplr jibjab, movity--}}
                                            {{--jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle--}}
                                            {{--quora plaxo ideeli hulu weebly balihoo...--}}
                                            {{--</div>--}}
                                        </div>
                                    </div>

                                    <div>
                                        <i class="fas fa-arrow-circle-right bg-blue"></i>
                                        <div class="timeline-item">
                                            <h3 class="timeline-header">7. Biodata berhasil diperbarui dan akan tampil di list biodata </h3>

                                            {{--<div class="timeline-body">--}}
                                            {{--Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,--}}
                                            {{--weebly ning heekya handango imeem plugg dopplr jibjab, movity--}}
                                            {{--jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle--}}
                                            {{--quora plaxo ideeli hulu weebly balihoo...--}}
                                            {{--</div>--}}
                                        </div>
                                    </div>

                                    <!-- END timeline item -->
                                    <div>
                                        <i class="fas fa-check bg-success"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('css')
    <style>
    </style>
@endsection

@section('js')
@endsection

