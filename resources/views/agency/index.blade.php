@extends('layouts.master')

@section('title')
    List Dinas
@endsection

@section('agency')
    active
@endsection

@section('menu')
    <i class="fa fa-landmark"></i> Dinas
@endsection

@section('breadcumb')
    <li class="breadcrumb-item"><a href="{{route('administrator.dashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item active">Dinas</li>
@endsection

@section('content')
    <div class="container-fluid">
        @if(session('msg'))
            <div class="alert alert-success">
                {{session('msg')}}
            </div>
        @elseif(session('msgError'))
            <div class="alert alert-danger">
                {{session('msgError')}}
            </div>
        @elseif($errors->any())
            <div class="alert alert-danger">
                Opps.. Ada yang error! <br>
                @foreach($errors->all() as $error)
                    {{$error}} <br>
                @endforeach
            </div>
        @endif

            <div class="jumbotron d-flex align-items-center justify-content-center">
                <img src="{{asset('assets/img/ill-list.svg')}}" class="illustration mr-5" alt="">
                <div class="col-md-5">
                    <h4 class="mb-3">List Dinas</h4>
                    <p style="margin-bottom: 0px;" class="text-muted">
                        Dibawah kamu dapat melihat list dinas. Kamu dapat menambah, mengedit, menghapus, mereset password serta melihat detail dari dinas.
                    </p>
                </div>
            </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header align-items-center justify-content-between" style="display: grid; grid-template-columns: repeat(2, auto)">
                        <h3 class="card-title">List Dinas</h3>
                        <a href="{{route('administrator.agencies.create')}}" class="btn text-bg text-bold text-primary"><i class="fa fa-plus-circle"></i> Tambah dinas</a>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example" class="table table-striped dt-responsive nowrap" style="width: 100%">
                            <thead>
                            <tr>
                                <th>Nama Dinas</th>
                                <th class="text-center">Jumlah Persyaratan</th>
                                <th>Email</th>
                                <th>Username</th>
                                <th>Password</th>
                                <th class="text-center">Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($agencies as $agency)
                                    <tr>
                                        <td>{{$agency->name}}</td>
                                        <td class="text-center">{{$agency->requirements->where('status', '!=', 1)->count()}}</td>
                                        <td>{{$agency->email ? $agency->email : '-'}}</td>
                                        <td>{{$agency->user->username}}</td>
                                        <td>{!! $agency->password ? $agency->password : '<span class="badge badge-info">password telah diganti oleh dinas</span>' !!}</td>
                                        <td class="text-center">
                                            <a href="{{route('administrator.agencies.destroy', ['agency' => $agency->id])}}" class="text-danger mr-3 btn-delete" title="Delete">
                                                <i class="fa fa-trash mr-1"></i> Delete
                                            </a>
                                            <a href="{{route('administrator.agencies.edit', ['agency' => $agency->id])}}" class="text-warning mr-3" title="Edit">
                                                <i class="fa fa-edit mr-1"></i> Edit
                                            </a>
                                            <a href="{{route('administrator.agencies.resetPassword', ['agency' => $agency->id])}}" class="btn btn-xs btn-primary btn-reset" title="Detail">
                                                <i class="fa fa-lock mr-1"></i> Reset Password
                                            </a>
                                            <a href="{{route('administrator.agencies.show', ['agency' => $agency->id])}}" class="btn btn-xs btn-default ml-2" title="Lihat Semua Biodata" data-toggle="tooltip">
                                                 Lihat Biodata <i class="fa fa-arrow-right ml-1"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>

    <form class="modal fade" id="modal-default" action="" method="post">
        @csrf
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Reset Password</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body d-flex justify-content-center">
                    <button type="submit" class="btn btn-primary mr-2"><i class="fa fa-lock mr-2"></i> Reset sekarang</button>
                    <button type="button" class="btn text-danger" data-dismiss="modal">Batalkan</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </form>
    <!-- /.modal -->
@endsection

@section('css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.bootstrap4.min.css">
    <link rel="stylesheet" href="{{asset('assets/plugins/datatables/responsive.bootstrap4.min.css')}}">
@endsection

@section('js')
    <!-- DataTables -->
    <script src="{{asset('assets/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables/responsive.bootstrap4.min.js')}}"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>
    <!-- page script -->
    <script>
        $(function () {
            $('#example').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
            $("#example1").DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
            });

            $('.btn-reset').click(function (e) {
                e.preventDefault();

                let url = $(this).attr('href');
                $('#modal-default').attr('action', url);
                $('#modal-default').modal('show');
            });
        });
    </script>
@endsection

