<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <style media="screen">
        button{

        }
    </style>
</head>
<body>
<div  style="padding:5px;padding-bottom:60px;margin-bottom:60px;float:left;width:50%;margin:0 auto;display:inline-block;" id="elem">
    <div>
        <div style="border:1px solid black;width: 100%;height:1000px;">
            <center>
                <h1>KARTU PENGAMBILAN</h1>
                <hr style="width:500px;"><br><br><br><br>
                Kode<br><br>
                <div class="kode" style="border: 1px solid black; width: 300px;height:300px;padding-top:5px;">
                    <?php echo base64_encode($biodata->nip); ?>
                </div>
            </center><br><br>
            <div style="margin-left: 100px;">
                <table style="width:100%">
                    <tr>
                        <td style="vertical-align: top;padding-top: 8px;width:30%">Nama</td>
                        <td style="vertical-align: top;padding-top: 8px;width:5%">:</td>
                        <td style="vertical-align: top;padding-top: 8px;width:65%"><?php echo $biodata->name; ?></td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;padding-top: 8px;">Dinas</td>
                        <td style="vertical-align: top;padding-top: 8px;">:</td>
                        <td style="vertical-align: top;padding-top: 8px;"><?php echo $biodata->requirement->agency->name; ?></td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;padding-top: 8px;">Pelayanan</td>
                        <td style="vertical-align: top;padding-top: 8px;">:</td>
                        <td style="vertical-align: top;padding-top: 8px;">
                            <?php
                            echo $requirementType->name
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;padding-top: 8px;">Tanggal Pengambilan</td>
                        <td style="vertical-align: top;padding-top: 8px;">:</td>
                        <td style="vertical-align: top;padding-top: 8px;">{{date('d/m/Y', strtotime($biodata->requirement->created_at))}}</td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;padding-top: 8px;">Catatan</td>
                        <td style="vertical-align: top;padding-top: 8px;">:</td>
                        <td style="vertical-align: top;padding-top: 8px;">Diharapkan membawa kartu ini pada saat pengambilan Izin belajar dan atau Surat Keterangan Lulus Kuliah di <b>BKPSDM</b> Kabupaten Subang.</td>
                    </tr>
                </table>

            </div>
            <div style="float:left; margin-right: 250px;margin-left: 50px;">
                <center><br><br><br><br><br><br><br><br><br><br>
                    <hr style="width:200px;border:1px solid black">
                    Petugas Pelayanan
                </center>
            </div>
            <!-- <div style="float:right; margin-left: 250px;margin-right: 50px;">
            <center><br><br><br><br><br><br><br><br><br><br><br><br><br>
                <hr style="width:200px;border:1px solid black">
                Pengambil
            </center>
            </div> -->
            <div style="float: right;margin-right: 50px;margin-top:185px;">
                <center>
                    <hr style="width:200px;border:1px solid black">
                    Pengambil
                </center>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function printC(element){
        var bodyTag = document.body.innerHTML;
        var content = document.getElementById(element).innerHTML;
        document.body.innerHTML = content;
        window.print();
        document.body.innerHTML = bodyTag;
    }
</script>
</body>
</html>