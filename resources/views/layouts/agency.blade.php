<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>{{ config('app.name', 'Laravel') }} | @yield('title')</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('assets/plugins/fontawesome-free/css/all.min.css')}}">
    <!-- Toastr -->
    <link rel="stylesheet" href="{{asset('assets/plugins/toastr/toastr.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/dist/css/animate.css')}}">
    @yield('css')
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('assets/dist/css/adminlte.min.css')}}">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{asset('assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
    {{--  sweatalert  --}}
    <link rel="stylesheet" href="{{asset('assets/plugins/sweetalert2/sweetalert2.css')}}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Merriweather:700|Rubik:400,600&display=swap" rel="stylesheet">

    <link rel="icon" href="{{asset('assets/dist/img/logo.jpg')}}">
    {{--custom--}}
    <link rel="stylesheet" href="{{asset('assets/dist/css/custom.css')}}">
    <style>
        .os-scrollbar-handle {
            background: #f9b27e !important;
        }
    </style>
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-light-orange elevation-1">
        <!-- Brand Logo -->
        <a href="{{route('agency.dashboard')}}" class="brand-link">
            <img src="{{asset('assets/dist/img/logo.jpg')}}" alt="e-Bimpro" class="brand-image img-circle elevation-3"
                 style="opacity: .8">
            <span class="brand-text font-weight-bold font-heading ml-2">e-Bimpro</span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">

            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <!-- Add icons to the links using the .nav-icon class
                         with font-awesome or any other icon font library -->

                    <li class="nav-header">Menu Utama</li>

                    <li class="nav-item">
                        <a href="{{route('agency.dashboard')}}" class="nav-link d-flex align-items-center justify-content-start @yield('dashboard')">
                            <i class="nav-icon fas fa-tachometer-alt mr-2" style="line-height: 24px;"></i>
                            <p>
                                Dashboard
                            </p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{route('agency.guide')}}" class="nav-link d-flex align-items-center justify-content-start @yield('guide')">
                            <i class="nav-icon fas fa-map-signs mr-2" style="line-height: 24px;"></i>
                            <p>
                                Panduan
                            </p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{route('agency.profile')}}" class="nav-link d-flex align-items-center justify-content-start @yield('profile')">
                            <i class="nav-icon fas fa-user-circle mr-2" style="line-height: 24px;"></i>
                            <p>
                                Profile
                            </p>
                        </a>
                    </li>

                    <li class="nav-header">Menu Persyaratan</li>

                    @foreach(\App\RequirementType::orderBy('name', 'asc')->get() as $rt)
                        <li class="nav-item">
                            <a href="{{route('agency.requirements.index', ['requirement_type' => $rt->id])}}" class="nav-link @yield('requirement-' . $rt->id)" data-toggle="tooltip" title="{{$rt->name}}" style="display: flex; align-items: flex-start;">
                                <i class="nav-icon fas fa-circle-notch mr-2" style="line-height: 24px;"></i>
                                <p  style="white-space: normal; display: grid; grid-template-columns: repeat(2, auto); align-items: start; grid-gap: 8px; justify-content: space-between; width: 100%;">
                                    <span style="line-height: 24px;">
                                        {{$rt->name}}
                                    </span>

                                    <p style="line-height: 24px;">
                                    @if($rt->requirements()->where('agency_id', Auth::user()->agency->id)->whereStatus(1)->count() > 0)
                                        <span class="badge badge-warning">{{$rt->requirements()->where('agency_id', Auth::user()->agency->id)->whereStatus(1)->count()}}</span>
                                    @endif
                                    @if($rt->requirements()->where('agency_id', Auth::user()->agency->id)->whereStatus(4)->count() > 0)
                                        <span class="badge badge-success">{{$rt->requirements()->where('agency_id', Auth::user()->agency->id)->whereStatus(4)->count()}}</span>
                                    @endif
                                    </p>
                                </p>
                            </a>
                        </li>
                    @endforeach

                    <li class="nav-header">Menu Otentikasi</li>

                    <li class="nav-item">
                        <a href="" onclick="event.preventDefault(); document.getElementById('form-logout').submit()" class="nav-link">
                         <i class="nav-icon fa fa-power-off"></i>
                            <p>
                                Logout
                            </p>
                        </a>
                    </li>

                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h6 class="m-0 text-dark">@yield('header')</h6>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            @yield('breadcumb')
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            @yield('content')
        </section>
        <!-- /.content -->
    </div>


    <form action="{{route('logout')}}" method="post" id="form-logout">@csrf</form>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <strong>Copyright &copy; 2020 <a href="mailto:email.ahmadirfan@gmail.com" target="_blank">Ahmad Irfan Maulana</a>.</strong>
        <div class="float-right d-none d-sm-inline-block">
            Template by <b>AdminLTE v.3.0.2</b>
        </div>
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{asset('assets/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- overlayScrollbars -->
<script src="{{asset('assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
<!-- SweetAlert2 -->
<script src="{{asset('assets/plugins/sweetalert2/sweetalert2.all.js')}}"></script>
<script src="{{asset('assets/plugins/sweetalert2/sweetalert2.min.js')}}"></script>
<!-- Toastr -->
<script src="{{asset('assets/plugins/toastr/toastr.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('assets/dist/js/adminlte.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{asset('assets/dist/js/pages/dashboard.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('assets/dist/js/demo.js')}}"></script>
@yield('js')
<script>
    $(function() {
        const Toast = Swal.mixin({
            toast: true,
            position: 'top',
            showConfirmButton: false,
            timer: 3000
        });

        $('.btn-delete').click(function (e) {
            e.preventDefault();

           let url = $(this).attr('href');
           let that = $(this);
           let token = $('meta[name="csrf-token"]').attr('content');

           let quest = window.confirm('Apakah yakin akan menghapus ?');
           if (!quest) {
                return;
           }

           $.ajax({
               url,
               method: 'post',
               data: {_method: 'DELETE', _token: token},
               success: function (response) {
                    toastr.success(response.msg);
                    console.log(that.parents('tr'));
                    that.parents('tr').remove();
               },
               errors: function (errors) {
                   alert(errors.msg);
               }
           })
        });
    });

    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })

</script>
</body>
</html>
