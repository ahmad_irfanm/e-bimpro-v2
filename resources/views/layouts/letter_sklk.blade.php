<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo $biodata->requirement->agency->name; ?> - <?php echo $biodata->name; ?></title>
</head>
<body>
<div  style="padding:5px;padding-bottom:60px;padding-top: 10px;margin-bottom:60px;border:1px solid #777;float:left;width:100%;margin:0 auto;display:inline-block;" id="elem">
    <img src="{{asset('assets/img/lks.png')}}" style="position: absolute;width:85px;">
    <div style="padding: 0px 15%;font-family: 'Times New Roman';">
        <center>
            <p style="font-weight: bold;margin-bottom:-20px; font-size:18px">PEMERINTAHAN KABUPATEN SUBANG</p>
            <p style="font-weight: bold; font-size: 20px; margin-bottom: 5px;">BADAN KEPEGAWAIAN DAN PENGEMBANGAN<br>SUMBER DAYA MANUSIA</p>
            <span style="font-size:11px;font-weight: bold;">JALAN KAPTEN TENDEAN NO.1 TELP.(0260)418116-418118 SUBANG</span>
        </center>
    </div>
    <hr>
    <hr style="background-color: black;margin-top: -7px; border: 2px solid black;">
    <div style="font-family: sans-serif; font-size: 13px;"><br>
        <center>
            <b><u>SURAT KETERANGAN LULUS PENDIDIKAN</u></b>
        </center>
        <span style="margin-left:247px;">Nomor : </span>
        <div style="padding:0px 5%;">
            </b></p>
            Yang Bertanda tangan di bawah ini:
            <ol type="a" style="margin-left:-25px;margin-top: 0px;">
                <li>Nama<span style="padding-right: 100px;"></span>: <b>Drs. H. CECEP SUPRIATIN, M.Si.</b></li>
                <li>Jabatan<span style="padding-right: 89px;"></span>: <b>Kepala BKPSDM Kabupaten Subang</b></li>
            </ol>
            Dengan ini menerangkan :
            <ol type="a" style="margin-left:-25px;margin-top: 0px;">
                <li>Nama<span style="padding-right: 100px;"></span>: <b><?php echo $biodata->name; ?></b></li>
                <li>NIP<span style="padding-right: 113px;"></span>: <?php echo $biodata->nip; ?></b></li>
                <li>Pangkat / Gol. Ruang &nbsp&nbsp;: <?php echo $biodata->rank; ?></li>
                <li>Jabatan<span style="padding-right: 89px;"></span>: <?php echo $biodata->position; ?></li>
                <li>Unit Kerja<span style="padding-right: 78px;"></span>: <?php echo $biodata->work_unit; ?></li>
            </ol>
            Bahwa yang bersangkutan telah menyelesaikan pendidikan dan mendapat ijazah pada :<br>
            Sekolah / Perguruan Tinggi : <?php echo $biodata->collage; ?><br>
            Akreditasi<span style="padding-right: 103px;"></span>: <?php echo $biodata->accreditation; ?><br>
            Jurusan / Program<span style="padding-right: 53px;"></span>: <?php echo $biodata->department; ?><br>
            Nomor Ijazah<span style="padding-right: 83px;"></span>: <?php echo $biodata->ijazah_number; ?><br>
            Tahun Kelulusan<span style="padding-right: 64px;"></span>: <?php echo $biodata->year_of_graduation; ?><br>
            Nilai / IPK<span style="padding-right: 103px;"></span>: <?php echo $biodata->score; ?><br><br>
            Dasar<span style="padding-right: 73px;"></span>:
            <div style="margin-left:50px;">
                <ol type="a" style="text-align: justify;">
                    <li>Surat Menteri Pendayagunaan Aparatur Negara dan Reformasi Birokrasi Nomor : B/1299/M.PAN-RB/3/2013 Perihal Surat Edaran Menteri Pendayagunaan Aparatur Negara dan Reformasi Birokrasi Nomor 04 Tahun 2013 tentang Pemberian Tugas Belajar dan Izin Belajar tanggal 25 Maret 2013;</li>
                    <li>Peraturan Bupati Subang Nomor 31 Tahun 2018 tentang Pemberian Tugas Belajar, Izin Belajar, Surat Keterangan Lulus Pendidikan, Ujian Dinas dan Ujian Penyesuaian Kenaikan Pangkat Pegawai Negeri Sipil di Lingkungan Pemerintah Kabupaten Subang;</li>
                    <li>
                        Surat dari Kepala <?php echo $biodata->requirement->agency->name; ?><br>
                        Nomor &nbsp; : <?php echo $biodata->letter_number; ?><br>
                        Tanggal : <?php echo date('d/m/Y', strtotime($biodata->letter_date)); ?><br>
                        Perihal &nbsp; : Permohonan Keterangan Lulus Pendidikan
                    </li>
                </ol>
            </div>
            <p style="text-align: justify;">Demikian surat keterangan ini dibuat dengan ketentuan tidak menuntut penyesuaian ijazah kecuali formasi memungkinkan dan sesuai Peraturan Perundang-undangan yang berlaku.</p>
        </div><br>
        <div style="float:right; margin-left:300px;	">
            <center>Dikeluarkan di Subang<br>
                Pada Tanggal <?php echo date('d/m/Y'); ?> <br>
                <b>KEPALA BADAN KEPEGAWAIAN DAN<br>
                    PENGEMBANGAN SUMBER DAYA MANUSIA<br>
                    KABUPATEN SUBANG<br><br><br><br><br><br>
                    <u>Drs. H. CECEP SUPRIATIN, M.Si.</u><br>
                    Pembina Utama Muda (IV/c)<br>
                    NIP. 19591103 198401 2 001</b></center>
        </div>
        <div class="" style="position: absolute; width: 85px;margin-top: 60px;margin-left:100px;">
            <?php echo '<img src="data:image/png;base64,' . DNS2D::getBarcodePNG($biodata->nip, "QRCODE") . '" alt="barcode"   />'; ?>
        </div>
        <div style="margin-top:180px;">
            Tembusan Yth. :<br>
            1. Bupati Subang (sebagai laporan);<br>
            2. Inspektur Inspektorat Daerah Kab.Subang;<br>
            3. Kepala <?php echo $biodata->requirement->agency->name; ?><br>
            </ol>
        </div>
    </div>
</div>