@foreach($requirementType->biodatas as $i => $biodata)
    <strong>
        {{$biodata->name}}
    </strong>

    <p class="text-muted">
        {{$requirement->getContent($biodata->id)}}
    </p>

    @if(Auth::user()->role != "agency")
        @if($i + 1 < $requirementType->biodatas->count())
            <hr>
        @endif
    @else
        <hr>
    @endif
@endforeach

@if(Auth::user()->role == "agency")
    <a href="{{route('agency.requirements.edit', ['requirement_type' => $requirementType->id, 'requirement' => $requirement->id])}}" class="btn text-warning btn-block btn-sm"><i class="fa fa-edit mr-2"></i> Edit Biodata</a>
@endif



