<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ config('app.name', 'Laravel') }} | @yield('title')</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('assets/plugins/fontawesome-free/css/all.min.css')}}">
    {{--  sweatalert  --}}
    <link rel="stylesheet" href="{{asset('assets/plugins/sweetalert2/sweetalert2.css')}}">
    <!-- Toastr -->
    <link rel="stylesheet" href="{{asset('assets/plugins/toastr/toastr.min.css')}}">

    <link rel="stylesheet" href="{{asset('assets/dist/css/animate.css')}}">

    @yield('css')
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('assets/dist/css/adminlte.min.css')}}">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{asset('assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
    <!-- Google Font: Source Sans Pro -->
    <link rel="icon" href="{{asset('assets/dist/img/logo.jpg')}}">
    {{--custom--}}
    <link rel="stylesheet" href="{{asset('assets/dist/css/custom.css')}}">
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="{{route('administrator.dashboard')}}" class="brand-link">
            <img src="{{asset('assets/dist/img/logo.jpg')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
                 style="opacity: .8">
            <span class="brand-text font-weight-light">e-Bimpro</span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">

            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <!-- Add icons to the links using the .nav-icon class
                         with font-awesome or any other icon font library -->


                    <li class="nav-header">Menu Utama</li>

                    <li class="nav-item">
                        <a href="{{route('administrator.dashboard')}}" class="nav-link @yield('dashboard')">
                            <i class="nav-icon fas fa-tachometer-alt"></i>
                            <p>
                                Dashboard
                            </p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{route('administrator.profile')}}" class="nav-link @yield('profile')">
                            <i class="nav-icon fas fa-user-circle"></i>
                            <p>
                                Profile
                            </p>
                        </a>
                    </li>

                    <li class="nav-header">Menu Persyaratan</li>

                    @foreach(\App\RequirementType::orderBy('name', 'asc')->get() as $rt)
                        <li class="nav-item">
                            <a href="{{route('administrator.requirements.index', ['requirement_type' => $rt->id])}}" class="nav-link @yield('requirement-' . $rt->id)" data-toggle="tooltip" data-title="{{$rt->name}}" style="display: flex; align-items: flex-start;">
                                <i class="nav-icon fas fa-circle-notch mr-2" style="line-height: 24px;"></i>
                                <p  style="white-space: normal; display: grid; grid-template-columns: repeat(2, auto); align-items: start; grid-gap: 8px; justify-content: space-between; width: 100%;">

                                    <span style="line-height: 24px;">
                                        {{$rt->name}}
                                    </span>

                                    <p style="line-height: 24px;">
                                        @if(Auth::user()->role == "admin1")
                                            @if($rt->requirements()->whereStatus(4)->count() > 0)
                                                <span class="badge badge-success">{{$rt->requirements()->whereStatus(4)->count()}}</span>
                                            @endif
                                            @if($rt->requirements()->whereStatus(2)->count() > 0)
                                                <span class="badge badge-info">{{$rt->requirements()->whereStatus(2)->count()}}</span>
                                            @endif
                                        @elseif(Auth::user()->role == "admin2")
                                            @if($rt->requirements()->whereStatus(3)->count() > 0)
                                                <span class="badge badge-primary">{{$rt->requirements()->whereStatus(3)->count()}}</span>
                                            @endif
                                        @endif
                                    </p>
                                </p>
                            </a>
                        </li>
                    @endforeach

                    <li class="nav-header">Menu Data</li>

                    <li class="nav-item">
                        <a href="{{route('administrator.requirement-types.index')}}" class="nav-link @yield('requirement-type')">
                            <i class="nav-icon fas fa-boxes"></i>
                            <p>
                                Tipe Persayaratan
                            </p>
                        </a>
                    </li>


                    <li class="nav-item">
                        <a href="{{route('administrator.biodatas.index')}}" class="nav-link @yield('biodatas')">
                            <i class="nav-icon fas fa-id-card"></i>
                            <p>
                                Form Biodata
                            </p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{route('administrator.agencies.index')}}" class="nav-link @yield('agency')">
                            <i class="nav-icon fas fa-landmark"></i>
                            <p>
                                Dinas
                            </p>
                        </a>
                    </li>

                    <li class="nav-header">Menu Otentikasi</li>
                    <li class="nav-item">
                        <a href="" onclick="event.preventDefault(); document.getElementById('form-logout').submit()" class="nav-link">
                            <i class="nav-icon fa fa-power-off"></i>
                            <p>
                                Logout
                            </p>
                        </a>
                    </li>

                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h6 style="font-weight: 500;" class="m-0 text-dark">@yield('menu')</h6>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            @yield('breadcumb')
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            @yield('content')
        </section>
        <!-- /.content -->
    </div>

    <form action="{{route('logout')}}" method="post" id="form-logout">@csrf</form>

    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <strong>Copyright &copy; 2020 <a href="mailto:email.ahmadirfan@gmail.com" target="_blank">Ahmad Irfan Maulana</a>.</strong>
        All rights reserved.
        <div class="float-right d-none d-sm-inline-block">
            Template by <b>AdminLTE v.3.0.2</b>
        </div>
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{asset('assets/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- overlayScrollbars -->
<script src="{{asset('assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
<!-- SweetAlert2 -->
<script src="{{asset('assets/plugins/sweetalert2/sweetalert2.min.js')}}"></script>
<!-- Toastr -->
<script src="{{asset('assets/plugins/toastr/toastr.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('assets/dist/js/adminlte.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{asset('assets/dist/js/pages/dashboard.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('assets/dist/js/demo.js')}}"></script>
@yield('js')
<script>
    $(function() {
        const Toast = Swal.mixin({
            toast: true,
            position: 'top',
            showConfirmButton: false,
            timer: 3000
        });

        $('body').delegate('.btn-delete', 'click', function (e) {
            e.preventDefault();

           let url = $(this).attr('href');
           let that = $(this);
           let token = $('meta[name="csrf-token"]').attr('content');

            Swal.fire({
                title: 'Konfirmasi',
                text: "Apakah kamu yakin ingin menghapus ini ?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, hapus sekarang',
                cancelButtonText: 'Jangan dulu'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url,
                        method: 'post',
                        data: {_method: 'DELETE', _token: token},
                        success: function (response) {
                            Swal.fire({
                                type: 'success',
                                title: 'Berhasil',
                                text: response.msg
                            }).then((result) => {
                                if (result.value) {
                                    location.reload();
                                }
                            });
                        },
                        error: function (error) {
                            Swal.fire({
                                type: 'error',
                                title: 'Ada yang error',
                                text: error.responseJSON.msg
                            }).then((result) => {
                                if (result.value) {
                                    location.reload();
                                }
                            });
                        }
                    })

                }
            });


        });
    });

    $('[data-toggle="tooltip"]').tooltip();

    setTimeout(() => {
        $('.alert').fadeOut();
    }, 3000);

</script>
</body>
</html>
