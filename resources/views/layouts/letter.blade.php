<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo $biodata->name; ?></title>
</head>
<body>
<div  style="padding:5px;padding-bottom:60px;padding-top:10px;margin-bottom:60px;border:1px solid #777;float:left;width:100%;margin:0 auto;display:inline-block;" id="elem">
    <img src="{{asset('assets/img/lks.png')}}" style="position: absolute;width:85px;">
    <div style="padding: 0px 15%;font-family: 'Times New Roman';">
        <center>
            <p style="font-weight: bold;margin-bottom:4px; font-size:18px;line-height: 16px">PEMERINTAHAN KABUPATEN SUBANG</p>
            <p style="font-weight: bold; font-size: 20px; margin-bottom: 5px;line-height: 22px">BADAN KEPEGAWAIAN DAN PENGEMBANGAN<br>SUMBER DAYA MANUSIA</p>
            <span style="font-weight:bold;font-size:11px;line-height: 16px">JALAN KAPTEN TENDEAN NO.1 TELP.(0260)418116-418118 SUBANG</span>
        </center>
    </div>
    <hr>
    <hr style="background-color: black;margin-top: -7px; border: 2px solid black;">
    <div style="font-family: sans-serif;">
        <center style="letter-spacing: 5px">SURAT IZIN</center>
        <span style="margin-left:200px;">Nomor :
      		<center>
      		<p style="font-size: 15;font-family: 'Times New Roman';"><b>
      			TENTANG<br>
      			PEMBERIAN IZIN BELAJAR BAGI PEGAWAI NEGERI SIPIL<br>
      			PEMERINTAH KABUPATEN SUBANG<br>
      			BUPATI SUBANG,
      		</b></p>
      		</center>
      		<div style="padding: 0px 5%;font-size: 13px;">
      			<span style="">Dasar<span style="padding-left:100px;">:</span></span><ol type="a" style="text-align: justify;">
      			<li>Surat Menteri Pendayagunaan Aparatur dan Reformasi Birokrasi Nomor : B/1299/M.PAS-RB/3/2013 Perihal surat Edaran Menteri Pendayagunaan Aparatur Negara Reformasi Birokrasi Nomor 04 Tahun 2013 tentang Pemberian Tugas Belajar dan izin Belajar tanggal 25 Maret 2013;</li>
      			<li>Peraturan Bupati Subang Nomor 31 Tahun 2018 tentang Pemberian Tugas Belajar, izin Belajar, Surat Keterangan Lulus pendidikan, Izin Penggunaan Gelar Akademik, Ujian Dinas dan Ujian Penyesuaian Kenaikan Pangkat pegawwai Negeri Sipil di Lingkungan Pemerintah Kabupaten Subang;</li>
      			<li>Surat dari Kepala <?php echo $biodata->requirement->agency->name; ?><br>Nomor :<?php echo $biodata->letter_number; ?></li></ol>
      			<center style="font-size:16px;"><b>MEMBERI IZIN :</b></center>
      			<span style="padding-bottom: 50px;">Kepada<span style="padding-left:100px;">:</span></span><br>
      			<span style="padding-left:20px;"></span>Nama<span style="padding-right: 100px;"></span>: <b><?php echo $biodata->name; ?></b><br>
      			<span style="padding-left:20px;"></span>NIP<span style="padding-right: 113px;"></span>: <?php echo $biodata->nip; ?><br>
      			<span style="padding-left:20px;"></span>Pangkat/ Gol Ruangan : <?php echo $biodata->rank; ?><br>
      			<span style="padding-left:20px;"></span>Jabatan<span style="padding-right: 85px;"></span> : <?php echo $biodata->position; ?><br>
      			<span style="padding-left:20px;"></span>Unit Kerja<span style="padding-right: 78px;"></span>: <?php echo $biodata->work_unit; ?> <br>
      			<span style="padding-left:20px;"></span>Akreditasi<span style="padding-right: 78px;"></span>: <?php echo $biodata->accreditation; ?><!-- iyeu --><br><br>
      			<span style="padding-left:20px; text-align: justify;">Untuk Mengikuti Kegiatan Pendidikan di Perguruan Tinggi <?php echo $biodata->collage; ?> Pada Program Studi <?php echo $biodata->program_study; ?> Jurusan <?php echo $biodata->department; ?> Tahun Akademik <?php echo $biodata->year_of_graduation; ?>.</span><br>
                </b></p>
                <span style="padding-left:20px;">Dengan Ketentuan:</span>
      			<ol style="text-align: justify;">
      		   		<li>Pendidikan yang akan ditempuh dapat mendukung pelaksanaan tugas jabatan pada unit organisasi;</li>
      				<li>Izin belajar ini diberikan diluar jam kerja;</li>
      				<li>Tidak mengikuti program Pendidikan "Kelas Jauh", dan kegiatan pendidikan diselenggarakan oleh lembaga pendidikan Negeri/Swasta yang terakreditasi berdasarkan peraturan perundang-undangan yang berlaku;</li>
      				<li>Biaya ditanggung sepenuhnya oleh yang bersangkutan dan;</li>
      				<li>Tidak akan menuntut penyesuaian ijazah kecuali formasi memungkinkan dan sesuai peraturan perundang-undangan yang berlaku.</li>
      			</ol>
      			<div style="float:right; margin-left:300px;	">
      				<center>Dikeluarkan di Subang<br>
      				Pada Tanggal : {{date('d/m/Y')}} <br>
      				<b>a.n BUPATI SUBANG<br>
      				KEPALA BADAN KEPEGAWAIAN DAN<br>
      				PENGEMBANGAN SUMBER DAYA MANUSIA<br>
      				KABUPATEN SUBANG<br><br><br><br><br><br>
      				<u>Drs. H. CECEP SUPRIATIN, M.Si.</u><br>
      				Pembina Utama Muda (IV/c)<br>
      				NIP. 19591103 198401 2 001</b></center>
      			</div>
            <div class="" style="position: absolute; width: 85px;margin-top: 40px;margin-left:100px;">
              <?php echo '<img src="data:image/png;base64,' . DNS2D::getBarcodePNG($biodata->nip, "QRCODE") . '" alt="barcode"   />'; ?>
            </div>
            <div class="" style="margin-top:150px;">
              Tembusan Yth. : <br>
              <span style="position:absolute;">
                1. Bupati Subang (sebagai laporan) <br>
                2. Inspektur Inspektorat Daerah Kabupaten Subang <br>
                3. Kepala {{$biodata->requirement->agency->name}}
              </span>
            </div>

      		</div>
    </div>
</div>
</body>
</html>