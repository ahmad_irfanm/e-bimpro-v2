@extends('layouts.master')

@section('title')
    List Persyaratan {{$requirementType->name}}
@endsection

@section('requirement-tree')
    menu-open
@endsection

@section('requirement')
    active
@endsection

@section('menu')
    Persyaratan
@endsection

@section('requirement-' . $requirementType->id)
    active
@endsection

@section('breadcumb')
    <li class="breadcrumb-item"><a href="{{route('administrator.dashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item active">{{$requirementType->name}}</li>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="jumbotron d-flex align-items-center justify-content-center">
            <img src="{{asset('assets/img/illustration-5.svg')}}" class="illustration mr-5" alt="">
            <div class="col-md-5">
                <h4 class="mb-3"><span style="font-weight: normal">{{$requirementType->name}}</h4>
                <p style="margin-bottom: 0px;" class="text-muted">Dibawah ini merupakan list dinas yang telah mengirimkan biodata dengan menu persyaratan {{$requirementType->name}}. Kamu dapat melihat list biodata berdasarkan dinas dengan menekan tombol <span class="text-info">Lihat semua biodata</span>.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header align-items-center justify-content-between" style="display: grid; grid-template-columns: repeat(2, auto)">
                        <h3 class="card-title">List Dinas </h3>
                        <a href="{{route('administrator.requirement-types.show', ['requirement_type' => $requirementType->id])}}" class="btn text-bg text-primary text-bold">Detail tipe persyaratan <i class="ml-2 fa fa-arrow-right"></i></a>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example1" class="table table-striped">
                            <thead>
                            <tr>
                                <th>Dinas</th>
                                <th class="text-center">
                                    Jumlah Biodata
                                </th>
                                @if(Auth::user()->role == "admin1")
                                    <th class="text-center">Belum Diverifikasi</th>
                                    <th class="text-center">Terverifikasi ke-1</th>
                                    <th class="text-center">Terverifikasi ke-2</th>
                                @elseif(Auth::user()->role == "admin2")
                                    <th class="text-center">Terverifikasi ke-1</th>
                                    <th class="text-center">Terverifikasi ke-2</th>
                                @endif
                                <th class="text-center">Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($requirementType->agencies() as $agency)
                                    <tr>
                                        <td>{{$agency->name}}</td>
                                        <td class="text-center">
                                            {{$agency->requirements()->whereRequirementTypeId($requirementType->id)->whereIn('status', [2,3,4])->count()}}
                                        </td>
                                        @if(Auth::user()->role == "admin1")
                                            <td class="text-center">
                                                @if($agency->requirements()->whereRequirementTypeId($requirementType->id)->whereStatus(2)->count() == 0)
                                                    -
                                                @else
                                                <span class="badge badge-info">
                                                    {{$agency->requirements()->whereRequirementTypeId($requirementType->id)->whereStatus(2)->count()}} bioidata
                                                </span>
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                @if($agency->requirements()->whereRequirementTypeId($requirementType->id)->whereStatus(3)->count() == 0)
                                                    -
                                                @else
                                                    <span class="badge badge-primary">
                                                    {{$agency->requirements()->whereRequirementTypeId($requirementType->id)->whereStatus(3)->count()}} bioidata
                                                </span>
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                @if($agency->requirements()->whereRequirementTypeId($requirementType->id)->whereStatus(4)->count() == 0)
                                                    -
                                                @else
                                                <span class="badge badge-success">
                                                    {{$agency->requirements()->whereRequirementTypeId($requirementType->id)->whereStatus(4)->count()}} bioidata
                                                </span>
                                                @endif
                                            </td>
                                        @elseif(Auth::user()->role == "admin2")
                                            <td class="text-center">
                                                @if($agency->requirements()->whereRequirementTypeId($requirementType->id)->whereStatus(3)->count() == 0)
                                                    -
                                                @else
                                                <span class="badge badge-primary">
                                                    {{$agency->requirements()->whereRequirementTypeId($requirementType->id)->whereStatus(3)->count()}} bioidata
                                                </span>
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                @if($agency->requirements()->whereRequirementTypeId($requirementType->id)->whereStatus(4)->count() == 0)
                                                    -
                                                @else
                                                    <span class="badge badge-success">
                                                    {{$agency->requirements()->whereRequirementTypeId($requirementType->id)->whereStatus(4)->count()}} bioidata
                                                </span>
                                                @endif
                                            </td>
                                            @endif
                                        <td class="text-center">
                                            <a href="{{route('administrator.requirements.filterByAgency', ['requirement_type' => $requirementType->id, 'agency' => $agency->id]) }}" class="btn btn-xs text-info" title="Lihat Biodata">
                                                 Lihat semua biodata <i class="fa fa-arrow-right ml-1"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>
@endsection

@section('css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.bootstrap4.min.css">
@endsection

@section('js')
    <!-- DataTables -->
    <script src="{{asset('assets/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>
    <!-- page script -->
    <script>
        $(function () {
            $("#example1").DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
            });
        });
    </script>
@endsection

