@extends('layouts.master')

@section('title')
    List Persyaratan {{$requirementType->name}} dari {{$agency->name}}
@endsection

@section('requirement-tree')
    menu-open
@endsection

@section('requirement')
    active
@endsection

@section('requirement-' . $requirementType->id)
    active
@endsection

@section('breadcumb')
    <li class="breadcrumb-item"><a href="{{route('administrator.dashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="{{route('administrator.requirements.index', ['requirement_type' => $requirementType->id])}}">{{$requirementType->name}}</a></li>
    <li class="breadcrumb-item active"> {{$agency->name}}</li>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="jumbotron d-flex align-items-center justify-content-center">
            <img src="{{asset('assets/img/illustration-12.svg')}}" class="illustration mr-5" alt="">
            <div class="col-md-5">
                <h4 class="mb-3"><span style="font-weight: normal">{{$agency->name}}</h4>
                <p style="margin-bottom: 0px;" class="text-muted">Dibawah ini merupakan list biodata dengan tipe persyaratan {{$requirementType->name}} berdasarkan {{$agency->name}}. Kamu dapat melihat detail persyaratan dengan menekan tombol <span class="text-info">Lihat persyaratan</span>.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header d-flex align-items-center justify-content-between">
                        <h3 class="card-title">List Persyaratan {{$requirementType->name}}</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example" class="table table-striped dt-responsive nowrap" style="width:100%">
                            <thead>
                            <tr>
                                @foreach($requirementType->biodatas as $biodata)
                                    <th>{{$biodata->name}}</th>
                                @endforeach

                                <th class="text-center">Status</th>
                                <th class="text-center">Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($agency->requirements()->where('status', '!=', 1)->whereRequirementTypeId($requirementType->id)->get() as $requirement)
                                <tr>
                                    @foreach($requirementType->biodatas as $biodata)
                                        <td>{{$requirement->getContent($biodata->id)}}</td>
                                    @endforeach
                                    <td class="text-center">
                                        <span class="badge badge-{{convertColor($requirement->status)}}">
                                            {{convertStatus($requirement->status)}}
                                        </span>
                                    </td>
                                    <td class="text-center">
                                        <a href="{{route('administrator.requirements.update', ['requirement_type' => $requirementType->id, 'requirement' => $requirement->id]) }}" class="btn btn-xs text-info mr-2" title="Lihat Persyaratan">
                                             Lihat persyaratan <i class="fa fa-arrow-right ml-1"></i>
                                        </a>
                                        @if (Auth::user()->role == 'admin2')
                                            <a href="{{route('administrator.requirements.destroy', ['requirement_type' => $requirementType->id, 'requirement' => $requirement->id]) }}" class="btn btn-xs btn-danger mr-2 btn-delete" title="Hapus Persyaratan">
                                                <i class="fa fa-trash"></i> Hapus
                                            </a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>
@endsection

@section('css')
    <!-- DataTables -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
    <link rel="stylesheet" href="{{asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}">
    <link rel="stylesheet" href="{{asset('assets/plugins/datatables/buttons.dataTables.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/plugins/datatables/buttons.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/plugins/datatables/responsive.bootstrap4.min.css')}}">
@endsection

@section('js')
    <!-- DataTables -->
    <script src="{{asset('assets/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables/responsive.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables/buttons.flash.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables/jszip.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables/vfs_fonts.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables/buttons.print.min.js')}}"></script>
    <!-- page script -->
    <script>
        $(function () {
            $('#example').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
            $("#example1").DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
            });
        });
    </script>
@endsection

