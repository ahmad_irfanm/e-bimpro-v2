@extends('layouts.master')

@section('title')
    {{$requirementType->name}} {{$requirement->getContent(getMainFirst()->id)}}
@endsection

@section('requirement-tree')
    menu-open
@endsection

@section('requirement')
    active
@endsection

@section('requirement-' . $requirementType->id)
    active
@endsection

@section('breadcumb')
    <li class="breadcrumb-item"><a href="{{route('administrator.dashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="{{route('administrator.requirements.index', ['requirement_type' => $requirementType->id])}}">{{$requirementType->name}}</a></li>
    <li class="breadcrumb-item"><a href="{{route('administrator.requirements.filterByAgency', ['requirement_type' => $requirementType->id, 'agency' => $requirement->agency->id])}}">{{$requirement->agency->name}}</a></li>
    <li class="breadcrumb-item active">{{$requirement->getContent(getMainFirst()->id)}}</li>

@endsection

@section('content')
    <div class="container-fluid">

        <div class="jumbotron d-flex align-items-center justify-content-center">
            <img src="{{asset('assets/img/illustration-8.svg')}}" class="illustration mr-5" alt="">
            <div class="col-md-5">
                <h4 class="mb-3"><span style="font-weight: normal">{{$requirement->getContent(getMainFirst()->id)}}</h4>
                <p style="margin-bottom: 0px;" class="text-muted">
                    Kamu dapat melihat detail biodata disebelah dikiri, dan detail persyaratan disebelah kanan. Cek semua persyaratan kemudian verifikasi. Kemudian kamu dapat mendownload surat jika status persyaratan telah selesai dan tipe perysaratan ini memungkinkan untuk mendownload surat.
                </p>
            </div>
        </div>

        <div class="row">

            <div class="col-md-3">
                <div class="card card-primary">
                    <div class="card-header align-items-center justify-content-between" style="display: grid; grid-template-columns: repeat(2, auto)">
                        <h4 class="card-title">Biodata</h4>
                    </div>
                    <div class="card-body">
                            <strong>
                                Dinas
                            </strong>

                            <p class="text-muted">
                                {{$requirement->agency->name}}
                            </p>

                            <hr>

                            @include('layouts.biodata')

                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="card card-default">
                    <div class="card-header d-flex align-items-center justify-content-between">
                        <h4 class="card-title">Persyaratan {{$requirementType->name}}</h4>
                        <div>
                            <i class="fa fa-paper-plane mr-1"></i> <span data-toggle="tooltip" title="Status persyaratan" class="badge badge-{{convertColor($requirement->status)}}">{{convertStatus($requirement->status)}}</span>
                            <i class="fa fa-calendar ml-4 mr-1"></i> <span data-toggle="tooltip" title="Tanggal entri persyaratan" class="badge badge-default">{{$requirement->created_at->diffForHumans()}}</span>
                        </div>
                    </div>
                    <div class="card-body align-items-center justify-content-between" style="display: grid; grid-template-columns: 1fr 0.3fr 1fr 0.3fr 1fr">
                        <a href="" class="btn btn-step btn-{{$requirement->status == 2 || $requirement->status == 3 || $requirement->status == 4 ? convertColor($requirement->status) : 'default disabled'}} mr-2" style="pointer-events: none; user-select: none; font-size: 14px; white-space: nowrap; overflow: hidden;">
                            <i class="fa fa-box mr-2"></i> Diterima
                        </a>
                        <div class="border-step bg-{{$requirement->status == 3 || $requirement->status == 4 ? convertColor($requirement->status) : 'default'}}"></div>
                        <a href="" class="btn btn-step btn-{{$requirement->status == 3 || $requirement->status == 4 ? convertColor($requirement->status) : 'default disabled'}} mr-2" style="pointer-events: none; user-select: none;  font-size: 14px; white-space: nowrap; overflow: hidden">
                            <i class="fa fa-check mr-2"></i> Terverifikasi
                        </a>
                        <div class="border-step bg-{{$requirement->status == 4 ? convertColor($requirement->status) : 'default'}}"></div>
                        <a href="" class="btn btn-step btn-{{$requirement->status == 4 ? convertColor($requirement->status) : 'default disabled'}}" style="pointer-events: none; user-select: none;   font-size: 14px; white-space: nowrap; overflow: hidden">
                            <i class="fa fa-check-double mr-2"></i> Selesai
                        </a>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            @foreach($requirementType->requirement_forms as $requirementForm)
                                @php
                                    $requirementPost = $requirement->requirement_posts()->whereRequirementFormId($requirementForm->id)->first()
                                @endphp
                                <div class="col-md-12">
                                    <div class="card card-outline card-{{ $requirementPost ? 'success' : 'danger' }}">
                                        <div class="card-header">
                                            <h3 class="card-title">{{$requirementForm->name}}</h3>
                                        </div>
                                        <!-- /.card-header -->
                                        <div class="card-body" style="padding: 0px;">
                                            @if($requirementPost)
                                                <img src="{{asset('storage/' . $requirementPost->file)}}" alt="{{$requirementForm->name}}" style="width: 100%; height: auto; object-fit: cover">
                                            @else
                                                <form action="{{route('agency.requirement-posts.store', ['requirement_type' => $requirementType->id, 'requirement' => $requirement->id])}}" enctype="multipart/form-data" class="form-input">
                                                    <input type="file" class="file-input" name="image"/>
                                                    <input type="hidden" name="requirement_form_id" value="{{$requirementForm->id}}">
                                                </form>
                                            @endif
                                        </div>
                                        <div class="card-body justify-content-between align-items-center" style="display: grid; grid-template-columns: repeat(2, auto)">
                                            @if($requirementPost)
                                                <div>
                                                    <a download="{{$requirementForm->name}}.jpg" href="{{asset('storage/' . $requirementPost->file)}}" class="btn btn-default btn-xs"><i class="fa fa-download mr-1"></i> Download file</a>
                                                </div>
                                            @else
                                                <small class="text-info"><i class="fa fa-exclamation-circle mr-1"></i> Belum diinputkan</small>
                                            @endif
                                        </div>
                                        <!-- /.card-body -->
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="card-body d-flex align-items-center justify-content-end">
                        @if(Auth::user()->role == "admin1")
                            @if($requirement->status == 2)
                                <a href="{{route('administrator.requirements.verify', ['requirement' => $requirement->id])}}" class="btn btn-primary btn-lg animated btn-animated tada infinite" id="btn-verify"><i class="fa fa-check mr-1"></i> Verifikasi</a>
                            @elseif($requirement->status == 3)
                                <span class="badge badge-primary">Telah diverifikasi ke-1</span>
                            @elseif($requirement->status == 4)
                                <span class="badge badge-success">Telah diverifikasi ke-2</span>
                                @if($requirementType->print_file)
                                <a href="{{route('administrator.requirements.print', ['requirement' => $requirement->id])}}" class="btn btn-success btn-lg animated btn-animated tada infinite" style="position:fixed; right: 20px; bottom: 20px;"><i class="fa fa-download mr-1"></i> Download surat</a>
                                @endif
                            @endif
                        @else
                            @if($requirement->status == 3)
                                <a href="{{route('administrator.requirements.finish', ['requirement' => $requirement->id])}}" class="btn btn-success btn-lg animated btn-animated tada infinite" id="btn-verify"><i class="fa fa-check-double mr-1"></i> Verifikasi</a>
                            @elseif($requirement->status == 4)
                                <span class="badge badge-success">Telah diverifikasi ke-2</span>
                            @endif
                        @endif
                    </div>
                </div>
            </div>

        </div>
    </div>
    </div>

@endsection

@section('css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}">
    <style>
        .btn-step {
            width: 100%;
        }
        .border-step {
            height: 4px;
            background: #f5f6fb;
        }
    </style>
@endsection

@section('js')
    <!-- DataTables -->
    <script src="{{asset('assets/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>

    <!-- page script -->
    <script>

        $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
            });
        });

        $('#btn-verify').click(function (e) {
            e.preventDefault();

            let url = $(this).attr('href');
            let token = $('meta[name="csrf-token"]').attr('content');

            Swal.fire({
                title: 'Konfirmasi',
                text: "Apakah semua persyaratan telah dicek dan ingin memverifikasinya ?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, verifikasi sekarang',
                cancelButtonText: 'Jangan dulu'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        method: 'post',
                        url,
                        data: {_token: token},
                        success: function (response) {
                            Swal.fire({
                                title: 'Berhasil',
                                text: response.msg,
                                type: 'success'
                            }).then(result => {
                                if (result.value) {
                                    location.reload();
                                }
                            });
                        },
                        error: function (error) {
                            Swal.fire({
                                title: 'Opps.. ada yang error',
                                text: error.msg,
                                type: 'error'
                            }).then(result => {
                                if (result.value) {
                                    location.reload();
                                }
                            });
                        }
                    })
                }
            });

        });

    </script>
@endsection

