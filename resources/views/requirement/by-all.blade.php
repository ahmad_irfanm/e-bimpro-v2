@extends('layouts.master')

@section('title')
    List Persyaratan {{$requirementType->name}}
@endsection

@section('requirement-tree')
    menu-open
@endsection

@section('requirement')
    active
@endsection

@section('menu')
    <i class="fa fa-paperclip"></i> Persyaratan
@endsection

@section('requirement-' . $requirementType->id)
    active
@endsection

@section('breadcumb')
    <li class="breadcrumb-item"><a href="{{route('administrator.dashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item active">{{$requirementType->name}}</li>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="jumbotron">
            <h1 class="text-center">
                {{$requirementType->name}}
            </h1>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header align-items-center justify-content-between" style="display: grid; grid-template-columns: repeat(2, auto)">
                        <h3 class="card-title">List Persyaratan <b>{{$requirementType->name}}</b> </h3>
                        <a href="{{ route('administrator.requirements.index', ['requirement_type' => $requirementType->id]) }}" class="btn btn-info"><i class="fa fa-landmark mr-1"></i> Tampilkan Perdinas</a>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Dinas</th>
                                <th>NIP</th>
                                <th>Nama</th>
                                <th class="text-center">
                                    Status
                                </th>
                                <th class="text-center">Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($requirementType->requirements as $requirement)
                                    <tr>
                                        <td>{{$requirement->agency->name}}</td>
                                        <td>{{$requirement->biodata->nip}}</td>
										<td>{{$requirement->biodata->name}}</td>
										<td class="text-center">
											<span class="badge badge-{{convertColor($requirement->status)}}">
												{{convertStatus($requirement->status)}}
											</span>
										</td>
                                        <td class="text-center">
                                            <a href="{{route('administrator.requirements.update', ['requirement_type' => $requirementType->id, 'requirement' => $requirement->id]) }}" class="btn btn-xs btn-default mr-2" title="Lihat Persyaratan">
                                            <i class="fa fa-clipboard-list mr-1"></i> Lihat persyaratan
											</a>
											@if (Auth::user()->role == 'admin2')
											<a href="{{route('administrator.requirements.destroy', ['requirement_type' => $requirementType->id, 'requirement' => $requirement->id]) }}" class="btn btn-xs btn-danger mr-2 btn-delete" title="Hapus Persyaratan">
												<i class="fa fa-trash"></i> Hapus
											</a>
											@endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Dinas</th>
                                <th>NIP</th>
                                <th>Nama</th>
                                <th class="text-center">
                                    Status
                                </th>
                                <th class="text-center">Aksi</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>
@endsection

@section('css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.bootstrap4.min.css">
@endsection

@section('js')
    <!-- DataTables -->
    <script src="{{asset('assets/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>
    <!-- page script -->
    <script>
        $(function () {
            $("#example1").DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
            });
        });
    </script>
@endsection

