<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBiodatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('biodatas', function (Blueprint $table) {
            $table->char('id', 36)->primary()->unique();
            $table->string('name');
            $table->string('placeholder')->nullable();
            $table->enum('type', ['numeric', 'text', 'textarea', 'radio', 'date', 'year']);
            $table->text('choices')->nullable();
            $table->boolean('main')->default(0);
            $table->text('attributes')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('biodatas');
    }
}
