<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequirementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requirements', function (Blueprint $table) {
            $table->char('id', 36)->primary()->unique();
            $table->char('agency_id', 36);
            $table->char('requirement_type_id', 36);
            $table->enum('status', [1, 2, 3, 4]);
            $table->dateTime('sent_at')->nullable();
            $table->dateTime('first_verify_at')->nullable();
            $table->dateTime('second_verify_at')->nullable();
            $table->foreign('agency_id')->on('agencies')->references('id')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('requirement_type_id')->on('requirement_types')->references('id')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requirements');
    }
}
