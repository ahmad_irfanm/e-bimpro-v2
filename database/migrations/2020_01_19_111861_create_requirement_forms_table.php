<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequirementFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requirement_forms', function (Blueprint $table) {
            $table->char('id', 36)->primary()->unique();
            $table->char('requirement_type_id', 36);
            $table->string('name');
            $table->text('description')->nullable();
            $table->foreign('requirement_type_id')->on('requirement_types')->references('id')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requirement_forms');
    }
}
