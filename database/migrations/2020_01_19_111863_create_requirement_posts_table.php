<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequirementPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requirement_posts', function (Blueprint $table) {
            $table->char('id', 36)->primary()->unique();
            $table->char('requirement_id', 36);
            $table->char('requirement_form_id', 36);
            $table->string('file');
            $table->string('file_type');
            $table->double('file_size');
            $table->char('file_ext', 5);
            $table->foreign('requirement_id')->on('requirements')->references('id')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('requirement_form_id')->on('requirement_forms')->references('id')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requirement_posts');
    }
}
