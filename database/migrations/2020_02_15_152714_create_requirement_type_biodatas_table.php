<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequirementTypeBiodatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requirement_type_biodata', function (Blueprint $table) {
            $table->char('id', 36)->primary()->unique();
            $table->char('requirement_type_id', 36);
            $table->char('biodata_id', 36);
            $table->string('name_as')->nullable();
            $table->string('placeholder_as')->nullable();
            $table->tinyInteger('order');
            $table->foreign('requirement_type_id')->on('requirement_types')->references('id')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('biodata_id')->on('biodatas')->references('id')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requirement_type_biodatas');
    }
}
