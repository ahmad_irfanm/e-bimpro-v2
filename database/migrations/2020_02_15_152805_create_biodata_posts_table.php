<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBiodataPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('biodata_posts', function (Blueprint $table) {
            $table->char('id', 36)->primary()->unique();
            $table->char('requirement_id', 36);
            $table->char('biodata_id', 36);
            $table->string('content');
            $table->foreign('requirement_id')->on('requirements')->references('id')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('biodata_id')->on('biodatas')->references('id')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('biodata_posts');
    }
}
