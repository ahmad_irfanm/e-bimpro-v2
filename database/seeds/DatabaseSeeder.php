<?php

use Illuminate\Database\Seeder;
use App\User;
use Faker\Factory as Faker;

use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        foreach (\App\RequirementPost::all() as $item) {

            $item->update([
                'file' => substr($item->file, 7, strlen($item->file) - 7),
            ]);

        }


//        foreach (\App\Agency::all() as $data) {
//            $data->update([
//                'id' => $faker->uuid
//            ]);
//        }
//
//        foreach (\App\Biodata::all() as $data) {
//            $data->update([
//                'id' => $faker->uuid
//            ]);
//        }
//
//        foreach (\App\RequirementType::all() as $data) {
//            $data->update([
//                'id' => $faker->uuid
//            ]);
//        }
//
//        foreach (\App\RequirementForm::all() as $data) {
//            $data->update([
//                'id' => $faker->uuid
//            ]);
//        }

//        foreach (\App\Agency::all() as $agency) {
//            $agency->delete();
//        }
//
//        foreach (range(1, 50) as $i) {
//            $password = $faker->regexify('[0-9]{8,8}');
//
//            $agency = \App\Agency::create([
//                'name' => $faker->company,
//                'email' => $faker->email,
//                'password' => $password
//            ]);
//
//            $user = User::create([
//                'agency_id' => $agency->id,
//                'username' => strtoupper($faker->regexify('[A-Za-z0-9]{8,8}')),
//                'password' => bcrypt($password),
//                'role' => 'agency'
//            ]);
//        }
//
//        $user = User::create([
//            'agency_id' => $agency->id,
//            'username' => 'firstagency',
//            'password' => bcrypt('password'),
//            'role' => 'agency'
//        ]);

//        $faker = Faker::create();
//
//        foreach (range(1, 5) as $i) {
//            $name = $faker->name(NULL);
//            $abbr = '';
//            foreach (explode(' ', $name) as $word) {
//                $abbr .= $word[0];
//            }
//
//            \App\RequirementType::create([
//                'name' => $name,
//                'abbr' => $abbr
//            ]);
//        }

        // $this->call(UsersTableSeeder::class);
    }
}
