<?php

use Illuminate\Database\Seeder;
use App\RequirementType;
use App\BiodataPost;
use Faker\Factory as Faker;

class RequirementTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // define
        $faker = Faker::create('id_ID');
        $requirementTypeId = '2d093e78-c955-31c6-88c4-bdcbb73e2cee';
        $agencyId = '0f4b52d8-7a8d-3a72-96ce-4230afdff09a';

        // get requirement type
        $requirementType = RequirementType::find($requirementTypeId);
        $biodatas = $requirementType->biodatas;

        // data amount
        $amount = 5;
        foreach (range(1, 5) as $i) {
            // post biodata
            $requirement = \App\Requirement::create([
                'requirement_type_id' => $requirementType->id,
                'status' => 1,
                'agency_id' => $agencyId
            ]);

            foreach ($biodatas as $biodata) {
                $content = "";
                if ($biodata->type == "text" || $biodata->type == "textarea") {
                    $content = $faker->text($maxNbChars = 10);
                } elseif ($biodata->type == "numeric") {
                    $content = $faker->numberBetween($min = 1000, $max = 9000);
                } elseif ($biodata->type == "radio") {
                    $content = $faker->randomElement($array = explode(PHP_EOL, $biodata->choices));
                }

                $biodataPost = BiodataPost::create([
                    'biodata_id' => $biodata->id,
                    'requirement_id' => $requirement->id,
                    'content' => $content
                ]);
            }
        }


    }
}
