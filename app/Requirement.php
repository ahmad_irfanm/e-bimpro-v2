<?php

namespace App;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;
use LasseRafn\InitialAvatarGenerator\InitialAvatar;

class Requirement extends Model
{
    public $incrementing = false;
    protected $keyType = 'string';

    //
    protected $guarded = [];

//    protected $dates = ['first_verify_at', 'second_verify_at', 'sent_at'];

    public function agency () {
        return $this->belongsTo('App\Agency');
    }

    public function getContent ($biodata_id, $original = false) {
        $biodataPost = $this->biodata_posts()->where('biodata_id', $biodata_id)->first();
        if (!$biodataPost) {
            return '-';
        }

        $content = $biodataPost->content;

        if (!$original) {
            if ($biodataPost->biodata->type == "date") {
                $content = dateLetter($content);
            }
        }

        return $content;
    }

    public function getAvatar ($size = false) {
        $avatar = new InitialAvatar;

        if (!$size)
            $size = 30;

        return $avatar->name($this->getContent(getMainFirst()->id))->autoFont()->rounded()
            ->background('#8BC34A')
            ->color('#fff')
            ->size($size)
            ->generateSvg()->toXMLString();
    }

    public function requirement_posts () {
        return $this->hasMany('App\RequirementPost');
    }

    public function biodata () {
        return $this->hasOne('App\Biodata');
    }

    public function biodata_posts () {
        return $this->hasMany('App\BiodataPost');
    }

    public function requirement_type ()
    {
        return $this->belongsTo('App\RequirementType');
    }
}
