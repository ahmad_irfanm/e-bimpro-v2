<?php

namespace App;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;

class Biodata extends Model
{
    public $incrementing = false;
    protected $keyType = 'string';

    //
    protected $guarded = [];

    public function requirement_types () {
        return $this->belongsToMany('App\RequirementType', 'requirement_type_biodata', 'biodata_id', 'requirement_type_id')
            ->withPivot('order');
    }

    public function biodata_posts () {
        return $this->hasMany('App\BiodataPost');
    }

}
