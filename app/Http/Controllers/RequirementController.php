<?php

namespace App\Http\Controllers;

use App\Agency;
use App\Requirement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\Console\Helper\Helper;
use Validator;
use App\RequirementType;
use Novay\WordTemplate\WordTemplate;

class RequirementController extends Controller
{
    public function __construct(Requirement $requirement)
    {
        $this->requirement = $requirement;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(RequirementType $requirementType)
    {
        //
        return view('requirement.index', [
            'requirementType' => $requirementType
        ]);
    }

	public function byAll(RequirementType $requirementType)
    {
        //
        return view('requirement.by-all', [
            'requirementType' => $requirementType
        ]);
    }

    public function filterByAgency (RequirementType $requirementType, Agency $agency)
    {
        return view('requirement.filter-by-agency', [
            'requirementType' => $requirementType,
            'agency' => $agency
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Requirement  $requirement
     * @return \Illuminate\Http\Response
     */
    public function show(RequirementType $requirementType, Requirement $requirement)
    {
        //
        return view('requirement.show', [
            'requirementType' => $requirementType,
            'requirement' => $requirement
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Requirement  $requirement
     * @return \Illuminate\Http\Response
     */
    public function edit(Requirement $requirement)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Requirement  $requirement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Requirement $requirement)
    {
        //
    }

    public function verify (Request $request, Requirement $requirement) {
        $action = $requirement->update([
            'status' => 3,
            'first_verify_at' => date('Y-m-d H:i:s')
        ]);

        return response()->json(['msg' => 'Persyaratan berhasil diverifikasi'], 200);
    }

    public function finish (Request $request, Requirement $requirement) {
        $action = $requirement->update([
            'status' => 4,
            'second_verify_at' => date('Y-m-d H:i:s')
        ]);

        return response()->json(['msg' => 'Persyaratan berhasil diverifikasi'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Requirement  $requirement
     * @return \Illuminate\Http\Response
     */
    public function destroy(RequirementType $requirementType, Requirement $requirement)
    {
        // unlink file
        foreach ($requirement->requirement_posts as $requirement_post) {
            Storage::delete($requirement_post->file);
        }

		// validation success
        $action = $requirement->delete();

        // if action success
        if ($action) {
            return response()->json(['msg' => 'Persyaratan berhasil dihapus'], 200);
        } else {
            return response()->json(['msg' => 'Persyaratan gagal dihapus. Silahkan coba kembali.'], 404);
        }
    }

    public function print (Requirement $requirement) {
        $requirementType = $requirement->requirement_type;
        $file_name = $requirementType->print_file;

        $file = \Illuminate\Support\Facades\URL::asset('/storage/app/' . $file_name);

        $array = [];
        foreach ($requirement->requirement_type->biodatas as $biodata) {
            $biodata->name = str_replace(' ', '_', $biodata->name);
            $biodata->name = str_replace('/', '_', $biodata->name);
            $biodata->name = str_replace('.', '', $biodata->name);

            $array['[' . strtoupper($biodata->name) . ']'] = $requirement->getContent($biodata->id);
        }

        $array['[DINAS]'] = $requirement->agency->name;


        $nama_file = $requirementType->name . ' - ' . $requirement->getContent(getMainFirst()->id) . '.doc';

        $app = new WordTemplate;
        return $app->export($file, $array, $nama_file);
    }
}
