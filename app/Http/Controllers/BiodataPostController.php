<?php

namespace App\Http\Controllers;

use App\Biodata;
use App\BiodataPost;
use App\Requirement;
use App\RequirementType;
use Illuminate\Http\Request;
use Auth;
use Validator;

class BiodataPostController extends Controller
{
    function __construct(Requirement $requirement, BiodataPost $biodataPost, Biodata $biodata)
    {
        $this->requirement = $requirement;
        $this->biodataPost = $biodataPost;
        $this->biodata = $biodata;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(RequirementType $requirementType)
    {
        //
        return view('biodata-post.index', [
            'requirementType' => $requirementType
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(RequirementType $requirementType)
    {
        //
        return view('biodata-post.form', [
            'requirementType' => $requirementType
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, RequirementType $requirementType)
    {
        $rules = [];
        foreach ($request->except('_token') as $biodata_id => $content) {
            $rules[$biodata_id] = 'required';

            $id = explode('input_', $biodata_id)[1];
            $type = $this->biodata->find($id)->type;

            if ($type == "numeric")
                $rules[$biodata_id] .= '|numeric';
        }

        // validations
        $validation = Validator::make($request->all(), $rules);


        // if validtaion fails
        if ($validation->fails())
            return redirect()->back()->withErrors($validation)->withInput($request->all());

        // if validation success
        $requirement = $this->requirement->create([
            'requirement_type_id' => $requirementType->id,
            'agency_id' => Auth::user()->agency->id,
            'status' => 1
        ]);

        foreach ($request->except(['_token']) as $biodata_id => $content) {
            $id = explode('input_', $biodata_id)[1];

            $action = $this->biodataPost->create([
                'requirement_id' => $requirement->id,
                'biodata_id' => $id,

                'content' => $content
            ]);
        }

        // if action success
        if ($action) {
            return redirect()->route('agency.requirements.show', ['requirement_type' => $requirementType->id, 'requirement' => $requirement->id])->with('msg', 'Biodata berhasil dibuat');
        } else {
            return redirect()->back()->with('msgError', 'Biodata gagal dibuat. Silahkan coba kembali.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BiodataPost  $biodataPost
     * @return \Illuminate\Http\Response
     */
    public function show(RequirementType $requirementType, Requirement $requirement)
    {
        if ($requirement->agency->id != Auth::user()->agency->id)
            abort(403);
        //
        return view('biodata-post.show', [
            'requirementType' => $requirementType,
            'requirement' => $requirement
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BiodataPost  $biodataPost
     * @return \Illuminate\Http\Response
     */
    public function edit(RequirementType $requirementType, Requirement $requirement)
    {
        if ($requirement->agency->id != Auth::user()->agency->id)
            abort(403);

        //
        return view('biodata-post.form', [
            'requirementType' => $requirementType,
            'requirement' => $requirement
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BiodataPost  $biodataPost
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RequirementType $requirementType, Requirement $requirement)
    {
        if ($requirement->agency->id != Auth::user()->agency->id)
            abort(403);


        $rules = [];
        foreach ($request->except(['_token', '_method']) as $biodata_id => $content) {
            $rules[$biodata_id] = 'required';

            $id = explode('input_', $biodata_id)[1];
            $type = $this->biodata->find($id)->type;

            if ($type == "numeric")
                $rules[$biodata_id] .= '|numeric';
        }

        // validations
        $validation = Validator::make($request->all(), $rules);


        // if validtaion fails
        if ($validation->fails())
            return redirect()->back()->withErrors($validation)->withInput($request->all());

        foreach ($request->except(['_token', '_method']) as $biodata_id => $content) {
            $id = explode('input_', $biodata_id)[1];

            $action = $this->biodataPost->where('biodata_id', $id)->where('requirement_id', $requirement->id)
            ->update([
                'content' => $content
            ]);
        }

        // if action success
        if ($action) {
            return redirect()->route('agency.requirements.show', ['requirement_type' => $requirementType->id, 'requirement' => $requirement->id])->with('msg', 'Biodata berhasil diperbarui');
        } else {
            return redirect()->back()->with('msgError', 'Biodata gagal diperbarui. Silahkan coba kembali.');
        }
    }

    public function sent (Request $request, Requirement $requirement) {
        // validations agency cannot access another agency data
        if (Auth::user()->agency->id != $requirement->agency_id) {
            return response()->json(['msg' => 'ID Dinas tidak sesuai'], 401);
        }

        $action = $requirement->update([
            'status' => 2,
            'sent_at' => date('Y-m-d H:i:s')
        ]);

        return response()->json(['msg' => 'Persyaratan berhasil dikirim'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BiodataPost  $biodataPost
     * @return \Illuminate\Http\Response
     */
    public function destroy(BiodataPost $biodataPost)
    {
        //
    }
}
