<?php

namespace App\Http\Controllers;

use App\RequirementType;
use App\Rules\MatchOldPassword;
use function foo\func;
use Illuminate\Http\Request;
use App\User;
use App\Requirement;
use Auth;
use Illuminate\Support\Facades\Hash;
use Validator;

class AdministratorController extends Controller
{
    //
    public function __construct(Requirement $requirement, RequirementType $requirementType)
    {
        $this->requirement = $requirement;
        $this->requirementType = $requirementType;
    }

    // dashboard
    public function dashboard () {
        $data['requirementTypes'] = $this->requirementType->all();
        $data['pie'] = [
            $this->requirement->whereStatus(2)->count(),
            $this->requirement->whereStatus(3)->count(),
            $this->requirement->whereStatus(4)->count()
        ];
        $data['pie'] = json_encode($data['pie']);

        $now = date('Y-m-d');
        $line = [];
        $labels = [];
        for ($i = 7; $i > 0; $i--) {
            $date = date('Y-m-d', strtotime('-' . $i . ' days', strtotime($now)));
            $line[] = $this->requirement->where('status', '!=', 1)->whereDate('created_at', '=', $date)->count();
            $labels[] = dateLetter($date);
        }
        $data['line'] = json_encode($line);
        $data['labels'] = json_encode($labels);

        return view('administrator.dashboard', $data);
    }

    // profile
    public function profile (Request $request) {
        if ($request->isMethod('post'))
            return $this->updateProfile($request);

        // get activities
        $results = [];
        $activities = $this->requirement->whereIn('status', [2, 3, 4])->get();

        $typeDate = ['sent_at', 'first_verify_at', 'second_verify_at'];
        foreach ($activities as $requirement) {

            foreach (range(1, $requirement->status - 1) as $key => $i) {
                $result = new \stdClass();
                $result->datetime = $requirement[$typeDate[$key]];
                $result->status = $i + 1;
                $result->requirement = $requirement;
                $results[] = $result;
            }

        }

        $results = collect($results)->sortByDesc(function ($activity, $key) {
            return $activity->datetime;
        })->values()->toArray();

        return view('administrator.profile', [
            'activities' => $results
        ]);
    }

    public function updateProfile ($request) {
        // validations
        $validation = Validator::make($request->all(), [
            'username' => 'required|unique:users,username'
        ]);

        // if validation fails
        if ($validation->fails())
            return redirect()->back()->withErrors($validation)->withInput($request->all());

        // if validation success
        $action = Auth::user()->update([
            'username' => $request->username
        ]);

        // if action success
        if ($action) {
            return redirect()->back()->with('msg', 'Berhasil diperbarui');
        } else {
            return redirect()->back()->with('msgError', 'Gagal diperbarui. Silahkan coba kembali');
        }

    }

    function changePassword (Request $request) {
        // validations
        $validation = Validator::make($request->all(), [
            'old_password' => ['required', new MatchOldPassword],
            'new_password' => 'required',
            'new_password_confirmation' => 'required|same:new_password'
        ]);

        // if validation fails
        if ($validation->fails())
            return redirect()->back()->withErrors($validation)->withInput($request->all());

        // if validation success
        $action = Auth::user()->update([
            'password' => Hash::make($request->new_password)
        ]);

        // if action success
        if ($action) {
            return redirect()->back()->with('msg', 'Password berhasil diperbarui');
        } else {
            return redirect()->back()->with('msgError', 'Password gagal diperbarui. Silahkan coba kembali');
        }
    }

}
