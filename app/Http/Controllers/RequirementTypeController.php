<?php

namespace App\Http\Controllers;

use App\Biodata;
use App\RequirementType;
use Validator;
use Illuminate\Http\Request;

class RequirementTypeController extends Controller
{
    public function __construct(RequirementType $rt, Biodata $biodata)
    {
        $this->rt = $rt;
        $this->biodata = $biodata;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $requirement_types = $this->rt->orderBy('id', 'DESC')->get();

        //
        return view('requirement-type.index', [
            'requirement_types' => $requirement_types
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('requirement-type.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validations
        $validation = Validator::make($request->all(), [
            'name' => 'required',
            'print_file' => 'mimes:rtf'
        ]);

        // validation fails
        if ($validation->fails())
            return redirect()->back()->withInput($request->all())->withErrors($validation);

        // define abbr
        $abbr = "";
        if ($request->abbr)
            $abbr = $request->abbr;
        else {
            foreach (explode(' ', $request->name) as $word) {
                $abbr .= $word[0];
            }
        }
        $abbr = strtoupper($abbr);

        // upload file template
        $print_file = NULL;
        if ($request->file('print_file')) {
            $print_file = $request->file('print_file');
            $print_file = $print_file->store('public/letter-templates/');
        }

        // validation success
        $action = $this->rt->create([
            'name' => $request->name,
            'abbr' => $abbr,
            'print_file' => $print_file
        ]);

        // if action success
        if ($action) {
            return redirect()->route('administrator.requirement-forms.index', ['requirement_type' => $action->id])->with('msg', 'Tipe persyaratan berhasil ditambahkan');
        } else {
            return redirect()->back()->with('msgError', 'Tipe persyaratan gagal ditambahkan. Silahkan coba kembali.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RequirementType  $requirementType
     * @return \Illuminate\Http\Response
     */
    public function show(RequirementType $requirementType)
    {
        //
        return view('requirement-type.show', [
            'requirementType' => $requirementType
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RequirementType  $requirementType
     * @return \Illuminate\Http\Response
     */
    public function edit(RequirementType $requirementType)
    {
        //
        return view('requirement-type.form', [
            'requirement_type' => $requirementType
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RequirementType  $requirementType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RequirementType $requirementType)
    {
        // validations
        $validation = Validator::make($request->all(), [
            'name' => 'required',
            'print_file' => 'mimes:rtf'
        ]);

        // validation fails
        if ($validation->fails())
            return redirect()->back()->withInput($request->all())->withErrors($validation);

        // define abbr
        $abbr = "";
        if ($request->abbr)
            $abbr = $request->abbr;
        else {
            foreach (explode(' ', $request->name) as $word) {
                $abbr .= $word[0];
            }
        }
        $abbr = strtoupper($abbr);

        $print_file = $requirementType->print_file;
        if ($request->file('print_file')) {
            $print_file = $request->file('print_file');
            $print_file = $print_file->store('public/letter-templates/');
        }

        // validation success
        $action = $requirementType->update([
            'name' => $request->name,
            'abbr' => $abbr,
            'print_file' => $print_file
        ]);

        // if action success
        if ($action) {
            return redirect()->back()->with('msg', 'Tipe persyaratan berhasil diperbarui');
        } else {
            return redirect()->back()->with('msgError', 'Tipe persyaratan gagal diperbarui. Silahkan coba kembali.');
        }
    }

    public function updateOrderBiodata (Request $request, RequirementType $requirementType) {
        foreach ($request->list as $i => $list) {
            $requirementType->biodatas()->updateExistingPivot($list, [
                'order' => $i+1
            ]);
        }
    }

    public function addBiodata (Request $request, RequirementType $requirementType) {
        $order = 1;
        if ($requirementType->biodatas()->count() > 0) {
            $last = $requirementType->biodatas()->orderBy('requirement_type_biodata.order', 'desc')->first()->pivot->order;
            $order = $last + 1;
        }

        $requirementType->biodatas()->attach($request->biodata_id, ['order' => $order]);

        return [
            'method' => 'add',
            'data' => $this->biodata->find($request->biodata_id)
        ];
    }

    public function removeBiodata (Request $request, RequirementType $requirementType) {
        $requirementType->biodatas()->detach($request->biodata_id);

        return [
            'method' => 'remove'
        ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RequirementType  $requirementType
     * @return \Illuminate\Http\Response
     */
    public function destroy(RequirementType $requirementType)
    {
        // validations
        if ($requirementType->requirements->count() > 0) {
            return response()->json(['message' => 'Tipe persyaratan ini memiliki data biodata persyaratan dari beberapa dinas. Hapus biodata persyaratan terlebih dahulu untuk menghapus.'], 401);
        }

        // validation success
        $action = $requirementType->delete();

        // if action success
        if ($action) {
            return response()->json(['msg' => 'Tipe persyaratan berhasil dihapus'], 200);
        } else {
            return response()->json(['msg' => 'Tipe persyaratan gagal dihapus. Silahkan coba kembali.'], 404);
        }
    }
}
