<?php

namespace App\Http\Controllers;

use App\Requirement;
use App\RequirementForm;
use App\RequirementPost;
use App\RequirementType;
use Illuminate\Http\Request;
use App\Biodata;
use Illuminate\Support\Facades\Storage;
use Validator;

class RequirementPostController extends Controller
{
    function __construct(RequirementPost $requirementPost) {
        $this->requirementPost = $requirementPost;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, RequirementType $requirementType, Requirement $requirement)
    {

        // validations
        $validation = Validator::make($request->all(), [
            'image' => 'required|mimes:png,jpg,jpeg,pdf|max:2000'
        ], [
            'image.required' => 'Persyaratan harus diisi',
            'image.mimes' => 'Tipe file harus bentuk JPG, PNG, atau PDF',
            'image.max' => 'File harus kurang dari 2MB'
        ]);
        if ($validation->fails()) {
            return response()->json(['errors' => $validation->errors()], 401);
        }

        $requirement_form = RequirementForm::find($request->requirement_form_id);

        //
        $image = $request->file('image');
        $extension = $image->getClientOriginalExtension();
        $filename_to_store =  strtolower(str_replace('.', '', str_replace('/', '_', str_replace(' ', '_', $requirement_form->name)))) . '.' . $extension;
        $file  = $image->storeAs('public/requirements/' . $requirementType->abbr . '/' . str_replace('.', '', $requirement->getContent(getMainFirst()->id)), $filename_to_store);

        // success
        $action = $this->requirementPost->create([
            'requirement_id' => $requirement->id,
            'requirement_form_id' => $request->requirement_form_id,
            'file' => $file,
            'file_type' => $image->getMimeType(),
            'file_size' => $image->getSize(),
            'file_ext' => '.' . $image->getClientOriginalExtension()
        ]);

        return response()->json([
            'message' => 'File persyaratan berhasil diupload',
            'image' => storage_path('app/' . $file)
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RequirementPost  $requirementPost
     * @return \Illuminate\Http\Response
     */
    public function show(RequirementPost $requirementPost)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RequirementPost  $requirementPost
     * @return \Illuminate\Http\Response
     */
    public function edit(RequirementPost $requirementPost)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RequirementPost  $requirementPost
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RequirementType $requirementType, Requirement $requirement, RequirementPost $requirementPost)
    {
        // validations
        $validation = Validator::make($request->all(), [
            'image' => 'required|mimes:jpg,png,jpeg,pdf|max:2000'
        ], [
            'image.required' => 'Persyaratan harus diisi',
            'image.mimes' => 'Tipe file harus bentuk JPG, PNG, atau PDF',
            'image.max' => 'File harus kurang dari 2MB'
        ]);
        if ($validation->fails()) {
            return response()->json(['errors' => $validation->errors()], 401);
        }

        // upload new file
        $image = $request->file('image');
        if ($image) {
            $requirement_form = RequirementForm::find($requirementPost->requirement_form_id);
            $extension = $image->getClientOriginalExtension();
            $filename_to_store =  strtolower(str_replace('.', '', str_replace('/', '_', str_replace(' ', '_', $requirement_form->name)))) . '.' . $extension;
            $file  = $image->storeAs('public/requirements/' . $requirementType->abbr . '/' . str_replace('.', '', $requirement->getContent(getMainFirst()->id)), $filename_to_store);
        }

        // success
        $action = $requirementPost->update([
            'file' => $file,
            'file_type' => $image->getMimeType(),
            'file_size' => $image->getSize(),
            'file_ext' => '.' . $image->getClientOriginalExtension()
        ]);

        return response()->json(['message' => 'File persyaratan berhasil diperbarui', 'image' => storage_path($file)], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RequirementPost  $requirementPost
     * @return \Illuminate\Http\Response
     */
    public function destroy(RequirementType $requirementType, Requirement $requirement, RequirementPost $requirementPost)
    {
        // validation success
        Storage::delete($requirementPost->file);
        $action = $requirementPost->delete();

        // if action success
        if ($action) {
            return response()->json(['msg' => 'File Persyaratan berhasil dihapus'], 200);
        } else {
            return response()->json(['msg' => 'File Persyaratan gagal dihapus. Silahkan coba kembali.'], 404);
        }
    }
}
