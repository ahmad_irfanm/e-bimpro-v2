<?php

namespace App\Http\Controllers;

use App\Biodata;
use App\RequirementType;
use Illuminate\Http\Request;
use Auth;
use Validator;

class BiodataController extends Controller
{
    function __construct(Biodata $biodata, RequirementType $requirementType)
    {
        $this->biodata = $biodata;
        $this->requirementType = $requirementType;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $biodatas = $this->biodata->get();
        //
        return view('biodata.index', [
            'biodatas' =>  $biodatas
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        //
        return view('biodata.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // rules
        $rules = [
            'name' => 'required|unique:biodatas,name'
        ];

        if ($request->type == "radio")
            $rules['choices'] = 'required';

        // validations
        $validation = Validator::make($request->all(), $rules);

        // validation fails
        if ($validation->fails())
            return redirect()->back()->withInput($request->all())->withErrors($validation);

        // validation success
        if (!$request->placeholder || empty($request->placeholder))
            $request->placeholder = $request->name;


        // insert
        $action = $this->biodata->create([
            'name' => $request->name,
            'placeholder' => $request->placeholder,
            'type' => $request->type,
            'choices' => $request->choices,
            'attributes' => NULL
        ]);

        // insert requireent type are related
        foreach ($request->requirement_type_biodata as $rtb) {
            $requirementType = $this->requirementType->find($rtb);
            $order = 1;

            if ($requirementType->biodatas()->count() > 0) {
                $last = $requirementType->biodatas()->orderBy('requirement_type_biodata.order', 'desc')->first()->pivot->order;
                $order = $last + 1;
            }

            $action->requirement_types()->attach($rtb, ['order' => $order]);
        }

        // success
        if ($action) {
            return redirect()->back()->with('msg', 'Form Biodata berhasil ditambahkan');
//            return redirect()->route('administrator.biodatas.show', ['biodata' => $action->id])->with('msg', 'Form Biodata berhasil ditambahkan');
        } else {
            return redirect()->back()->with('msgError', 'Form Biodata gagal ditambahkan. Silahkan coba kembali.');
        }

    }

    public function addRequirementType (Request $request, Biodata $biodata) {
        $order = 1;
        if ($biodata->requirement_types()->count() > 0) {
            $last = $biodata->requirement_types()->orderBy('requirement_type_biodata.order', 'desc')->first()->pivot->order;
            $order = $last + 1;
        }

        $biodata->requirement_types()->attach($request->requirement_type_id, ['order' => $order]);

        return [
            'method' => 'add',
            'data' => $this->requirementType->find($request->requirement_type_id),
            'requiremnet _type_id' => $request->requirement_type_id
        ];
    }

    public function removeRequirementType (Request $request, Biodata $biodata) {
        $biodata->requirement_types()->detach($request->requirement_type_id);

        return [
            'method' => 'remove'
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Biodata  $biodata
     * @return \Illuminate\Http\Response
     */
    public function show(Biodata $biodata)
    {
        //
        return view('biodata.show', [
            'biodata' => $biodata
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Biodata  $biodata
     * @return \Illuminate\Http\Response
     */
    public function edit(Biodata $biodata)
    {
        //
        return view('biodata.form', [
            'biodata' => $biodata
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Biodata  $biodata
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Biodata $biodata)
    {
        //
        // rules
        $rules = [
            'name' => 'required|unique:biodatas,name,' . $biodata->id
        ];

        if ($request->type == "radio")
            $rules['choices'] = 'required';

        // validations
        $validation = Validator::make($request->all(), $rules);

        // validation fails
        if ($validation->fails())
            return redirect()->back()->withInput($request->all())->withErrors($validation);

        // validation success
        if (!$request->placeholder || empty($request->placeholder))
            $request->placeholder = $request->name;


        // insert
        $action = $biodata->update([
            'name' => $request->name,
            'placeholder' => $request->placeholder,
            'type' => $request->type,
            'choices' => $request->choices,
            'attributes' => NULL
        ]);

        // success
        if ($action) {
//            return redirect()->route('administrator.biodatas.show', ['biodata' => $biodata->id])->with('msg', 'Form Biodata berhasil ditambahkan');
            return redirect()->back()->with('msg', 'Form Biodata berhasil ditambahkan');
        } else {
            return redirect()->back()->with('msgError', 'Form Biodata gagal ditambahkan. Silahkan coba kembali.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Biodata  $biodata
     * @return \Illuminate\Http\Response
     */
    public function destroy(Biodata $biodata)
    {
        // validations
        if ($biodata->biodata_posts->count() > 0) {
            return response()->json([
                'msg' => 'Form biodata gagal dihapus karena memiliki persyaratan dari dinas'
            ], 401);
        } elseif ($biodata->main == 1) {
            return response()->json([
                'msg' => 'Form biodata gagal dihapus karena ini merupakan form biodata utama'
            ], 401);
        }

        //
        $biodata->delete();

        return response()->json([
            'msg' => $biodata->biodata_posts->count()
        ], 200);
    }
}
