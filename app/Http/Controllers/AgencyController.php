<?php

namespace App\Http\Controllers;

use App\Agency;
use App\Requirement;
use App\User;
use Faker\Factory;
use Illuminate\Http\Request;
use Validator;
use App\RequirementType;
use Auth;
use Illuminate\Support\Facades\Hash;
use App\Rules\MatchOldPassword;

class AgencyController extends Controller
{
public function __construct(Agency $agency, RequirementType $requirementType, Requirement $requirement)
    {
        $this->agency = $agency;
        $this->requirementType = $requirementType;
        $this->requirement = $requirement;
    }

    public function dashboard () {
        $requirementTypes = $this->requirementType->get();

        $pie = [
            Auth::user()->agency->requirements()->whereStatus(1)->count(),
            Auth::user()->agency->requirements()->whereStatus(2)->count(),
            Auth::user()->agency->requirements()->whereStatus(3)->count(),
            Auth::user()->agency->requirements()->whereStatus(4)->count()
        ];
        $pie = json_encode($pie);

        $now = date('Y-m-d');
        $line = [];
        $labels = [];
        for ($i = 7; $i > 0; $i--) {
            $date = date('Y-m-d', strtotime('-' . $i . ' days', strtotime($now)));
            $line[] = Auth::user()->agency->requirements()->whereDate('created_at', '=', $date)->count();
            $labels[] = dateLetter($date);
        }
        $line = json_encode($line);
        $labels = json_encode($labels);

        return view('agency.dashboard',[
            'requirementTypes' => $requirementTypes,
            'pie' => $pie,
            'line' => $line,
            'labels' => $labels
        ]);
    }

    public function guide () {
        return view('agency.guide');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $agencies = $this->agency->orderBy('created_at', 'desc')->get();

        //
        return view('agency.index', [
            'agencies' => $agencies
        ]);
    }

    public function profile (Request $request) {
        if ($request->isMethod('post'))
            $this->updateProfile($request);

        $activities = Auth::user()->agency->requirements()->get();

        $results = [];
        $typeDate = ['created_at', 'sent_at', 'first_verify_at', 'second_verify_at'];
        foreach ($activities as $requirement) {

            foreach (range(1, $requirement->status) as $key => $i) {
                $result = new \stdClass();
                $result->datetime = $requirement[$typeDate[$key]];
                $result->status = $i;
                $result->requirement = $requirement;
                $results[] = $result;
            }

        }

        $results = collect($results)->sortByDesc(function ($activity, $key) {
            return $activity->datetime;
        })->values()->toArray();

        return view('agency.profile', [
            'activities' => $results
        ]);
    }

    public function updateProfile ($request) {
        // validations
        $validation = Validator::make($request->all(), [
            'username' => 'required|unique:users,username,' . Auth::user()->id,
            'agency_email' => 'required|unique:agencies,email,' . Auth::user()->agency->id,
            'agency_name' => 'required'
        ]);

        // if validation fails
        if ($validation->fails())
            return redirect()->back()->withErrors($validation)->withInput($request->all());

        // if validation success
        Auth::user()->agency->update([
            'email' => $request->agency_email,
            'name' => $request->agency_name
        ]);
        $action = Auth::user()->update([
            'username' => $request->username
        ]);

        // if action success
        if ($action) {
            return redirect()->back()->with('msg', 'Berhasil diperbarui');
        } else {
            return redirect()->back()->with('msgError', 'Gagal diperbarui. Silahkan coba kembali');
        }
    }

    function changePassword (Request $request) {
        // validations
        $validation = Validator::make($request->all(), [
            'old_password' => ['required', new MatchOldPassword],
            'new_password' => 'required|min:8',
            'new_password_confirmation' => 'required|same:new_password'
        ]);

        // if validation fails
        if ($validation->fails())
            return redirect()->back()->withErrors($validation)->withInput($request->all());

        // if validation success
        Auth::user()->agency->update([
            'password' => NULL
        ]);
        $action = Auth::user()->update([
            'password' => Hash::make($request->new_password)
        ]);

        // if action success
        if ($action) {
            return redirect()->back()->with('msg', 'Password berhasil diperbarui');
        } else {
            return redirect()->back()->with('msgError', 'Password gagal diperbarui. Silahkan coba kembali');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('agency.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validations
        $validation = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        // validation fails
        if ($validation->fails())
            return redirect()->back()->withInput($request->all())->withErrors($validation);

        // validation success
        $username = Factory::create()->regexify('[A-Za-z0-9]{8,8}');
        $password = strtoupper(Factory::create()->regexify('[0-9]{8,8}'));

        $action = $this->agency->create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $password
        ]);
        $action->user()->create([
            'username' => strtoupper($username),
            'password' => bcrypt($password),
            'role' => 'agency'
        ]);

        // if action success
        if ($action) {
            return redirect()->back()->with('msg', 'Dinas berhasil ditambahkan');
        } else {
            return redirect()->back()->with('msgError', 'Dinas gagal ditambahkan. Silahkan coba kembali.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Agency  $agency
     * @return \Illuminate\Http\Response
     */
    public function show(Agency $agency)
    {
        //
        return view('agency.show', [
            'agency' => $agency
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Agency  $agency
     * @return \Illuminate\Http\Response
     */
    public function edit(Agency $agency)
    {
        //
        return view('agency.form', [
            'agency' => $agency
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Agency  $agency
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Agency $agency)
    {
        //
        // validations
        $validation = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        // validation fails
        if ($validation->fails())
            return redirect()->back()->withInput($request->all())->withErrors($validation);

        // validation success
        $action = $agency->update([
            'name' => $request->name,
            'email' => $request->email
        ]);

        // if action success
        if ($action) {
            return redirect()->back()->with('msg', 'Dinas berhasil diperbarui');
        } else {
            return redirect()->back()->with('msgError', 'Dinas gagal diperbarui. Silahkan coba kembali.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Agency  $agency
     * @return \Illuminate\Http\Response
     */
    public function destroy(Agency $agency)
    {
        // validations
        if ($agency->requirements->where('status', '!=', 1)->count() > 0) {
            return response()->json(['msg' => 'Dinas ini memiliki data biodata persyaratan. Hapus biodata persyaratan terlebih dahulu untuk menghapus.'], 401);
        }

        // validation success
        $action = $agency->delete();

        // if action success
        if ($action) {
            return response()->json(['msg' => 'Dinas berhasil dihapus'], 200);
        } else {
            return response()->json(['msg' => 'Dinas gagal dihapus. Silahkan coba kembali.'], 404);
        }
    }

    public function resetPassword (Request $request, Agency $agency) {
        // define faker
        $faker = Factory::create();

        // generate password
        $realPassword = $faker->regexify('[0-9]{8,8}');
        $newPassword = Hash::make($realPassword);

        // update password
        $agency->update([
            'password' => $realPassword
        ]);
        $action = $agency->user->update([
            'password' => $newPassword
        ]);

        // if action success
        if ($action) {
            return redirect()->back()->with('msg', 'Password berhasil direset.');
        } else {
            return redirect()->back()->with('msgError', 'Password gagal direset.');
        }
    }
}
