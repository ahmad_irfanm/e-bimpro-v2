<?php

namespace App\Http\Controllers;

use App\Requirement;
use App\RequirementForm;
use App\RequirementType;
use Illuminate\Http\Request;
use Validator;

class RequirementFormController extends Controller
{
    public function __construct(RequirementForm $rf)
    {
        $this->rf = $rf;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(RequirementType $requirementType)
    {
        //
        return view('requirement-form.index', [
            'requirementType' => $requirementType
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(RequirementType $requirementType)
    {
        //
        return view('requirement-form.form', [
            'requirementType' => $requirementType
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, RequirementType $requirementType)
    {
        // validations
        $validation = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        // validation fails
        if ($validation->fails())
            return redirect()->back()->withInput($request->all())->withErrors($validation);

        // validation success
        $action = $this->rf->create([
            'requirement_type_id' => $requirementType->id,
            'name' => $request->name,
            'description' => $request->description
        ]);

        // if action success
        if ($action) {
            return redirect()->back()->with('msg', 'Form persyaratan berhasil ditambahkan');
        } else {
            return redirect()->back()->with('msgError', 'Form persyaratan ditambahkan. Silahkan coba kembali.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RequirementForm  $requirementForm
     * @return \Illuminate\Http\Response
     */
    public function show(RequirementForm $requirementForm)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RequirementForm  $requirementForm
     * @return \Illuminate\Http\Response
     */
    public function edit(RequirementType $requirementType, RequirementForm $requirementForm)
    {
        //
        return view('requirement-form.form', [
            'requirementType' => $requirementType,
            'requirementForm' => $requirementForm
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RequirementForm  $requirementForm
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RequirementType $requirementType, RequirementForm $requirementForm)
    {
        // validations
        $validation = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        // validation fails
        if ($validation->fails())
            return redirect()->back()->withInput($request->all())->withErrors($validation);

        // validation success
        $action = $requirementForm->update([
            'name' => $request->name,
            'description' => $request->description
        ]);

        // if action success
        if ($action) {
            return redirect()->back()->with('msg', 'Form persyaratan berhasil diperbarui');
        } else {
            return redirect()->back()->with('msgError', 'Form persyaratan diperbarui. Silahkan coba kembali.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RequirementForm  $requirementForm
     * @return \Illuminate\Http\Response
     */
    public function destroy(RequirementType $requirementType, RequirementForm $requirementForm)
    {
        // validation success
        $action = $requirementForm->delete();

        // if action success
        if ($action) {
            return response()->json(['msg' => 'Form persyaratan berhasil dihapus'], 200);
        } else {
            return response()->json(['msg' => 'Form persyaratan gagal dihapus. Silahkan coba kembali.'], 404);
        }
    }
}
