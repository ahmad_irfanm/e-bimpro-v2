<?php

namespace App;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;

class Agency extends Model
{
    public $incrementing = false;
    protected $keyType = 'string';
    //
    protected $guarded = [];

    public function user () {
        return $this->hasOne('App\User');
    }

    public function requirements () {
        return $this->hasMany('App\Requirement');
    }
}
