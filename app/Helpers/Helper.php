<?php

function getMain () {
    return \App\Biodata::where('main', 1)->get();
}

function getMainFirst () {
    return getMain()[0];
}

function getDatetime ($status, $r) {
    if ($status == 1) {
        $status = $r->created_at;
    } elseif ($status == 2) {
        $status = $r->sent_at;
    } elseif ($status == 3) {
        $status = $r->first_verify_at;
    } elseif ($status == 4) {
        $status = $r->second_verify_at;
    }

    return convertDate($status);
}

function getOnlyTime ($status, $r) {
    if ($status == 1) {
        $status = $r->created_at;
    } elseif ($status == 2) {
        $status = $r->sent_at;
    } elseif ($status == 3) {
        $status = $r->first_verify_at;
    } elseif ($status == 4) {
        $status = $r->second_verify_at;
    }

    return getTime($status);
}


function convertStatus ($status) {
    if ($status == 1) {
        $status = "Belum dikirim";
    } elseif ($status == 2) {
        $status = "Terkirim";
    } elseif ($status == 3) {
        $status = "Verifikasi 1";
    } elseif ($status == 4) {
        $status = "Verifikasi 2";
    }

    return $status;
}

function convertIcon ($status) {
    if ($status == 1) {
        $status = "box-open";
    }
    elseif ($status == 2) {
        $status = "envelope";
    } elseif ($status == 3) {
        $status = "check";
    } elseif ($status == 4) {
        $status = "check-double";
    }

    return $status;
}

function convertColor ($color) {
    if ($color == 1) {
        $color = "warning";
    } elseif ($color == 2) {
        $color = "info";
    } elseif ($color == 3) {
        $color = "primary";
    } elseif ($color == 4) {
        $color = "success";
    }

    return $color;
}

function convertRole ($role) {
    if ($role == "admin1") {
        return "Admin ke-1";
    } elseif ($role == "admin2") {
        return "Admin ke-2";
    } else {
        return "Dinas";
    }
}

function convertDate ($date) {
    $now = date('Y-m-d');
    if ($now == date('Y-m-d', strtotime($date))) {
        return "Hari ini";
    } elseif (date('Y-m-d', strtotime('-1 Days', strtotime($now))) == date('Y-m-d', strtotime($date))) {
        return "Kemarin";
    }

    return dateLetter($date);
}

function dateLetter ($date) {
    $day = date('d', strtotime($date));
    $month = date('m', strtotime($date));
    $year = date('Y', strtotime($date));

    if ($month == 1) {
        $month = "Januari";
    } else if ($month == 2) {
        $month = "Februari";
    } else if ($month == 3) {
        $month = "Maret";
    } else if ($month == 4) {
        $month = "April";
    } else if ($month == 5) {
        $month = "Mei";
    } else if ($month == 6) {
        $month = "Juni";
    } else if ($month == 7) {
        $month = "Juli";
    } else if ($month == 8) {
        $month = "Agustus";
    } else if ($month == 9) {
        $month = "September";
    } else if ($month == 10) {
        $month = "Oktober";
    } else if ($month == 11) {
        $month = "November";
    } else if ($month == 12) {
        $month = "Desember";
    }

    return $day . ' ' . $month . ' ' . $year;
}

function getTime ($datetime) {
    return date('H:i', strtotime($datetime));
}

function shownField ($field, $abbr) {
    $fieldsExcept = [];

    // all except SKLK
    if ($abbr != 'SKLK')
        $fieldsExcept = ['ijazah_number', 'year_of_graduation', 'score'];
    if ($abbr == "SIB") {
        unset($fieldsExcept[1]);
    }

    if ($abbr != "PKF" && $abbr != "PKFG" && $abbr != "KPPI" && $abbr != "PKS" && $abbr != "KGB")
        $fieldsExcept = array_merge($fieldsExcept, [
            'study_old',
            'study_old_year_of_graduation',
            'study_old_degree',
            'study_new',
            'study_new_year_of_graduation',
            'study_new_degree',
            'study_new_ijazah_number',
            'study_new_almamater',
            'position_old',
            'position_old_tmt',
            'position_old_esselon',
            'position_new',
            'position_new_tmt',
            'position_new_esselon',
            'pak_old',
            'pak_old_number',
            'pak_old_assessment_time',
            'pak_old_main_element',
            'pak_old_supporting_element',
            'pak_new',
            'pak_new_number',
            'pak_new_assessment_time',
            'pak_new_main_element',
            'pak_new_supporting_element'
        ]);


    if ($abbr != "PKF" && $abbr != "PKFG" && $abbr != "PKS" && $abbr != "KPPI" && $abbr != "KPO")
        $fieldsExcept = array_merge($fieldsExcept, ['direct_supervisor_name', 'direct_supervisor_nip', 'direct_supervisor_rank', 'direct_supervisor_position']);

    // KGB
    if ($abbr == "KGB")
        $fieldsExcept = array_merge($fieldsExcept, [
            'letter_date',
            'accreditation',
            'address',
            'study_new',
            'study_old',
            'position_old',
            'position_old_tmt',
            'position_old_esselon',
            'position_new',
            'position_new_tmt',
            'position_new_esselon',
            'pak_old',
            'pak_old_number',
            'pak_old_assessment_time',
            'pak_old_main_element',
            'pak_old_supporting_element',
            'pak_new',
            'pak_new_number',
            'pak_new_assessment_time',
            'pak_new_main_element',
            'pak_new_supporting_element'
        ]);

    // all except PKF
    if ($abbr == "PKF" || $abbr == "PKFG") {
        $fieldsExcept = array_merge($fieldsExcept, ['collage', 'program_study', 'accreditation', 'study_new_ijazah_number', 'study_new_almamater', 'position_new_esselon', 'position_old_esselon']);
    }

    // all except PKS
    if ($abbr == "PKS") {
        $fieldsExcept = array_merge($fieldsExcept, [
            'collage',
            'program_study',
            'accreditation',
            'study_new_ijazah_number',
            'study_new_almamater',
            'pak_old',
            'pak_old_number',
            'pak_old_assessment_time',
            'pak_old_main_element',
            'pak_old_supporting_element',
            'pak_new',
            'pak_new_number',
            'pak_new_assessment_time',
            'pak_new_main_element',
            'pak_new_supporting_element'
        ]);
    }

    // all except PI
    if ($abbr == "KPPI")
        $fieldsExcept = array_merge($fieldsExcept, [
            'collage',
            'program_study',
            'accreditation',
            'position_new_esselon',
            'position_old_esselon',
            'pak_old',
            'pak_old_number',
            'pak_old_assessment_time',
            'pak_old_main_element',
            'pak_old_supporting_element',
            'pak_new',
            'pak_new_number',
            'pak_new_assessment_time',
            'pak_new_main_element',
            'pak_new_supporting_element'
        ]);

    if ($abbr == "KPO" || $abbr == "KPPI" || $abbr == "PKF" || $abbr == "PKFG" || $abbr == "PKS") {
        $fieldsExcept = array_merge($fieldsExcept, ['position']);
    }

    // all except PPPAO and PPPAOKG
    if ($abbr == 'PPPAO' || $abbr == 'PPPAOKG')
        $fieldsExcept = array_merge($fieldsExcept, ['accreditation']);

    // all except PPPAOKG
    if ($abbr == 'PPPAOKG')
        $fieldsExcept = array_merge($fieldsExcept, ['department']);

    // SRR
    if ($abbr == 'SRR')
       $fieldsExcept = array_merge($fieldsExcept, ['program_study', 'accreditation', 'address']);

    // KAS, KAU, and KAG
    if ($abbr == 'KAS' || $abbr == 'KAU' || $abbr == "KAG")
        $fieldsExcept = array_merge($fieldsExcept, ['rank', 'collage', 'department', 'program_study', 'accreditation', 'address']);

    // for position_class
    if ($abbr != 'PPPAO')
        $fieldsExcept = array_merge($fieldsExcept, ['position_class']);

    // for desc point 1 and desc point 2
    if ($abbr != 'PPPAO' && $abbr != 'PPPAOKG')
        $fieldsExcept = array_merge($fieldsExcept, ['desc_point1', 'desc_point2']);


    // RPIO
    if ($abbr == "RPIO") {
        unset($fieldsExcept[array_search('position_class', $fieldsExcept)]);
        unset($fieldsExcept[array_search('ijazah_number', $fieldsExcept)]);
        $fieldsExcept = array_merge($fieldsExcept, ['accreditation', 'address']);
    }

    // PP
    if ($abbr == "PP") {
        $fieldsExcept = array_merge($fieldsExcept, ['program_study', 'accreditation', 'department']);
    }

    // PI and UD
    if ($abbr == "UD" || $abbr == "PI")
        unset($fieldsExcept[array_search('ijazah_number', $fieldsExcept)]);




    return !in_array($field, $fieldsExcept);
}

function alias ($field, $abbr) {

    $label = "";
    $placeholder = "";

    // default
    if ($field == 'number') {
        $label = "Nomor Surat";
        $placeholder = "Nomor Surat";
    } elseif ($field == 'letter_date') {
        $label = "Tanggal Surat";
    } elseif ($field == 'name') {
        $label = "Nama";
        $placeholder = "Nama";
    } elseif ($field == 'nip') {
        $label = "NIP";
        $placeholder = "NIP";
    } elseif ($field == 'rank') {
        $label = "Pangkat/Gol.";
        $placeholder = "Pangkat/Gol.";
    } elseif ($field == 'position') {
        $label = 'Jabatan';
        $placeholder = "Jabatan";
    } elseif ($field == 'work_unit') {
        $label = "Unit Kerja";
        $placeholder = "Unit Kerja";
    } elseif ($field == 'collage') {
        $label = "Perguruan Tinggi";
        $placeholder = "Perguruan Tinggi";
    } elseif ($field == 'department') {
        $label = "Jurusan";
        $placeholder = "Jurusan";
    } elseif ($field == 'program_study') {
        $label = "Program Study";
        $placeholder = "Program Study";
    } elseif ($field == 'ijazah_number') {
        $label = "Nomor Ijazah";
        $placeholder = "Nomor Ijazah";
    } elseif ($field == 'year_of_graduation') {
        $label = "Tahun Kelulusan";
        $placeholder = "2018/2019";
    } elseif ($field == 'score') {
        $label = "Nilai/IPK";
        $placeholder = "3.5";
    } elseif ($field == 'accreditation') {
        $label = "Terakreditasi";
        $placeholder = $placeholder;
    } elseif ($field == 'address') {
        $label = "Alamat Rumah";
        $placeholder = "Alamat Rumah";
    } elseif ($field == 'position_class') {
        $label = "Kelas Jabatan";
        $placeholder = "Kelas Jabatan";
    } elseif ($field == 'desc_point1') {
        $label = "Ket. Point 1";
        $placeholder = "Dilsi surat rekomendasi dari OPD";
    } elseif ($field == 'desc_point2') {
        $label = "Ket. Point 2";
        $placeholder = "Diisi surat lolos butuh dari OPD yang dituju";
    } elseif ($field == "direct_supervisor_name") {
        $label = "Nama Atasan Langsung";
        $placeholder = $label;
    } elseif ($field == "direct_supervisor_nip") {
        $label = "NIP Atasan Langsung";
        $placeholder = $label;
    } elseif ($field == "direct_supervisor_rank") {
        $label = "Pangkat Atasan Langsung";
        $placeholder = $label;
    } elseif ($field == "direct_supervisor_position") {
        $label = "Jabatan Atasan Langsung";
        $placeholder = $label;
    }  elseif ($field == "study_old") {
        $label = "PT Pendidikan lama";
        $placeholder = $label;
    } elseif ($field == "study_old_year_of_graduation") {
        $label = "Tahun lulus";
        $placeholder = $label;
    } elseif ($field == "study_old_degree") {
        $label = "Gelar";
        $placeholder = $label;
    }  elseif ($field == "study_new") {
        $label = "PT Pendidikan baru";
        $placeholder = $label;
    } elseif ($field == "study_new_year_of_graduation") {
        $label = "Tahun lulus";
        $placeholder = $label;
    } elseif ($field == "study_new_degree") {
        $label = "Gelar";
        $placeholder = $label;
    } elseif ($field == "study_new_ijazah_number") {
        $label = "Nomor ijazah";
        $placeholder = $label;
    } elseif ($field == "study_new_almamater") {
        $label = "Sekolah";
        $placeholder = $label;
    } elseif ($field == "position_old") {
        $label = "Jabatan lama";
        $placeholder = $label;
    }  elseif ($field == "position_old_tmt") {
        $label = "TMT";
        $placeholder = $label;
    } elseif ($field == "position_old_esselon") {
        $label = "Esselon";
        $placeholder = $label;
    } elseif ($field == "position_new_tmt") {
        $label = "TMT";
        $placeholder = $label;
    } elseif ($field == "position_new") {
        $label = "Jabatan baru";
        $placeholder = $label;
    }  elseif ($field == "position_new_esselon") {
        $label = "Esselon";
        $placeholder = $label;
    } elseif ($field == "position_new_tmt") {
        $label = "TMT";
        $placeholder = $label;
    } elseif ($field == "pak_old") {
        $label = "PAK lama";
        $placeholder = $label;
    } elseif ($field == "pak_old_number") {
        $label = "No.PAK";
        $placeholder = $label;
    } elseif ($field == "pak_old_assessment_time") {
        $label = "Masa penilaian";
        $placeholder = $label;
    } elseif ($field == "pak_old_main_element") {
        $label = "Unsur utama";
        $placeholder = $label;
    } elseif ($field == "pak_old_supporting_element") {
        $label = "Unsur penunjang";
        $placeholder = $label;
    } elseif ($field == "pak_new") {
        $label = "PAK baru";
        $placeholder = $label;
    } elseif ($field == "pak_new_number") {
        $label = "No.PAK";
        $placeholder = $label;
    } elseif ($field == "pak_new_assessment_time") {
        $label = "Masa penilaian";
        $placeholder = $label;
    } elseif ($field == "pak_new_main_element") {
        $label = "Unsur utama";
        $placeholder = $label;
    } elseif ($field == "pak_new_supporting_element") {
        $label = "Unsur penunjang";
        $placeholder = $label;
    } elseif ($field == "phone") {
        $label = "No. HP";
        $placeholder = $label;
    }

    // custom by abbr
    if ($abbr == "PPPAO") {
        if ($field == 'collage') {
            $label = "Sebagai";
            $placeholder = "Isi nama jabatan di OPD yang dituju";
        } else if ($field == 'department') {
            $label = "Kelas Jabatan";
            $placeholder = "Isi kelas jabatan di OPD yang dituju";
        } else if ($field == 'program_study') {
            $label = "Pada";
            $placeholder = "Isi nama jabatan atas langsung di OPD yang dituju";
        }
    } elseif ($abbr == 'PPPAOKG') {
        if ($field == 'collage') {
            $label = "Sebagai";
            $placeholder = "Isi nama jabatan di OPD yang dituju";
        } else if ($field == 'program_study') {
            $label = "Pada";
            $placeholder = "Isi satuan pendidikan formal yang dituju";
        }
    } elseif ($abbr == "SRR") {
        if ($field == 'position') {
            $label = "Jabatan Sekarang";
            $placeholder = 'Jabatan Sekarang';
        } elseif ($field == 'collage') {
            $label = 'Jabatan yang dituju';
            $placeholder = 'Jabtan yang dituju';
        } elseif ($field == 'department') {
            $label = 'Esselon';
            $placeholder = 'Esselon';
        }
    } elseif ($abbr == "SIB") {
        if ($field == "year_of_graduation") {
            $label = "Tahun Akademik";
            $placeholder = "Tahun Akademik";
        }
    } elseif ($abbr == "RPIO") {
        if ($field == "position") {
            $label = "Jabatan Lama";
            $placeholder = "Jabatan Lama";
        } elseif ($field == "collage") {
            $label = "Jabatan Baru";
            $placeholder = "Jabatan Baru";
        } elseif ($field == "program_study") {
            $label = "Jabatan Atasan Langsung Lama";
            $placeholder = "Atasan Langsung Lama";
        }  elseif ($field == "ijazah_number") {
            $label = "Jabatan Atasan Langsung Baru";
            $placeholder = "Atasan Langsung Baru";
        } elseif ($field == "department") {
            $label = "Kelas jabatan baru";
            $placeholder = "Atasan Langsung Lama";
        } elseif ($field == "position_class") {
            $label = "Kelas jabatan lama";
        }
    }elseif ($abbr == "PKF" || $abbr == "PKFG") {
        if ($field == "department") {
            $label = "Pangkat Usulan";
            $placeholder = $label;
        } elseif ($field == "rank") {
            $label = "Pangkat Terakhir";
            $placeholder = $label;
        } elseif ($field == "position_old") {
            $label = "Jabatan fungsional lama";
            $placeholder = $label;
        } elseif ($field == "position_new") {
            $label = "Jabatan fungsional baru";
            $placeholder = $label;
        }
    } elseif ($abbr == "PKS") {
        if ($field == "department") {
            $label = "Pangkat Usulan";
            $placeholder = $label;
        } elseif ($field == "rank") {
            $label = "Pangkat Terakhir";
            $placeholder = $label;
        } elseif ($field == "position_old") {
            $label = "Jabatan struktural lama";
            $placeholder = $label;
        } elseif ($field == "position_new") {
            $label = "Jabatan struktural baru";
            $placeholder = $label;
        }
    } elseif ($abbr == "KPPI") {
        if ($field == "department") {
            $label = "Pangkat Usulan";
            $placeholder = $label;
        } elseif ($field == "rank") {
            $label = "Pangkat Terakhir";
            $placeholder = $label;
        } elseif ($field == "department") {
            $label = "Pangkat Usulan";
            $placeholder = $label;
        } elseif ($field == "rank") {
            $label = "Pangkat Terakhir";
            $placeholder = $label;
        } elseif ($field == "collage") {
            $label = $label."/Sekolah";
            $placeholder = $label;
        }
    } elseif ($abbr == "PP") {
        if ($field == "collage") {
            $label = "Pensiun";
            $placeholder = $label;
        }
    } elseif ($abbr == "KGB") {
        if ($field == "number") {
            $label = "Tanggal/Nomor";
            $placeholder = $label;
        } elseif ($field == "rank") {
            $label = "Tempat/Tgl Lahir";
            $placeholder = $label;
        } elseif ($field == "position") {
            $label = "Pangkat/Jabatan";
            $placeholder = $label;
        } elseif ($field == "work_unit") {
            $label = "Kantor/Tempat";
            $placeholder = $label;
        } elseif ($field == "study_old_year_of_graduation") {
            $label = "Gaji Pokok Lama";
            $placeholder = $label;
        } elseif ($field == "study_old_degree") {
            $label = "Oleh Pejabat";
            $placeholder = $label;
        } elseif ($field == "study_new_year_of_graduation") {
            $label = "Tanggal mulai berlaku gaji";
            $placeholder = $label;
        } elseif ($field == "study_new_degree") {
            $label = "Masa Kerja";
            $placeholder = "Contoh: 02 Tahun 08 Bulan";
        } elseif ($field == "study_new_ijazah_number") {
            $label = "Gaji pokok baru";
            $placeholder = $label;
        } elseif ($field == "study_new_almamater") {
            $label = "Berdasarkan masa kerja";
            $placeholder = "Contoh: 02 Tahun 08 Bulan";
        } elseif ($field == "collage") {
            $label = "Dalam golongan";
            $placeholder = $label;
        } elseif ($field == "department") {
            $label = "Mulai tanggal";
            $placeholder = $label;
        } elseif ($field == "study_new_almamater") {
            $label = "Berdasarkan masa kerja";
            $placeholder = $label;
        } elseif ($field == "program_study") {
            $label = "Kenaikan gaji y.a.d";
            $placeholder = $label;
        }
    } elseif ($abbr == "PI" || $abbr == "UD") {
        if ($field == "ijazah_number") {
            $label = "Nilai SKP Terakhir";
            $placeholder = $label;
        }
    } elseif ($abbr == "KPO") {
        if ($field == "department") {
            $label = "Pangkat Usulan";
            $placeholder = $label;
        } elseif ($field == "rank") {
            $label = "Pangkat Terakhir";
            $placeholder = $label;
        }
    }

    return [
        'label' => $label,
        'placeholder' => $placeholder
    ];
}

function label ($field, $abbr) {
    return alias($field, $abbr)['label'];
}

function placeholder ($field, $abbr) {
    return alias($field, $abbr)['placeholder'];
}
