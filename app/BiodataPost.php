<?php

namespace App;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;
use InitialAvatar;

class BiodataPost extends Model
{
    public $incrementing = false;
    protected $keyType = 'string';

    //
    protected $guarded = [];

    public function requirement () {
        return $this->belongsTo('App\Requirement');
    }



    public function biodata () {
        return $this->belongsTo('App\Biodata');
    }
}
