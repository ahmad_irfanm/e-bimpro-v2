<?php

namespace App;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;

class RequirementForm extends Model
{
    public $incrementing = false;
    protected $keyType = 'string';

    //
    protected $guarded = [];
}
