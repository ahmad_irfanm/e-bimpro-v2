<?php

namespace App;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;

class RequirementType extends Model
{
    public $incrementing = false;
    protected $keyType = 'string';

    //
    protected $guarded = [];

    public function requirement_forms () {
        return $this->hasMany('App\RequirementForm');
    }

    public function requirements () {
        return $this->hasMany('App\Requirement');
    }

    public function agencies () {
        $agencies = collect($this->requirements()->where('status', '!=', 1)->get())->map(function ($requirement) {
            return $requirement->agency;
        })->unique()->values()->all();

        return $agencies;
    }

    public function biodatas () {
        return $this->belongsToMany('App\Biodata', 'requirement_type_biodata', 'requirement_type_id', 'biodata_id')
            ->withPivot('order');
    }
}
