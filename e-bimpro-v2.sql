-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 22, 2020 at 02:42 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `e-bimpro-v2`
--

-- --------------------------------------------------------

--
-- Table structure for table `agencies`
--

CREATE TABLE `agencies` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `agencies`
--

INSERT INTO `agencies` (`id`, `name`, `email`, `password`, `created_at`, `updated_at`) VALUES
('0f4b52d8-7a8d-3a72-96ce-4230afdff09a', 'First Agency', 'email.firstagency@gmail.com', 'password', '2020-02-16 07:37:12', '2020-02-22 18:48:58');

-- --------------------------------------------------------

--
-- Table structure for table `biodatas`
--

CREATE TABLE `biodatas` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `placeholder` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` enum('numeric','text','textarea','radio','date','year') COLLATE utf8mb4_unicode_ci NOT NULL,
  `choices` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `main` tinyint(1) NOT NULL DEFAULT 0,
  `attributes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `biodatas`
--

INSERT INTO `biodatas` (`id`, `name`, `placeholder`, `type`, `choices`, `main`, `attributes`, `created_at`, `updated_at`) VALUES
('0179160b-8b21-38c1-a673-98b4a3a2c007', 'Pangkat/Gol.', 'Pangkat/Gol.', 'text', NULL, 0, NULL, '2020-02-16 23:37:55', '2020-02-22 18:48:58'),
('0207cc87-c743-3070-9366-faf1c24f3ddf', 'Kelas Jabatan', 'Kelas Jabatan', 'text', NULL, 0, NULL, '2020-02-17 04:51:53', '2020-02-22 18:49:02'),
('020a0a56-c870-3760-ac0e-1a5a2e55938e', 'Perguruan Tinggi', 'Perguruan Tinggi', 'text', NULL, 0, NULL, '2020-02-16 23:38:31', '2020-02-22 18:48:58'),
('02691749-f7f9-336c-ac9f-0d95676379f0', 'No. HP', 'No. HP', 'text', NULL, 0, NULL, '2020-02-16 23:40:16', '2020-02-22 18:48:59'),
('0352819e-783e-343c-bc5e-3afdaf5a626b', 'Ket. Point 1', 'Diidi surat rekomendasi dari OPD', 'text', NULL, 0, NULL, '2020-02-17 04:54:01', '2020-02-22 18:49:02'),
('05b16bdf-6f23-3003-8792-e01bd8e64d9e', 'Masa Kerja', 'Masa Kerja', 'text', NULL, 0, NULL, '2020-02-17 04:28:52', '2020-02-22 18:49:01'),
('070cc358-77ac-3601-88a8-53b457ae7dba', 'Jabatan Lama', 'Jabatan Lama', 'text', NULL, 0, NULL, '2020-02-17 04:43:52', '2020-02-22 18:49:02'),
('09cfd508-fcc2-3984-8577-2a3387c88894', 'Kantor/Tempat', 'Kantor/Tempat', 'text', NULL, 0, NULL, '2020-02-17 04:28:04', '2020-02-22 18:49:01'),
('0b87d7c4-de71-3f95-b904-20990f9cabf5', 'Gaji Pokok Baru', 'Gaji Pokok Baru', 'numeric', NULL, 0, NULL, '2020-02-17 04:29:06', '2020-02-22 18:49:01'),
('0bbfc503-e321-34a7-9841-ae4b6b97f23d', 'Kelas Jabatan Lama', 'Kelas Jabatan Lama', 'text', NULL, 0, NULL, '2020-02-17 04:56:58', '2020-02-22 18:49:03'),
('10084ef9-7242-37d6-a90b-45797da2e3d4', 'PT Pendidikan Baru', 'PT Pendidikan Baru', 'text', NULL, 0, NULL, '2020-02-17 00:01:01', '2020-02-22 18:49:00'),
('12013cac-8cd2-3aa2-b189-60c1701f30c2', 'Jabatan Struktural Baru TMT', 'Jabatan Struktural Baru TMT', 'text', NULL, 0, NULL, '2020-02-17 00:45:25', '2020-02-22 18:49:01'),
('13424751-f315-39ab-a95a-48cba591ccd2', 'Jabatan Lama TMT', 'Jabatan Lama TMT', 'text', NULL, 0, NULL, '2020-02-17 04:44:22', '2020-02-22 18:49:02'),
('176d295f-c95d-3198-bf65-adebd41778cf', 'Jabatan Struktural Lama', 'Jabatan Struktural Lama', 'text', NULL, 0, NULL, '2020-02-17 00:44:46', '2020-02-22 18:49:01'),
('184e08d2-2b80-3d6a-bf33-e414d3cbea38', 'Jabatan Fungsional Lama TMT', 'Jabatan Fungsional Lama TMT', 'text', NULL, 0, NULL, '2020-02-17 00:02:19', '2020-02-22 18:49:00'),
('1a9b6e27-91db-3466-bfe0-5fdb302b4307', 'Tahun Kelulusan', 'Tahun Kelulusan', 'text', NULL, 0, NULL, '2020-02-16 23:52:31', '2020-02-22 18:48:59'),
('1b0937a6-2dce-3fb9-9226-17d3491a13e5', 'Pangkat Atasan Langsung', 'Pangkat Atasan Langsung', 'text', NULL, 0, NULL, '2020-02-16 23:59:03', '2020-02-22 18:48:59'),
('234acfeb-36c5-3e0f-a4d4-cef16b0e4054', 'Pensiun', 'Pensiun', 'radio', 'BUP\r\nMDA\r\nAPS', 0, NULL, '2020-02-17 04:31:30', '2020-02-22 18:49:02'),
('277fccdf-4136-3a0c-8110-e33b886b4a14', 'PT Pendidikan Lama Gelar', 'PT Pendidikan Lama Gelar', 'text', NULL, 0, NULL, '2020-02-17 00:00:46', '2020-02-22 18:49:00'),
('2a9de6f8-0b60-3727-81d1-ff99f252457d', 'PT Pendidikan Lama Tahun Lulus', 'PT Pendidikan Lama Tahun Lulus', 'text', NULL, 0, NULL, '2020-02-17 00:00:21', '2020-02-22 18:48:59'),
('2fc7184b-a608-3af6-a1fe-0b3487f3d06a', 'Jabatan Baru', 'Jabatan Baru', 'text', NULL, 0, NULL, '2020-02-17 04:44:04', '2020-02-22 18:49:02'),
('31259478-0629-386c-ade3-34e8e5328827', 'Jabatan Fungsional Lama', 'Jabatan Fungsional Lama', 'text', NULL, 0, NULL, '2020-02-17 00:02:02', '2020-02-22 18:49:00'),
('32123519-a5c3-33c4-9347-4fe94eea0a90', 'Pangkat/Jabatan', 'Pangkat/Jabatan', 'text', NULL, 0, NULL, '2020-02-17 04:27:18', '2020-02-22 18:49:01'),
('328e919c-f630-3efe-9a4d-4af7e3a242e4', 'Berdasarkan Masa Kerja', 'Berdasarkan Masa Kerja', 'text', NULL, 0, NULL, '2020-02-17 04:29:20', '2020-02-22 18:49:02'),
('35bb370f-b4fc-3848-9563-2a568480549a', 'Sebagai', 'Sebagai', 'text', NULL, 0, NULL, '2020-02-17 04:52:54', '2020-02-22 18:49:02'),
('381f1c3b-ee0b-3dcc-9b34-4f6b56ab275b', 'Jabatan', 'Jabatan', 'text', NULL, 0, NULL, '2020-02-16 23:38:06', '2020-02-22 18:48:58'),
('3b6a13f6-825c-30e1-bd00-bf0c9428bce9', 'Dalam Golongan', 'Dalam Golongan', 'text', NULL, 0, NULL, '2020-02-17 04:27:43', '2020-02-22 18:49:01'),
('3cb10c58-88a1-37af-91fa-8191e61b6fca', 'Jabatan yang Dituju', 'Jabatan yang Dituju', 'text', NULL, 0, NULL, '2020-02-17 05:00:33', '2020-02-22 18:49:03'),
('403bd54b-4cf4-309a-9481-d75c02caf197', 'Masa Penilaian', 'Masa Penilaian', 'text', NULL, 0, NULL, '2020-02-17 00:04:18', '2020-02-22 18:49:00'),
('432b612b-7fa4-3be0-9e3e-1a31833d3323', 'PAK Baru No. PAK', 'PAK Baru No. PAK', 'text', NULL, 0, NULL, '2020-02-17 00:27:04', '2020-02-22 18:49:00'),
('43780e3e-f2d5-3b5a-b199-d1ce0bdc1fd3', 'PAK Lama', 'PAK Lama', 'text', NULL, 0, NULL, '2020-02-17 00:03:52', '2020-02-22 18:49:00'),
('494346fa-f333-3963-b272-63c59aa1733f', 'Nama Atasan Langsung', 'Nama Atasan Langsung', 'text', NULL, 0, NULL, '2020-02-16 23:58:36', '2020-02-22 18:48:59'),
('4a3f261e-8e20-30c2-ad1a-763c14a6d2be', 'Mulai Tanggal', 'Mulai Tanggal', 'date', NULL, 0, NULL, '2020-02-17 04:27:34', '2020-02-22 18:49:01'),
('4ab8f73a-a4c3-381a-a322-645e18b48568', 'PAK Baru Masa Penilaian', 'PAK Baru Masa Penilaian', 'text', NULL, 0, NULL, '2020-02-17 00:27:20', '2020-02-22 18:49:00'),
('5bd533ad-3ed3-3f3d-bf83-ec6674c3b7a6', 'Jabatan Sekarang', 'Jabatan Sekarang', 'text', NULL, 0, NULL, '2020-02-17 04:59:38', '2020-02-22 18:49:03'),
('5ce3da61-f6c3-3540-9e9a-8a80d1039caa', 'Ket. Point 2', 'Diisi surat lolos butuh dari OPD yang Dituju', 'text', NULL, 0, NULL, '2020-02-17 04:54:25', '2020-02-22 18:49:03'),
('5fb39655-971c-3fb5-bd65-5795ee252066', 'PAK Baru Unsur Utama', 'PAK Baru Unsur Utama', 'text', NULL, 0, NULL, '2020-02-17 00:27:32', '2020-02-22 18:49:01'),
('63f10304-2b94-3251-afdb-6d098b0322b8', 'Jabatan Struktural Lama Esselon', 'Jabatan Struktural Lama Esselon', 'text', NULL, 0, NULL, '2020-02-17 00:45:11', '2020-02-22 18:49:01'),
('653a326c-016f-3425-8e99-b472bb1ebd68', 'PAK Baru', 'PAK Baru', 'text', NULL, 0, NULL, '2020-02-17 00:26:50', '2020-02-22 18:49:00'),
('673f037d-1ad1-3a80-9258-2d71bad304a3', 'Nilai SKP Terakhir', 'Nilai SKP Terakhir', 'text', NULL, 0, NULL, '2020-02-16 23:57:23', '2020-02-22 18:48:59'),
('6b8a273f-59ed-3f63-b5f2-96ae4593c407', 'Alamat Rumah', 'Alamat Rumah', 'textarea', NULL, 0, NULL, '2020-02-16 23:40:30', '2020-02-22 18:48:59'),
('6ccdda23-125e-301f-bca7-c160e29ca09c', 'PT Pendidikan Lama', 'PT Pendidikan Lama', 'text', NULL, 0, NULL, '2020-02-16 23:59:48', '2020-02-22 18:48:59'),
('720fa63d-c2c0-3bb8-bace-a31ed2ee25a1', 'Gaji Pokok Lama', 'Gaji Pokok Lama', 'numeric', NULL, 0, NULL, '2020-02-17 04:28:17', '2020-02-22 18:49:01'),
('73d028e8-d6ae-3439-a3c7-c42b3dd15cf8', 'Nilai/IPK', 'Nilai/IPK', 'numeric', NULL, 0, NULL, '2020-02-16 23:52:45', '2020-02-22 18:48:59'),
('7443dd48-c69d-307b-bf4b-73b47002c2e9', 'Pangkat Terakhir', 'Pangkat Terakhir', 'text', NULL, 0, NULL, '2020-02-16 23:58:00', '2020-02-22 18:48:59'),
('756d8d0b-f640-368f-9f2d-4690b0089660', 'Tahun Akademik', 'Tahun Akademik', 'text', NULL, 0, NULL, '2020-02-16 23:39:11', '2020-02-22 18:48:59'),
('77c677ed-1a2e-3d7d-99c1-b20589e30965', 'No. PAK', 'No. PAK', 'text', NULL, 0, NULL, '2020-02-17 00:04:01', '2020-02-22 18:49:00'),
('7e09b85f-7322-39fa-a15f-d9ac3e441990', 'Tempat/Tgl Lahir', 'Tempat/Tgl Lahir', 'text', NULL, 0, NULL, '2020-02-17 04:27:06', '2020-02-22 18:49:01'),
('8ab38b3c-1e66-31c2-b8bc-61b7d4374de5', 'Kelas Jabatan Baru', 'Kelas Jabatan Baru', 'text', NULL, 0, NULL, '2020-02-17 04:57:33', '2020-02-22 18:49:03'),
('8d724505-f3db-3d29-a0a5-66b3ce24983f', 'Pangkat Usulan', 'Pangkat Usulan', 'text', NULL, 0, NULL, '2020-02-16 23:58:19', '2020-02-22 18:48:59'),
('8dcf99fd-48bd-3ae2-a30f-080ced062f7a', 'Oleh Pejabat', 'Oleh Pejabat', 'text', NULL, 0, NULL, '2020-02-17 04:28:25', '2020-02-22 18:49:01'),
('8ff905b0-2427-3e16-92ce-e8a0284ca16b', 'Nomor Ijazah', 'Nomor Ijazah', 'text', NULL, 0, NULL, '2020-02-16 23:52:06', '2020-02-22 18:48:59'),
('951899a0-ea63-39a0-8193-12a447a7ba82', 'Jabatan Fungsional Baru TMT', 'Jabatan Fungsional Baru TMT', 'text', NULL, 0, NULL, '2020-02-17 00:03:32', '2020-02-22 18:49:00'),
('95b8fd1f-4306-3cdd-a12c-49923bf92be3', 'Jabatan Fungsional Baru', 'Jabatan Fungsional Baru', 'text', NULL, 0, NULL, '2020-02-17 00:03:17', '2020-02-22 18:49:00'),
('9e227ea0-4eb8-329d-b774-f97a392c929c', 'Jabatan Struktural Lama TMT', 'Jabatan Struktural Lama TMT', 'text', NULL, 0, NULL, '2020-02-17 00:45:00', '2020-02-22 18:49:01'),
('a3b10b5e-db9a-3b3c-a4da-75a405054835', 'Tanggal Mulai Berlaku Gaji', 'Tanggal Mulai Berlaku Gaji', 'date', NULL, 0, NULL, '2020-02-17 04:28:39', '2020-02-22 18:49:01'),
('a44a9dab-4a40-3391-93af-a6b72a610f84', 'PT Pendidikan Lama Tahun Lulus', 'PT Pendidikan Lama Tahun Lulus', 'text', NULL, 0, NULL, '2020-02-17 00:01:19', '2020-02-22 18:49:00'),
('a99b63eb-21e7-3a03-87ac-eed969f5bc76', 'Terakreditasi', 'Terakreditasi', 'radio', 'Ya\r\nTidak', 0, NULL, '2020-02-16 23:39:41', '2020-02-22 18:48:59'),
('a9bba2e8-2d1f-34af-ae51-19ff58f2cef9', 'Nomor Surat', 'Nomor Surat', 'text', NULL, 0, NULL, '2020-02-16 23:37:19', '2020-02-22 18:48:58'),
('ad7f44ca-04e1-3d03-a58a-999fe681e894', 'PT Pendidikan Baru Nomor Ijazah', 'PT Pendidikan Baru Nomor Ijazah', 'text', NULL, 0, NULL, '2020-02-17 04:42:55', '2020-02-22 18:49:02'),
('ae5cec94-7352-345f-b319-a7dc71b25fc8', 'PAK Lama Unsur Utama', 'PAK Lama Unsur Utama', 'text', NULL, 0, NULL, '2020-02-17 00:04:45', '2020-02-22 18:49:00'),
('ae8dc37d-bdbb-31a4-b2b6-9ee523d1ba3d', 'PAK Lama Unsur Penunjang', 'PAK Lama Unsur Penunjang', 'text', NULL, 0, NULL, '2020-02-17 00:04:58', '2020-02-22 18:49:00'),
('b073b4cb-4f26-3e1f-8fe8-6f2bff17ec94', 'Kelas Jabatan di OPD yang Dituju', 'Kelas Jabatan di OPD yang Dituju', 'text', NULL, 0, NULL, '2020-02-17 04:52:33', '2020-02-22 18:49:02'),
('b3c7c5b5-e2f3-3005-a45f-08444b9f3730', 'PT Pendidikan Baru Gelar', 'PT Pendidikan Baru Gelar', 'text', NULL, 0, NULL, '2020-02-17 00:01:37', '2020-02-22 18:49:00'),
('b921ff7f-2428-322b-ba43-469814d8b3fc', 'Jabatan Atasan Langsung Baru', 'Jabatan Atasan Langsung Baru', 'text', NULL, 0, NULL, '2020-02-17 04:58:11', '2020-02-22 18:49:03'),
('bbd66af4-4212-36c0-bffe-6c735e2e5136', 'NIP Atasan Langsung', 'NIP Atasan Langsung', 'text', NULL, 0, NULL, '2020-02-16 23:58:51', '2020-02-22 18:48:59'),
('be7066e4-51b0-3ef7-9b07-34e3c280d20f', 'PT Pendidikan Baru Sekolah', 'PT Pendidikan Baru Sekolah', 'text', NULL, 0, NULL, '2020-02-17 04:43:16', '2020-02-22 18:49:02'),
('c07374f0-35cd-384d-9413-1cddaa64294b', 'PAK Baru Unsur Penunjang', 'PAK Baru Unsur Penunjang', 'text', NULL, 0, NULL, '2020-02-17 00:27:46', '2020-02-22 18:49:01'),
('c3e8479a-44ec-31a3-9694-7e0a15599817', 'Jabatan Baru TMT', 'Jabatan Baru TMT', 'text', NULL, 0, NULL, '2020-02-17 04:44:33', '2020-02-22 18:49:02'),
('c5db7848-792d-3f5d-b527-360d696f296e', 'Esselon', 'Esselon', 'radio', 'III.a\r\nIII.b\r\nIV.a\r\nIV.b', 0, NULL, '2020-02-17 05:00:01', '2020-02-22 18:49:03'),
('c88742a0-9041-3aa9-8894-f75698ade167', 'Pada', 'Isi nama jabatan di OPD yang dituju', 'text', NULL, 0, NULL, '2020-02-17 04:53:14', '2020-02-22 18:49:02'),
('d0ba6947-572a-31e6-96ca-712bf7d20195', 'Kenaikan Gaji y.a.d', 'Kenaikan Gaji y.a.d', 'text', NULL, 0, NULL, '2020-02-17 04:27:56', '2020-02-22 18:49:01'),
('d2228828-387f-3f8e-b803-d6ee989e803d', 'Nama', 'Nama', 'text', NULL, 1, NULL, '2020-02-16 23:37:27', '2020-02-22 18:48:58'),
('d59562a7-c14e-3340-b4e4-4fb7fd3dcd56', 'NIP', 'NIP', 'text', NULL, 1, NULL, '2020-02-16 23:37:36', '2020-02-22 18:48:58'),
('e2189caa-f961-30f2-9fbf-36591dcf28a3', 'Jabatan Atasan Langsung', 'Jabatan Atasan Langsung', 'text', NULL, 0, NULL, '2020-02-16 23:59:15', '2020-02-22 18:48:59'),
('edd3dd93-3485-3cbc-828c-55bce7f0fda0', 'Tanggal/Nomor', 'Tanggal/Nomor', 'text', NULL, 0, NULL, '2020-02-17 04:26:53', '2020-02-22 18:49:01'),
('f145556a-c4d3-3443-91c3-3df27bd98732', 'Jurusan', 'Jurusan', 'text', NULL, 0, NULL, '2020-02-16 23:38:19', '2020-02-22 18:48:58'),
('f3f68364-2b63-305b-a9ed-8667f7c43bc9', 'Tanggal Surat', 'Tanggal Surat', 'date', NULL, 0, NULL, '2020-02-16 09:59:26', '2020-02-22 18:48:58'),
('f688c278-b0b9-3465-a735-6fe16da9bcdb', 'Program Studi', 'Program Studi', 'text', NULL, 0, NULL, '2020-02-16 23:38:48', '2020-02-22 18:48:58'),
('f8e33e39-3343-3328-a765-dc359a9d2232', 'Unit Kerja', 'Unit Kerja', 'text', NULL, 0, NULL, '2020-02-16 23:39:00', '2020-02-22 18:48:58'),
('fbaa5890-63d9-3895-9193-48540c4b8b74', 'Jabatan Atasan Langsung Lama', 'Jabatan Atasan Langsung Lama', 'text', NULL, 0, NULL, '2020-02-17 04:58:04', '2020-02-22 18:49:03');

-- --------------------------------------------------------

--
-- Table structure for table `biodata_posts`
--

CREATE TABLE `biodata_posts` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `requirement_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `biodata_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `biodata_posts`
--

INSERT INTO `biodata_posts` (`id`, `requirement_id`, `biodata_id`, `content`, `created_at`, `updated_at`) VALUES
('0318fd27-2302-4084-a278-9774660cc5d9', 'a2ef9429-4461-491c-889c-0f92a65218c8', '0bbfc503-e321-34a7-9841-ae4b6b97f23d', 'Mollitia.', '2020-02-22 06:02:43', '2020-02-22 06:02:43'),
('076004b7-ed73-4545-a955-00583531983d', '142dc189-b676-4654-9c86-41f82d8c7931', '5ce3da61-f6c3-3540-9e9a-8a80d1039caa', 'Labore.', '2020-02-22 05:10:09', '2020-02-22 05:10:09'),
('09aac8d9-8974-4eb9-9ea3-836c787350d3', 'b5022337-56f8-4fe3-b402-a9da7e8a75e0', '05b16bdf-6f23-3003-8792-e01bd8e64d9e', 'Et quia.', '2020-02-22 19:13:18', '2020-02-22 19:13:18'),
('0cbed24a-bedd-47db-bccd-d7e954efbe24', '142dc189-b676-4654-9c86-41f82d8c7931', '35bb370f-b4fc-3848-9563-2a568480549a', 'Mollitia.', '2020-02-22 05:10:09', '2020-02-22 05:10:09'),
('0fdc3644-121b-4be2-ba89-99620582a611', '142dc189-b676-4654-9c86-41f82d8c7931', 'f3f68364-2b63-305b-a9ed-8667f7c43bc9', 'asd', '2020-02-22 05:10:09', '2020-02-22 05:10:09'),
('1087e5de-66ab-4a9a-9a29-a55249709db5', 'f9307d4f-970b-4cb0-9416-7ce662298659', '8ff905b0-2427-3e16-92ce-e8a0284ca16b', 'Iste a.', '2020-02-22 06:23:06', '2020-02-22 06:23:06'),
('13c70091-1c38-494b-a98c-2000f852c9a3', 'fcdc7dfe-f64a-4ddc-95f5-d858ac7e7ef7', 'f3f68364-2b63-305b-a9ed-8667f7c43bc9', 'asd', '2020-02-22 06:20:03', '2020-02-22 06:20:03'),
('1c91a066-3d71-4ffd-b222-c1411c19ce6c', '142dc189-b676-4654-9c86-41f82d8c7931', '0179160b-8b21-38c1-a673-98b4a3a2c007', 'Quos qui.', '2020-02-22 05:10:09', '2020-02-22 05:10:09'),
('1ca4d928-b23d-449a-ab9a-d6ce4632f981', 'a2ef9429-4461-491c-889c-0f92a65218c8', 'a9bba2e8-2d1f-34af-ae51-19ff58f2cef9', 'In.', '2020-02-22 06:02:43', '2020-02-22 06:02:43'),
('2016671f-cfa9-4b3b-b38f-fb21903d05b4', 'fcdc7dfe-f64a-4ddc-95f5-d858ac7e7ef7', '02691749-f7f9-336c-ac9f-0d95676379f0', 'At autem.', '2020-02-22 06:20:03', '2020-02-22 06:20:03'),
('22e7042d-6f0d-4bbb-a2cf-c7a73ad8f31f', 'a2ef9429-4461-491c-889c-0f92a65218c8', 'b921ff7f-2428-322b-ba43-469814d8b3fc', 'Ut aut.', '2020-02-22 06:02:43', '2020-02-22 06:02:43'),
('24682389-6ae5-407f-8892-16ec37830c69', 'f9307d4f-970b-4cb0-9416-7ce662298659', 'f688c278-b0b9-3465-a735-6fe16da9bcdb', 'Quos.', '2020-02-22 06:23:06', '2020-02-22 06:23:06'),
('2856d423-c410-4271-b07c-9b15d724b6f3', '26fac2cc-45df-4b60-a9e3-f3e1ea5d597f', 'c88742a0-9041-3aa9-8894-f75698ade167', 'Fugit.', '2020-02-22 05:00:49', '2020-02-22 05:00:49'),
('2b23b996-1143-4666-b0cf-4f1a29ec4751', 'f9307d4f-970b-4cb0-9416-7ce662298659', 'd59562a7-c14e-3340-b4e4-4fb7fd3dcd56', 'Nemo.', '2020-02-22 06:23:06', '2020-02-22 06:23:06'),
('3688dfa6-346f-4f7c-8737-a27448a1f50c', 'a2ef9429-4461-491c-889c-0f92a65218c8', '070cc358-77ac-3601-88a8-53b457ae7dba', 'Iste.', '2020-02-22 06:02:43', '2020-02-22 06:02:43'),
('36d97092-f8bd-4f1f-8e07-c297b4d2efac', 'b5022337-56f8-4fe3-b402-a9da7e8a75e0', 'a3b10b5e-db9a-3b3c-a4da-75a405054835', 'asd', '2020-02-22 19:13:18', '2020-02-22 19:13:18'),
('3ae8edc8-7e41-45fd-8fcf-f76dc14f5525', 'fcdc7dfe-f64a-4ddc-95f5-d858ac7e7ef7', '756d8d0b-f640-368f-9f2d-4690b0089660', 'Maxime.', '2020-02-22 06:20:03', '2020-02-22 06:20:03'),
('3b3f1281-0a75-448f-be34-f8e41eb00cde', 'a2ef9429-4461-491c-889c-0f92a65218c8', '2fc7184b-a608-3af6-a1fe-0b3487f3d06a', 'Ullam.', '2020-02-22 06:02:43', '2020-02-22 06:02:43'),
('3bd3414b-02e0-4157-9e02-c4e63a18a21f', 'f9307d4f-970b-4cb0-9416-7ce662298659', 'f145556a-c4d3-3443-91c3-3df27bd98732', 'Sed ea.', '2020-02-22 06:23:06', '2020-02-22 06:23:06'),
('46c55dac-8910-40f1-aa90-3a2f4d993acc', '142dc189-b676-4654-9c86-41f82d8c7931', 'f8e33e39-3343-3328-a765-dc359a9d2232', 'Voluptate.', '2020-02-22 05:10:09', '2020-02-22 05:10:09'),
('484d71bc-f489-408c-8d1d-be12fcd1bcb7', '26fac2cc-45df-4b60-a9e3-f3e1ea5d597f', '0179160b-8b21-38c1-a673-98b4a3a2c007', 'Iusto.', '2020-02-22 05:00:49', '2020-02-22 05:00:49'),
('4998da7a-5365-4a8b-ba62-66a8f482a572', 'fcdc7dfe-f64a-4ddc-95f5-d858ac7e7ef7', '020a0a56-c870-3760-ac0e-1a5a2e55938e', 'Quos.', '2020-02-22 06:20:03', '2020-02-22 06:20:03'),
('4a6fedb9-6aac-45e4-9a51-410303371c40', 'b5022337-56f8-4fe3-b402-a9da7e8a75e0', '7e09b85f-7322-39fa-a15f-d9ac3e441990', 'Rerum est.', '2020-02-22 19:13:18', '2020-02-22 19:13:18'),
('4eb0ebd9-bd60-405a-9cb1-5403fb5ac231', 'fcdc7dfe-f64a-4ddc-95f5-d858ac7e7ef7', '381f1c3b-ee0b-3dcc-9b34-4f6b56ab275b', 'Autem.', '2020-02-22 06:20:03', '2020-02-22 06:20:03'),
('51c88f15-2c54-4a27-af8b-bde2db6f2f77', '26fac2cc-45df-4b60-a9e3-f3e1ea5d597f', 'b073b4cb-4f26-3e1f-8fe8-6f2bff17ec94', 'Aut.', '2020-02-22 05:00:49', '2020-02-22 05:00:49'),
('53537040-1611-46e2-baca-0f89310bb8e9', '26fac2cc-45df-4b60-a9e3-f3e1ea5d597f', '5ce3da61-f6c3-3540-9e9a-8a80d1039caa', 'Officiis.', '2020-02-22 05:00:49', '2020-02-22 05:00:49'),
('54cf3de6-c132-437e-baed-21983e6d0a89', '26fac2cc-45df-4b60-a9e3-f3e1ea5d597f', '0207cc87-c743-3070-9366-faf1c24f3ddf', 'Animi.', '2020-02-22 05:00:49', '2020-02-22 05:00:49'),
('57e48f49-03aa-4c7a-94e3-188147271295', '142dc189-b676-4654-9c86-41f82d8c7931', '6b8a273f-59ed-3f63-b5f2-96ae4593c407', 'Est.', '2020-02-22 05:10:09', '2020-02-22 05:10:09'),
('58d89917-c36a-45aa-b191-a90968b0630c', '142dc189-b676-4654-9c86-41f82d8c7931', '381f1c3b-ee0b-3dcc-9b34-4f6b56ab275b', 'Ea nihil.', '2020-02-22 05:10:09', '2020-02-22 05:10:09'),
('59788b80-de37-4e37-a5bc-8ce08855592c', 'b5022337-56f8-4fe3-b402-a9da7e8a75e0', '0b87d7c4-de71-3f95-b904-20990f9cabf5', '7275', '2020-02-22 19:13:18', '2020-02-22 19:13:18'),
('6091fcb3-2c26-4cf6-8fee-0bb6258b35e1', 'f9307d4f-970b-4cb0-9416-7ce662298659', 'd2228828-387f-3f8e-b803-d6ee989e803d', 'Ea.', '2020-02-22 06:23:06', '2020-02-22 06:23:06'),
('67ef1738-20a3-4ecf-bab2-031bffc5d154', '26fac2cc-45df-4b60-a9e3-f3e1ea5d597f', '35bb370f-b4fc-3848-9563-2a568480549a', 'Ipsa sunt.', '2020-02-22 05:00:49', '2020-02-22 05:00:49'),
('68fedcbd-6317-45e0-aac9-2d195f11d2b1', 'b5022337-56f8-4fe3-b402-a9da7e8a75e0', 'd0ba6947-572a-31e6-96ca-712bf7d20195', 'Rerum.', '2020-02-22 19:13:18', '2020-02-22 19:13:18'),
('6a4ae98f-cd6d-4ada-864d-a0ca8e95be33', 'fcdc7dfe-f64a-4ddc-95f5-d858ac7e7ef7', 'f8e33e39-3343-3328-a765-dc359a9d2232', 'Maiores.', '2020-02-22 06:20:03', '2020-02-22 06:20:03'),
('75459ecd-c3de-4d04-b257-24955b39b2ed', 'f9307d4f-970b-4cb0-9416-7ce662298659', 'a99b63eb-21e7-3a03-87ac-eed969f5bc76', 'Tidak', '2020-02-22 06:23:06', '2020-02-22 06:23:06'),
('7c5670cb-59fb-4133-8efe-c3c38ed0e9e5', '26fac2cc-45df-4b60-a9e3-f3e1ea5d597f', '02691749-f7f9-336c-ac9f-0d95676379f0', 'Quae.', '2020-02-22 05:00:49', '2020-02-22 05:00:49'),
('80816ebd-9391-43fa-8ff4-f038ec9f5a2c', 'a2ef9429-4461-491c-889c-0f92a65218c8', '02691749-f7f9-336c-ac9f-0d95676379f0', 'Non qui.', '2020-02-22 06:02:43', '2020-02-22 06:02:43'),
('81fb278d-b15c-44b8-9d61-fb856cd7749b', 'fcdc7dfe-f64a-4ddc-95f5-d858ac7e7ef7', 'd59562a7-c14e-3340-b4e4-4fb7fd3dcd56', 'Qui qui.', '2020-02-22 06:20:03', '2020-02-22 06:20:03'),
('8319cfbe-874f-4754-a07c-446224f6dce0', '142dc189-b676-4654-9c86-41f82d8c7931', 'a9bba2e8-2d1f-34af-ae51-19ff58f2cef9', 'Eos sed.', '2020-02-22 05:10:09', '2020-02-22 05:10:09'),
('88788b8d-8c20-40e3-aabd-80dd66fac732', '26fac2cc-45df-4b60-a9e3-f3e1ea5d597f', '0352819e-783e-343c-bc5e-3afdaf5a626b', 'Sint.', '2020-02-22 05:00:49', '2020-02-22 05:00:49'),
('89f05080-8fa7-42eb-8f68-9330657f6873', '142dc189-b676-4654-9c86-41f82d8c7931', 'c88742a0-9041-3aa9-8894-f75698ade167', 'Id.', '2020-02-22 05:10:09', '2020-02-22 05:10:09'),
('8a2f7fea-52bc-4904-8a11-ab29e069bb4a', '26fac2cc-45df-4b60-a9e3-f3e1ea5d597f', 'f3f68364-2b63-305b-a9ed-8667f7c43bc9', 'asd', '2020-02-22 05:00:49', '2020-02-22 05:00:49'),
('8b1e3fe4-7f3c-421d-ba41-354f64546da0', 'f9307d4f-970b-4cb0-9416-7ce662298659', 'f3f68364-2b63-305b-a9ed-8667f7c43bc9', 'asd', '2020-02-22 06:23:06', '2020-02-22 06:23:06'),
('8e7ec859-3f7d-4236-871c-feee491ba7e6', 'a2ef9429-4461-491c-889c-0f92a65218c8', 'd2228828-387f-3f8e-b803-d6ee989e803d', 'Similique.', '2020-02-22 06:02:43', '2020-02-22 06:02:43'),
('9105f052-c752-4d09-b7ef-782daea774f9', 'f9307d4f-970b-4cb0-9416-7ce662298659', '756d8d0b-f640-368f-9f2d-4690b0089660', 'Ducimus.', '2020-02-22 06:23:06', '2020-02-22 06:23:06'),
('93a385cd-cbf4-476b-b0f3-d6a8d9c0f7e8', '142dc189-b676-4654-9c86-41f82d8c7931', '0352819e-783e-343c-bc5e-3afdaf5a626b', 'Doloribus.', '2020-02-22 05:10:09', '2020-02-22 05:10:09'),
('98fb30ac-d1ae-44d7-a9cd-9d49328839ca', 'b5022337-56f8-4fe3-b402-a9da7e8a75e0', '4a3f261e-8e20-30c2-ad1a-763c14a6d2be', 'asd', '2020-02-22 19:13:18', '2020-02-22 19:13:18'),
('9b5b3ac0-e924-4121-9a10-cf21ab47e6a8', 'f9307d4f-970b-4cb0-9416-7ce662298659', 'a9bba2e8-2d1f-34af-ae51-19ff58f2cef9', 'Iure.', '2020-02-22 06:23:06', '2020-02-22 06:23:06'),
('a2cc90f2-8565-4685-a701-968ec3a5fe30', 'b5022337-56f8-4fe3-b402-a9da7e8a75e0', 'd59562a7-c14e-3340-b4e4-4fb7fd3dcd56', 'Ipsa.', '2020-02-22 19:13:18', '2020-02-22 19:13:18'),
('a3e13170-848f-4c0b-a3e5-06c09362ebc1', 'b5022337-56f8-4fe3-b402-a9da7e8a75e0', '02691749-f7f9-336c-ac9f-0d95676379f0', 'Quas ea.', '2020-02-22 19:13:18', '2020-02-22 19:13:18'),
('a496712f-2fcd-4b81-b460-f4f41124f002', '26fac2cc-45df-4b60-a9e3-f3e1ea5d597f', 'a9bba2e8-2d1f-34af-ae51-19ff58f2cef9', 'Id.', '2020-02-22 05:00:49', '2020-02-22 05:00:49'),
('a7df255b-c05a-4df8-aeb3-05a0fca24192', '26fac2cc-45df-4b60-a9e3-f3e1ea5d597f', '381f1c3b-ee0b-3dcc-9b34-4f6b56ab275b', 'Est fuga.', '2020-02-22 05:00:49', '2020-02-22 05:00:49'),
('a8a6bc12-f854-4bea-8868-4896f24d8664', 'b5022337-56f8-4fe3-b402-a9da7e8a75e0', '720fa63d-c2c0-3bb8-bace-a31ed2ee25a1', '4179', '2020-02-22 19:13:18', '2020-02-22 19:13:18'),
('a8b8678b-d5c2-4ec5-89a5-6610f5cacf4f', 'f9307d4f-970b-4cb0-9416-7ce662298659', '381f1c3b-ee0b-3dcc-9b34-4f6b56ab275b', 'Enim.', '2020-02-22 06:23:06', '2020-02-22 06:23:06'),
('a935c0a2-0424-42a2-b48a-a7b34a253c18', 'b5022337-56f8-4fe3-b402-a9da7e8a75e0', 'edd3dd93-3485-3cbc-828c-55bce7f0fda0', 'Accusamus.', '2020-02-22 19:13:18', '2020-02-22 19:13:18'),
('a977085a-d51b-4ff4-9158-9da1e8ae1894', 'b5022337-56f8-4fe3-b402-a9da7e8a75e0', 'd2228828-387f-3f8e-b803-d6ee989e803d', 'Et', '2020-02-22 19:13:17', '2020-02-22 19:13:17'),
('af1e1a9e-7a65-4555-8a53-c65dae4b18ab', 'f9307d4f-970b-4cb0-9416-7ce662298659', '020a0a56-c870-3760-ac0e-1a5a2e55938e', 'Et illum.', '2020-02-22 06:23:06', '2020-02-22 06:23:06'),
('b21cebdc-8979-4438-b2a5-78ce29a7b669', 'fcdc7dfe-f64a-4ddc-95f5-d858ac7e7ef7', '0179160b-8b21-38c1-a673-98b4a3a2c007', 'Magnam.', '2020-02-22 06:20:03', '2020-02-22 06:20:03'),
('bd3f5f0b-3605-4dd5-b83d-d2fa85c8525c', '26fac2cc-45df-4b60-a9e3-f3e1ea5d597f', 'd2228828-387f-3f8e-b803-d6ee989e803d', 'Officia.', '2020-02-22 05:00:49', '2020-02-22 05:00:49'),
('beef344d-6319-497d-b88e-a29ac807c720', '26fac2cc-45df-4b60-a9e3-f3e1ea5d597f', 'd59562a7-c14e-3340-b4e4-4fb7fd3dcd56', 'Suscipit.', '2020-02-22 05:00:49', '2020-02-22 05:00:49'),
('c0d8a477-c793-4ec9-871a-60fa55ac7984', '26fac2cc-45df-4b60-a9e3-f3e1ea5d597f', 'f8e33e39-3343-3328-a765-dc359a9d2232', 'Nisi.', '2020-02-22 05:00:49', '2020-02-22 05:00:49'),
('c11548ef-5c7b-40cf-8110-5ceff30c3ada', 'f9307d4f-970b-4cb0-9416-7ce662298659', '6b8a273f-59ed-3f63-b5f2-96ae4593c407', 'Ut eos.', '2020-02-22 06:23:06', '2020-02-22 06:23:06'),
('c285b50e-9824-465a-a5b2-9b1cbf5e1879', 'a2ef9429-4461-491c-889c-0f92a65218c8', '0179160b-8b21-38c1-a673-98b4a3a2c007', 'Veritatis.', '2020-02-22 06:02:43', '2020-02-22 06:02:43'),
('c28f8418-c1ea-4b20-b251-f0ddf5846d54', 'b5022337-56f8-4fe3-b402-a9da7e8a75e0', '32123519-a5c3-33c4-9347-4fe94eea0a90', 'Labore.', '2020-02-22 19:13:18', '2020-02-22 19:13:18'),
('c4477739-8916-4f52-9122-b89f0765bbec', 'fcdc7dfe-f64a-4ddc-95f5-d858ac7e7ef7', 'd2228828-387f-3f8e-b803-d6ee989e803d', 'Et natus.', '2020-02-22 06:20:03', '2020-02-22 06:20:03'),
('c6b541ce-11a9-436e-82cc-eca508bec6e5', 'a2ef9429-4461-491c-889c-0f92a65218c8', '8ab38b3c-1e66-31c2-b8bc-61b7d4374de5', 'Commodi.', '2020-02-22 06:02:43', '2020-02-22 06:02:43'),
('c8300986-1a5e-4227-a8e9-9ccf2cdea08d', 'b5022337-56f8-4fe3-b402-a9da7e8a75e0', '09cfd508-fcc2-3984-8577-2a3387c88894', 'Id nihil.', '2020-02-22 19:13:18', '2020-02-22 19:13:18'),
('c9ed01df-f219-4428-8d00-ae4763291986', 'fcdc7dfe-f64a-4ddc-95f5-d858ac7e7ef7', 'f688c278-b0b9-3465-a735-6fe16da9bcdb', 'Provident.', '2020-02-22 06:20:03', '2020-02-22 06:20:03'),
('cc721345-d02c-49a8-ac67-ba4f3700d3cc', 'f9307d4f-970b-4cb0-9416-7ce662298659', '02691749-f7f9-336c-ac9f-0d95676379f0', 'Natus a.', '2020-02-22 06:23:06', '2020-02-22 06:23:06'),
('d159fb45-3969-44f2-a23a-9da6b49e3add', 'f9307d4f-970b-4cb0-9416-7ce662298659', '0179160b-8b21-38c1-a673-98b4a3a2c007', 'Culpa.', '2020-02-22 06:23:06', '2020-02-22 06:23:06'),
('d3ea9c1b-9f56-475d-b365-54d3486a90b9', 'a2ef9429-4461-491c-889c-0f92a65218c8', 'fbaa5890-63d9-3895-9193-48540c4b8b74', 'Quae qui.', '2020-02-22 06:02:43', '2020-02-22 06:02:43'),
('d53eb0c8-8b84-4185-b077-31f4af68827c', 'fcdc7dfe-f64a-4ddc-95f5-d858ac7e7ef7', '6b8a273f-59ed-3f63-b5f2-96ae4593c407', 'Et quam.', '2020-02-22 06:20:03', '2020-02-22 06:20:03'),
('d6f01c41-8cf3-4e74-9a1a-9a6a2d59d011', 'fcdc7dfe-f64a-4ddc-95f5-d858ac7e7ef7', 'a99b63eb-21e7-3a03-87ac-eed969f5bc76', 'Tidak', '2020-02-22 06:20:03', '2020-02-22 06:20:03'),
('d71bab31-6201-4a64-ba60-7feb6f7d049e', 'a2ef9429-4461-491c-889c-0f92a65218c8', 'f3f68364-2b63-305b-a9ed-8667f7c43bc9', 'asd', '2020-02-22 06:02:43', '2020-02-22 06:02:43'),
('dc3175b8-9cc0-4020-9a7a-55f9fae03b46', '142dc189-b676-4654-9c86-41f82d8c7931', 'd2228828-387f-3f8e-b803-d6ee989e803d', 'Sit eaque.', '2020-02-22 05:10:09', '2020-02-22 05:10:09'),
('dd8a0c8f-500b-4acd-a776-32df907767ab', '26fac2cc-45df-4b60-a9e3-f3e1ea5d597f', '6b8a273f-59ed-3f63-b5f2-96ae4593c407', 'Pariatur.', '2020-02-22 05:00:49', '2020-02-22 05:00:49'),
('e25ca7ff-2657-4641-8884-4db754ae674c', 'b5022337-56f8-4fe3-b402-a9da7e8a75e0', '3b6a13f6-825c-30e1-bd00-bf0c9428bce9', 'Quisquam.', '2020-02-22 19:13:18', '2020-02-22 19:13:18'),
('e44bc922-7da8-47fd-b620-1792d50955a7', '142dc189-b676-4654-9c86-41f82d8c7931', 'd59562a7-c14e-3340-b4e4-4fb7fd3dcd56', 'Saepe.', '2020-02-22 05:10:09', '2020-02-22 05:10:09'),
('eed24e32-ea3f-43b4-b60a-aba21ed7c3a1', '142dc189-b676-4654-9c86-41f82d8c7931', '02691749-f7f9-336c-ac9f-0d95676379f0', 'Sed.', '2020-02-22 05:10:09', '2020-02-22 05:10:09'),
('f00584d0-bf75-40bd-bc70-465f55f19b0d', 'f9307d4f-970b-4cb0-9416-7ce662298659', '73d028e8-d6ae-3439-a3c7-c42b3dd15cf8', '1507', '2020-02-22 06:23:06', '2020-02-22 06:23:06'),
('f592b1d4-0a0b-41ea-b169-ae15601e388e', 'f9307d4f-970b-4cb0-9416-7ce662298659', 'f8e33e39-3343-3328-a765-dc359a9d2232', 'Et.', '2020-02-22 06:23:06', '2020-02-22 06:23:06'),
('f7f8135a-23f3-4168-b4b2-278974a18fae', 'f9307d4f-970b-4cb0-9416-7ce662298659', '1a9b6e27-91db-3466-bfe0-5fdb302b4307', 'Eaque.', '2020-02-22 06:23:06', '2020-02-22 06:23:06'),
('f886099e-5a74-4dec-87b8-7cdfc61840e1', 'fcdc7dfe-f64a-4ddc-95f5-d858ac7e7ef7', 'f145556a-c4d3-3443-91c3-3df27bd98732', 'Harum.', '2020-02-22 06:20:03', '2020-02-22 06:20:03'),
('f8ee45a7-de4f-467b-9295-7ae382cb66ef', 'fcdc7dfe-f64a-4ddc-95f5-d858ac7e7ef7', 'a9bba2e8-2d1f-34af-ae51-19ff58f2cef9', 'Amet.', '2020-02-22 06:20:03', '2020-02-22 06:20:03'),
('fa4acace-fb14-40ee-a057-39e2f3508ca2', 'b5022337-56f8-4fe3-b402-a9da7e8a75e0', '8dcf99fd-48bd-3ae2-a30f-080ced062f7a', 'Et ab quo.', '2020-02-22 19:13:18', '2020-02-22 19:13:18'),
('fb0241a0-1023-403f-bf62-6d0dcd8180f3', 'a2ef9429-4461-491c-889c-0f92a65218c8', 'd59562a7-c14e-3340-b4e4-4fb7fd3dcd56', 'Dolor.', '2020-02-22 06:02:43', '2020-02-22 06:02:43'),
('fe65d7e5-0c5e-4325-a8de-09cf3f60570a', 'b5022337-56f8-4fe3-b402-a9da7e8a75e0', '328e919c-f630-3efe-9a4d-4af7e3a242e4', 'Et quidem.', '2020-02-22 19:13:18', '2020-02-22 19:13:18');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2020_01_19_111858_create_agencies_table', 1),
(2, '2020_01_19_111859_create_users_table', 1),
(3, '2020_01_19_111860_create_requirement_types_table', 1),
(4, '2020_01_19_111861_create_requirement_forms_table', 1),
(5, '2020_01_19_111862_create_requirements_table', 1),
(6, '2020_01_19_111863_create_requirement_posts_table', 1),
(7, '2020_01_20_111941_create_biodatas_table', 1),
(8, '2020_02_15_152714_create_requirement_type_biodatas_table', 1),
(9, '2020_02_15_152805_create_biodata_posts_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `requirements`
--

CREATE TABLE `requirements` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `agency_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `requirement_type_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('1','2','3','4') COLLATE utf8mb4_unicode_ci NOT NULL,
  `sent_at` datetime DEFAULT NULL,
  `first_verify_at` datetime DEFAULT NULL,
  `second_verify_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `requirements`
--

INSERT INTO `requirements` (`id`, `agency_id`, `requirement_type_id`, `status`, `sent_at`, `first_verify_at`, `second_verify_at`, `created_at`, `updated_at`) VALUES
('142dc189-b676-4654-9c86-41f82d8c7931', '0f4b52d8-7a8d-3a72-96ce-4230afdff09a', '54ebc251-94e6-3fd7-937b-444ee125d027', '4', '2020-02-22 12:11:37', '2020-02-22 12:11:50', '2020-02-22 12:11:59', '2020-02-22 05:10:09', '2020-02-22 05:11:59'),
('26fac2cc-45df-4b60-a9e3-f3e1ea5d597f', '0f4b52d8-7a8d-3a72-96ce-4230afdff09a', 'db8db0a8-2617-3788-9897-5ef3b666f2a8', '4', '2020-02-22 12:01:51', '2020-02-22 12:02:13', '2020-02-22 12:02:45', '2020-02-22 05:00:49', '2020-02-22 05:02:45'),
('a2ef9429-4461-491c-889c-0f92a65218c8', '0f4b52d8-7a8d-3a72-96ce-4230afdff09a', 'ecc5f0f0-c7e8-306d-9bb6-440a97bb771b', '4', '2020-02-22 13:03:26', '2020-02-22 13:03:41', '2020-02-22 13:03:53', '2020-02-22 06:02:43', '2020-02-22 06:03:53'),
('b5022337-56f8-4fe3-b402-a9da7e8a75e0', '0f4b52d8-7a8d-3a72-96ce-4230afdff09a', '6c88f416-c616-3623-9ea1-7f22e3f8c54a', '4', '2020-02-22 11:40:20', '2020-02-22 11:49:20', '2020-02-22 11:49:46', '2020-02-22 19:13:17', '2020-02-22 04:49:46'),
('f9307d4f-970b-4cb0-9416-7ce662298659', '0f4b52d8-7a8d-3a72-96ce-4230afdff09a', '1d9c1b26-1d83-3afc-a05d-a8a38214bd1f', '4', '2020-02-22 13:24:12', '2020-02-22 13:24:27', '2020-02-22 13:24:37', '2020-02-22 06:23:06', '2020-02-22 06:24:37'),
('fcdc7dfe-f64a-4ddc-95f5-d858ac7e7ef7', '0f4b52d8-7a8d-3a72-96ce-4230afdff09a', '69ffc6ec-5bb4-3a01-aedc-292e462ad5b4', '4', '2020-02-22 13:20:59', '2020-02-22 13:21:16', '2020-02-22 13:21:35', '2020-02-22 06:20:03', '2020-02-22 06:21:35');

-- --------------------------------------------------------

--
-- Table structure for table `requirement_forms`
--

CREATE TABLE `requirement_forms` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `requirement_type_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `requirement_forms`
--

INSERT INTO `requirement_forms` (`id`, `requirement_type_id`, `name`, `description`, `created_at`, `updated_at`) VALUES
('010ce426-54aa-3c31-af3c-31760194f5b7', 'd4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', 'Fotokopi sah SK Pengangkatan Kembali', NULL, '2020-01-30 11:49:13', '2020-02-22 18:49:21'),
('0320b002-c84d-3aae-b077-1c1603894c6c', '9ca65942-60ab-38c5-8081-b8d6d75a38f6', 'Fotokopi sah SK Kenaikan Pangkat terakhir', NULL, '2020-01-30 11:54:52', '2020-02-22 18:49:22'),
('038308a1-2e1d-3397-9132-ce343a4997b1', '54ebc251-94e6-3fd7-937b-444ee125d027', 'Surat rekomendasi lolos Butuh dari Instansi yang dituju/menerima dengan mencantumkan nama jabatan dan kelas jabatan (Asli Cap Basah)', NULL, '2020-02-02 11:01:10', '2020-02-22 18:49:13'),
('03af8985-e5d4-3345-aa38-0090d6188782', '1d9c1b26-1d83-3afc-a05d-a8a38214bd1f', 'Surat Keterangan (Hukdis) dari SKPD', NULL, '2020-01-23 07:53:41', '2020-02-22 18:49:18'),
('03b80105-02e0-3e01-b574-c875d2920f2c', 'd4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', 'Fotokopi sah PAK lama', NULL, '2020-01-30 11:51:48', '2020-02-22 18:49:21'),
('03d0cf9d-36c7-3f49-9cf2-dbe3b740b118', 'd4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', 'Nota Usul Kenaikan Pangkat', NULL, '2020-01-30 11:46:43', '2020-02-22 18:49:20'),
('06c9964a-7783-3dd4-a949-28e2f3428cb0', '2d093e78-c955-31c6-88c4-bdcbb73e2cee', 'FC. SK PNS / SK Terakhir dilegalisir', NULL, '2020-02-03 07:49:33', '2020-02-22 18:49:14'),
('07128c9e-66ec-33a7-9bee-ae8c45a264da', '980a10df-873d-3d11-a31c-8c46bb20195f', 'Pas Photo 3x4 dan 4x6 masing masing 2 lembar', NULL, '2020-01-23 07:48:01', '2020-02-22 18:49:09'),
('0725b5cb-6b3b-38a4-a7b5-57a10b75831c', '1690ac5e-f0a2-3cff-aa58-4d2c7ecbe38e', 'Fotokopi sah SK Jabatan baru', NULL, '2020-01-30 12:02:44', '2020-02-22 18:49:08'),
('078a7588-1c5a-3b26-8e2a-28169ab3ba7e', '1690ac5e-f0a2-3cff-aa58-4d2c7ecbe38e', 'Fotokopi sah SK Kenaikan Pangkat terakhir', NULL, '2020-01-30 12:01:54', '2020-02-22 18:49:08'),
('0862e123-8d9e-3158-9bf3-9da2af8b76dc', '8cda4fd0-23da-39d6-9797-7fd7abba5eee', 'Surat Keterangan dari pejabat yang berwenang apabila terdapat permasalahan', NULL, '2020-01-30 11:45:36', '2020-02-22 18:49:20'),
('08ea8c48-f761-3c5f-8e58-d0836d4c1385', '9ca65942-60ab-38c5-8081-b8d6d75a38f6', 'Lampiran Akreditasi BAN-PT', NULL, '2020-01-30 11:55:10', '2020-02-22 18:49:05'),
('094fe148-d5e5-3d6f-a827-897ab9ca2130', '54ebc251-94e6-3fd7-937b-444ee125d027', 'FC. SK Jafung (legalisir) (untuk guru)', NULL, '2020-02-02 10:59:11', '2020-02-22 18:49:12'),
('09d5c517-d2db-3bd2-a24a-b83b54e2d667', '5b5e143b-0d14-344c-b554-c333fb66b241', 'Surat Keterangan Uraian Tugas', NULL, '2020-01-23 07:49:29', '2020-02-22 18:49:13'),
('0b542c53-3476-3966-98fe-4dc21fedc094', '8cda4fd0-23da-39d6-9797-7fd7abba5eee', 'Fotokopi sah SK Jabatan Fungsional baru (terakhir)', NULL, '2020-01-30 11:44:05', '2020-02-22 18:49:19'),
('0b6ee839-5629-305c-a2a6-997c636f555c', '69ffc6ec-5bb4-3a01-aedc-292e462ad5b4', 'SKP 1 (satu) tahun terakhir', NULL, '2020-01-23 05:00:31', '2020-02-22 18:49:19'),
('0b758f1f-4e14-37d0-baab-840538f51be0', '1690ac5e-f0a2-3cff-aa58-4d2c7ecbe38e', 'Fotokopi sah SK Jabatan Pertama', NULL, '2020-01-30 12:02:25', '2020-02-22 18:49:08'),
('0c730976-7cd3-3801-93f5-18591f2d8a0d', '5b5e143b-0d14-344c-b554-c333fb66b241', 'Foto Copy Surat Izin Belajar Atau Surat Ket. Lulus Kuliah', NULL, '2020-01-22 11:17:09', '2020-02-22 18:49:05'),
('0d00dd8a-a155-3073-b14d-bcfd61613ea9', '1690ac5e-f0a2-3cff-aa58-4d2c7ecbe38e', 'Nota Usul Kenaikan Pangkat', NULL, '2020-01-30 12:01:29', '2020-02-22 18:49:08'),
('0e577817-8c0d-3a47-b489-190e82e570ec', 'ece49cf7-4d45-385f-afc0-b899d74fbbfb', 'Fotokopi sah Penilaian Prestasi Kerja/SKP 2 Tahun terakhir', NULL, '2020-01-23 08:22:07', '2020-02-22 18:49:18'),
('0eaeb569-7bb1-338f-a256-0b7a1aa7a7b9', 'd4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', 'Asli PAK baru', NULL, '2020-01-30 11:51:52', '2020-02-22 18:49:21'),
('106e63e2-9fd5-3828-b71d-07d703fbc4d7', '17f83b86-af2e-32fe-8190-36d488400e82', 'FC. STTPL Prajabatan dilegalisir', NULL, '2020-02-03 07:54:21', '2020-02-22 18:49:14'),
('12d5368f-cad0-3a99-b063-1d9083d17fac', '189539c4-fda9-380e-bc41-c121bc07c65a', 'SK PNS', NULL, '2020-02-15 21:14:07', '2020-02-22 18:49:15'),
('137f70ee-05fe-3819-a3c9-ae930d6189cf', '189539c4-fda9-380e-bc41-c121bc07c65a', 'Karis / Karsu', NULL, '2020-02-15 21:13:40', '2020-02-22 18:49:15'),
('14d7f80d-9893-3c61-97b4-faab2e869b8a', '189539c4-fda9-380e-bc41-c121bc07c65a', 'Hukuman Disiplin (Hukdis)', NULL, '2020-02-15 21:14:40', '2020-02-22 18:49:15'),
('1560f661-be63-3975-86a7-25020e6103e0', '1690ac5e-f0a2-3cff-aa58-4d2c7ecbe38e', 'Surat Keterangan dari pejabat yang berwenang apabila terdapat permasalahan', NULL, '2020-01-30 12:03:40', '2020-02-22 18:49:09'),
('16386904-5403-3211-b88f-fa183ac51959', '6358495e-ec55-350e-b123-e36b8ef5aba0', 'Fotokopi sah SK Tugas Belajar', NULL, '2020-01-30 11:59:49', '2020-02-22 18:49:07'),
('164974f6-a45d-338f-8b2f-49a65648cb52', 'd4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', 'Fotokopi sah SK CPNS', NULL, '2020-01-30 11:47:05', '2020-02-22 18:49:20'),
('17fe5c52-79f9-36ba-b2db-c101084a8b4c', '9ca65942-60ab-38c5-8081-b8d6d75a38f6', 'Nota Usul Kenaikan Pangkat', NULL, '2020-01-30 11:54:21', '2020-02-22 18:49:21'),
('1a076b94-31f3-3111-ac37-9124bdbee746', 'ece49cf7-4d45-385f-afc0-b899d74fbbfb', 'Fotokopi sah Ijazah', NULL, '2020-01-23 08:21:21', '2020-02-22 18:49:18'),
('1a1c6fa9-0db9-37de-bee5-54857a262f82', '11cd4a37-6ea2-324b-b58c-db79c1b6c49a', 'FC. Surat Nikah Legalisir KUA', NULL, '2020-02-03 07:52:27', '2020-02-22 18:49:14'),
('1a2e4fd4-5758-33f3-a2f7-878b32a4b576', '980a10df-873d-3d11-a31c-8c46bb20195f', 'Surat Ket. Hukdis Dari Kepala Perangkat Daerah', NULL, '2020-01-23 07:46:32', '2020-02-22 18:49:06'),
('1a53771a-e32b-378e-9452-6dd3cd2a7b0e', '972c7e95-7dea-3185-9df1-01ee8f0a3e1c', 'Asli PAK baru', NULL, '2020-01-30 12:06:29', '2020-02-22 18:49:11'),
('1b15bdc2-3a80-3d11-a5c4-50f990f336fd', '189539c4-fda9-380e-bc41-c121bc07c65a', 'KTP', NULL, '2020-02-15 21:15:32', '2020-02-22 18:49:16'),
('1b73301f-2f3d-3aee-896b-a39d90012d82', '5b5e143b-0d14-344c-b554-c333fb66b241', 'Fotocopy SK Pangkat Terakhir', NULL, '2020-01-23 07:52:07', '2020-02-22 18:49:17'),
('1f2b74e8-e068-3d06-acd9-251c8cae3c2b', 'db8db0a8-2617-3788-9897-5ef3b666f2a8', 'FC. Kenaikan Pangkat Terakhir (legalisir)', NULL, '2020-01-31 14:17:18', '2020-02-22 18:49:11'),
('21158d45-6d30-3fc5-a89e-66015e1a2727', '9ca65942-60ab-38c5-8081-b8d6d75a38f6', 'Fotokopi sah SK PNS', NULL, '2020-01-30 11:54:39', '2020-02-22 18:49:22'),
('22d03285-37ad-3fc5-b7f1-3a3e2e161f65', '1690ac5e-f0a2-3cff-aa58-4d2c7ecbe38e', 'Fotokopi sah Ijazah', NULL, '2020-01-30 12:02:01', '2020-02-22 18:49:08'),
('22fdf0ab-0f26-362d-b818-ce7bf1b45310', '5b5e143b-0d14-344c-b554-c333fb66b241', 'Fotocopy SKP 2 Tahun terakhir', NULL, '2020-01-23 07:48:55', '2020-02-22 18:49:11'),
('23bf74f0-144a-383f-b421-ae418c84cad0', '189539c4-fda9-380e-bc41-c121bc07c65a', 'KGB', NULL, '2020-02-15 21:14:24', '2020-02-22 18:49:15'),
('24194302-d070-3242-b06f-91591393d29a', '972c7e95-7dea-3185-9df1-01ee8f0a3e1c', 'Fotokopi sah SK Jabatan terakhir', NULL, '2020-01-30 12:05:07', '2020-02-22 18:49:09'),
('2654e03d-772b-36b8-9f20-a47cee82e0c4', 'd4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', 'Fotokopi sah SK Jabatan Fungsional baru (terakhir)', NULL, '2020-01-30 11:48:53', '2020-02-22 18:49:21'),
('266b5b48-c005-3b8c-a552-ca03440b068f', '972c7e95-7dea-3185-9df1-01ee8f0a3e1c', 'Fotokopi sah SK Kenaikan Pangkat terakhir', NULL, '2020-01-30 12:04:49', '2020-02-22 18:49:09'),
('269037d7-864d-3ed6-ad09-17e4ef305017', '6358495e-ec55-350e-b123-e36b8ef5aba0', 'Fotokopi sah Penilaian Prestasi Kerja PNS 2 tahun terakhir', NULL, '2020-01-30 12:00:06', '2020-02-22 18:49:07'),
('2700960f-df85-3845-9113-82d0695f386d', 'd4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', 'Fotokopi sah SK Kenaikan Pangkat terakhir', NULL, '2020-01-30 11:47:18', '2020-02-22 18:49:20'),
('2854349d-69f8-3d73-ab95-5462863a8031', '11cd4a37-6ea2-324b-b58c-db79c1b6c49a', 'FC. SK PNS / SK Terakhir dilegalisir', NULL, '2020-02-03 07:52:20', '2020-02-22 18:49:14'),
('2b4dbf1e-2a55-349e-93b4-ea9e6ddbd3c6', '972c7e95-7dea-3185-9df1-01ee8f0a3e1c', 'Fotokopi sah ijazah terbarunya', NULL, '2020-01-30 12:06:43', '2020-02-22 18:49:11'),
('2fd8f165-2f08-3c9b-a5eb-7d9ff7841661', '8cda4fd0-23da-39d6-9797-7fd7abba5eee', 'Fotokopi sah Transkrip Nilai', NULL, '2020-01-30 11:42:53', '2020-02-22 18:49:19'),
('325385bf-ccd4-3599-b705-cef8d3602714', 'ecc5f0f0-c7e8-306d-9bb6-440a97bb771b', 'Surat Perintah Terakhir', NULL, '2020-02-07 05:56:52', '2020-02-22 18:49:16'),
('360fc8e6-6c0e-3aa8-b79d-fd70e40976f2', '9ca65942-60ab-38c5-8081-b8d6d75a38f6', 'Fotokopi sah SK Jabatan Pejabat penilai', NULL, '2020-01-30 11:56:43', '2020-02-22 18:49:06'),
('36941ff2-f7be-308c-80fa-5a1c1a732369', '8cda4fd0-23da-39d6-9797-7fd7abba5eee', 'Asli PAK baru', NULL, '2020-01-30 11:45:04', '2020-02-22 18:49:20'),
('36bb8dac-aee0-3333-9db2-fb7c54ae5fd9', '9ca65942-60ab-38c5-8081-b8d6d75a38f6', 'Fotokopi sah SK CPNS', NULL, '2020-01-30 11:54:30', '2020-02-22 18:49:22'),
('37718511-4fbf-30e0-89f3-fb7b3f0868c1', '5b5e143b-0d14-344c-b554-c333fb66b241', 'Surat Keterangan Kepela Perangkat daerah tentnag tugas dan Ijazah yang diperoleh (Linear)', NULL, '2020-01-23 07:50:59', '2020-02-22 18:49:16'),
('37f92526-ebd7-3041-9e8b-b0fd452f882a', '8cda4fd0-23da-39d6-9797-7fd7abba5eee', 'Fotokopi sah Penilaian Prestasi Kerja/SKP 2 Tahun terakhir', NULL, '2020-01-30 11:45:18', '2020-02-22 18:49:20'),
('3bd6a293-0bb5-3d22-ac64-f7d0981c177e', '2d093e78-c955-31c6-88c4-bdcbb73e2cee', 'Pas Photo Isteri Uk 3x4 2 buah', NULL, '2020-02-03 07:48:50', '2020-02-22 18:49:13'),
('3d0df18d-147a-3f7c-8a40-651c1532dcc6', '6358495e-ec55-350e-b123-e36b8ef5aba0', 'Surat Pernyataan Kepala OPD', NULL, '2020-01-30 11:58:32', '2020-02-22 18:49:07'),
('3d1e2d76-0d95-3499-a36f-1a1216e028fb', '1690ac5e-f0a2-3cff-aa58-4d2c7ecbe38e', 'Surat Pernyataan Kepala OPD', NULL, '2020-01-30 12:01:25', '2020-02-22 18:49:08'),
('3f5861b0-939f-3816-94f1-b9de819b27a0', 'ece49cf7-4d45-385f-afc0-b899d74fbbfb', 'Fotokopi sah SK Jabatan Atasan Pejabat Penilai', NULL, '2020-01-23 08:23:29', '2020-02-22 18:49:19'),
('4068552f-8d8e-3a38-afac-e9cd0b32f7c4', 'ecc5f0f0-c7e8-306d-9bb6-440a97bb771b', 'FC. SK PNS / SK Terakhir dilegalisir', NULL, '2020-02-07 05:56:35', '2020-02-22 18:49:16'),
('40b03ed2-cd67-33a3-890d-8e61c3acdcb6', '8cda4fd0-23da-39d6-9797-7fd7abba5eee', 'Surat Pernyataan Kepala OPD', NULL, '2020-01-30 11:41:25', '2020-02-22 18:49:19'),
('40c3f529-e706-333f-9a88-e4089ce1a8ca', '8cda4fd0-23da-39d6-9797-7fd7abba5eee', 'Surat Pengantar (Usulan dari Instansi)', NULL, '2020-01-30 11:41:14', '2020-02-22 18:49:19'),
('410c8067-4d26-3523-bd2e-5ebff203c71d', '03cd67b7-b836-3703-82d0-63e7c7f24ac7', 'SK Jabatan Terakhir', NULL, '2020-02-02 16:36:04', '2020-02-22 18:49:13'),
('41b95b45-b8e1-3ede-82d2-33af477e0c74', '9ca65942-60ab-38c5-8081-b8d6d75a38f6', 'Fotokopi sah Transkrip Nilai', NULL, '2020-01-30 11:55:17', '2020-02-22 18:49:05'),
('4235d76d-abcb-32c1-a165-366898ee75ce', '972c7e95-7dea-3185-9df1-01ee8f0a3e1c', 'Surat Pengantar (Usulan dari Instansi)', NULL, '2020-01-30 12:04:16', '2020-02-22 18:49:09'),
('4319dae5-4919-3aeb-95e1-ccb325ea2d7b', 'ece49cf7-4d45-385f-afc0-b899d74fbbfb', 'Surat Keterangan dari pejabat yang berwenang apabila terdapat permasalahan', NULL, '2020-01-23 08:22:47', '2020-02-22 18:49:18'),
('43352140-4edd-3143-ad7d-041a46e342d3', '9ca65942-60ab-38c5-8081-b8d6d75a38f6', 'Izin Belajar', NULL, '2020-01-30 11:55:19', '2020-02-22 18:49:06'),
('4392a2ec-5067-3388-b129-a997e922bf22', 'ece49cf7-4d45-385f-afc0-b899d74fbbfb', 'Surat Keterangan tidak pertnah dijatuhi hukuman disiplin', NULL, '2020-01-23 08:23:07', '2020-02-22 18:49:18'),
('43b00796-ecc3-384e-ab8d-49f39a17b9e1', '972c7e95-7dea-3185-9df1-01ee8f0a3e1c', 'Fotokopi sah SK Pengaktifan kembali dari Tugas Belajar', NULL, '2020-01-30 12:06:14', '2020-02-22 18:49:10'),
('44a85877-bb8f-3612-b5e5-ab08e36ccd8a', '972c7e95-7dea-3185-9df1-01ee8f0a3e1c', 'Fotokopi sah SK Pembebasan Sementara', NULL, '2020-01-30 12:05:14', '2020-02-22 18:49:10'),
('479869df-b542-3104-bb43-b35e362a1a51', 'db8db0a8-2617-3788-9897-5ef3b666f2a8', 'FC. Karpeg (legalisir)', NULL, '2020-01-31 14:18:10', '2020-02-22 18:49:12'),
('48327103-041e-36a4-869b-38621d4bb7aa', 'ece49cf7-4d45-385f-afc0-b899d74fbbfb', 'Surat Penyataan Kepala OPD', NULL, '2020-01-23 08:20:22', '2020-02-22 18:49:18'),
('4c715d22-273d-37ae-aa23-6bc996fba8f9', 'd4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', 'Fotokopi sah Penilaian Prestasi Kerja/SKP 2 Tahun terakhir', NULL, '2020-01-30 11:52:15', '2020-02-22 18:49:21'),
('4eedbea1-eb33-370c-9fa4-d7442c3d2c50', '5b5e143b-0d14-344c-b554-c333fb66b241', 'Fotocopy Ijazah Legalisir', NULL, '2020-01-23 07:49:10', '2020-02-22 18:49:11'),
('4f2f0007-c487-38ef-b53d-93b93ee7557f', '9ca65942-60ab-38c5-8081-b8d6d75a38f6', 'Fotokopi sah Penilaian Prestasi Kerja/SKP 2 tahun terakhir', NULL, '2020-01-30 11:56:34', '2020-02-22 18:49:06'),
('4f8d28c1-1ecb-34ae-8cee-d7fdfbfd7ce6', '972c7e95-7dea-3185-9df1-01ee8f0a3e1c', 'Fotokopi sah SK Jabatan Pejabat Penilai', NULL, '2020-01-30 12:05:30', '2020-02-22 18:49:10'),
('4fffd569-0cbd-371c-9ce6-f50668a42806', '972c7e95-7dea-3185-9df1-01ee8f0a3e1c', 'Fotokopi sah SK CPNS', NULL, '2020-01-30 12:04:39', '2020-02-22 18:49:09'),
('504966f9-eea7-3a95-99c9-f080fc241c5b', '972c7e95-7dea-3185-9df1-01ee8f0a3e1c', 'Nota Usul Kenaikan Pangkat', NULL, '2020-01-30 12:04:31', '2020-02-22 18:49:09'),
('532febae-9cf5-3ae1-8694-19c91873dd10', 'd4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', 'Surat Keterangan dari pejabat yang berwenang apabila terdapat permasalahan', NULL, '2020-01-30 11:52:23', '2020-02-22 18:49:21'),
('54307497-c46a-3d04-a2d0-035087a6f8dd', '8cda4fd0-23da-39d6-9797-7fd7abba5eee', 'Fotokopi sah PAK lama', NULL, '2020-01-30 11:44:58', '2020-02-22 18:49:20'),
('54bbf567-ebe0-32ee-804c-2255e4d68453', '8cda4fd0-23da-39d6-9797-7fd7abba5eee', 'Fotokopi sah SK Kenaikan Pangkat terakhir', NULL, '2020-01-30 11:42:31', '2020-02-22 18:49:19'),
('55ada64e-1abf-3837-a50a-ef03b07e5c11', '1690ac5e-f0a2-3cff-aa58-4d2c7ecbe38e', 'Fotokopi sah SK PNS', NULL, '2020-01-30 12:01:47', '2020-02-22 18:49:08'),
('562d03e9-3cdc-3d45-bb92-6305aea09a54', '1690ac5e-f0a2-3cff-aa58-4d2c7ecbe38e', 'Fotokopi sah Penilaian Prestasi Kerja/SKP 2 Tahun terakhir', NULL, '2020-01-30 12:03:31', '2020-02-22 18:49:09'),
('56e870a6-b4af-3c8b-b5e8-9c68a84c10bb', '189539c4-fda9-380e-bc41-c121bc07c65a', 'SK CPNS', NULL, '2020-02-15 21:14:00', '2020-02-22 18:49:15'),
('57c1d5a7-34f6-3324-bae3-bc044bccd56c', '69ffc6ec-5bb4-3a01-aedc-292e462ad5b4', 'Data Mahasiswa (Forlapdikti)', NULL, '2020-01-23 05:00:59', '2020-02-22 18:49:21'),
('5b56f3b7-57cc-32da-8864-d313e2f6a9c4', '189539c4-fda9-380e-bc41-c121bc07c65a', 'SKP 2 Tahun Terakhir', NULL, '2020-02-15 21:16:38', '2020-02-22 18:49:16'),
('5cc63be5-a087-3b96-9687-2fcb41a83a9b', 'd4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', 'Izin Belajar', NULL, '2020-01-30 11:47:55', '2020-02-22 18:49:20'),
('5d1f3b03-3cc7-3a8a-b4fc-05636ddbda22', '189539c4-fda9-380e-bc41-c121bc07c65a', 'Konversi NIP', NULL, '2020-02-15 21:13:48', '2020-02-22 18:49:15'),
('611c8061-3b5e-3017-ac66-d53a89924416', '6358495e-ec55-350e-b123-e36b8ef5aba0', 'Fotokopi sah SK PNS', NULL, '2020-01-30 11:58:53', '2020-02-22 18:49:07'),
('639becf6-456a-38bd-ba9d-9985fbc0791b', '1690ac5e-f0a2-3cff-aa58-4d2c7ecbe38e', 'Fotokopi sah Transkrip Nilai', NULL, '2020-01-30 12:02:12', '2020-02-22 18:49:08'),
('64a7ad00-7079-310f-8476-8c7bcb01a7b4', '69ffc6ec-5bb4-3a01-aedc-292e462ad5b4', 'Surat Keterangan (Hukdis) dari SKPD', NULL, '2020-01-23 05:00:51', '2020-02-22 18:49:21'),
('65f24477-9abe-3fb7-9ceb-60b3ae2024cc', '6358495e-ec55-350e-b123-e36b8ef5aba0', 'Surat Keterangan dari pejabat yang berwenang apabila terdapat permasalan', NULL, '2020-01-30 12:00:18', '2020-02-22 18:49:07'),
('6721d66b-e481-3aab-a93e-c988e46c50bc', '1690ac5e-f0a2-3cff-aa58-4d2c7ecbe38e', 'Daftar Riwayat dan Kepangkatan', NULL, '2020-01-30 12:03:18', '2020-02-22 18:49:09'),
('67d5d4f8-0d81-3dbf-8c8b-566eb47695c6', '1d9c1b26-1d83-3afc-a05d-a8a38214bd1f', 'Data Mahasiswa (Forlapdikti)', NULL, '2020-01-23 07:54:00', '2020-02-22 18:49:18'),
('68158ff6-7fdd-366c-b993-a7ea258da17a', 'd4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', 'Fotokopi sah SK Pengangkatan dalam Jabatan Fungsional sebelumnya, Fotokopi sah Sertifikat telah mengikutai dan lulus Diklat Pengangkatan dan Lulus Uji Kompetensi sesuai dengan Permenpan masing-masing', NULL, '2020-01-30 11:48:42', '2020-02-22 18:49:20'),
('698e9268-d570-3b5b-931e-9df7623bca58', '69ffc6ec-5bb4-3a01-aedc-292e462ad5b4', 'Jadwal Perkuliahan', NULL, '2020-01-23 05:00:38', '2020-02-22 18:49:19'),
('6a247b6f-a526-3681-b66f-f3d3ac245328', '189539c4-fda9-380e-bc41-c121bc07c65a', 'Surat Keterangan Meninggal (MDA)', NULL, '2020-02-15 21:17:16', '2020-02-22 18:49:16'),
('6a606bcd-d3c0-3179-993e-f96740a522ea', '1d9c1b26-1d83-3afc-a05d-a8a38214bd1f', 'SK Pangkat Terakhir', NULL, '2020-01-23 07:52:48', '2020-02-22 18:49:17'),
('6b4a46c7-47f4-35ea-92a1-02b42a73fe41', '972c7e95-7dea-3185-9df1-01ee8f0a3e1c', 'Surat Keterangan dari pejabat yang berwenang apabila terdapat permasalahan', NULL, '2020-01-30 12:07:14', '2020-02-22 18:49:11'),
('6bcaaa47-1ee6-30a1-8e72-d4cc440461c0', '972c7e95-7dea-3185-9df1-01ee8f0a3e1c', 'Fotokopi sah PAK lama', NULL, '2020-01-30 12:06:25', '2020-02-22 18:49:10'),
('6be380f3-ace5-3f93-add9-5c8372e9e25b', '5b5e143b-0d14-344c-b554-c333fb66b241', 'Forlap Dikti dan Akreditasi Kampus', NULL, '2020-01-23 07:51:13', '2020-02-22 18:49:17'),
('6c7a3903-d5c5-310d-9e0c-a255049a645d', '17f83b86-af2e-32fe-8190-36d488400e82', 'Pas Photo Uk 2x3 2 buah', NULL, '2020-02-03 07:53:24', '2020-02-22 18:49:14'),
('6cefe296-6b51-3444-85df-9fcd36a8c724', 'ecc5f0f0-c7e8-306d-9bb6-440a97bb771b', 'Surat Pengantar dari Perangkat Daerah', NULL, '2020-02-07 05:56:15', '2020-02-22 18:49:16'),
('6d67ad00-45a2-325f-b3ab-20743b053ee9', 'db8db0a8-2617-3788-9897-5ef3b666f2a8', 'Permohonan YBS ke Kepala BKPSDM disertain alasan Pindah', NULL, '2020-01-31 14:18:36', '2020-02-22 18:49:12'),
('704f1e42-62c7-3e39-aee6-c39d2983e690', '54ebc251-94e6-3fd7-937b-444ee125d027', 'FC. Karpeg (legalisir)', NULL, '2020-02-02 10:58:48', '2020-02-22 18:49:12'),
('718a8666-5302-3bbf-bb80-8bcf6208e5b2', '972c7e95-7dea-3185-9df1-01ee8f0a3e1c', 'Surat Pernyataan Kepala OPD', NULL, '2020-01-30 12:04:23', '2020-02-22 18:49:09'),
('7364c599-ceb5-3b11-9810-21df123453c7', '69ffc6ec-5bb4-3a01-aedc-292e462ad5b4', 'Surat Pernyataan dari Perguruan Tinggi', NULL, '2020-01-23 05:00:43', '2020-02-22 18:49:20'),
('74921cf0-d99f-328d-9f6d-6305505e29fb', '8cda4fd0-23da-39d6-9797-7fd7abba5eee', 'Fotokopi sah PNS', NULL, '2020-01-30 11:42:15', '2020-02-22 18:49:19'),
('74aba862-86d9-353d-9937-91ae905a0766', '189539c4-fda9-380e-bc41-c121bc07c65a', 'Karpeg', NULL, '2020-02-15 21:13:30', '2020-02-22 18:49:15'),
('75f89e7c-5339-31e0-8ed9-8d6573ee889d', '54ebc251-94e6-3fd7-937b-444ee125d027', 'FC. SK PNS (legalisir)', NULL, '2020-02-02 10:59:19', '2020-02-22 18:49:12'),
('76b9a91f-8a3f-3e30-8b7d-7849883c98e0', '6358495e-ec55-350e-b123-e36b8ef5aba0', 'Surat Pengantar (Usulan dari Instansi)', NULL, '2020-01-30 11:58:24', '2020-02-22 18:49:07'),
('7c6df974-2b09-3a83-9415-76bee7c46ce2', '189539c4-fda9-380e-bc41-c121bc07c65a', 'Kartu Keluarga', NULL, '2020-02-15 21:15:37', '2020-02-22 18:49:16'),
('7ca26ca6-4d6d-38fc-a522-1723ef567a45', '972c7e95-7dea-3185-9df1-01ee8f0a3e1c', 'Surat keterangan tidak pernah dijatuhi hukuman disiplin dari BKPSDM', NULL, '2020-01-30 12:07:29', '2020-02-22 18:49:11'),
('7e121aa4-2440-3240-96f4-eb2a72a3daeb', '972c7e95-7dea-3185-9df1-01ee8f0a3e1c', 'Fotokopi sah SK Tugas Belajar', NULL, '2020-01-30 12:05:50', '2020-02-22 18:49:10'),
('7f8c4240-988c-39a7-bad6-00fdcafedbed', '54ebc251-94e6-3fd7-937b-444ee125d027', 'FC. Kenaikan Pangkat Terakhir (legalisir)', NULL, '2020-02-02 10:58:55', '2020-02-22 18:49:12'),
('7f99894f-9866-3015-b773-9306b3bffcff', '6358495e-ec55-350e-b123-e36b8ef5aba0', 'Fotokopi sah SK Pembebasan Sementara', NULL, '2020-01-30 11:59:25', '2020-02-22 18:49:07'),
('7fcf3fca-c9b4-3c7d-830a-17b1b306a710', '03cd67b7-b836-3703-82d0-63e7c7f24ac7', 'SK Pangkat Terakhir', NULL, '2020-02-02 16:35:40', '2020-02-22 18:49:13'),
('7fdc4641-bb9f-3f56-9121-ce4549d8018f', '69ffc6ec-5bb4-3a01-aedc-292e462ad5b4', 'Surat Pengantar dari SKPD', NULL, '2020-01-23 05:00:19', '2020-02-22 18:49:18'),
('80dc684f-e655-3cd6-99e1-1384c7efcb8d', '189539c4-fda9-380e-bc41-c121bc07c65a', 'Daftar Susunan Keluarga', NULL, '2020-02-15 21:15:11', '2020-02-22 18:49:16'),
('821cfea0-448e-3e54-a27d-cb000736e5ad', '980a10df-873d-3d11-a31c-8c46bb20195f', 'Pas Photo 3x4 dan 4x6 masing masing 2 lembar', NULL, '2020-01-23 07:46:57', '2020-02-22 18:49:07'),
('82b7a7c2-e674-389e-bef4-0d14209b4e93', 'd4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', 'Surat Pernyataan Kepala OPD', NULL, '2020-01-30 11:46:39', '2020-02-22 18:49:20'),
('83a83f85-0ee1-3598-9e1a-9b6f4f429cd4', 'db8db0a8-2617-3788-9897-5ef3b666f2a8', 'SKP (1 tahun terakhir) (legalisir)', NULL, '2020-01-31 14:18:04', '2020-02-22 18:49:11'),
('848cc1a6-2030-324c-b2c0-c285005c2a48', '980a10df-873d-3d11-a31c-8c46bb20195f', 'Fotocopy SK Jabatan Terakhir (bagi calon peserta Ujian Dinas Tk. II)', NULL, '2020-01-23 07:47:44', '2020-02-22 18:49:09'),
('85490514-adfc-3e2f-b563-6c575145fc92', '9ca65942-60ab-38c5-8081-b8d6d75a38f6', 'Surat Keterangan tidak pernah dijatuhi hukuman disiplin', NULL, '2020-01-30 11:57:07', '2020-02-22 18:49:06'),
('87b828e6-4833-3461-b5d9-52e721884da0', '1690ac5e-f0a2-3cff-aa58-4d2c7ecbe38e', 'Fotokopi sah SK Jabatan sebelumnya (lama)', NULL, '2020-01-30 12:02:37', '2020-02-22 18:49:08'),
('883f1811-f396-3921-9399-5fb3ed1f0043', 'db8db0a8-2617-3788-9897-5ef3b666f2a8', 'FC. SK CPNS (legalisir)', NULL, '2020-01-31 14:16:59', '2020-02-22 18:49:11'),
('8c889385-6256-366c-9b55-ca5ec7dbd21e', '6358495e-ec55-350e-b123-e36b8ef5aba0', 'Fotokopi sah SK Jabatan Pejabat Penilai', NULL, '2020-01-30 11:59:31', '2020-02-22 18:49:07'),
('8d8a072e-6ace-3f48-a8ce-60f59c978171', '6358495e-ec55-350e-b123-e36b8ef5aba0', 'Fotokopi sah SK Kenaikan Pangkat terakhir', NULL, '2020-01-30 11:59:01', '2020-02-22 18:49:07'),
('8e658d2c-fef9-3825-894e-1f3311dc5c17', '9ca65942-60ab-38c5-8081-b8d6d75a38f6', 'Surat Keterangan dari pejabat yang berwenang apabila terdapat permasalahan', NULL, '2020-01-30 11:57:00', '2020-02-22 18:49:06'),
('8e97cb1e-e582-3a05-bfc7-b45e940d86d6', '972c7e95-7dea-3185-9df1-01ee8f0a3e1c', 'Fotokopi sah SK PNS', NULL, '2020-01-30 12:04:44', '2020-02-22 18:49:09'),
('8f626586-ca81-3ad6-aff8-dabfa55625c3', 'd4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', 'Fotocopy sah SK pembagian tugas mengajar (SKBM) 1 tahun terakhir', NULL, '2020-01-30 11:52:57', '2020-02-22 18:49:21'),
('8f7599de-acea-33cc-934c-2a53936c3b94', '9ca65942-60ab-38c5-8081-b8d6d75a38f6', 'Fotokopi sah Surat Tanda Lulusan Ujian Penyesuaian kenaikan Pangkat (STLUPKP)', NULL, '2020-01-30 11:55:41', '2020-02-22 18:49:06'),
('9221e794-19a3-3e8c-bc03-41c9bed3603e', '6358495e-ec55-350e-b123-e36b8ef5aba0', 'Surat keterangan tidak pernah dijatuhi hukuman disiplin dari BKPSDM', NULL, '2020-01-30 12:00:36', '2020-02-22 18:49:07'),
('938f080f-9144-3be7-a0e8-f91f7cfb2f0e', 'ece49cf7-4d45-385f-afc0-b899d74fbbfb', 'Fotokopi sah Tanda Lulus Ujian Dinas', NULL, '2020-01-23 08:21:45', '2020-02-22 18:49:18'),
('96435d71-8138-373b-8125-fbe7f89436a8', '5b5e143b-0d14-344c-b554-c333fb66b241', 'Fotocopy Transkip Nilai Legalisir', NULL, '2020-01-23 07:49:21', '2020-02-22 18:49:12'),
('9a48bbdd-f8d4-3e34-a3e1-096fa0d74a0d', '6c88f416-c616-3623-9ea1-7f22e3f8c54a', 'Skp terakhir legalisir', NULL, '2020-02-15 21:11:58', '2020-02-22 18:49:15'),
('9d313c7b-f34c-329c-8fc1-710437dc10e0', 'db8db0a8-2617-3788-9897-5ef3b666f2a8', 'FC. SP Mutasi Terakhir dari kepala BKPSDM', NULL, '2020-01-31 14:16:50', '2020-02-22 18:49:11'),
('9ea8d757-c8c7-3e33-96bf-75c4eb6397e0', '2d093e78-c955-31c6-88c4-bdcbb73e2cee', 'FC. SK CPNS dilegalisir', NULL, '2020-02-03 07:49:12', '2020-02-22 18:49:13'),
('9ebee518-861d-3558-a902-e519475a9d80', '54ebc251-94e6-3fd7-937b-444ee125d027', 'Permohonan YBS ke Kepala BKPSDM disertain alasan Pindah', NULL, '2020-02-02 10:59:36', '2020-02-22 18:49:13'),
('9f03e6ee-c9df-39fa-a37d-fda198ee5821', '8cda4fd0-23da-39d6-9797-7fd7abba5eee', 'Izin Belajar', NULL, '2020-01-30 11:42:56', '2020-02-22 18:49:19'),
('9f3e2582-f55b-3067-ac83-10bfd5d70de4', '6c88f416-c616-3623-9ea1-7f22e3f8c54a', 'Kgb terakhir legalisir', NULL, '2020-02-15 21:11:52', '2020-02-22 18:49:14'),
('a07447e4-c5a5-3673-a697-5b1704f47cd9', '54ebc251-94e6-3fd7-937b-444ee125d027', 'FC. SK CPNS (legalisir)', NULL, '2020-02-02 10:59:02', '2020-02-22 18:49:12'),
('a329c175-c440-3fd6-8cc7-036ae1ceb3a8', '69ffc6ec-5bb4-3a01-aedc-292e462ad5b4', 'Sertifikat Akreditasi', NULL, '2020-01-30 12:12:25', '2020-02-22 18:49:11'),
('a7364aff-f226-365b-8121-7de474e96598', '11cd4a37-6ea2-324b-b58c-db79c1b6c49a', 'Pas Photo Suami Uk 3x4 2 buah', NULL, '2020-02-03 07:51:55', '2020-02-22 18:49:14'),
('a866b97f-62b1-3b39-97d6-2b0d10898abf', 'd4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', 'Surat Keterangan tidak pernah dijatuhi hukuman disiplin', NULL, '2020-01-30 11:53:22', '2020-02-22 18:49:21'),
('aba209b4-7211-3ad8-86e3-9ba88493c438', '9ca65942-60ab-38c5-8081-b8d6d75a38f6', 'Uraian Tudas lama dan baru dari Pimpinan unit kerjanya paling rendah Eselon II', NULL, '2020-01-30 11:55:59', '2020-02-22 18:49:06'),
('ad17b040-83fa-36f3-95ab-2a976b7a18cd', 'd4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', 'Fotokopi sah SK Jabatan Atasan Pejabat Penilai', NULL, '2020-01-30 11:49:35', '2020-02-22 18:49:21'),
('ae824e1d-5430-3e00-bead-d63995058686', '8cda4fd0-23da-39d6-9797-7fd7abba5eee', 'Fotokopi sah SK Pembebasan Sementara', NULL, '2020-01-30 11:44:20', '2020-02-22 18:49:19'),
('ae878bef-b8aa-355b-ae86-429cd7af7844', '1d9c1b26-1d83-3afc-a05d-a8a38214bd1f', 'Surat Pengantar dari SKPD', NULL, '2020-01-23 07:53:08', '2020-02-22 18:49:17'),
('b21db275-2aea-369b-a60a-9400a332a0c6', '189539c4-fda9-380e-bc41-c121bc07c65a', 'Akte Kelahiran Anak', NULL, '2020-02-15 21:15:29', '2020-02-22 18:49:16'),
('b2ac38c4-722a-38f5-a2d8-e48d9f80ea72', '6358495e-ec55-350e-b123-e36b8ef5aba0', 'Nota Usul Kenaikan Pangkat', NULL, '2020-01-30 11:58:35', '2020-02-22 18:49:07'),
('b2d75494-5fac-391b-b533-661eacebf96e', 'ece49cf7-4d45-385f-afc0-b899d74fbbfb', 'Surat Pengantar (Usulan dari Instansi)', NULL, '2020-01-23 08:20:13', '2020-02-22 18:49:18'),
('b31c4048-fd42-3164-8961-fcecf0f09ffd', '1690ac5e-f0a2-3cff-aa58-4d2c7ecbe38e', 'Surat Pengantar (Usulan dari Instansi)', NULL, '2020-01-30 12:01:17', '2020-02-22 18:49:08'),
('b454d45b-daa0-3bf4-841b-659d0cde2882', '9ca65942-60ab-38c5-8081-b8d6d75a38f6', 'Surat Pengantar (Usulan dari Instansi)', NULL, '2020-01-30 11:54:07', '2020-02-22 18:49:21'),
('b5574f8e-d408-3649-a141-6a9fa00fe18e', 'ece49cf7-4d45-385f-afc0-b899d74fbbfb', 'Fotokopi sah SK Jabatan Pejabat Penilai', NULL, '2020-01-23 08:23:17', '2020-02-22 18:49:19'),
('b557d76c-9b67-35a2-ac68-8f0e35039a0f', '5b5e143b-0d14-344c-b554-c333fb66b241', 'Surat Ket. Hukdis dari Kepala Perangkat Daerah', NULL, '2020-01-23 07:50:08', '2020-02-22 18:49:15'),
('b583ef66-a0c9-3e09-ada7-b862c3ec3c60', '5b5e143b-0d14-344c-b554-c333fb66b241', 'Pas Photo 3x4 dan 4x6 masing masing 2 lembar', NULL, '2020-01-23 07:49:50', '2020-02-22 18:49:14'),
('b5a3f0a2-e938-3114-a661-4f756a859f5c', '54ebc251-94e6-3fd7-937b-444ee125d027', 'Surat Permohonan pindah dari instansi asal dengan mencantumkan nama jabatan dan kelas jabatan (Asli Cap Basah)', NULL, '2020-02-02 10:59:51', '2020-02-22 18:49:13'),
('b7711b94-766d-3012-a8ee-9af45af259b5', '03cd67b7-b836-3703-82d0-63e7c7f24ac7', 'Surat Rekomendasi Rotasi/Promosi', NULL, '2020-02-02 16:35:23', '2020-02-22 18:49:13'),
('b7761b47-59fa-3ae8-92d7-1f79ae41cc0e', '54ebc251-94e6-3fd7-937b-444ee125d027', 'SKP (1 tahun terakhir) (legalisir)', NULL, '2020-02-02 10:59:43', '2020-02-22 18:49:13'),
('b886242d-9627-3b7d-9b93-d12a8d4b5ee3', 'd4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', 'Fotokopi sah sertifikat pendidik', NULL, '2020-01-30 11:52:05', '2020-02-22 18:49:21'),
('b8b67cb6-f638-3dc1-a087-9aa79ae7660b', '8cda4fd0-23da-39d6-9797-7fd7abba5eee', 'Fotokopi sah SK Jabatan Pejabat Penilai', NULL, '2020-01-30 11:44:45', '2020-02-22 18:49:19'),
('bbf5f1e2-7843-3988-9b8e-ef5a864401ed', '1d9c1b26-1d83-3afc-a05d-a8a38214bd1f', 'Ijazah Terakhir (legalisir)', NULL, '2020-01-23 07:53:19', '2020-02-22 18:49:17'),
('bcf4b385-0d41-39e6-bdeb-940ab6791550', 'd4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', 'Surat Pengantar (Usulan dari Instansi)', NULL, '2020-01-30 11:46:32', '2020-02-22 18:49:20'),
('be3d00c7-f60d-36ed-8b9c-80157c30d7c8', 'ece49cf7-4d45-385f-afc0-b899d74fbbfb', 'Nota Usul Kenaikan Pangkat', NULL, '2020-01-23 08:20:30', '2020-02-22 18:49:18'),
('becc8408-bb63-3ef2-95ca-137d66a926be', '8cda4fd0-23da-39d6-9797-7fd7abba5eee', 'Fotokopi sah SK Jabatan Atasan Pejabat Penilai', NULL, '2020-01-30 11:44:54', '2020-02-22 18:49:20'),
('c0e57978-c8b4-39d6-860e-1d35868f0365', 'd4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', 'Fotokopi sah Ijazah, Forlap Dikti dan BAN PPT', NULL, '2020-01-30 11:47:42', '2020-02-22 18:49:20'),
('c5203311-5654-3e3a-b978-8786a0c91ddd', '9ca65942-60ab-38c5-8081-b8d6d75a38f6', 'Surat Pernyataan Kepala OPD', NULL, '2020-01-30 11:54:16', '2020-02-22 18:49:21'),
('c54409b3-7ae2-3441-b055-6457b52b1815', '189539c4-fda9-380e-bc41-c121bc07c65a', 'DPCP', NULL, '2020-02-15 21:16:11', '2020-02-22 18:49:16'),
('c61628f3-c7c3-3df7-a8c7-714facc6a089', '54ebc251-94e6-3fd7-937b-444ee125d027', 'DSO / Keadaan Guru antar instansi (untuk guru)', NULL, '2020-02-02 10:58:41', '2020-02-22 18:49:12'),
('c6668bf4-481f-35d0-a26d-b8a86ce2c72a', '17f83b86-af2e-32fe-8190-36d488400e82', 'FC. SK PNS dilegalisir', NULL, '2020-02-03 07:54:06', '2020-02-22 18:49:14'),
('c76537e3-0ed4-35b0-b2f1-4ae960529fb9', '972c7e95-7dea-3185-9df1-01ee8f0a3e1c', 'Fotokopi sah SK Jabatan Atasan Pejabat Penilai', NULL, '2020-01-30 12:05:42', '2020-02-22 18:49:10'),
('c8b75c1f-8275-35ad-8600-6e1ee7f613f7', 'db8db0a8-2617-3788-9897-5ef3b666f2a8', 'Surat rekomendasi lolos Butuh dari Instansi yang dituju/menerima dengan mencantumkan nama jabatan dan kelas jabatan (Asli Cap Basah)', NULL, '2020-01-31 14:16:35', '2020-02-22 18:49:11'),
('c8cdfd4a-1411-3c61-9757-3ec73781d889', '03cd67b7-b836-3703-82d0-63e7c7f24ac7', 'Surat Keterangan tidak pertnah dijatuhi hukuman disiplin', NULL, '2020-02-02 16:36:25', '2020-02-22 18:49:13'),
('c9a0cbba-a2d8-3d92-8bfe-4bbf0166e64d', 'db8db0a8-2617-3788-9897-5ef3b666f2a8', 'FC. SK PNS (legalisir)', NULL, '2020-01-31 14:17:07', '2020-02-22 18:49:11'),
('cb66eb62-8b5e-330f-a589-2b5afa5435b4', '6358495e-ec55-350e-b123-e36b8ef5aba0', 'Fotokopi sah SK CPNS', NULL, '2020-01-30 11:58:40', '2020-02-22 18:49:07'),
('cb9782b0-dc68-3f84-a0c2-d7522203ad33', '8cda4fd0-23da-39d6-9797-7fd7abba5eee', 'Fotokopi sah Ijazah, Forlap Dikti dan BAN PT', NULL, '2020-01-30 11:42:43', '2020-02-22 18:49:19'),
('cd5c6b5b-8f73-3036-8aba-91130cc3b370', '1d9c1b26-1d83-3afc-a05d-a8a38214bd1f', 'Sertifikasi Akreditasi', NULL, '2020-01-23 07:52:57', '2020-02-22 18:49:17'),
('cf647196-8ad1-3d5a-8ade-93c3204ae554', '972c7e95-7dea-3185-9df1-01ee8f0a3e1c', 'Fotokopi sah SK Pengangkatan Kembali', NULL, '2020-01-30 12:05:24', '2020-02-22 18:49:10'),
('d00eb004-5802-3486-a210-cd81bb4db206', '9ca65942-60ab-38c5-8081-b8d6d75a38f6', 'Fotokopi sah Sertifikat Pendidik + Sertifikat Lainnya', NULL, '2020-01-30 11:56:13', '2020-02-22 18:49:06'),
('d3d563d9-53c7-3c0e-ba18-b752f38ce0fd', '980a10df-873d-3d11-a31c-8c46bb20195f', 'Fotocopy SK Pangkat Terakhir', NULL, '2020-01-23 07:46:06', '2020-02-22 18:49:05'),
('d3ec24a3-2edf-3670-8776-5bdb5e5eb517', 'd4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', 'Fotokopi sah SK Pembebasan Sementara', NULL, '2020-01-30 11:49:02', '2020-02-22 18:49:21'),
('d44abae4-c744-3aa8-9eda-4e3e5cee4490', '8cda4fd0-23da-39d6-9797-7fd7abba5eee', 'Fotokopi sah SK CPNS', NULL, '2020-01-30 11:42:07', '2020-02-22 18:49:19'),
('d64aa59c-f2e1-3593-a2fa-0cf1dbe782be', '6358495e-ec55-350e-b123-e36b8ef5aba0', 'Fotokopi sah SK Jabatan terakhir', NULL, '2020-01-30 11:59:17', '2020-02-22 18:49:07'),
('d708251f-c356-330b-8ff9-8c5dedf084d4', '6c88f416-c616-3623-9ea1-7f22e3f8c54a', 'Sk pangkat terakhir legalisir', NULL, '2020-02-15 21:11:47', '2020-02-22 18:49:14'),
('d7b85597-3e6d-3a27-8423-051125036425', '189539c4-fda9-380e-bc41-c121bc07c65a', 'Surat Pernyataan Tidak Menyimpan Barang Milik Negara', NULL, '2020-02-15 21:17:01', '2020-02-22 18:49:16'),
('d7c21d80-fb99-3f04-9d1a-d6e69a9ef676', '8cda4fd0-23da-39d6-9797-7fd7abba5eee', 'Fotokopi sah SK Pengangkatan Kembali', NULL, '2020-01-30 11:44:39', '2020-02-22 18:49:19'),
('d9a680c9-017e-36cc-8e10-b4c84e0031be', '9ca65942-60ab-38c5-8081-b8d6d75a38f6', 'Fotokopi sah Ijazah', NULL, '2020-01-30 11:54:56', '2020-02-22 18:49:22'),
('dae6ff74-18a3-3dcb-9fe9-8d9364be9995', 'ece49cf7-4d45-385f-afc0-b899d74fbbfb', 'Fotokopi sah SK Kenaikan Pangkat terakhir', NULL, '2020-01-23 08:21:09', '2020-02-22 18:49:18'),
('dc2faced-d3d4-3e54-a289-2d4c4026c106', '69ffc6ec-5bb4-3a01-aedc-292e462ad5b4', 'SK Pangkat Terakhir', NULL, '2020-01-23 05:00:26', '2020-02-22 18:49:18'),
('dc3c6742-8a10-3bbb-89d0-9937733edd69', '17f83b86-af2e-32fe-8190-36d488400e82', 'FC. SK CPNS dilegalisir', NULL, '2020-02-03 07:53:46', '2020-02-22 18:49:14'),
('dcffcbfa-de44-3ccc-9ad4-b5dfcdf1313f', '1690ac5e-f0a2-3cff-aa58-4d2c7ecbe38e', 'Surat Keterangan tidak pernah dijatuhi hukuman disiplin', NULL, '2020-01-30 12:03:52', '2020-02-22 18:49:09'),
('dd8ff4ca-f934-362a-8ed0-eeb6aa7c38c6', '189539c4-fda9-380e-bc41-c121bc07c65a', 'Hukuman Pidana', NULL, '2020-02-15 21:17:22', '2020-02-22 18:49:16'),
('dda3496c-528d-35cc-92a7-300b247d05c4', '1690ac5e-f0a2-3cff-aa58-4d2c7ecbe38e', 'Fotokopi sah SK Jabatan Atasan Pejabat Penilai', NULL, '2020-01-30 12:03:02', '2020-02-22 18:49:09'),
('ddd8280c-39ba-3137-abf8-b84cef9e6506', '8cda4fd0-23da-39d6-9797-7fd7abba5eee', 'Nota Usul kenaikan Pangkat', NULL, '2020-01-30 11:41:33', '2020-02-22 18:49:19'),
('df0bd10a-c3ae-3ebd-8570-6a33d37917e6', '9ca65942-60ab-38c5-8081-b8d6d75a38f6', 'Fotokopi sah SK Jabatan Atasan Pejabat Penilai', NULL, '2020-01-30 11:56:53', '2020-02-22 18:49:06'),
('e12b6a8a-01b6-316d-8193-0434ab843da0', '11cd4a37-6ea2-324b-b58c-db79c1b6c49a', 'Pas Photo Suami Uk 3x4 2 buah', NULL, '2020-02-03 07:52:02', '2020-02-22 18:49:14'),
('e14551c3-6a65-34e2-aaaa-bf9a3403a2d2', '2d093e78-c955-31c6-88c4-bdcbb73e2cee', 'FC. Surat Nikah Legalisir KUA', NULL, '2020-02-03 07:50:55', '2020-02-22 18:49:14'),
('e2b587fb-333f-3a2c-a7ae-3fd41f2778f9', '972c7e95-7dea-3185-9df1-01ee8f0a3e1c', 'Fotokopi sah Penilaian Prestasi Kerja PNS 2 tahun terakhir', NULL, '2020-01-30 12:06:59', '2020-02-22 18:49:11'),
('e2d5aed2-e25e-36b1-adb3-90a121753402', 'ece49cf7-4d45-385f-afc0-b899d74fbbfb', 'Fotokopi sah Transkip Nilai', NULL, '2020-01-23 08:21:30', '2020-02-22 18:49:18'),
('e73ed7ba-886a-36bc-8e93-67da6098b591', '8cda4fd0-23da-39d6-9797-7fd7abba5eee', 'Fotokopi sah SK Pengangkatan dalam Jabatan Fungsioinal sebelumnya, Fotokopi sah Sertifikat telah mengikuti dan lulus Diklat Pengangkatan dan Lulus Uji Kompetensi sesuai dengan permenpan masing-masing', NULL, '2020-01-30 11:43:53', '2020-02-22 18:49:19'),
('e784e731-52e3-3ffe-99a8-aca7449ce8aa', 'd4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', 'Fotokopi sah Transkip Nilai', NULL, '2020-01-30 11:47:49', '2020-02-22 18:49:20'),
('e7a28b3d-ac37-309c-a5e8-fa74b5a21fa5', '54ebc251-94e6-3fd7-937b-444ee125d027', 'FC. SP Mutasi Terakhir dari kepala BKPSDM', NULL, '2020-02-02 10:59:27', '2020-02-22 18:49:13'),
('e80e0131-7f48-3769-ab71-1ede8d837c54', '189539c4-fda9-380e-bc41-c121bc07c65a', 'Surat permohonan pensiun', NULL, '2020-02-15 21:13:24', '2020-02-22 18:49:15'),
('e8a2a944-1337-3a26-8cca-d54f9e0b1e0f', 'd4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', 'Fotokopi sah SK Jabatan Pejabat Penilai', NULL, '2020-01-30 11:49:23', '2020-02-22 18:49:21'),
('e8d75099-5dcf-3fb9-8792-892de0c31d4a', '6358495e-ec55-350e-b123-e36b8ef5aba0', 'Fotokopi sah SK Jabatan Atasan Pejabat Penilai', NULL, '2020-01-30 11:59:43', '2020-02-22 18:49:07'),
('ea1cfa86-4467-3e87-b086-2aace790e9e5', '1690ac5e-f0a2-3cff-aa58-4d2c7ecbe38e', 'Fotokopi sah SK CPNS', NULL, '2020-01-30 12:01:38', '2020-02-22 18:49:08'),
('eb4bf4bd-675f-3ec3-84fb-9e61c49834a8', '1d9c1b26-1d83-3afc-a05d-a8a38214bd1f', 'Surat Keterangan Perkuliahan dari OPD', NULL, '2020-01-23 07:53:52', '2020-02-22 18:49:18'),
('edf4b8cc-206a-3049-8ff7-34fa52980aca', '980a10df-873d-3d11-a31c-8c46bb20195f', 'Forocopy SKP 2 Tahun Terakhir', NULL, '2020-01-23 07:47:17', '2020-02-22 18:49:08'),
('f017816a-0670-307c-b6d1-bf95682633a5', '189539c4-fda9-380e-bc41-c121bc07c65a', 'Surat Nikah', NULL, '2020-02-15 21:15:18', '2020-02-22 18:49:16'),
('f025f66b-7722-3418-a31c-5513797d95cd', '8cda4fd0-23da-39d6-9797-7fd7abba5eee', 'Surat Keterangan tidak pernah dijatuhi hukuman disiplin', NULL, '2020-01-30 11:45:50', '2020-02-22 18:49:20'),
('f0ff3cb3-ff6b-3cdc-ab73-9a34091751da', 'db8db0a8-2617-3788-9897-5ef3b666f2a8', 'Surat Permohonan pindah dari instansi asal dengan mencantumkan nama jabatan dan kelas jabatan (Asli Cap Basah)', NULL, '2020-01-31 14:15:59', '2020-02-22 18:49:11'),
('f393aa12-ab76-3e80-8dde-a96e7b2c8396', '5b5e143b-0d14-344c-b554-c333fb66b241', 'Surat Rekomendasi Kepala Perangkat Daerah tentang memperoleh Ijazah sebelum CPNS/PNS', NULL, '2020-01-23 07:50:31', '2020-02-22 18:49:16'),
('f42b7e95-d316-35db-aa16-48f3e1a41f04', 'ece49cf7-4d45-385f-afc0-b899d74fbbfb', 'Fotokopi sah SK PNS', NULL, '2020-01-23 08:20:54', '2020-02-22 18:49:18'),
('f5140277-f3d7-3cfb-aa30-a12dbec91976', '11cd4a37-6ea2-324b-b58c-db79c1b6c49a', 'FC. SK CPNS dilegalisir', NULL, '2020-02-03 07:52:14', '2020-02-22 18:49:14'),
('f558b3a9-ae7a-349d-b41b-f66733b448e6', '189539c4-fda9-380e-bc41-c121bc07c65a', 'Daftar Riwayat Pekerjaan', NULL, '2020-02-15 21:14:56', '2020-02-22 18:49:15'),
('f599e4e8-8aff-3303-83f3-3020db3baafc', '11cd4a37-6ea2-324b-b58c-db79c1b6c49a', 'Pas Photo Suami Uk 3x4 2 buah', NULL, '2020-02-03 07:51:55', '2020-02-22 18:49:14'),
('f8341b85-3b4d-326f-95a8-0646ce38e1b1', 'ece49cf7-4d45-385f-afc0-b899d74fbbfb', 'Fotokopi sah SK CPNS', NULL, '2020-01-23 08:20:43', '2020-02-22 18:49:18'),
('f96608cd-476d-3218-99e7-f755622b3a62', '189539c4-fda9-380e-bc41-c121bc07c65a', 'SK Terakhir', NULL, '2020-02-15 21:14:15', '2020-02-22 18:49:15'),
('fcb880a2-7af9-3770-b1e6-9a3a609ce00c', '6358495e-ec55-350e-b123-e36b8ef5aba0', 'Fotokopi sah PAK lama', NULL, '2020-01-30 11:59:55', '2020-02-22 18:49:07'),
('fccadbbb-fc23-388f-9c1f-f6fce2b30516', '1d9c1b26-1d83-3afc-a05d-a8a38214bd1f', 'Transkip Nilai (legalisir)', NULL, '2020-01-23 07:53:27', '2020-02-22 18:49:18'),
('fdcd497c-7e1d-3def-93a4-963dfc4eab32', '1690ac5e-f0a2-3cff-aa58-4d2c7ecbe38e', 'Fotokopi sah SK Jabatan Pejabat Penilai', NULL, '2020-01-30 12:02:52', '2020-02-22 18:49:08'),
('fdf47d10-8a2b-3d53-8878-6f61e60515f1', '9ca65942-60ab-38c5-8081-b8d6d75a38f6', 'Lampiran Forlap Dikti', NULL, '2020-01-30 11:55:02', '2020-02-22 18:49:22'),
('fe932528-5d7d-3d10-b634-6898d5844b63', '189539c4-fda9-380e-bc41-c121bc07c65a', 'Foto Ukuran 3x4 Sebanyak 7 Lembar', NULL, '2020-02-15 21:16:28', '2020-02-22 18:49:16'),
('ff6ab1e3-58e4-37ee-ab18-25deea0264ab', 'd4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', 'Fotokopi sah SK PNS', NULL, '2020-01-30 11:47:11', '2020-02-22 18:49:20');

-- --------------------------------------------------------

--
-- Table structure for table `requirement_posts`
--

CREATE TABLE `requirement_posts` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `requirement_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `requirement_form_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_size` double NOT NULL,
  `file_ext` char(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `requirement_posts`
--

INSERT INTO `requirement_posts` (`id`, `requirement_id`, `requirement_form_id`, `file`, `file_type`, `file_size`, `file_ext`, `created_at`, `updated_at`) VALUES
('1247e197-38d9-48aa-9857-dd3ddc45d6a6', 'f9307d4f-970b-4cb0-9416-7ce662298659', 'fccadbbb-fc23-388f-9c1f-f6fce2b30516', 'public/requirements/SKLK/Ea/lW40KBemJqQ9RJMuZaPB6bh7aGvfHte8xb8fj60g.png', 'image/png', 18737, '.PNG', '2020-02-22 06:23:55', '2020-02-22 06:23:55'),
('1493a241-6a7d-4685-a2aa-b90e321bd360', 'b5022337-56f8-4fe3-b402-a9da7e8a75e0', 'd708251f-c356-330b-8ff9-8c5dedf084d4', 'public/requirements/KGB/Et/TvVIyb7t56AeIbGuIgN5r8F7iMcYfC4O51Hj2KAf.png', 'image/png', 34661, '.PNG', '2020-02-22 04:40:15', '2020-02-22 04:40:15'),
('1b646a6a-ce85-442c-833c-9707553a5819', 'a2ef9429-4461-491c-889c-0f92a65218c8', '325385bf-ccd4-3599-b705-cef8d3602714', 'public/requirements/RPIO/Similique/PuIQxNAMHhb7AqxxWiTBMBaQLbvIQZMOXsi09JY4.png', 'image/png', 18737, '.PNG', '2020-02-22 06:03:11', '2020-02-22 06:03:11'),
('1c0b2586-3647-4149-9e90-6ae3b58931bc', 'fcdc7dfe-f64a-4ddc-95f5-d858ac7e7ef7', '7364c599-ceb5-3b11-9810-21df123453c7', 'public/requirements/SIB/Et natus/WmaF8fheRAdp6Ubk5CfFBNqo5EpkDqXJvDQRtp0C.jpeg', 'image/jpeg', 10965117, '.jpg', '2020-02-22 06:20:40', '2020-02-22 06:20:40'),
('1d75be2c-e28d-4517-9438-a8aefec58eca', '26fac2cc-45df-4b60-a9e3-f3e1ea5d597f', '6d67ad00-45a2-325f-b3ab-20743b053ee9', 'public/requirements/PPPAO/Officia/xQGKelA2jx4sHmmFhziCOsCpDhYpyWYpqDSIEaBL.png', 'image/png', 18737, '.PNG', '2020-02-22 05:01:14', '2020-02-22 05:01:14'),
('2bedf4d3-ddd6-4046-94cf-7213fcfb403a', '142dc189-b676-4654-9c86-41f82d8c7931', '7f8c4240-988c-39a7-bad6-00fdcafedbed', 'public/requirements/PPPAOKG/Sit eaque/vGhtpLnS0xQ6r1MBIowGwQUoJeHUY0nNJbRkelXD.png', 'image/png', 34661, '.PNG', '2020-02-22 05:11:01', '2020-02-22 05:11:01'),
('2f32736d-7073-47fa-86ad-7fc19d77bf3d', 'b5022337-56f8-4fe3-b402-a9da7e8a75e0', '9f3e2582-f55b-3067-ac83-10bfd5d70de4', 'public/requirements/KGB/Et/JZV2HQg0Rus3ptPyPi8t4TlkuwMFBpakRzwYZwKY.png', 'image/png', 36348, '.PNG', '2020-02-22 04:40:09', '2020-02-22 04:40:09'),
('33bb205c-9771-4a98-9a10-562f60dcc499', '142dc189-b676-4654-9c86-41f82d8c7931', 'b5a3f0a2-e938-3114-a661-4f756a859f5c', 'public/requirements/PPPAOKG/Sit eaque/00sx6Aokwe0UdCkj7UguUAG3YLbm1lLqJp56vq7n.png', 'image/png', 612536, '.png', '2020-02-22 05:11:17', '2020-02-22 05:11:17'),
('382aea8a-0856-4b6f-8555-0949cbe016da', 'f9307d4f-970b-4cb0-9416-7ce662298659', '67d5d4f8-0d81-3dbf-8c8b-566eb47695c6', 'public/requirements/SKLK/Ea/B1iPFcrG371gpcaqGtxCtkvqz9ziSzMQTSaSq2Tq.png', 'image/png', 612536, '.png', '2020-02-22 06:23:36', '2020-02-22 06:23:36'),
('3aabb637-4088-436b-941a-8c446ba41d60', '142dc189-b676-4654-9c86-41f82d8c7931', 'a07447e4-c5a5-3673-a697-5b1704f47cd9', 'public/requirements/PPPAOKG/Sit eaque/E3mXWBtBETGNy4Z4DBpIoRcoubnhJk6SM5uJxJsw.png', 'image/png', 18737, '.PNG', '2020-02-22 05:11:12', '2020-02-22 05:11:12'),
('3c07d283-41bc-42d2-8f5f-247891ee7633', '26fac2cc-45df-4b60-a9e3-f3e1ea5d597f', 'f0ff3cb3-ff6b-3cdc-ab73-9a34091751da', 'public/requirements/PPPAO/Officia/Sg7kqxCkZfbbV2cS8uIYeDD8A75yXuo7s6GR0Kfg.png', 'image/png', 34661, '.PNG', '2020-02-22 05:01:47', '2020-02-22 05:01:47'),
('3de27b39-df23-483e-8b2e-71448ea8f8f8', 'fcdc7dfe-f64a-4ddc-95f5-d858ac7e7ef7', '7fdc4641-bb9f-3f56-9121-ce4549d8018f', 'public/requirements/SIB/Et natus/aKs3bHvviaHk5ewOnKFPDVHaTpWMXDRea5JNzWax.png', 'image/png', 54350, '.PNG', '2020-02-22 06:20:46', '2020-02-22 06:20:46'),
('43c87329-d5f9-4463-8113-b0cf2694f78e', 'fcdc7dfe-f64a-4ddc-95f5-d858ac7e7ef7', '57c1d5a7-34f6-3324-bae3-bc044bccd56c', 'public/requirements/SIB/Et natus/APSltKfHhhaGnQvcYSrvQKRrVgCLwxNGZYTbrjxw.png', 'image/png', 18737, '.PNG', '2020-02-22 06:20:25', '2020-02-22 06:20:25'),
('47e7e36a-0cf0-45e7-bdfa-e3f791a74b5f', 'fcdc7dfe-f64a-4ddc-95f5-d858ac7e7ef7', '64a7ad00-7079-310f-8476-8c7bcb01a7b4', 'public/requirements/SIB/Et natus/ueeqnTSvrcHjLzDJ6bcaZSWOf14NSVnC33rDt6mz.png', 'image/png', 18737, '.PNG', '2020-02-22 06:20:31', '2020-02-22 06:20:31'),
('48d9952c-e82b-43cc-a856-65d9cb04fe29', 'fcdc7dfe-f64a-4ddc-95f5-d858ac7e7ef7', 'dc2faced-d3d4-3e54-a289-2d4c4026c106', 'public/requirements/SIB/Et natus/Q5uNYa7LvwUNwWVWIFnrUgm0PrV1Q0adG3Hp3YZe.png', 'image/png', 34661, '.PNG', '2020-02-22 06:20:55', '2020-02-22 06:20:55'),
('5723a3c9-b905-497d-b3c3-eef83668055c', '26fac2cc-45df-4b60-a9e3-f3e1ea5d597f', '883f1811-f396-3921-9399-5fb3ed1f0043', 'public/requirements/PPPAO/Officia/vF70XgXNxPMIrmpcPHmbu5T6PoDEFV4jWm0ybDCK.png', 'image/png', 34661, '.PNG', '2020-02-22 05:01:26', '2020-02-22 05:01:26'),
('644ec829-2dc5-460d-a2a7-b056405195a4', 'a2ef9429-4461-491c-889c-0f92a65218c8', '6cefe296-6b51-3444-85df-9fcd36a8c724', 'public/requirements/RPIO/Similique/vebtpu4cPhF5cw5NdocSPDJxyFKKZ7ndxSkPOkDG.png', 'image/png', 34661, '.PNG', '2020-02-22 06:03:22', '2020-02-22 06:03:22'),
('6f581fab-b10c-41d8-95f6-c39e230221d3', 'f9307d4f-970b-4cb0-9416-7ce662298659', 'eb4bf4bd-675f-3ec3-84fb-9e61c49834a8', 'public/requirements/SKLK/Ea/ff5o72h7Hvlh4ioQ4HtWcSOonRy8IeZCT6QWjuE6.jpeg', 'image/jpeg', 10965117, '.jpg', '2020-02-22 06:23:59', '2020-02-22 06:23:59'),
('73c622a9-72cb-4a0c-9ed1-86716aef3501', '26fac2cc-45df-4b60-a9e3-f3e1ea5d597f', '83a83f85-0ee1-3598-9e1a-9b6f4f429cd4', 'public/requirements/PPPAO/Officia/kX3Ju9MqidRpVoddJLXeBGmmtpVQxZmQywzeSKPI.png', 'image/png', 34661, '.PNG', '2020-02-22 05:01:20', '2020-02-22 05:01:20'),
('88b5901e-270c-4516-970f-855d351e5872', '26fac2cc-45df-4b60-a9e3-f3e1ea5d597f', 'c9a0cbba-a2d8-3d92-8bfe-4bbf0166e64d', 'public/requirements/PPPAO/Officia/ET0bCMiQPUcHl35Dcj7qozosLwKuMBovbd65r7fB.png', 'image/png', 54350, '.PNG', '2020-02-22 05:01:42', '2020-02-22 05:01:42'),
('8a984f6d-cd25-469d-8880-034ca18c6493', '142dc189-b676-4654-9c86-41f82d8c7931', 'e7a28b3d-ac37-309c-a5e8-fa74b5a21fa5', 'public/requirements/PPPAOKG/Sit eaque/iXEUwlAmwTciFdPR2V0T2XKa49bkPph9FtxQxZJT.png', 'image/png', 18737, '.PNG', '2020-02-22 05:11:33', '2020-02-22 05:11:33'),
('9bc58d8e-cc56-4ebb-93ac-db721a879cc0', '142dc189-b676-4654-9c86-41f82d8c7931', '094fe148-d5e5-3d6f-a827-897ab9ca2130', 'public/requirements/PPPAOKG/Sit eaque/OI7LSem2r1OyO2ZKeAxUB9YBinFMJ4ANJDfL2j02.png', 'image/png', 18737, '.PNG', '2020-02-22 05:10:45', '2020-02-22 05:10:45'),
('9bf3c6eb-0cab-44ee-b2e6-2e3e83daf14a', '142dc189-b676-4654-9c86-41f82d8c7931', '75f89e7c-5339-31e0-8ed9-8d6573ee889d', 'public/requirements/PPPAOKG/Sit eaque/L5KeLfdZabKCzbl8hz5qDCifSXxnezNPlaRhSigQ.png', 'image/png', 54350, '.PNG', '2020-02-22 05:10:56', '2020-02-22 05:10:56'),
('9f1f37cf-e53b-483c-be82-b04e25b7030a', '142dc189-b676-4654-9c86-41f82d8c7931', '704f1e42-62c7-3e39-aee6-c39d2983e690', 'public/requirements/PPPAOKG/Sit eaque/ca16XYzvMd9NsKwP67v1B5fk9kib7ccHOKroLB1H.png', 'image/png', 54350, '.PNG', '2020-02-22 05:10:51', '2020-02-22 05:10:51'),
('a3579871-f7ec-4ce1-b9ac-d7ed1c5a17e4', '26fac2cc-45df-4b60-a9e3-f3e1ea5d597f', '479869df-b542-3104-bb43-b35e362a1a51', 'public/requirements/PPPAO/Officia/b93PxkOOZR4ZgU9h3GdwRmgFBFtCe3JT64SGISXj.png', 'image/png', 18737, '.PNG', '2020-02-22 05:01:08', '2020-02-22 05:01:08'),
('a5c5fbde-ac6f-46de-9db3-4d34c71a8430', 'b5022337-56f8-4fe3-b402-a9da7e8a75e0', '9a48bbdd-f8d4-3e34-a3e1-096fa0d74a0d', 'public/requirements/KGB/Et/jyq86ZAuvXAAObmytbwwg1TGTKu0IHPviA9JrSXZ.png', 'image/png', 54350, '.PNG', '2020-02-22 04:39:59', '2020-02-22 04:39:59'),
('a6d74347-a08c-4121-ba78-41efc5b6f24e', '26fac2cc-45df-4b60-a9e3-f3e1ea5d597f', '1f2b74e8-e068-3d06-acd9-251c8cae3c2b', 'public/requirements/PPPAO/Officia/y471TsdCrjxAqXK2CyQWClC79CezlnXs2urf9ZBk.png', 'image/png', 18737, '.PNG', '2020-02-22 05:01:03', '2020-02-22 05:01:03'),
('a85fb0c0-c598-4fd4-9506-27c350fd30f8', 'fcdc7dfe-f64a-4ddc-95f5-d858ac7e7ef7', '698e9268-d570-3b5b-931e-9df7623bca58', 'public/requirements/SIB/Et natus/G9TCC2NO9T9dCIbPFHbTQKcFMsnKtCjsWjOYM7Xd.png', 'image/png', 18737, '.PNG', '2020-02-22 06:20:36', '2020-02-22 06:20:36'),
('b667272e-9b6a-4883-8bc6-716c84aefb8b', 'f9307d4f-970b-4cb0-9416-7ce662298659', '6a606bcd-d3c0-3179-993e-f96740a522ea', 'public/requirements/SKLK/Ea/nIApzFDEBC2AJ0aU0tGpqNCnQM3Yrm2gMGsSPXNU.png', 'image/png', 18737, '.PNG', '2020-02-22 06:23:41', '2020-02-22 06:23:41'),
('b817bf2a-ce82-4d0a-9d20-8c9aeb381cb6', 'fcdc7dfe-f64a-4ddc-95f5-d858ac7e7ef7', 'a329c175-c440-3fd6-8cc7-036ae1ceb3a8', 'public/requirements/SIB/Et natus/25Cw8koBgBXuQL0NYWCJrOhdrQktRE1MZDLpqURL.png', 'image/png', 18737, '.PNG', '2020-02-22 06:20:51', '2020-02-22 06:20:51'),
('bc2bbf61-f9d5-4086-b0c4-b24794168eb0', 'f9307d4f-970b-4cb0-9416-7ce662298659', '03af8985-e5d4-3345-aa38-0090d6188782', 'public/requirements/SKLK/Ea/xCpU8Ypzglw5DIvcxjlU3IKQFggJuB7FVVFZUPIJ.png', 'image/png', 18737, '.PNG', '2020-02-22 06:23:30', '2020-02-22 06:23:30'),
('bc3e0cc6-e743-4b0d-af32-eebe83cace2b', 'a2ef9429-4461-491c-889c-0f92a65218c8', '4068552f-8d8e-3a38-afac-e9cd0b32f7c4', 'public/requirements/RPIO/Similique/4Mu1fMud42taEYo0c5ptHjXSryfCTKsU3KxNzdd9.png', 'image/png', 54350, '.PNG', '2020-02-22 06:03:16', '2020-02-22 06:03:16'),
('c0fb25d6-deba-4972-b57f-9967a6020475', '26fac2cc-45df-4b60-a9e3-f3e1ea5d597f', '9d313c7b-f34c-329c-8fc1-710437dc10e0', 'public/requirements/PPPAO/Officia/Rhd9FqdH5EzePQaZU1lGerGFH6cK8gUXXBOmsIPd.png', 'image/png', 36348, '.PNG', '2020-02-22 05:01:32', '2020-02-22 05:01:32'),
('c14c1927-7e00-4e4d-8b18-30e03a7b9d1f', 'f9307d4f-970b-4cb0-9416-7ce662298659', 'cd5c6b5b-8f73-3036-8aba-91130cc3b370', 'public/requirements/SKLK/Ea/pQRMtCJAIm7sTJHxHZTWJzpWggNTvFvVVvBgVXxj.png', 'image/png', 34661, '.PNG', '2020-02-22 06:24:04', '2020-02-22 06:24:04'),
('c2441d1d-a31d-4260-ae77-08ee3d5b6e64', 'f9307d4f-970b-4cb0-9416-7ce662298659', 'ae878bef-b8aa-355b-ae86-429cd7af7844', 'public/requirements/SKLK/Ea/sbDIbIIuur1aU4ZaMrFUOacz9K62HG1y7NMtFaKn.jpeg', 'image/jpeg', 194368, '.jpg', '2020-02-22 06:23:46', '2020-02-22 06:23:46'),
('cb5ebe31-7d8a-4246-afb0-80daaaf8e931', '142dc189-b676-4654-9c86-41f82d8c7931', '9ebee518-861d-3558-a902-e519475a9d80', 'public/requirements/PPPAOKG/Sit eaque/sAu1Z8IXLv57vqUDiTWFLJi19rXJtc9oujCJ0Yqh.png', 'image/png', 34661, '.PNG', '2020-02-22 05:11:07', '2020-02-22 05:11:07'),
('d762406c-be25-43b5-80b4-f8a895cf3bad', 'fcdc7dfe-f64a-4ddc-95f5-d858ac7e7ef7', '0b6ee839-5629-305c-a2a6-997c636f555c', 'public/requirements/SIB/Et natus/R9V0f7ulUheJhqS6Vr3ZtR6HzCFTLqfy5e0LiXIY.png', 'image/png', 612536, '.png', '2020-02-22 06:20:19', '2020-02-22 06:20:19'),
('dbe06667-3e4c-44ff-9491-83165e015d65', '142dc189-b676-4654-9c86-41f82d8c7931', '038308a1-2e1d-3397-9132-ce343a4997b1', 'public/requirements/PPPAOKG/Sit eaque/MPeSjnyKQ47eeA8Y04EH7JQq33UXEDMauDbFyG5B.png', 'image/png', 18737, '.PNG', '2020-02-22 05:10:38', '2020-02-22 05:10:38'),
('de45a15a-7739-46e8-8630-7b1b4c6a40e9', '26fac2cc-45df-4b60-a9e3-f3e1ea5d597f', 'c8b75c1f-8275-35ad-8600-6e1ee7f613f7', 'public/requirements/PPPAO/Officia/dEJMnChMDD48qraCQNkXh9vIKqjcyvHFwJFs8m7B.png', 'image/png', 54350, '.PNG', '2020-02-22 05:01:37', '2020-02-22 05:01:37'),
('e4154c07-e09b-424c-90d4-4c8633a09af5', '142dc189-b676-4654-9c86-41f82d8c7931', 'b7761b47-59fa-3ae8-92d7-1f79ae41cc0e', 'public/requirements/PPPAOKG/Sit eaque/apTQkpKkUqSLFzeaCoPNfJGHOJ6CdHuTLxf3xTAe.png', 'image/png', 54350, '.PNG', '2020-02-22 05:11:22', '2020-02-22 05:11:22'),
('e9f8dbf6-f68c-418a-afa1-f26c95e92a4e', '142dc189-b676-4654-9c86-41f82d8c7931', 'c61628f3-c7c3-3df7-a8c7-714facc6a089', 'public/requirements/PPPAOKG/Sit eaque/Nij3xGbaQW7qQ9vyXh37KHqSNFLHsgU9qygggp9B.png', 'image/png', 34661, '.PNG', '2020-02-22 05:11:27', '2020-02-22 05:11:27'),
('efa6e6f1-520a-480a-8796-e19df5011d89', 'f9307d4f-970b-4cb0-9416-7ce662298659', 'bbf5f1e2-7843-3988-9b8e-ef5a864401ed', 'public/requirements/SKLK/Ea/qI4NOnr82lMqAV85zLkTsmEA1FUDN6WdDPEbSfni.png', 'image/png', 54350, '.PNG', '2020-02-22 06:23:50', '2020-02-22 06:23:50');

-- --------------------------------------------------------

--
-- Table structure for table `requirement_types`
--

CREATE TABLE `requirement_types` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abbr` char(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  `print_file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `requirement_types`
--

INSERT INTO `requirement_types` (`id`, `name`, `abbr`, `print_file`, `created_at`, `updated_at`) VALUES
('03cd67b7-b836-3703-82d0-63e7c7f24ac7', 'Surat Rekomendasi Rotasi/Promosi', 'SRR', NULL, '2020-02-02 16:33:55', '2020-02-22 18:49:04'),
('11cd4a37-6ea2-324b-b58c-db79c1b6c49a', 'KARSU', 'KAU', NULL, '2020-02-03 07:47:42', '2020-02-22 18:49:04'),
('1690ac5e-f0a2-3cff-aa58-4d2c7ecbe38e', 'Persyaratan KP Struktural (ESELON)', 'PKS', '', '2020-01-23 08:18:28', '2020-02-22 18:49:04'),
('17f83b86-af2e-32fe-8190-36d488400e82', 'KARPEG', 'KAG', NULL, '2020-02-03 07:47:50', '2020-02-22 18:49:04'),
('189539c4-fda9-380e-bc41-c121bc07c65a', 'Pengajuan Pensiun', 'PP', NULL, '2020-02-15 21:12:33', '2020-02-22 18:49:05'),
('1d9c1b26-1d83-3afc-a05d-a8a38214bd1f', 'Surat Keterangan Lulus Kuliah', 'SKLK', 'public/letter-templates//e0mUyFmyN2VEMrVagTxS4780QCvS9Nis40Wg015C.rtf', '2020-01-22 08:29:39', '2020-02-22 18:49:05'),
('2d093e78-c955-31c6-88c4-bdcbb73e2cee', 'KARIS', 'KAS', NULL, '2020-02-03 07:47:23', '2020-02-22 18:49:04'),
('54ebc251-94e6-3fd7-937b-444ee125d027', 'Proses Perpindahan PNS Antar OPD Khusus Guru', 'PPPAOKG', 'public/letter-templates//eUzM82TnvJC59m6lHfMOlV1tgMUUApv29uwYdax0_.rtf', '2020-02-02 10:56:26', '2020-02-22 18:49:04'),
('5b5e143b-0d14-344c-b554-c333fb66b241', 'Penyesuaian Ijazah', 'PI', '', '2020-01-22 10:26:42', '2020-02-22 18:49:03'),
('6358495e-ec55-350e-b123-e36b8ef5aba0', 'Persyaratan KP Sedang Melaksanakan Tugas Belajar', 'PKSMTB', '', '2020-01-23 08:19:11', '2020-02-22 18:49:04'),
('69ffc6ec-5bb4-3a01-aedc-292e462ad5b4', 'Surat Izin Belajar', 'SIB', 'public/letter-templates//f3szmTrRyDEWRQhaF5QOeJBqYKeduYuo0A3DE7Ci.rtf', '2020-01-22 08:28:47', '2020-02-22 18:49:05'),
('6c88f416-c616-3623-9ea1-7f22e3f8c54a', 'Kenaikan Gaji Berkala (KGB)', 'KGB', 'public/letter-templates//iVTxpgMpcwJf4rHBR71mO9sFFkxGRyKJ2QSduVx2.rtf', '2020-02-15 21:11:28', '2020-02-22 18:49:05'),
('8cda4fd0-23da-39d6-9797-7fd7abba5eee', 'Persyaratan KP Fungsional', 'PKF', '', '2020-01-23 08:17:40', '2020-02-22 18:49:04'),
('972c7e95-7dea-3185-9df1-01ee8f0a3e1c', 'Persyaratan KP Telah Melaksanakan Tugas Belajar', 'PKTMTB', '', '2020-01-23 08:19:31', '2020-02-22 18:49:04'),
('980a10df-873d-3d11-a31c-8c46bb20195f', 'Ujian Dinas', 'UD', '', '2020-01-22 10:26:59', '2020-02-22 18:49:03'),
('9ca65942-60ab-38c5-8081-b8d6d75a38f6', 'Persyaratan KP Penyesuaian Ijazah (PI)', 'KPPI', '', '2020-01-23 08:18:48', '2020-02-22 18:49:04'),
('d4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', 'Persyaratan KP Fungsional (Guru)', 'PKFG', '', '2020-01-23 08:18:03', '2020-02-22 18:49:04'),
('db8db0a8-2617-3788-9897-5ef3b666f2a8', 'Proses Perpindahan PNS Antar OPD', 'PPPAO', 'public/letter-templates//eUzM82TnvJC59m6lHfMOlV1tgMUUApv29uwYdax0.rtf', '2020-01-31 14:12:18', '2020-02-22 18:49:04'),
('ecc5f0f0-c7e8-306d-9bb6-440a97bb771b', 'Rekomendasi Perpindahan Intern OPD', 'RPIO', 'public/letter-templates//PcmyIqnIcfcufeOras2zaaGncQTyhql6btoRaWMH.rtf', '2020-02-07 05:15:33', '2020-02-22 18:49:05'),
('ece49cf7-4d45-385f-afc0-b899d74fbbfb', 'Persyaratan KP Regular (KPO)', 'KPO', '', '2020-01-23 08:16:39', '2020-02-22 18:49:03');

-- --------------------------------------------------------

--
-- Table structure for table `requirement_type_biodata`
--

CREATE TABLE `requirement_type_biodata` (
  `id` bigint(22) NOT NULL,
  `requirement_type_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `biodata_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_as` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `placeholder_as` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `requirement_type_biodata`
--

INSERT INTO `requirement_type_biodata` (`id`, `requirement_type_id`, `biodata_id`, `name_as`, `placeholder_as`, `order`, `created_at`, `updated_at`) VALUES
(104, '2d093e78-c955-31c6-88c4-bdcbb73e2cee', 'f3f68364-2b63-305b-a9ed-8667f7c43bc9', NULL, NULL, 1, NULL, NULL),
(105, '17f83b86-af2e-32fe-8190-36d488400e82', 'f3f68364-2b63-305b-a9ed-8667f7c43bc9', NULL, NULL, 1, NULL, NULL),
(106, '11cd4a37-6ea2-324b-b58c-db79c1b6c49a', 'f3f68364-2b63-305b-a9ed-8667f7c43bc9', NULL, NULL, 1, NULL, NULL),
(108, '189539c4-fda9-380e-bc41-c121bc07c65a', 'f3f68364-2b63-305b-a9ed-8667f7c43bc9', NULL, NULL, 1, NULL, NULL),
(109, '5b5e143b-0d14-344c-b554-c333fb66b241', 'f3f68364-2b63-305b-a9ed-8667f7c43bc9', NULL, NULL, 1, NULL, NULL),
(110, '8cda4fd0-23da-39d6-9797-7fd7abba5eee', 'f3f68364-2b63-305b-a9ed-8667f7c43bc9', NULL, NULL, 1, NULL, NULL),
(111, 'd4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', 'f3f68364-2b63-305b-a9ed-8667f7c43bc9', NULL, NULL, 2, NULL, NULL),
(112, '9ca65942-60ab-38c5-8081-b8d6d75a38f6', 'f3f68364-2b63-305b-a9ed-8667f7c43bc9', NULL, NULL, 2, NULL, NULL),
(113, 'ece49cf7-4d45-385f-afc0-b899d74fbbfb', 'f3f68364-2b63-305b-a9ed-8667f7c43bc9', NULL, NULL, 2, NULL, NULL),
(114, '6358495e-ec55-350e-b123-e36b8ef5aba0', 'f3f68364-2b63-305b-a9ed-8667f7c43bc9', NULL, NULL, 2, NULL, NULL),
(115, '1690ac5e-f0a2-3cff-aa58-4d2c7ecbe38e', 'f3f68364-2b63-305b-a9ed-8667f7c43bc9', NULL, NULL, 2, NULL, NULL),
(116, '972c7e95-7dea-3185-9df1-01ee8f0a3e1c', 'f3f68364-2b63-305b-a9ed-8667f7c43bc9', NULL, NULL, 2, NULL, NULL),
(117, 'db8db0a8-2617-3788-9897-5ef3b666f2a8', 'f3f68364-2b63-305b-a9ed-8667f7c43bc9', NULL, NULL, 1, NULL, NULL),
(118, '54ebc251-94e6-3fd7-937b-444ee125d027', 'f3f68364-2b63-305b-a9ed-8667f7c43bc9', NULL, NULL, 2, NULL, NULL),
(119, 'ecc5f0f0-c7e8-306d-9bb6-440a97bb771b', 'f3f68364-2b63-305b-a9ed-8667f7c43bc9', NULL, NULL, 1, NULL, NULL),
(121, '1d9c1b26-1d83-3afc-a05d-a8a38214bd1f', 'f3f68364-2b63-305b-a9ed-8667f7c43bc9', NULL, NULL, 1, NULL, NULL),
(122, '03cd67b7-b836-3703-82d0-63e7c7f24ac7', 'f3f68364-2b63-305b-a9ed-8667f7c43bc9', NULL, NULL, 2, NULL, NULL),
(123, '980a10df-873d-3d11-a31c-8c46bb20195f', 'f3f68364-2b63-305b-a9ed-8667f7c43bc9', NULL, NULL, 1, NULL, NULL),
(146, '2d093e78-c955-31c6-88c4-bdcbb73e2cee', 'a9bba2e8-2d1f-34af-ae51-19ff58f2cef9', NULL, NULL, 2, NULL, NULL),
(147, '17f83b86-af2e-32fe-8190-36d488400e82', 'a9bba2e8-2d1f-34af-ae51-19ff58f2cef9', NULL, NULL, 2, NULL, NULL),
(148, '11cd4a37-6ea2-324b-b58c-db79c1b6c49a', 'a9bba2e8-2d1f-34af-ae51-19ff58f2cef9', NULL, NULL, 2, NULL, NULL),
(150, '189539c4-fda9-380e-bc41-c121bc07c65a', 'a9bba2e8-2d1f-34af-ae51-19ff58f2cef9', NULL, NULL, 2, NULL, NULL),
(151, '5b5e143b-0d14-344c-b554-c333fb66b241', 'a9bba2e8-2d1f-34af-ae51-19ff58f2cef9', NULL, NULL, 2, NULL, NULL),
(152, '8cda4fd0-23da-39d6-9797-7fd7abba5eee', 'a9bba2e8-2d1f-34af-ae51-19ff58f2cef9', NULL, NULL, 2, NULL, NULL),
(153, 'd4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', 'a9bba2e8-2d1f-34af-ae51-19ff58f2cef9', NULL, NULL, 3, NULL, NULL),
(154, '9ca65942-60ab-38c5-8081-b8d6d75a38f6', 'a9bba2e8-2d1f-34af-ae51-19ff58f2cef9', NULL, NULL, 3, NULL, NULL),
(155, 'ece49cf7-4d45-385f-afc0-b899d74fbbfb', 'a9bba2e8-2d1f-34af-ae51-19ff58f2cef9', NULL, NULL, 3, NULL, NULL),
(156, '6358495e-ec55-350e-b123-e36b8ef5aba0', 'a9bba2e8-2d1f-34af-ae51-19ff58f2cef9', NULL, NULL, 3, NULL, NULL),
(157, '1690ac5e-f0a2-3cff-aa58-4d2c7ecbe38e', 'a9bba2e8-2d1f-34af-ae51-19ff58f2cef9', NULL, NULL, 3, NULL, NULL),
(158, '972c7e95-7dea-3185-9df1-01ee8f0a3e1c', 'a9bba2e8-2d1f-34af-ae51-19ff58f2cef9', NULL, NULL, 3, NULL, NULL),
(159, 'db8db0a8-2617-3788-9897-5ef3b666f2a8', 'a9bba2e8-2d1f-34af-ae51-19ff58f2cef9', NULL, NULL, 2, NULL, NULL),
(160, '54ebc251-94e6-3fd7-937b-444ee125d027', 'a9bba2e8-2d1f-34af-ae51-19ff58f2cef9', NULL, NULL, 3, NULL, NULL),
(161, 'ecc5f0f0-c7e8-306d-9bb6-440a97bb771b', 'a9bba2e8-2d1f-34af-ae51-19ff58f2cef9', NULL, NULL, 2, NULL, NULL),
(162, '69ffc6ec-5bb4-3a01-aedc-292e462ad5b4', 'a9bba2e8-2d1f-34af-ae51-19ff58f2cef9', NULL, NULL, 2, NULL, NULL),
(163, '1d9c1b26-1d83-3afc-a05d-a8a38214bd1f', 'a9bba2e8-2d1f-34af-ae51-19ff58f2cef9', NULL, NULL, 2, NULL, NULL),
(164, '03cd67b7-b836-3703-82d0-63e7c7f24ac7', 'a9bba2e8-2d1f-34af-ae51-19ff58f2cef9', NULL, NULL, 3, NULL, NULL),
(165, '980a10df-873d-3d11-a31c-8c46bb20195f', 'a9bba2e8-2d1f-34af-ae51-19ff58f2cef9', NULL, NULL, 2, NULL, NULL),
(166, '2d093e78-c955-31c6-88c4-bdcbb73e2cee', 'd2228828-387f-3f8e-b803-d6ee989e803d', NULL, NULL, 3, NULL, NULL),
(167, '17f83b86-af2e-32fe-8190-36d488400e82', 'd2228828-387f-3f8e-b803-d6ee989e803d', NULL, NULL, 3, NULL, NULL),
(168, '11cd4a37-6ea2-324b-b58c-db79c1b6c49a', 'd2228828-387f-3f8e-b803-d6ee989e803d', NULL, NULL, 3, NULL, NULL),
(169, '6c88f416-c616-3623-9ea1-7f22e3f8c54a', 'd2228828-387f-3f8e-b803-d6ee989e803d', NULL, NULL, 2, NULL, NULL),
(170, '189539c4-fda9-380e-bc41-c121bc07c65a', 'd2228828-387f-3f8e-b803-d6ee989e803d', NULL, NULL, 3, NULL, NULL),
(171, '5b5e143b-0d14-344c-b554-c333fb66b241', 'd2228828-387f-3f8e-b803-d6ee989e803d', NULL, NULL, 3, NULL, NULL),
(172, '8cda4fd0-23da-39d6-9797-7fd7abba5eee', 'd2228828-387f-3f8e-b803-d6ee989e803d', NULL, NULL, 3, NULL, NULL),
(173, 'd4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', 'd2228828-387f-3f8e-b803-d6ee989e803d', NULL, NULL, 4, NULL, NULL),
(174, '9ca65942-60ab-38c5-8081-b8d6d75a38f6', 'd2228828-387f-3f8e-b803-d6ee989e803d', NULL, NULL, 4, NULL, NULL),
(175, 'ece49cf7-4d45-385f-afc0-b899d74fbbfb', 'd2228828-387f-3f8e-b803-d6ee989e803d', NULL, NULL, 4, NULL, NULL),
(176, '6358495e-ec55-350e-b123-e36b8ef5aba0', 'd2228828-387f-3f8e-b803-d6ee989e803d', NULL, NULL, 4, NULL, NULL),
(177, '1690ac5e-f0a2-3cff-aa58-4d2c7ecbe38e', 'd2228828-387f-3f8e-b803-d6ee989e803d', NULL, NULL, 4, NULL, NULL),
(178, '972c7e95-7dea-3185-9df1-01ee8f0a3e1c', 'd2228828-387f-3f8e-b803-d6ee989e803d', NULL, NULL, 4, NULL, NULL),
(179, 'db8db0a8-2617-3788-9897-5ef3b666f2a8', 'd2228828-387f-3f8e-b803-d6ee989e803d', NULL, NULL, 3, NULL, NULL),
(180, '54ebc251-94e6-3fd7-937b-444ee125d027', 'd2228828-387f-3f8e-b803-d6ee989e803d', NULL, NULL, 4, NULL, NULL),
(181, 'ecc5f0f0-c7e8-306d-9bb6-440a97bb771b', 'd2228828-387f-3f8e-b803-d6ee989e803d', NULL, NULL, 3, NULL, NULL),
(182, '69ffc6ec-5bb4-3a01-aedc-292e462ad5b4', 'd2228828-387f-3f8e-b803-d6ee989e803d', NULL, NULL, 3, NULL, NULL),
(183, '1d9c1b26-1d83-3afc-a05d-a8a38214bd1f', 'd2228828-387f-3f8e-b803-d6ee989e803d', NULL, NULL, 3, NULL, NULL),
(184, '03cd67b7-b836-3703-82d0-63e7c7f24ac7', 'd2228828-387f-3f8e-b803-d6ee989e803d', NULL, NULL, 4, NULL, NULL),
(185, '980a10df-873d-3d11-a31c-8c46bb20195f', 'd2228828-387f-3f8e-b803-d6ee989e803d', NULL, NULL, 3, NULL, NULL),
(186, '2d093e78-c955-31c6-88c4-bdcbb73e2cee', 'd59562a7-c14e-3340-b4e4-4fb7fd3dcd56', NULL, NULL, 4, NULL, NULL),
(187, '17f83b86-af2e-32fe-8190-36d488400e82', 'd59562a7-c14e-3340-b4e4-4fb7fd3dcd56', NULL, NULL, 4, NULL, NULL),
(188, '11cd4a37-6ea2-324b-b58c-db79c1b6c49a', 'd59562a7-c14e-3340-b4e4-4fb7fd3dcd56', NULL, NULL, 4, NULL, NULL),
(189, '6c88f416-c616-3623-9ea1-7f22e3f8c54a', 'd59562a7-c14e-3340-b4e4-4fb7fd3dcd56', NULL, NULL, 3, NULL, NULL),
(190, '189539c4-fda9-380e-bc41-c121bc07c65a', 'd59562a7-c14e-3340-b4e4-4fb7fd3dcd56', NULL, NULL, 4, NULL, NULL),
(191, '5b5e143b-0d14-344c-b554-c333fb66b241', 'd59562a7-c14e-3340-b4e4-4fb7fd3dcd56', NULL, NULL, 4, NULL, NULL),
(192, '8cda4fd0-23da-39d6-9797-7fd7abba5eee', 'd59562a7-c14e-3340-b4e4-4fb7fd3dcd56', NULL, NULL, 4, NULL, NULL),
(193, 'd4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', 'd59562a7-c14e-3340-b4e4-4fb7fd3dcd56', NULL, NULL, 5, NULL, NULL),
(194, '9ca65942-60ab-38c5-8081-b8d6d75a38f6', 'd59562a7-c14e-3340-b4e4-4fb7fd3dcd56', NULL, NULL, 5, NULL, NULL),
(195, 'ece49cf7-4d45-385f-afc0-b899d74fbbfb', 'd59562a7-c14e-3340-b4e4-4fb7fd3dcd56', NULL, NULL, 5, NULL, NULL),
(196, '6358495e-ec55-350e-b123-e36b8ef5aba0', 'd59562a7-c14e-3340-b4e4-4fb7fd3dcd56', NULL, NULL, 5, NULL, NULL),
(197, '1690ac5e-f0a2-3cff-aa58-4d2c7ecbe38e', 'd59562a7-c14e-3340-b4e4-4fb7fd3dcd56', NULL, NULL, 5, NULL, NULL),
(198, '972c7e95-7dea-3185-9df1-01ee8f0a3e1c', 'd59562a7-c14e-3340-b4e4-4fb7fd3dcd56', NULL, NULL, 5, NULL, NULL),
(199, 'db8db0a8-2617-3788-9897-5ef3b666f2a8', 'd59562a7-c14e-3340-b4e4-4fb7fd3dcd56', NULL, NULL, 4, NULL, NULL),
(200, '54ebc251-94e6-3fd7-937b-444ee125d027', 'd59562a7-c14e-3340-b4e4-4fb7fd3dcd56', NULL, NULL, 5, NULL, NULL),
(201, 'ecc5f0f0-c7e8-306d-9bb6-440a97bb771b', 'd59562a7-c14e-3340-b4e4-4fb7fd3dcd56', NULL, NULL, 4, NULL, NULL),
(202, '69ffc6ec-5bb4-3a01-aedc-292e462ad5b4', 'd59562a7-c14e-3340-b4e4-4fb7fd3dcd56', NULL, NULL, 4, NULL, NULL),
(203, '1d9c1b26-1d83-3afc-a05d-a8a38214bd1f', 'd59562a7-c14e-3340-b4e4-4fb7fd3dcd56', NULL, NULL, 4, NULL, NULL),
(204, '03cd67b7-b836-3703-82d0-63e7c7f24ac7', 'd59562a7-c14e-3340-b4e4-4fb7fd3dcd56', NULL, NULL, 5, NULL, NULL),
(205, '980a10df-873d-3d11-a31c-8c46bb20195f', 'd59562a7-c14e-3340-b4e4-4fb7fd3dcd56', NULL, NULL, 4, NULL, NULL),
(210, '189539c4-fda9-380e-bc41-c121bc07c65a', '0179160b-8b21-38c1-a673-98b4a3a2c007', NULL, NULL, 5, NULL, NULL),
(211, '5b5e143b-0d14-344c-b554-c333fb66b241', '0179160b-8b21-38c1-a673-98b4a3a2c007', NULL, NULL, 5, NULL, NULL),
(216, '6358495e-ec55-350e-b123-e36b8ef5aba0', '0179160b-8b21-38c1-a673-98b4a3a2c007', NULL, NULL, 6, NULL, NULL),
(218, '972c7e95-7dea-3185-9df1-01ee8f0a3e1c', '0179160b-8b21-38c1-a673-98b4a3a2c007', NULL, NULL, 6, NULL, NULL),
(219, 'db8db0a8-2617-3788-9897-5ef3b666f2a8', '0179160b-8b21-38c1-a673-98b4a3a2c007', NULL, NULL, 5, NULL, NULL),
(220, '54ebc251-94e6-3fd7-937b-444ee125d027', '0179160b-8b21-38c1-a673-98b4a3a2c007', NULL, NULL, 6, NULL, NULL),
(221, 'ecc5f0f0-c7e8-306d-9bb6-440a97bb771b', '0179160b-8b21-38c1-a673-98b4a3a2c007', NULL, NULL, 5, NULL, NULL),
(222, '69ffc6ec-5bb4-3a01-aedc-292e462ad5b4', '0179160b-8b21-38c1-a673-98b4a3a2c007', NULL, NULL, 5, NULL, NULL),
(223, '1d9c1b26-1d83-3afc-a05d-a8a38214bd1f', '0179160b-8b21-38c1-a673-98b4a3a2c007', NULL, NULL, 5, NULL, NULL),
(224, '03cd67b7-b836-3703-82d0-63e7c7f24ac7', '0179160b-8b21-38c1-a673-98b4a3a2c007', NULL, NULL, 6, NULL, NULL),
(225, '980a10df-873d-3d11-a31c-8c46bb20195f', '0179160b-8b21-38c1-a673-98b4a3a2c007', NULL, NULL, 5, NULL, NULL),
(226, '2d093e78-c955-31c6-88c4-bdcbb73e2cee', '381f1c3b-ee0b-3dcc-9b34-4f6b56ab275b', NULL, NULL, 5, NULL, NULL),
(227, '17f83b86-af2e-32fe-8190-36d488400e82', '381f1c3b-ee0b-3dcc-9b34-4f6b56ab275b', NULL, NULL, 5, NULL, NULL),
(228, '11cd4a37-6ea2-324b-b58c-db79c1b6c49a', '381f1c3b-ee0b-3dcc-9b34-4f6b56ab275b', NULL, NULL, 5, NULL, NULL),
(230, '189539c4-fda9-380e-bc41-c121bc07c65a', '381f1c3b-ee0b-3dcc-9b34-4f6b56ab275b', NULL, NULL, 6, NULL, NULL),
(231, '5b5e143b-0d14-344c-b554-c333fb66b241', '381f1c3b-ee0b-3dcc-9b34-4f6b56ab275b', NULL, NULL, 6, NULL, NULL),
(236, '6358495e-ec55-350e-b123-e36b8ef5aba0', '381f1c3b-ee0b-3dcc-9b34-4f6b56ab275b', NULL, NULL, 7, NULL, NULL),
(238, '972c7e95-7dea-3185-9df1-01ee8f0a3e1c', '381f1c3b-ee0b-3dcc-9b34-4f6b56ab275b', NULL, NULL, 7, NULL, NULL),
(239, 'db8db0a8-2617-3788-9897-5ef3b666f2a8', '381f1c3b-ee0b-3dcc-9b34-4f6b56ab275b', NULL, NULL, 6, NULL, NULL),
(240, '54ebc251-94e6-3fd7-937b-444ee125d027', '381f1c3b-ee0b-3dcc-9b34-4f6b56ab275b', NULL, NULL, 7, NULL, NULL),
(242, '69ffc6ec-5bb4-3a01-aedc-292e462ad5b4', '381f1c3b-ee0b-3dcc-9b34-4f6b56ab275b', NULL, NULL, 6, NULL, NULL),
(243, '1d9c1b26-1d83-3afc-a05d-a8a38214bd1f', '381f1c3b-ee0b-3dcc-9b34-4f6b56ab275b', NULL, NULL, 6, NULL, NULL),
(245, '980a10df-873d-3d11-a31c-8c46bb20195f', '381f1c3b-ee0b-3dcc-9b34-4f6b56ab275b', NULL, NULL, 6, NULL, NULL),
(251, '5b5e143b-0d14-344c-b554-c333fb66b241', 'f145556a-c4d3-3443-91c3-3df27bd98732', NULL, NULL, 7, NULL, NULL),
(256, '6358495e-ec55-350e-b123-e36b8ef5aba0', 'f145556a-c4d3-3443-91c3-3df27bd98732', NULL, NULL, 8, NULL, NULL),
(258, '972c7e95-7dea-3185-9df1-01ee8f0a3e1c', 'f145556a-c4d3-3443-91c3-3df27bd98732', NULL, NULL, 8, NULL, NULL),
(262, '69ffc6ec-5bb4-3a01-aedc-292e462ad5b4', 'f145556a-c4d3-3443-91c3-3df27bd98732', NULL, NULL, 7, NULL, NULL),
(263, '1d9c1b26-1d83-3afc-a05d-a8a38214bd1f', 'f145556a-c4d3-3443-91c3-3df27bd98732', NULL, NULL, 7, NULL, NULL),
(265, '980a10df-873d-3d11-a31c-8c46bb20195f', 'f145556a-c4d3-3443-91c3-3df27bd98732', NULL, NULL, 7, NULL, NULL),
(271, '5b5e143b-0d14-344c-b554-c333fb66b241', '020a0a56-c870-3760-ac0e-1a5a2e55938e', NULL, NULL, 8, NULL, NULL),
(276, '6358495e-ec55-350e-b123-e36b8ef5aba0', '020a0a56-c870-3760-ac0e-1a5a2e55938e', NULL, NULL, 9, NULL, NULL),
(278, '972c7e95-7dea-3185-9df1-01ee8f0a3e1c', '020a0a56-c870-3760-ac0e-1a5a2e55938e', NULL, NULL, 9, NULL, NULL),
(282, '69ffc6ec-5bb4-3a01-aedc-292e462ad5b4', '020a0a56-c870-3760-ac0e-1a5a2e55938e', NULL, NULL, 8, NULL, NULL),
(283, '1d9c1b26-1d83-3afc-a05d-a8a38214bd1f', '020a0a56-c870-3760-ac0e-1a5a2e55938e', NULL, NULL, 8, NULL, NULL),
(285, '980a10df-873d-3d11-a31c-8c46bb20195f', '020a0a56-c870-3760-ac0e-1a5a2e55938e', NULL, NULL, 8, NULL, NULL),
(291, '5b5e143b-0d14-344c-b554-c333fb66b241', 'f688c278-b0b9-3465-a735-6fe16da9bcdb', NULL, NULL, 9, NULL, NULL),
(296, '6358495e-ec55-350e-b123-e36b8ef5aba0', 'f688c278-b0b9-3465-a735-6fe16da9bcdb', NULL, NULL, 10, NULL, NULL),
(298, '972c7e95-7dea-3185-9df1-01ee8f0a3e1c', 'f688c278-b0b9-3465-a735-6fe16da9bcdb', NULL, NULL, 10, NULL, NULL),
(302, '69ffc6ec-5bb4-3a01-aedc-292e462ad5b4', 'f688c278-b0b9-3465-a735-6fe16da9bcdb', NULL, NULL, 9, NULL, NULL),
(303, '1d9c1b26-1d83-3afc-a05d-a8a38214bd1f', 'f688c278-b0b9-3465-a735-6fe16da9bcdb', NULL, NULL, 9, NULL, NULL),
(305, '980a10df-873d-3d11-a31c-8c46bb20195f', 'f688c278-b0b9-3465-a735-6fe16da9bcdb', NULL, NULL, 9, NULL, NULL),
(306, '2d093e78-c955-31c6-88c4-bdcbb73e2cee', 'f8e33e39-3343-3328-a765-dc359a9d2232', NULL, NULL, 10, NULL, NULL),
(310, '189539c4-fda9-380e-bc41-c121bc07c65a', 'f8e33e39-3343-3328-a765-dc359a9d2232', NULL, NULL, 8, NULL, NULL),
(311, '5b5e143b-0d14-344c-b554-c333fb66b241', 'f8e33e39-3343-3328-a765-dc359a9d2232', NULL, NULL, 10, NULL, NULL),
(316, '6358495e-ec55-350e-b123-e36b8ef5aba0', 'f8e33e39-3343-3328-a765-dc359a9d2232', NULL, NULL, 11, NULL, NULL),
(318, '972c7e95-7dea-3185-9df1-01ee8f0a3e1c', 'f8e33e39-3343-3328-a765-dc359a9d2232', NULL, NULL, 11, NULL, NULL),
(322, '69ffc6ec-5bb4-3a01-aedc-292e462ad5b4', 'f8e33e39-3343-3328-a765-dc359a9d2232', NULL, NULL, 10, NULL, NULL),
(323, '1d9c1b26-1d83-3afc-a05d-a8a38214bd1f', 'f8e33e39-3343-3328-a765-dc359a9d2232', NULL, NULL, 13, NULL, NULL),
(325, '980a10df-873d-3d11-a31c-8c46bb20195f', 'f8e33e39-3343-3328-a765-dc359a9d2232', NULL, NULL, 10, NULL, NULL),
(342, '69ffc6ec-5bb4-3a01-aedc-292e462ad5b4', '756d8d0b-f640-368f-9f2d-4690b0089660', NULL, NULL, 11, NULL, NULL),
(343, '1d9c1b26-1d83-3afc-a05d-a8a38214bd1f', '756d8d0b-f640-368f-9f2d-4690b0089660', NULL, NULL, 14, NULL, NULL),
(351, '5b5e143b-0d14-344c-b554-c333fb66b241', 'a99b63eb-21e7-3a03-87ac-eed969f5bc76', NULL, NULL, 12, NULL, NULL),
(356, '6358495e-ec55-350e-b123-e36b8ef5aba0', 'a99b63eb-21e7-3a03-87ac-eed969f5bc76', NULL, NULL, 13, NULL, NULL),
(358, '972c7e95-7dea-3185-9df1-01ee8f0a3e1c', 'a99b63eb-21e7-3a03-87ac-eed969f5bc76', NULL, NULL, 13, NULL, NULL),
(362, '69ffc6ec-5bb4-3a01-aedc-292e462ad5b4', 'a99b63eb-21e7-3a03-87ac-eed969f5bc76', NULL, NULL, 12, NULL, NULL),
(363, '1d9c1b26-1d83-3afc-a05d-a8a38214bd1f', 'a99b63eb-21e7-3a03-87ac-eed969f5bc76', NULL, NULL, 15, NULL, NULL),
(365, '980a10df-873d-3d11-a31c-8c46bb20195f', 'a99b63eb-21e7-3a03-87ac-eed969f5bc76', NULL, NULL, 12, NULL, NULL),
(366, '2d093e78-c955-31c6-88c4-bdcbb73e2cee', '02691749-f7f9-336c-ac9f-0d95676379f0', NULL, NULL, 13, NULL, NULL),
(367, '17f83b86-af2e-32fe-8190-36d488400e82', '02691749-f7f9-336c-ac9f-0d95676379f0', NULL, NULL, 7, NULL, NULL),
(368, '11cd4a37-6ea2-324b-b58c-db79c1b6c49a', '02691749-f7f9-336c-ac9f-0d95676379f0', NULL, NULL, 7, NULL, NULL),
(369, '6c88f416-c616-3623-9ea1-7f22e3f8c54a', '02691749-f7f9-336c-ac9f-0d95676379f0', NULL, NULL, 16, NULL, NULL),
(371, '5b5e143b-0d14-344c-b554-c333fb66b241', '02691749-f7f9-336c-ac9f-0d95676379f0', NULL, NULL, 13, NULL, NULL),
(376, '6358495e-ec55-350e-b123-e36b8ef5aba0', '02691749-f7f9-336c-ac9f-0d95676379f0', NULL, NULL, 14, NULL, NULL),
(378, '972c7e95-7dea-3185-9df1-01ee8f0a3e1c', '02691749-f7f9-336c-ac9f-0d95676379f0', NULL, NULL, 14, NULL, NULL),
(379, 'db8db0a8-2617-3788-9897-5ef3b666f2a8', '02691749-f7f9-336c-ac9f-0d95676379f0', NULL, NULL, 14, NULL, NULL),
(382, '69ffc6ec-5bb4-3a01-aedc-292e462ad5b4', '02691749-f7f9-336c-ac9f-0d95676379f0', NULL, NULL, 13, NULL, NULL),
(383, '1d9c1b26-1d83-3afc-a05d-a8a38214bd1f', '02691749-f7f9-336c-ac9f-0d95676379f0', NULL, NULL, 16, NULL, NULL),
(385, '980a10df-873d-3d11-a31c-8c46bb20195f', '02691749-f7f9-336c-ac9f-0d95676379f0', NULL, NULL, 14, NULL, NULL),
(390, '189539c4-fda9-380e-bc41-c121bc07c65a', '6b8a273f-59ed-3f63-b5f2-96ae4593c407', NULL, NULL, 12, NULL, NULL),
(391, '5b5e143b-0d14-344c-b554-c333fb66b241', '6b8a273f-59ed-3f63-b5f2-96ae4593c407', NULL, NULL, 14, NULL, NULL),
(396, '6358495e-ec55-350e-b123-e36b8ef5aba0', '6b8a273f-59ed-3f63-b5f2-96ae4593c407', NULL, NULL, 15, NULL, NULL),
(398, '972c7e95-7dea-3185-9df1-01ee8f0a3e1c', '6b8a273f-59ed-3f63-b5f2-96ae4593c407', NULL, NULL, 15, NULL, NULL),
(399, 'db8db0a8-2617-3788-9897-5ef3b666f2a8', '6b8a273f-59ed-3f63-b5f2-96ae4593c407', NULL, NULL, 15, NULL, NULL),
(403, '1d9c1b26-1d83-3afc-a05d-a8a38214bd1f', '6b8a273f-59ed-3f63-b5f2-96ae4593c407', NULL, NULL, 17, NULL, NULL),
(405, '980a10df-873d-3d11-a31c-8c46bb20195f', '6b8a273f-59ed-3f63-b5f2-96ae4593c407', NULL, NULL, 15, NULL, NULL),
(406, '1d9c1b26-1d83-3afc-a05d-a8a38214bd1f', '8ff905b0-2427-3e16-92ce-e8a0284ca16b', NULL, NULL, 11, NULL, NULL),
(407, '1d9c1b26-1d83-3afc-a05d-a8a38214bd1f', '1a9b6e27-91db-3466-bfe0-5fdb302b4307', NULL, NULL, 10, NULL, NULL),
(408, '1d9c1b26-1d83-3afc-a05d-a8a38214bd1f', '73d028e8-d6ae-3439-a3c7-c42b3dd15cf8', NULL, NULL, 12, NULL, NULL),
(409, '5b5e143b-0d14-344c-b554-c333fb66b241', '673f037d-1ad1-3a80-9258-2d71bad304a3', NULL, NULL, 11, NULL, NULL),
(410, 'ece49cf7-4d45-385f-afc0-b899d74fbbfb', '7443dd48-c69d-307b-bf4b-73b47002c2e9', NULL, NULL, 16, NULL, NULL),
(462, '69ffc6ec-5bb4-3a01-aedc-292e462ad5b4', 'f3f68364-2b63-305b-a9ed-8667f7c43bc9', NULL, NULL, 1, NULL, NULL),
(463, '69ffc6ec-5bb4-3a01-aedc-292e462ad5b4', '6b8a273f-59ed-3f63-b5f2-96ae4593c407', NULL, NULL, 14, NULL, NULL),
(464, '17f83b86-af2e-32fe-8190-36d488400e82', 'f8e33e39-3343-3328-a765-dc359a9d2232', NULL, NULL, 6, NULL, NULL),
(465, '11cd4a37-6ea2-324b-b58c-db79c1b6c49a', 'f8e33e39-3343-3328-a765-dc359a9d2232', NULL, NULL, 6, NULL, NULL),
(466, '6c88f416-c616-3623-9ea1-7f22e3f8c54a', 'edd3dd93-3485-3cbc-828c-55bce7f0fda0', NULL, NULL, 1, NULL, NULL),
(467, '6c88f416-c616-3623-9ea1-7f22e3f8c54a', '7e09b85f-7322-39fa-a15f-d9ac3e441990', NULL, NULL, 4, NULL, NULL),
(468, '6c88f416-c616-3623-9ea1-7f22e3f8c54a', '32123519-a5c3-33c4-9347-4fe94eea0a90', NULL, NULL, 5, NULL, NULL),
(469, '6c88f416-c616-3623-9ea1-7f22e3f8c54a', '4a3f261e-8e20-30c2-ad1a-763c14a6d2be', NULL, NULL, 6, NULL, NULL),
(470, '6c88f416-c616-3623-9ea1-7f22e3f8c54a', '3b6a13f6-825c-30e1-bd00-bf0c9428bce9', NULL, NULL, 7, NULL, NULL),
(471, '6c88f416-c616-3623-9ea1-7f22e3f8c54a', 'd0ba6947-572a-31e6-96ca-712bf7d20195', NULL, NULL, 8, NULL, NULL),
(472, '6c88f416-c616-3623-9ea1-7f22e3f8c54a', '09cfd508-fcc2-3984-8577-2a3387c88894', NULL, NULL, 10, NULL, NULL),
(473, '6c88f416-c616-3623-9ea1-7f22e3f8c54a', '720fa63d-c2c0-3bb8-bace-a31ed2ee25a1', NULL, NULL, 9, NULL, NULL),
(474, '6c88f416-c616-3623-9ea1-7f22e3f8c54a', '8dcf99fd-48bd-3ae2-a30f-080ced062f7a', NULL, NULL, 11, NULL, NULL),
(475, '6c88f416-c616-3623-9ea1-7f22e3f8c54a', 'a3b10b5e-db9a-3b3c-a4da-75a405054835', NULL, NULL, 12, NULL, NULL),
(476, '6c88f416-c616-3623-9ea1-7f22e3f8c54a', '05b16bdf-6f23-3003-8792-e01bd8e64d9e', NULL, NULL, 13, NULL, NULL),
(477, '6c88f416-c616-3623-9ea1-7f22e3f8c54a', '0b87d7c4-de71-3f95-b904-20990f9cabf5', NULL, NULL, 14, NULL, NULL),
(478, '6c88f416-c616-3623-9ea1-7f22e3f8c54a', '328e919c-f630-3efe-9a4d-4af7e3a242e4', NULL, NULL, 15, NULL, NULL),
(479, '189539c4-fda9-380e-bc41-c121bc07c65a', '234acfeb-36c5-3e0f-a4d4-cef16b0e4054', NULL, NULL, 7, NULL, NULL),
(480, '189539c4-fda9-380e-bc41-c121bc07c65a', '02691749-f7f9-336c-ac9f-0d95676379f0', NULL, NULL, 11, NULL, NULL),
(481, '8cda4fd0-23da-39d6-9797-7fd7abba5eee', '7443dd48-c69d-307b-bf4b-73b47002c2e9', NULL, NULL, 5, NULL, NULL),
(482, '8cda4fd0-23da-39d6-9797-7fd7abba5eee', '8d724505-f3db-3d29-a0a5-66b3ce24983f', NULL, NULL, 6, NULL, NULL),
(483, '8cda4fd0-23da-39d6-9797-7fd7abba5eee', '494346fa-f333-3963-b272-63c59aa1733f', NULL, NULL, 8, NULL, NULL),
(484, '8cda4fd0-23da-39d6-9797-7fd7abba5eee', 'bbd66af4-4212-36c0-bffe-6c735e2e5136', NULL, NULL, 9, NULL, NULL),
(488, '8cda4fd0-23da-39d6-9797-7fd7abba5eee', 'f8e33e39-3343-3328-a765-dc359a9d2232', NULL, NULL, 7, NULL, NULL),
(490, '8cda4fd0-23da-39d6-9797-7fd7abba5eee', '1b0937a6-2dce-3fb9-9226-17d3491a13e5', NULL, NULL, 10, NULL, NULL),
(491, '8cda4fd0-23da-39d6-9797-7fd7abba5eee', 'e2189caa-f961-30f2-9fbf-36591dcf28a3', NULL, NULL, 11, NULL, NULL),
(492, '8cda4fd0-23da-39d6-9797-7fd7abba5eee', '6ccdda23-125e-301f-bca7-c160e29ca09c', NULL, NULL, 12, NULL, NULL),
(493, '8cda4fd0-23da-39d6-9797-7fd7abba5eee', '2a9de6f8-0b60-3727-81d1-ff99f252457d', NULL, NULL, 13, NULL, NULL),
(494, '8cda4fd0-23da-39d6-9797-7fd7abba5eee', '277fccdf-4136-3a0c-8110-e33b886b4a14', NULL, NULL, 14, NULL, NULL),
(495, '8cda4fd0-23da-39d6-9797-7fd7abba5eee', '10084ef9-7242-37d6-a90b-45797da2e3d4', NULL, NULL, 15, NULL, NULL),
(496, '8cda4fd0-23da-39d6-9797-7fd7abba5eee', 'a44a9dab-4a40-3391-93af-a6b72a610f84', NULL, NULL, 16, NULL, NULL),
(497, '8cda4fd0-23da-39d6-9797-7fd7abba5eee', 'b3c7c5b5-e2f3-3005-a45f-08444b9f3730', NULL, NULL, 17, NULL, NULL),
(498, '8cda4fd0-23da-39d6-9797-7fd7abba5eee', '31259478-0629-386c-ade3-34e8e5328827', NULL, NULL, 18, NULL, NULL),
(499, '8cda4fd0-23da-39d6-9797-7fd7abba5eee', '184e08d2-2b80-3d6a-bf33-e414d3cbea38', NULL, NULL, 19, NULL, NULL),
(500, '8cda4fd0-23da-39d6-9797-7fd7abba5eee', '95b8fd1f-4306-3cdd-a12c-49923bf92be3', NULL, NULL, 20, NULL, NULL),
(501, '8cda4fd0-23da-39d6-9797-7fd7abba5eee', '951899a0-ea63-39a0-8193-12a447a7ba82', NULL, NULL, 21, NULL, NULL),
(502, '8cda4fd0-23da-39d6-9797-7fd7abba5eee', '43780e3e-f2d5-3b5a-b199-d1ce0bdc1fd3', NULL, NULL, 22, NULL, NULL),
(503, '8cda4fd0-23da-39d6-9797-7fd7abba5eee', '77c677ed-1a2e-3d7d-99c1-b20589e30965', NULL, NULL, 23, NULL, NULL),
(504, '8cda4fd0-23da-39d6-9797-7fd7abba5eee', '403bd54b-4cf4-309a-9481-d75c02caf197', NULL, NULL, 24, NULL, NULL),
(505, '8cda4fd0-23da-39d6-9797-7fd7abba5eee', 'ae5cec94-7352-345f-b319-a7dc71b25fc8', NULL, NULL, 25, NULL, NULL),
(506, '8cda4fd0-23da-39d6-9797-7fd7abba5eee', 'ae8dc37d-bdbb-31a4-b2b6-9ee523d1ba3d', NULL, NULL, 26, NULL, NULL),
(507, '8cda4fd0-23da-39d6-9797-7fd7abba5eee', '653a326c-016f-3425-8e99-b472bb1ebd68', NULL, NULL, 27, NULL, NULL),
(508, '8cda4fd0-23da-39d6-9797-7fd7abba5eee', '432b612b-7fa4-3be0-9e3e-1a31833d3323', NULL, NULL, 28, NULL, NULL),
(509, '8cda4fd0-23da-39d6-9797-7fd7abba5eee', '4ab8f73a-a4c3-381a-a322-645e18b48568', NULL, NULL, 29, NULL, NULL),
(510, '8cda4fd0-23da-39d6-9797-7fd7abba5eee', '5fb39655-971c-3fb5-bd65-5795ee252066', NULL, NULL, 30, NULL, NULL),
(511, '8cda4fd0-23da-39d6-9797-7fd7abba5eee', 'c07374f0-35cd-384d-9413-1cddaa64294b', NULL, NULL, 31, NULL, NULL),
(512, '8cda4fd0-23da-39d6-9797-7fd7abba5eee', '02691749-f7f9-336c-ac9f-0d95676379f0', NULL, NULL, 32, NULL, NULL),
(513, '8cda4fd0-23da-39d6-9797-7fd7abba5eee', '6b8a273f-59ed-3f63-b5f2-96ae4593c407', NULL, NULL, 33, NULL, NULL),
(514, 'd4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', '7443dd48-c69d-307b-bf4b-73b47002c2e9', NULL, NULL, 6, NULL, NULL),
(515, 'd4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', '8d724505-f3db-3d29-a0a5-66b3ce24983f', NULL, NULL, 7, NULL, NULL),
(516, 'd4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', 'f8e33e39-3343-3328-a765-dc359a9d2232', NULL, NULL, 8, NULL, NULL),
(517, 'd4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', '494346fa-f333-3963-b272-63c59aa1733f', NULL, NULL, 9, NULL, NULL),
(518, 'd4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', 'bbd66af4-4212-36c0-bffe-6c735e2e5136', NULL, NULL, 10, NULL, NULL),
(519, 'd4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', '1b0937a6-2dce-3fb9-9226-17d3491a13e5', NULL, NULL, 11, NULL, NULL),
(520, 'd4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', 'e2189caa-f961-30f2-9fbf-36591dcf28a3', NULL, NULL, 12, NULL, NULL),
(521, 'd4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', '6ccdda23-125e-301f-bca7-c160e29ca09c', NULL, NULL, 13, NULL, NULL),
(522, 'd4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', '2a9de6f8-0b60-3727-81d1-ff99f252457d', NULL, NULL, 14, NULL, NULL),
(523, 'd4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', '277fccdf-4136-3a0c-8110-e33b886b4a14', NULL, NULL, 15, NULL, NULL),
(524, 'd4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', '10084ef9-7242-37d6-a90b-45797da2e3d4', NULL, NULL, 16, NULL, NULL),
(525, 'd4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', 'a44a9dab-4a40-3391-93af-a6b72a610f84', NULL, NULL, 17, NULL, NULL),
(526, 'd4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', 'b3c7c5b5-e2f3-3005-a45f-08444b9f3730', NULL, NULL, 18, NULL, NULL),
(527, 'd4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', '31259478-0629-386c-ade3-34e8e5328827', NULL, NULL, 19, NULL, NULL),
(528, 'd4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', '184e08d2-2b80-3d6a-bf33-e414d3cbea38', NULL, NULL, 20, NULL, NULL),
(529, 'd4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', '95b8fd1f-4306-3cdd-a12c-49923bf92be3', NULL, NULL, 21, NULL, NULL),
(530, 'd4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', '951899a0-ea63-39a0-8193-12a447a7ba82', NULL, NULL, 22, NULL, NULL),
(531, 'd4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', '43780e3e-f2d5-3b5a-b199-d1ce0bdc1fd3', NULL, NULL, 23, NULL, NULL),
(532, 'd4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', '77c677ed-1a2e-3d7d-99c1-b20589e30965', NULL, NULL, 24, NULL, NULL),
(533, 'd4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', '403bd54b-4cf4-309a-9481-d75c02caf197', NULL, NULL, 25, NULL, NULL),
(534, 'd4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', 'ae5cec94-7352-345f-b319-a7dc71b25fc8', NULL, NULL, 26, NULL, NULL),
(535, 'd4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', 'ae8dc37d-bdbb-31a4-b2b6-9ee523d1ba3d', NULL, NULL, 27, NULL, NULL),
(536, 'd4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', '653a326c-016f-3425-8e99-b472bb1ebd68', NULL, NULL, 28, NULL, NULL),
(537, 'd4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', '432b612b-7fa4-3be0-9e3e-1a31833d3323', NULL, NULL, 29, NULL, NULL),
(538, 'd4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', '4ab8f73a-a4c3-381a-a322-645e18b48568', NULL, NULL, 30, NULL, NULL),
(539, 'd4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', '5fb39655-971c-3fb5-bd65-5795ee252066', NULL, NULL, 31, NULL, NULL),
(540, 'd4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', 'c07374f0-35cd-384d-9413-1cddaa64294b', NULL, NULL, 32, NULL, NULL),
(541, 'd4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', '02691749-f7f9-336c-ac9f-0d95676379f0', NULL, NULL, 33, NULL, NULL),
(542, 'd4a6b0a2-3ed8-302b-b6ca-a7ca82b2e291', '6b8a273f-59ed-3f63-b5f2-96ae4593c407', NULL, NULL, 34, NULL, NULL),
(543, '9ca65942-60ab-38c5-8081-b8d6d75a38f6', '7443dd48-c69d-307b-bf4b-73b47002c2e9', NULL, NULL, 6, NULL, NULL),
(544, '9ca65942-60ab-38c5-8081-b8d6d75a38f6', '8d724505-f3db-3d29-a0a5-66b3ce24983f', NULL, NULL, 7, NULL, NULL),
(545, '9ca65942-60ab-38c5-8081-b8d6d75a38f6', 'f8e33e39-3343-3328-a765-dc359a9d2232', NULL, NULL, 8, NULL, NULL),
(546, '9ca65942-60ab-38c5-8081-b8d6d75a38f6', '494346fa-f333-3963-b272-63c59aa1733f', NULL, NULL, 9, NULL, NULL),
(547, '9ca65942-60ab-38c5-8081-b8d6d75a38f6', 'bbd66af4-4212-36c0-bffe-6c735e2e5136', NULL, NULL, 10, NULL, NULL),
(548, '9ca65942-60ab-38c5-8081-b8d6d75a38f6', '1b0937a6-2dce-3fb9-9226-17d3491a13e5', NULL, NULL, 11, NULL, NULL),
(549, '9ca65942-60ab-38c5-8081-b8d6d75a38f6', 'e2189caa-f961-30f2-9fbf-36591dcf28a3', NULL, NULL, 12, NULL, NULL),
(550, '9ca65942-60ab-38c5-8081-b8d6d75a38f6', '6ccdda23-125e-301f-bca7-c160e29ca09c', NULL, NULL, 13, NULL, NULL),
(551, '9ca65942-60ab-38c5-8081-b8d6d75a38f6', '2a9de6f8-0b60-3727-81d1-ff99f252457d', NULL, NULL, 14, NULL, NULL),
(552, '9ca65942-60ab-38c5-8081-b8d6d75a38f6', '277fccdf-4136-3a0c-8110-e33b886b4a14', NULL, NULL, 15, NULL, NULL),
(553, '9ca65942-60ab-38c5-8081-b8d6d75a38f6', '10084ef9-7242-37d6-a90b-45797da2e3d4', NULL, NULL, 16, NULL, NULL),
(554, '9ca65942-60ab-38c5-8081-b8d6d75a38f6', 'a44a9dab-4a40-3391-93af-a6b72a610f84', NULL, NULL, 17, NULL, NULL),
(555, '9ca65942-60ab-38c5-8081-b8d6d75a38f6', 'b3c7c5b5-e2f3-3005-a45f-08444b9f3730', NULL, NULL, 18, NULL, NULL),
(556, '9ca65942-60ab-38c5-8081-b8d6d75a38f6', 'ad7f44ca-04e1-3d03-a58a-999fe681e894', NULL, NULL, 19, NULL, NULL),
(557, '9ca65942-60ab-38c5-8081-b8d6d75a38f6', 'be7066e4-51b0-3ef7-9b07-34e3c280d20f', NULL, NULL, 20, NULL, NULL),
(558, '9ca65942-60ab-38c5-8081-b8d6d75a38f6', '070cc358-77ac-3601-88a8-53b457ae7dba', NULL, NULL, 21, NULL, NULL),
(559, '9ca65942-60ab-38c5-8081-b8d6d75a38f6', '2fc7184b-a608-3af6-a1fe-0b3487f3d06a', NULL, NULL, 22, NULL, NULL),
(560, '9ca65942-60ab-38c5-8081-b8d6d75a38f6', '13424751-f315-39ab-a95a-48cba591ccd2', NULL, NULL, 23, NULL, NULL),
(561, '9ca65942-60ab-38c5-8081-b8d6d75a38f6', 'c3e8479a-44ec-31a3-9694-7e0a15599817', NULL, NULL, 24, NULL, NULL),
(562, '9ca65942-60ab-38c5-8081-b8d6d75a38f6', '02691749-f7f9-336c-ac9f-0d95676379f0', NULL, NULL, 25, NULL, NULL),
(563, '9ca65942-60ab-38c5-8081-b8d6d75a38f6', '6b8a273f-59ed-3f63-b5f2-96ae4593c407', NULL, NULL, 26, NULL, NULL),
(565, 'ece49cf7-4d45-385f-afc0-b899d74fbbfb', '020a0a56-c870-3760-ac0e-1a5a2e55938e', NULL, NULL, 17, NULL, NULL),
(566, 'ece49cf7-4d45-385f-afc0-b899d74fbbfb', 'f688c278-b0b9-3465-a735-6fe16da9bcdb', NULL, NULL, 18, NULL, NULL),
(567, 'ece49cf7-4d45-385f-afc0-b899d74fbbfb', 'f8e33e39-3343-3328-a765-dc359a9d2232', NULL, NULL, 19, NULL, NULL),
(568, 'ece49cf7-4d45-385f-afc0-b899d74fbbfb', '494346fa-f333-3963-b272-63c59aa1733f', NULL, NULL, 20, NULL, NULL),
(569, 'ece49cf7-4d45-385f-afc0-b899d74fbbfb', 'bbd66af4-4212-36c0-bffe-6c735e2e5136', NULL, NULL, 21, NULL, NULL),
(570, 'ece49cf7-4d45-385f-afc0-b899d74fbbfb', '1b0937a6-2dce-3fb9-9226-17d3491a13e5', NULL, NULL, 22, NULL, NULL),
(571, 'ece49cf7-4d45-385f-afc0-b899d74fbbfb', 'e2189caa-f961-30f2-9fbf-36591dcf28a3', NULL, NULL, 23, NULL, NULL),
(572, 'ece49cf7-4d45-385f-afc0-b899d74fbbfb', 'a99b63eb-21e7-3a03-87ac-eed969f5bc76', NULL, NULL, 24, NULL, NULL),
(573, 'ece49cf7-4d45-385f-afc0-b899d74fbbfb', '02691749-f7f9-336c-ac9f-0d95676379f0', NULL, NULL, 25, NULL, NULL),
(574, 'ece49cf7-4d45-385f-afc0-b899d74fbbfb', '6b8a273f-59ed-3f63-b5f2-96ae4593c407', NULL, NULL, 26, NULL, NULL),
(575, '1690ac5e-f0a2-3cff-aa58-4d2c7ecbe38e', '7443dd48-c69d-307b-bf4b-73b47002c2e9', NULL, NULL, 6, NULL, NULL),
(576, '1690ac5e-f0a2-3cff-aa58-4d2c7ecbe38e', '8d724505-f3db-3d29-a0a5-66b3ce24983f', NULL, NULL, 7, NULL, NULL),
(577, '1690ac5e-f0a2-3cff-aa58-4d2c7ecbe38e', 'f8e33e39-3343-3328-a765-dc359a9d2232', NULL, NULL, 8, NULL, NULL),
(578, '1690ac5e-f0a2-3cff-aa58-4d2c7ecbe38e', '494346fa-f333-3963-b272-63c59aa1733f', NULL, NULL, 9, NULL, NULL),
(579, '1690ac5e-f0a2-3cff-aa58-4d2c7ecbe38e', 'bbd66af4-4212-36c0-bffe-6c735e2e5136', NULL, NULL, 10, NULL, NULL),
(580, '1690ac5e-f0a2-3cff-aa58-4d2c7ecbe38e', '1b0937a6-2dce-3fb9-9226-17d3491a13e5', NULL, NULL, 11, NULL, NULL),
(581, '1690ac5e-f0a2-3cff-aa58-4d2c7ecbe38e', 'e2189caa-f961-30f2-9fbf-36591dcf28a3', NULL, NULL, 12, NULL, NULL),
(582, '1690ac5e-f0a2-3cff-aa58-4d2c7ecbe38e', '6ccdda23-125e-301f-bca7-c160e29ca09c', NULL, NULL, 13, NULL, NULL),
(583, '1690ac5e-f0a2-3cff-aa58-4d2c7ecbe38e', '2a9de6f8-0b60-3727-81d1-ff99f252457d', NULL, NULL, 14, NULL, NULL),
(584, '1690ac5e-f0a2-3cff-aa58-4d2c7ecbe38e', '277fccdf-4136-3a0c-8110-e33b886b4a14', NULL, NULL, 15, NULL, NULL),
(585, '1690ac5e-f0a2-3cff-aa58-4d2c7ecbe38e', '10084ef9-7242-37d6-a90b-45797da2e3d4', NULL, NULL, 16, NULL, NULL),
(586, '1690ac5e-f0a2-3cff-aa58-4d2c7ecbe38e', 'a44a9dab-4a40-3391-93af-a6b72a610f84', NULL, NULL, 17, NULL, NULL),
(587, '1690ac5e-f0a2-3cff-aa58-4d2c7ecbe38e', 'b3c7c5b5-e2f3-3005-a45f-08444b9f3730', NULL, NULL, 18, NULL, NULL),
(588, '1690ac5e-f0a2-3cff-aa58-4d2c7ecbe38e', '176d295f-c95d-3198-bf65-adebd41778cf', NULL, NULL, 19, NULL, NULL),
(589, '1690ac5e-f0a2-3cff-aa58-4d2c7ecbe38e', '9e227ea0-4eb8-329d-b774-f97a392c929c', NULL, NULL, 20, NULL, NULL),
(590, '1690ac5e-f0a2-3cff-aa58-4d2c7ecbe38e', '63f10304-2b94-3251-afdb-6d098b0322b8', NULL, NULL, 21, NULL, NULL),
(591, '1690ac5e-f0a2-3cff-aa58-4d2c7ecbe38e', '12013cac-8cd2-3aa2-b189-60c1701f30c2', NULL, NULL, 22, NULL, NULL),
(592, '1690ac5e-f0a2-3cff-aa58-4d2c7ecbe38e', '02691749-f7f9-336c-ac9f-0d95676379f0', NULL, NULL, 23, NULL, NULL),
(593, '1690ac5e-f0a2-3cff-aa58-4d2c7ecbe38e', '6b8a273f-59ed-3f63-b5f2-96ae4593c407', NULL, NULL, 24, NULL, NULL),
(594, 'db8db0a8-2617-3788-9897-5ef3b666f2a8', '0207cc87-c743-3070-9366-faf1c24f3ddf', NULL, NULL, 7, NULL, NULL),
(595, 'db8db0a8-2617-3788-9897-5ef3b666f2a8', 'b073b4cb-4f26-3e1f-8fe8-6f2bff17ec94', NULL, NULL, 8, NULL, NULL),
(596, 'db8db0a8-2617-3788-9897-5ef3b666f2a8', '35bb370f-b4fc-3848-9563-2a568480549a', NULL, NULL, 9, NULL, NULL),
(597, 'db8db0a8-2617-3788-9897-5ef3b666f2a8', 'c88742a0-9041-3aa9-8894-f75698ade167', NULL, NULL, 10, NULL, NULL),
(598, 'db8db0a8-2617-3788-9897-5ef3b666f2a8', '0352819e-783e-343c-bc5e-3afdaf5a626b', NULL, NULL, 12, NULL, NULL),
(599, 'db8db0a8-2617-3788-9897-5ef3b666f2a8', '5ce3da61-f6c3-3540-9e9a-8a80d1039caa', NULL, NULL, 13, NULL, NULL),
(600, 'db8db0a8-2617-3788-9897-5ef3b666f2a8', 'f8e33e39-3343-3328-a765-dc359a9d2232', NULL, NULL, 11, NULL, NULL),
(601, '54ebc251-94e6-3fd7-937b-444ee125d027', '35bb370f-b4fc-3848-9563-2a568480549a', NULL, NULL, 8, NULL, NULL),
(602, '54ebc251-94e6-3fd7-937b-444ee125d027', 'c88742a0-9041-3aa9-8894-f75698ade167', NULL, NULL, 9, NULL, NULL),
(603, '54ebc251-94e6-3fd7-937b-444ee125d027', 'f8e33e39-3343-3328-a765-dc359a9d2232', NULL, NULL, 10, NULL, NULL),
(604, '54ebc251-94e6-3fd7-937b-444ee125d027', '0352819e-783e-343c-bc5e-3afdaf5a626b', NULL, NULL, 11, NULL, NULL),
(605, '54ebc251-94e6-3fd7-937b-444ee125d027', '5ce3da61-f6c3-3540-9e9a-8a80d1039caa', NULL, NULL, 12, NULL, NULL),
(606, '54ebc251-94e6-3fd7-937b-444ee125d027', '02691749-f7f9-336c-ac9f-0d95676379f0', NULL, NULL, 13, NULL, NULL),
(607, '54ebc251-94e6-3fd7-937b-444ee125d027', '6b8a273f-59ed-3f63-b5f2-96ae4593c407', NULL, NULL, 14, NULL, NULL),
(608, 'ecc5f0f0-c7e8-306d-9bb6-440a97bb771b', '070cc358-77ac-3601-88a8-53b457ae7dba', NULL, NULL, 6, NULL, NULL),
(609, 'ecc5f0f0-c7e8-306d-9bb6-440a97bb771b', '0bbfc503-e321-34a7-9841-ae4b6b97f23d', NULL, NULL, 7, NULL, NULL),
(610, 'ecc5f0f0-c7e8-306d-9bb6-440a97bb771b', '2fc7184b-a608-3af6-a1fe-0b3487f3d06a', NULL, NULL, 8, NULL, NULL),
(611, 'ecc5f0f0-c7e8-306d-9bb6-440a97bb771b', '8ab38b3c-1e66-31c2-b8bc-61b7d4374de5', NULL, NULL, 9, NULL, NULL),
(612, 'ecc5f0f0-c7e8-306d-9bb6-440a97bb771b', 'fbaa5890-63d9-3895-9193-48540c4b8b74', NULL, NULL, 10, NULL, NULL),
(613, 'ecc5f0f0-c7e8-306d-9bb6-440a97bb771b', 'b921ff7f-2428-322b-ba43-469814d8b3fc', NULL, NULL, 11, NULL, NULL),
(614, 'ecc5f0f0-c7e8-306d-9bb6-440a97bb771b', '02691749-f7f9-336c-ac9f-0d95676379f0', NULL, NULL, 13, NULL, NULL),
(615, '03cd67b7-b836-3703-82d0-63e7c7f24ac7', '5bd533ad-3ed3-3f3d-bf83-ec6674c3b7a6', NULL, NULL, 7, NULL, NULL),
(616, '03cd67b7-b836-3703-82d0-63e7c7f24ac7', 'c5db7848-792d-3f5d-b527-360d696f296e', NULL, NULL, 8, NULL, NULL),
(617, '03cd67b7-b836-3703-82d0-63e7c7f24ac7', '3cb10c58-88a1-37af-91fa-8191e61b6fca', NULL, NULL, 9, NULL, NULL),
(618, '03cd67b7-b836-3703-82d0-63e7c7f24ac7', 'f8e33e39-3343-3328-a765-dc359a9d2232', NULL, NULL, 10, NULL, NULL),
(619, '03cd67b7-b836-3703-82d0-63e7c7f24ac7', '02691749-f7f9-336c-ac9f-0d95676379f0', NULL, NULL, 11, NULL, NULL),
(620, '980a10df-873d-3d11-a31c-8c46bb20195f', '673f037d-1ad1-3a80-9258-2d71bad304a3', NULL, NULL, 11, NULL, NULL),
(621, 'ecc5f0f0-c7e8-306d-9bb6-440a97bb771b', 'f8e33e39-3343-3328-a765-dc359a9d2232', NULL, NULL, 12, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `agency_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` enum('admin1','admin2','agency') COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `agency_id`, `username`, `password`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
('1', NULL, 'admin1', '$2y$10$Gkdro9rKy1jr7VfYzH1BLOsugNWzPMhT1ex8vV0jMDF78Qm6S.RK6', 'admin1', NULL, '2020-02-16 07:37:12', '2020-02-18 07:27:06'),
('2', NULL, 'admin2', '$2y$10$6M.teH9ICJexmx8z0CCL3uNICo3dvQ8U6cu5aa20c6k3f5mBAIaHO', 'admin2', NULL, '2020-02-16 07:37:12', '2020-02-16 07:37:12'),
('3', '0f4b52d8-7a8d-3a72-96ce-4230afdff09a', 'firstagency', '$2y$10$z2n/KL6wT4WuDkm7zYkk9OGXX9rH2DGgLpT/ukzZwnzCsuReDukbK', 'agency', NULL, '2020-02-16 07:37:12', '2020-02-16 07:37:12');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `agencies`
--
ALTER TABLE `agencies`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `agencies_email_unique` (`email`);

--
-- Indexes for table `biodatas`
--
ALTER TABLE `biodatas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `biodata_posts`
--
ALTER TABLE `biodata_posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `biodata_posts_requirement_id_foreign` (`requirement_id`),
  ADD KEY `biodata_posts_biodata_id_foreign` (`biodata_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `requirements`
--
ALTER TABLE `requirements`
  ADD PRIMARY KEY (`id`),
  ADD KEY `requirements_agency_id_foreign` (`agency_id`),
  ADD KEY `requirements_requirement_type_id_foreign` (`requirement_type_id`);

--
-- Indexes for table `requirement_forms`
--
ALTER TABLE `requirement_forms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `requirement_forms_requirement_type_id_foreign` (`requirement_type_id`);

--
-- Indexes for table `requirement_posts`
--
ALTER TABLE `requirement_posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `requirement_posts_requirement_id_foreign` (`requirement_id`),
  ADD KEY `requirement_posts_requirement_form_id_foreign` (`requirement_form_id`);

--
-- Indexes for table `requirement_types`
--
ALTER TABLE `requirement_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `requirement_type_biodata`
--
ALTER TABLE `requirement_type_biodata`
  ADD PRIMARY KEY (`id`),
  ADD KEY `requirement_type_biodata_requirement_type_id_foreign` (`requirement_type_id`),
  ADD KEY `requirement_type_biodata_biodata_id_foreign` (`biodata_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`),
  ADD KEY `users_agency_id_foreign` (`agency_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `requirement_type_biodata`
--
ALTER TABLE `requirement_type_biodata`
  MODIFY `id` bigint(22) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=622;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `biodata_posts`
--
ALTER TABLE `biodata_posts`
  ADD CONSTRAINT `biodata_posts_biodata_id_foreign` FOREIGN KEY (`biodata_id`) REFERENCES `biodatas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `biodata_posts_requirement_id_foreign` FOREIGN KEY (`requirement_id`) REFERENCES `requirements` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `requirements`
--
ALTER TABLE `requirements`
  ADD CONSTRAINT `requirements_agency_id_foreign` FOREIGN KEY (`agency_id`) REFERENCES `agencies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `requirements_requirement_type_id_foreign` FOREIGN KEY (`requirement_type_id`) REFERENCES `requirement_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `requirement_forms`
--
ALTER TABLE `requirement_forms`
  ADD CONSTRAINT `requirement_forms_requirement_type_id_foreign` FOREIGN KEY (`requirement_type_id`) REFERENCES `requirement_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `requirement_posts`
--
ALTER TABLE `requirement_posts`
  ADD CONSTRAINT `requirement_posts_requirement_form_id_foreign` FOREIGN KEY (`requirement_form_id`) REFERENCES `requirement_forms` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `requirement_posts_requirement_id_foreign` FOREIGN KEY (`requirement_id`) REFERENCES `requirements` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `requirement_type_biodata`
--
ALTER TABLE `requirement_type_biodata`
  ADD CONSTRAINT `requirement_type_biodata_biodata_id_foreign` FOREIGN KEY (`biodata_id`) REFERENCES `biodatas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `requirement_type_biodata_requirement_type_id_foreign` FOREIGN KEY (`requirement_type_id`) REFERENCES `requirement_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_agency_id_foreign` FOREIGN KEY (`agency_id`) REFERENCES `agencies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
